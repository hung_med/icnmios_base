//
//  MedicalLocationViewController.swift
//  iCNM
//
//  Created by Hoang Van Trung on 11/22/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import SDWebImage
import RealmSwift

class MedicalLocationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var lblEmpty: UILabel!
    @IBOutlet weak var btnAddNew: UIButton!
    @IBOutlet weak var tableView: UITableView!
    let locationOwn = LocationOwn()
    var arrLocationOwn = [LocationOwn]()
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Quản lý địa điểm y tế"
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(MedicalLocationViewController.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        btnAddNew.layer.cornerRadius = 4.0
        btnAddNew.layer.masksToBounds = true
        btnAddNew.isHidden = true
        let addNew = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(MedicalLocationViewController.addNewLocation))
        self.navigationItem.rightBarButtonItem  = addNew
        self.tableView.delegate = self
        self.tableView.dataSource = self
        let nib = UINib(nibName: "MedicalLocationCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "MedicalLocationCell")
        self.getListLocationOwn()
        self.getLocationInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionAddNew(_ sender: Any) {
        self.addNewLocation()
    }
    func getListLocationOwn() {
        if let user = currentUser{
            LocationOwn().getLocationOwn(param:user.doctorID, success: { (result) in
                if let new = result {
                    if new.isEmpty {
                        self.lblEmpty.text = "Chưa có địa điểm y tế nào"
                        self.tableView.isHidden = true
                        self.btnAddNew.isHidden = false
                    } else {
                        self.tableView.isHidden = false
                        self.arrLocationOwn = new
                        self.tableView.reloadData()
                        self.btnAddNew.isHidden = true
                    }
                } else {
                    self.lblEmpty.text = "Chưa có địa điểm y tế nào"
                    self.tableView.isHidden = true
                    self.btnAddNew.isHidden = false
                }
            }){ (error, response) in
                
            }
        }
    }
   
    func getLocationInfo() {
        let param = ["locationID": 56548]
        LocationOwn().getLocationInfo(param: param, success: { (result) in
            if let new = result {
                print(new)
            } else {
                
            }
        }) { (error, response) in
            print(response)
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrLocationOwn.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MedicalLocationCell", for: indexPath) as! MedicalLocationCell
        let itemLocationOwn = arrLocationOwn[indexPath.row]
        cell.name.text = itemLocationOwn.Name
        cell.address.text = itemLocationOwn.Address
        
        Common.sharedInstance.setRatingStar(imgViewStar1: cell.imgStar1, imgViewStar2: cell.imgStar2, imgViewStar3: cell.imgStar3, imgViewStar4: cell.imgStar4, imgViewStar5: cell.imgStar5, ratingScore: itemLocationOwn.AverageRating)
        
        cell.averageRating.text = "(\(itemLocationOwn.AverageRating) đánh giá)"
        let url = URL(string: API.baseURLImage + "iCNMImage/" + "LocationImage/"
            + "\(itemLocationOwn.LocationID)/" + "\(itemLocationOwn.ImagesStr)")
        cell.imgStr.sd_setImage(with: url)
        cell.verifyStatus.backgroundColor = UIColor.clear
        if itemLocationOwn.VerifyPhone {
            cell.verifyStatus.text = "Đã xác thực"
            cell.verifyStatus.textColor = UIColor(hex:"1976D2")
        }else {
            cell.verifyStatus.text = "Chưa xác thực"
            cell.verifyStatus.textColor = UIColor.red
        }
        switch itemLocationOwn.LocationTypeID {
        case 1:
            cell.locationType.text = "Bác sĩ"
            break
        case 2:
            cell.locationType.text = "Bệnh viện"
            break
        case 4:
            cell.locationType.text = "Phòng khám"
            break
        case 5:
            cell.locationType.text = "Nhà thuốc"
            break
        default:
            cell.locationType.text = "Không xác định"
            break
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.openLocationDetail(arrLocationOwn[indexPath.row])
    }
    
    func openLocationDetail(_ locationOwn: LocationOwn) {
        let storyboard = UIStoryboard(name: "MedicalLocation", bundle: nil)
        let medicalDetail = storyboard.instantiateViewController(withIdentifier: "MedicalLocationDetail") as! MedicalLocationDetailVC
        medicalDetail.locationOwn = locationOwn
        medicalDetail.updateAvatarBlock = { () -> Void in
            self.getListLocationOwn()
        }
        self.navigationController?.pushViewController(medicalDetail, animated: true)
    }
    
    func addNewLocation() {
        let storyboard = UIStoryboard(name: "MedicalLocation", bundle: nil)
        let medicalDetail = storyboard.instantiateViewController(withIdentifier: "MedicalLocationDetail") as! MedicalLocationDetailVC
        medicalDetail.isAddNewLocation = true
        medicalDetail.updateAvatarBlock = { () -> Void in
            self.getListLocationOwn()
        }
        self.navigationController?.pushViewController(medicalDetail, animated: true)
    }
}
