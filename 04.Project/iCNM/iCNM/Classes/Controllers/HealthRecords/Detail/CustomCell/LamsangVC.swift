//
//  LamsangVC.swift
//  iCNM
//
//  Created by Mac osx on 11/21/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
protocol LamsangVCDelegate {
    func resultDataMedicalCilinnical(isUpdateItem:Bool, currentIndex:Int, medicalCilinnical:MedicalCilinnical, tag:Int)
}

class LamsangVC: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate, LamsangCustomCellDelegate {
    
    @IBOutlet weak var myTableView: UITableView!
    var medicalCilinnical:MedicalCilinnical?=nil
    var currentIndex:Int? = 0
    var currentTag:Int? = 0
    var isUpdateItem:Bool? = false
    var delegate:LamsangVCDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        myTableView.delegate = self
        myTableView.dataSource = self
        
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        title = "Lâm sàng và cận Lâm sàng"
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        
        let nib = UINib(nibName: "LamsangCustomCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "LamsangCell")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.medicalCilinnical != nil{
            self.delegate?.resultDataMedicalCilinnical(isUpdateItem:isUpdateItem!, currentIndex:currentIndex!, medicalCilinnical: self.medicalCilinnical!, tag:self.currentTag!)
        }
    }

    func resultMedicalCilinnical(medicalCilinnical:MedicalCilinnical){
        self.medicalCilinnical = medicalCilinnical
    }

    // MARK: - Table view data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LamsangCell", for: indexPath) as! LamsangCustomCell
        cell.delegate = self
        if self.medicalCilinnical != nil{
            if let dateCilin = self.medicalCilinnical?.dateCilin{
                let result = resultDateFormatStandard(str: dateCilin)
                cell.txtNgaykham.text = result
                cell.txtNgaykham.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                //medicalCilinnical.dateCilin = ngaykham
            }
            if let dateSchedule = self.medicalCilinnical?.dateSchedule{
                let result = resultDateFormatStandard(str: dateSchedule)
                cell.txtHenKham.text = result
                cell.txtHenKham.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                //medicalCilinnical.dateSchedule = dateSchedule
            }
            if let tiensu = self.medicalCilinnical?.historyCilin{
                cell.txtTiensu.text = tiensu
                cell.txtTiensu.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                //medicalCilinnical.historyCilin = tiensu
            }
            if let mach = self.medicalCilinnical?.mach{
                
                cell.txtMach.text = "\(mach)"
                cell.txtMach.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
               // medicalCilinnical.mach = mach
            }
            if let nhietDo = self.medicalCilinnical?.nhietDo{
                
                cell.txtNhietdo.text = "\(nhietDo)"
                cell.txtNhietdo.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.nhietDo = nhietDo
            }
            if let ha = self.medicalCilinnical?.ha{
                
                cell.txtHA.text = "\(ha)"
                cell.txtHA.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.ha = ha
            }
            if let nhiptho = self.medicalCilinnical?.nhipTho{
               
                cell.txtNhiptho.text = "\(nhiptho)"
                cell.txtNhiptho.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
              //  medicalCilinnical.nhipTho = nhiptho
            }
            if let height = self.medicalCilinnical?.height{
                
                cell.txtCao.text = "\(height)"
                cell.txtCao.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.height = height
            }
            if let weight = self.medicalCilinnical?.weight{
                
                cell.txtCannang.text = "\(weight)"
                cell.txtCannang.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.weight = weight
            }
            if let bmi = self.medicalCilinnical?.bMI{
                
                cell.txtBMI.text = "\(bmi)"
                cell.txtBMI.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.bMI = bmi
            }
            if let vongBung = self.medicalCilinnical?.vongBung{
                
                cell.txtVongbung.text = "\(vongBung)"
                cell.txtVongbung.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.vongBung = vongBung
            }
            if let rightEye = self.medicalCilinnical?.rightEye{
                
                cell.txtMatphai_NoK.text = "\(rightEye)"
                cell.txtMatphai_NoK.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
            //    medicalCilinnical.rightEye = rightEye
            }
            if let leftEye = self.medicalCilinnical?.leftEye{
                
                cell.txtMattrai_NoK.text = "\(leftEye)"
                cell.txtMattrai_NoK.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.leftEye = leftEye
            }
            if let rightEyeGlass = self.medicalCilinnical?.rightEyeGlass{
                
                cell.txtMatphai_CoK.text = "\(rightEyeGlass)"
                cell.txtMatphai_CoK.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.rightEyeGlass = rightEyeGlass
            }
            if let leftEyeGlass = self.medicalCilinnical?.leftEyeGlass{
                
                cell.txtMattrai_CoK.text = "\(leftEyeGlass)"
                cell.txtMattrai_CoK.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.leftEyeGlass = leftEyeGlass
            }
            if let daNiemMac = self.medicalCilinnical?.daNiemMac{
                
                cell.txtNiemmac.text = daNiemMac
                cell.txtNiemmac.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.daNiemMac = daNiemMac
            }
            if let toanThanNote = self.medicalCilinnical?.toanThanNote{
                
                cell.txtKhac.text = toanThanNote
                cell.txtKhac.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.toanThanNote = toanThanNote
            }
            if let timMach = self.medicalCilinnical?.timMach{
                
                cell.txtTimmach.text = timMach
                cell.txtTimmach.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.timMach = timMach
            }
            if let hoHap = self.medicalCilinnical?.hoHap{
                
                cell.txtHohap.text = hoHap
                cell.txtHohap.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.hoHap = hoHap
            }
            if let tieuHoa = self.medicalCilinnical?.tieuHoa{
                
                cell.txtTieuhoa.text = tieuHoa
                cell.txtTieuhoa.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
            //    medicalCilinnical.tieuHoa = tieuHoa
            }
            if let tietNieu = self.medicalCilinnical?.tietNieu{
                
                cell.txtTietlieu.text = tietNieu
                cell.txtTietlieu.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.tietNieu = tietNieu
            }
            if let coXuongKhop = self.medicalCilinnical?.coXuongKhop{
                
                cell.txtCoXuongKhop.text = coXuongKhop
                cell.txtCoXuongKhop.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.coXuongKhop = coXuongKhop
            }
            if let noiTiet = self.medicalCilinnical?.noiTiet{
                
                cell.txtNoitiet.text = noiTiet
                cell.txtNoitiet.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
            //    medicalCilinnical.noiTiet = noiTiet
            }
            if let thanKinh = self.medicalCilinnical?.thanKinh{
                
                cell.txtThankinh.text = thanKinh
                cell.txtThankinh.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.thanKinh = thanKinh
            }
            if let tamThan = self.medicalCilinnical?.tamThan{
                
                cell.txtTamthan.text = tamThan
                cell.txtTamthan.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                //medicalCilinnical.tamThan = tamThan
            }
            if let ngoaiKhoa = self.medicalCilinnical?.ngoaiKhoa{
                
                cell.txtNgoaikhoa.text = ngoaiKhoa
                cell.txtNgoaikhoa.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.ngoaiKhoa = ngoaiKhoa
            }
            if let sanPhuKhoa = self.medicalCilinnical?.sanPhuKhoa{
                
                cell.txtSanphukhoa.text = sanPhuKhoa
                cell.txtSanphukhoa.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.sanPhuKhoa = sanPhuKhoa
            }
            if let taiMuiHong = self.medicalCilinnical?.taiMuiHong{
                
                cell.txtTaimuihong.text = taiMuiHong
                cell.txtTaimuihong.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.taiMuiHong = taiMuiHong
            }
            if let rangHamMat = self.medicalCilinnical?.rangHamMat{
                
                cell.txtRHM.text = rangHamMat
                cell.txtRHM.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
            //    medicalCilinnical.rangHamMat = rangHamMat
            }
            if let mat = self.medicalCilinnical?.mat{
               
                cell.txtMat.text = mat
                cell.txtMat.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
            //    medicalCilinnical.mat = mat
            }
            if let daLieu = self.medicalCilinnical?.daLieu{
                
                cell.txtDalieu.text = daLieu
                cell.txtDalieu.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
            //    medicalCilinnical.daLieu = daLieu
            }
            if let dinhDuong = self.medicalCilinnical?.dinhDuong{
                
                cell.txtDinhduong.text = dinhDuong
                cell.txtDinhduong.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
            //    medicalCilinnical.dinhDuong = dinhDuong
            }
            if let vanDong = self.medicalCilinnical?.vanDong{
                
                cell.txtVandong.text = vanDong
                cell.txtVandong.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.vanDong = vanDong
            }
            if let danhGia = self.medicalCilinnical?.danhGia{
                
                cell.txtDanhgia.text = danhGia
                cell.txtDanhgia.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
            //    medicalCilinnical.danhGia = danhGia
            }
            if let coQuanKhac = self.medicalCilinnical?.coQuanKhac{
                
                cell.txtOther.text = coQuanKhac
                cell.txtOther.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
            //    medicalCilinnical.coQuanKhac = coQuanKhac
            }
            if let huyetHoc = self.medicalCilinnical?.huyetHoc{
                
                cell.txtHuyethoc.text = huyetHoc
                cell.txtHuyethoc.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
            //    medicalCilinnical.huyetHoc = huyetHoc
            }
            if let sinhHoaMau = self.medicalCilinnical?.sinhHoaMau{
                cell.txtSinhhoamau.text = sinhHoaMau
                cell.txtSinhhoamau.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                //medicalCilinnical.sinhHoaMau = sinhHoaMau
            }
            if let sinhHoaNuocTieu = self.medicalCilinnical?.sinhHoaNuocTieu{
                cell.txtSinhhoaNT.text = sinhHoaNuocTieu
                cell.txtSinhhoaNT.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.sinhHoaNuocTieu = sinhHoaNuocTieu
            }
            if let sieuAmOBung = self.medicalCilinnical?.sieuAmOBung{
                cell.txtSieuam.text = sieuAmOBung
                cell.txtSieuam.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.sieuAmOBung = sieuAmOBung
            }
            if let chanDoan = self.medicalCilinnical?.chanDoan{
                
                cell.txtKetluan.text = chanDoan
                cell.txtKetluan.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
              //  medicalCilinnical.chanDoan = chanDoan
            }
            if let tuvan = self.medicalCilinnical?.tuVan{
                
                cell.txtTuvan.text = tuvan
                cell.txtTuvan.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
             //   medicalCilinnical.tuVan = tuvan
            }
            if let bacsiName = self.medicalCilinnical?.bacSiName{
                
                cell.txtBacsikham.text = bacsiName
                cell.txtBacsikham.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
            //    medicalCilinnical.bacSiName = bacsiName
            }
            
        }else{
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 2350.0
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }

}
