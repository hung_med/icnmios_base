//
//  HealthRecordsCell.swift
//  iCNM
//
//  Created by Mac osx on 8/22/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
protocol KhuyettatCustomCellDelegate {
    func resultMedicalHealthFactory(medicalHealthFactory:MedicalHealthFactory)
}
class KhuyettatCustomCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet var lblThinhluc: FWFloatingLabelTextField!
    @IBOutlet var lblKhuyettat: FWFloatingLabelTextField!
    @IBOutlet var lblKhehomoi: FWFloatingLabelTextField!
    @IBOutlet var lblChan: FWFloatingLabelTextField!
    @IBOutlet var lblTay: FWFloatingLabelTextField!
    @IBOutlet var lblThiluc: FWFloatingLabelTextField!
    @IBOutlet var lblCongveo: FWFloatingLabelTextField!
    
    var delegate: KhuyettatCustomCellDelegate?
    var medicalHealFactory = MedicalHealthFactoryUser.sharedInstance.medicalHealFactoriesObj
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblThinhluc.delegate = self
        lblKhuyettat.delegate = self
        lblKhehomoi.delegate = self
        lblChan.delegate = self
        lblTay.delegate = self
        lblThiluc.delegate = self
        lblCongveo.delegate = self
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(TiensubenhtatCustomCell.didSelectCell))
        addGestureRecognizer(gesture)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

// MARK: - Actions
extension KhuyettatCustomCell {
    func didSelectCell() {
//        lblThinhluc.becomeFirstResponder()
//        lblKhuyettat.becomeFirstResponder()
//        lblKhehomoi.becomeFirstResponder()
//        lblChan.becomeFirstResponder()
//        lblTay.becomeFirstResponder()
//        lblThiluc.becomeFirstResponder()
//        lblCongveo.becomeFirstResponder()
    }
    
    @IBAction func textFieldInTableViewCell_SKCN(_ sender: UITextField) {
        if let result = sender.text {
            
            switch sender.tag {
            case 0:
                if !result.isEmpty{
                    medicalHealFactory.khuyetTatThinhLuc = result.trim()
                }else{
                    medicalHealFactory.khuyetTatThinhLuc = ""
                }
            case 1:
                if !result.isEmpty{
                    medicalHealFactory.ktThiLuc = result.trim()
                }else{
                    medicalHealFactory.ktThiLuc = ""
                }
            case 2:
                if !result.isEmpty{
                    medicalHealFactory.ktTay = result.trim()
                }else{
                    medicalHealFactory.ktTay = ""
                }
            case 3:
                if !result.isEmpty{
                    medicalHealFactory.ktChan = result.trim()
                }else{
                    medicalHealFactory.ktChan = ""
                }
            case 4:
                if !result.isEmpty{
                    medicalHealFactory.congVeoCS = result.trim()
                }else{
                    medicalHealFactory.congVeoCS = ""
                }
            case 5:
                if !result.isEmpty{
                    medicalHealFactory.kheHoMoiVM = result.trim()
                }else{
                    medicalHealFactory.kheHoMoiVM = ""
                }
            case 6:
                if !result.isEmpty{
                    medicalHealFactory.khuyetTatNote = result.trim()
                }else{
                    medicalHealFactory.khuyetTatNote = ""
                }
            default:
                break
            }
            
            delegate?.resultMedicalHealthFactory(medicalHealthFactory:medicalHealFactory)
        }
    }
 
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == lblThinhluc{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == lblKhuyettat{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == lblKhehomoi{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == lblChan{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == lblTay{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == lblThiluc{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == lblCongveo{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else{
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
}

