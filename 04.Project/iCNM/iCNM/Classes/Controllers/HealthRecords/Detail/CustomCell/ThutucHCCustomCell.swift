//
//  HealthRecordsCell.swift
//  iCNM
//
//  Created by Mac osx on 8/22/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
protocol ThutucHCCustomCellDelegate {
    func resultGender(medicalProfile:MedicalProfile)
    func resultMedicalProfile(medicalProfile:MedicalProfile)
}
class ThutucHCCustomCell: UITableViewCell, FWComboBoxDelegate, UITextFieldDelegate {
    @IBOutlet weak var lblAddress: FWFloatingLabelTextField!
    @IBOutlet weak var radio_Nu: ISRadioButton!
    @IBOutlet weak var radio_Nam: ISRadioButton!
    
    @IBOutlet weak var lblSoBHYT: FWFloatingLabelTextField!
    @IBOutlet weak var lblSoCMT: FWFloatingLabelTextField!
    @IBOutlet weak var lblBirthDay: FWFloatingLabelTextField!
    @IBOutlet weak var lblEmailAddress: FWFloatingLabelTextField!
    @IBOutlet weak var lblUserName: FWFloatingLabelTextField!
    @IBOutlet weak var lblPhoneNumber: FWFloatingLabelTextField!
    @IBOutlet weak var lblWeight: FWFloatingLabelTextField!
    @IBOutlet weak var lblHeight: FWFloatingLabelTextField!
   
    @IBOutlet weak var btnBirthday: UIButton!
    var listGroupBloods:[String]?
    
    var delegate: ThutucHCCustomCellDelegate?
    var gender:String? = nil
    @IBOutlet weak var provinceComboBox: FWComboBox!
    @IBOutlet weak var districtComboBox: FWComboBox!
    @IBOutlet weak var gBloodComboBox: FWComboBox!
   
    public var currentProvinceIndex:Int? = 0
    public var currentDistrictIndex:Int? = 0
    public var currentGBloodIndex:Int? = 0
    var isCheck:Bool? = false
    var medicalProfile = MedicalProfile()
    var bloodGroupStr:String?
    public var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    private var provinces:[Province]? {
        didSet {
            populateProvinceComboBox2()
        }
    }
    
    private var districts:[District]? {
        didSet {
            populateDistrictComboBox2()
        }
    }
    
    private var groupBlood:[String]? {
        didSet {
            populateGroupBloodComboBox()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        provinceComboBox.delegate = self
        districtComboBox.delegate = self
        gBloodComboBox.delegate = self
        lblHeight.delegate = self
        lblWeight.delegate = self
        lblAddress.delegate = self
        lblSoBHYT.delegate = self
        lblSoCMT.delegate = self
        lblBirthDay.delegate = self
        lblEmailAddress.delegate = self
        lblUserName.delegate = self
        lblPhoneNumber.delegate = self
        //lblEmailAddress.addRegx(strRegx: "[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}", errorMsg: "Địa chỉ email không hợp lệ")
        self.setIconInTextField(currentTextField: self.lblUserName, icon: "user-circle", name: "Họ tên")
        self.setIconInTextField(currentTextField: self.lblBirthDay, icon: "birthday", name: "Ngày sinh")
        self.setIconInTextField(currentTextField: self.lblEmailAddress, icon: "email", name: "Email")
        self.setIconInTextField(currentTextField: self.lblAddress, icon: "home", name: " Địa chỉ")
        self.setIconInTextField(currentTextField: self.lblPhoneNumber, icon: "phone-square", name: " Số điện thoại")
        
        lblBirthDay.isUserInteractionEnabled = false
        provinceComboBox.layer.borderWidth = 1
        provinceComboBox.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        provinceComboBox.layer.cornerRadius = 4
        provinceComboBox.layer.masksToBounds = true
        
        districtComboBox.layer.borderWidth = 1
        districtComboBox.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        districtComboBox.layer.cornerRadius = 4
        districtComboBox.layer.masksToBounds = true
        
        gBloodComboBox.layer.borderWidth = 1
        gBloodComboBox.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        gBloodComboBox.layer.cornerRadius = 4
        gBloodComboBox.layer.masksToBounds = true
        
        districtComboBox.defaultTitle = "Chọn Quận/Huyện"
        provinceComboBox.defaultTitle = "Chọn Tỉnh/TP"
        gBloodComboBox.defaultTitle = "N/máu"
        
        requestData()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(ThutucHCCustomCell.didSelectCell))
        addGestureRecognizer(gesture)
        
      //  self.lblBirthDay.addTarget(self, action: #selector(showChoiceBirthDay), for: .touchDown)
    }
    
    @IBAction func touchGenderRadioButton(_ sender: ISRadioButton) {
        if sender == radio_Nam {
            gender = "M"
            radio_Nam.isSelected = true
            radio_Nu.isSelected = false
        } else {
            gender = "F"
            radio_Nu.isSelected = true
            radio_Nam.isSelected = false
        }
        medicalProfile.gender = gender!
        delegate?.resultGender(medicalProfile:medicalProfile)
    }
    
    @IBAction func showChoiceBirthDay(_ sender: Any) {
        //if !isCheck!{
            let currentDate = Date()
            var dateComponents = DateComponents()
            dateComponents.year = -100
            let threeMonthAgo = Calendar.current.date(byAdding: dateComponents, to: currentDate)
            
            let formatter = DateFormatter()
            let formatter1 = DateFormatter()
            formatter1.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            formatter.dateFormat = "dd/MM/yyyy"
            DatePickerDialog().show("Chọn ngày", doneButtonTitle: "Chọn", cancelButtonTitle: "Huỷ", minimumDate: threeMonthAgo, maximumDate: currentDate, datePickerMode: .date) { (date) in
                if let dateString = date {
                    let formatDateStr = formatter.string(from: dateString)
                    let formatDateStr1 = formatter1.string(from: dateString)
                    if !formatDateStr1.isEmpty{
                        let result = resultDateFormatStandard(str: formatDateStr)
                        self.lblBirthDay.text = result
                        self.lblBirthDay.textColor = UIColor(hexColor: 0x1976D2, alpha: 1.0)
                    }
                    
                    self.medicalProfile.birthDay = formatDateStr1
                    self.delegate?.resultMedicalProfile(medicalProfile:self.medicalProfile)
                }
            }
        //}
        
    }
    
    func requestData() {
        Province.requestAllProvinces(success: { (provinces) in
            self.provinces = provinces
            if let currentProvinceID = self.currentUser?.userDoctor?.userInfo?.provinceID {
                if let province = self.provinces?.filter({ (province) -> Bool in
                    return province.id == currentProvinceID
                }).first {
                    province.requestAllDistrict(success: { (districts) in
                        self.districts = districts
                        self.populateDistrictComboBox2()
                    }, fail: { (error, response) in
                        
                    })
                } else {
                    
                }
            } else {
               
            }
        }) { (error, response) in
            
        }
    }
    
    func populateProvinceComboBox2() {
        if let provinces = self.provinces {
            let activeProvinces = provinces.filter({ (province) -> Bool in
                return province.active
            })
            provinceComboBox.dataSource = activeProvinces.map({ (province) -> String in
                return province.name ?? ""
            })
            if let currentProvinceID = currentUser?.userDoctor?.userInfo?.provinceID, currentProvinceIndex == nil {
                for (index, province) in activeProvinces.enumerated() {
                    if province.id == currentProvinceID {
                        currentProvinceIndex = index
                        break
                    }
                }
            }
            provinceComboBox.selectRow(at: currentProvinceIndex)
            
        } else {
            provinceComboBox.selectRow(at: nil)
            provinceComboBox.dataSource = [String]()
        }
    }
    
    func populateDistrictComboBox2() {
        if let districts = self.districts {
            districtComboBox.dataSource = districts.map({ (district) -> String in
                return district.name ?? ""
            })
            if let currentDistrictID = currentUser?.userDoctor?.userInfo?.districtID, currentDistrictIndex == nil {
                for (index, district) in districts.enumerated() {
                    if district.id == currentDistrictID {
                        currentDistrictIndex = index
                        break
                    }
                }
            }
            districtComboBox.selectRow(at: currentDistrictIndex)
        } else {
            districtComboBox.selectRow(at: nil)
            districtComboBox.dataSource = [String]()
        }
    }
    
    func populateGroupBloodComboBox() {
        listGroupBloods = ["O", "O-", "O+", "A", "A-", "A+", "B", "B-"]
        if let groupBloods = listGroupBloods {
            gBloodComboBox.dataSource = groupBloods.map({ (groupBlood) -> String in
                return groupBlood
            })
            
            
        } else {
            gBloodComboBox.selectRow(at: nil)
            gBloodComboBox.dataSource = [String]()
        }
    }
    
    func resultGroupBloodComboBox(){
        for i in 0..<Int((self.listGroupBloods?.count)!){
            if let bloodGroupStr = self.bloodGroupStr{
                let splitSpaceStr = bloodGroupStr.trimmingCharacters(in: .whitespaces)
                if self.listGroupBloods?[i] == splitSpaceStr{
                    gBloodComboBox.selectRow(at: i)
                    return
                }
            }
            
        }
    }
    
    func fwComboBox(comboBox: FWComboBox, didSelectAtIndex index: Int) {
        if comboBox == provinceComboBox {
            currentProvinceIndex = index
            currentDistrictIndex = nil
            requestDistricts()
        } else if comboBox == districtComboBox {
            currentDistrictIndex = index
        } else if comboBox == gBloodComboBox{
            currentGBloodIndex = index
        }
        
        medicalProfile.districtID = currentDistrictIndex
        medicalProfile.provinceID = currentProvinceIndex
        medicalProfile.bloodGroupStr = (self.listGroupBloods?[currentGBloodIndex!])!
        //self.medicalProfileObj = medicalProfile
        delegate?.resultMedicalProfile(medicalProfile:medicalProfile)
    }

    func requestDistricts() {
        if let provinceIndex = currentProvinceIndex {
            if let provincesList = self.provinces {
                let province = provincesList[provinceIndex]
                province.requestAllDistrict(success: { (districts) in
                    self.districts = districts
                    self.populateDistrictComboBox2()
                }, fail: { (error, response) in
                    
                })
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setIconInTextField(currentTextField:UITextField, icon:String, name:String){
        let imageView = UIImageView()
        imageView.image = UIImage(named: icon)?.maskWithColor(color: UIColor.lightGray)
        imageView.frame = CGRect(x: 15, y: 0, width: 15, height: 15)
        imageView.contentMode = .scaleAspectFit
        currentTextField.leftViewMode = UITextFieldViewMode.always
        currentTextField.leftView = imageView
        currentTextField.addSubview(imageView)
        let placeholder = NSMutableAttributedString(string: name, attributes: [NSForegroundColorAttributeName: UIColor.lightGray])
        currentTextField.attributedPlaceholder = placeholder
    }
}

// MARK: - Actions
extension ThutucHCCustomCell {
    
    func didSelectCell() {
//        isCheck = true
        //return
//        lblUserName.becomeFirstResponder()
//        lblSoBHYT.becomeFirstResponder()
//        lblSoCMT.becomeFirstResponder()
//        lblBirthDay.becomeFirstResponder()
//        lblEmailAddress.becomeFirstResponder()
//        lblPhoneNumber.becomeFirstResponder()
//        lblWeight.becomeFirstResponder()
//        lblHeight.becomeFirstResponder()
    }
    
    @IBAction func textFieldValueChanged(_ sender: UITextField) {
        if let result = sender.text {
            if sender.tag != 3{
                switch sender.tag {
                case 0:
                    if !result.isEmpty{
                        medicalProfile.patientName = result.trim()
                    }else{
                        medicalProfile.patientName = ""
                    }
                case 1:
                    if !result.isEmpty{
                        medicalProfile.mobilePhone = result.trim()
                    }else{
                       medicalProfile.mobilePhone = ""
                    }
                case 2:
                    if !result.isEmpty{
                        medicalProfile.email = result.trim()
                    }else{
                        medicalProfile.email = ""
                    }
                case 4:
                    if !result.isEmpty{
                        medicalProfile.idNumber = result.trim()
                    }else{
                        medicalProfile.idNumber = ""
                    }
                case 5:
                    if !result.isEmpty{
                        medicalProfile.healthInsuranceID = result.trim()
                    }else{
                        medicalProfile.healthInsuranceID = ""
                    }
                case 8:
                    if !result.isEmpty{
                        medicalProfile.address = result.trim()
                    }else{
                        medicalProfile.address = ""
                    }
                case 9:
                    if !result.isEmpty{
                        let height = Int(result)
                        medicalProfile.height2 = height!
                    }else{
                        medicalProfile.height2 = 0
                    }
                case 10:
                    if !result.isEmpty{
                        let weight = Int(result)
                        medicalProfile.weight = weight!
                    }else{
                        medicalProfile.weight = 0
                    }
                default:
                    break
                }
            }
            
            //self.medicalProfileObj = medicalProfile
            delegate?.resultMedicalProfile(medicalProfile:medicalProfile)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == lblWeight{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 3
        }else if textField == lblHeight{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 3
        }else if textField == lblUserName{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == lblEmailAddress{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == lblSoCMT{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 16
        }else if textField == lblSoBHYT{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 16
        }else if textField == lblAddress{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 100
        }else if textField == lblPhoneNumber{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 12
        }else{
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
}

