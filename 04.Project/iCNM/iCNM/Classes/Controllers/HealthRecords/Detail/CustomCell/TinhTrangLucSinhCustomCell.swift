//
//  HealthRecordsCell.swift
//  iCNM
//
//  Created by Mac osx on 8/22/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import BEMCheckBox
protocol TinhTrangLucSinhCustomCellDelegate {
    func resultMedicalHealthFactory(medicalHealthFactory:MedicalHealthFactory)
}

class TinhTrangLucSinhCustomCell: UITableViewCell, BEMCheckBoxDelegate, UITextFieldDelegate {

    @IBOutlet var checkbox1: BEMCheckBox!
    @IBOutlet var checkbox2: BEMCheckBox!
    @IBOutlet var checkbox3: BEMCheckBox!
    @IBOutlet var checkbox4: BEMCheckBox!
    @IBOutlet weak var lblKhac: FWFloatingLabelTextField!
    @IBOutlet weak var lblDitatBS: FWFloatingLabelTextField!
    @IBOutlet weak var lblChieudai: FWFloatingLabelTextField!
    @IBOutlet weak var lblCannang: FWFloatingLabelTextField!
    
    var delegate: TinhTrangLucSinhCustomCellDelegate?
    var medicalHealFactory = MedicalHealthFactoryUser.sharedInstance.medicalHealFactoriesObj
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        checkbox1.delegate = self
        checkbox2.delegate = self
        checkbox3.delegate = self
        checkbox4.delegate = self
        lblKhac.delegate = self
        lblChieudai.delegate = self
        lblCannang.delegate = self
        lblDitatBS.delegate = self
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(TinhTrangLucSinhCustomCell.didSelectCell))
        addGestureRecognizer(gesture)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func didTap(_ checkBox: BEMCheckBox){
        switch checkBox.tag {
        case 0:
            medicalHealFactory.deThuong = checkBox.on
        case 1:
            medicalHealFactory.deMo = checkBox.on
        case 2:
            medicalHealFactory.deThieuThang = checkBox.on
        case 3:
            medicalHealFactory.biNgat = checkBox.on
        default:
            break
        }
        
        delegate?.resultMedicalHealthFactory(medicalHealthFactory:medicalHealFactory)
    }
}

// MARK: - Actions
extension TinhTrangLucSinhCustomCell {
    func didSelectCell() {
//        lblDitatBS.becomeFirstResponder()
//        lblKhac.becomeFirstResponder()
//        lblChieudai.becomeFirstResponder()
//        lblCannang.becomeFirstResponder()
    }
    
    @IBAction func textFieldInTableViewCell(_ sender: UITextField) {
        if let result = sender.text {
            switch sender.tag {
            case 0:
                if !result.isEmpty{
                    if let cn = Int(result){
                        medicalHealFactory.cnLucDe = cn
                    }
                }else{
                    medicalHealFactory.cnLucDe = 0
                }
            case 1:
                if !result.isEmpty{
                    if let cd = Int(result){
                        medicalHealFactory.cdLucDe = cd
                    }
                }else{
                    medicalHealFactory.cdLucDe = 0
                }
            case 2:
                if !result.isEmpty{
                    medicalHealFactory.diTatBS = result.trim()
                }else{
                   medicalHealFactory.diTatBS = ""
                }
            case 3:
                if !result.isEmpty{
                    medicalHealFactory.ttLSNote = result.trim()
                }else{
                    medicalHealFactory.ttLSNote = ""
                }
            default:
                break
            }
            
            delegate?.resultMedicalHealthFactory(medicalHealthFactory:medicalHealFactory)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == lblChieudai{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == lblCannang{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == lblDitatBS{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 100
        }else if textField == lblKhac{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else{
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
