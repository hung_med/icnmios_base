//
//  VacxinCustomViewCell.swift
//  iCNM
//
//  Created by Mac osx on 8/28/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class KhamLSCustomViewCell: UICollectionViewCell {
    @IBOutlet var view1: UIView!
    @IBOutlet var lblNgaykham: UILabel!
    @IBOutlet var lblChuandoan: UILabel!
    @IBOutlet var lblTuvan: UILabel!
    @IBOutlet var lblBsKham: UILabel!
    @IBOutlet var lblNgayhenkham: UILabel!
    @IBOutlet weak var btnMore: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        view1.layer.cornerRadius = 4.0
        view1.layer.masksToBounds = true
        view1.layer.borderColor = UIColor.lightGray.cgColor;
        view1.layer.borderWidth = 1.0
        
        view1.layer.shadowOffset = CGSize(width:1, height:20)
        view1.layer.shadowColor = UIColor.lightGray.cgColor
        view1.layer.shadowOpacity = 1
        view1.clipsToBounds = false
        let shadowFrame: CGRect = (self.layer.bounds)
        let shadowPath: CGPath = UIBezierPath(rect: shadowFrame).cgPath
        view1.layer.shadowPath = shadowPath
    }

}
