//
//  HealthRecordsCell.swift
//  iCNM
//
//  Created by Mac osx on 8/22/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import BEMCheckBox
class TiemchungTreemCustomCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet var myView: UIView!
    @IBOutlet var lblSolantiem: UILabel!
    @IBOutlet var lblNgayHentiem: UILabel!
    @IBOutlet var lblPUSauTiem: UILabel!
    @IBOutlet var lblNgaytiem: UILabel!
    @IBOutlet var lblLoaiVx: UILabel!
    @IBOutlet var lblNumberTitle: UILabel!
    @IBOutlet var lblNumber: UITextField!
    @IBOutlet var btnAddMuitiem: UIButton!
    @IBOutlet var lblNameDefault: UILabel!
    @IBOutlet weak var spaceBottonView: NSLayoutConstraint!
    @IBOutlet var myCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblNumber.delegate = self
        btnAddMuitiem.layer.cornerRadius = 4.0
        btnAddMuitiem.layer.masksToBounds = true
        btnAddMuitiem.layer.borderColor = UIColor.lightGray.cgColor
        btnAddMuitiem.layer.borderWidth = 0.5
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension TiemchungTreemCustomCell {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        myCollectionView.delegate = dataSourceDelegate
        myCollectionView.dataSource = dataSourceDelegate
        myCollectionView.tag = row
        myCollectionView.layer.cornerRadius = 4.0
        myCollectionView.layer.cornerRadius = 4.0
        myCollectionView.backgroundColor = UIColor.white
        myCollectionView.setContentOffset(myCollectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        myCollectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { myCollectionView.contentOffset.x = newValue }
        get { return myCollectionView.contentOffset.x }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == lblNumberTitle{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 2
        }else{
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
