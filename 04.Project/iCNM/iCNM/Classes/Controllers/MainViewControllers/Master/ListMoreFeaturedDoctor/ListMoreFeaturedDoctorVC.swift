//
//  ListMoreFeaturedDoctorVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import ESPullToRefresh
import Firebase
import FirebaseDatabase

class ListMoreFeaturedDoctorVC: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var myTableView: UITableView!
    var userOnline = [Int]()
    var loadPeople = 3
    var totalPeople = 0
    var searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 250, height: 18))
    var searchOn : UIBarButtonItem!
    var searchOff : UIBarButtonItem!
    let databaseReference = Database.database().reference()
    fileprivate var pageNumber = 1
    fileprivate var footerScrollView = ESRefreshFooterAnimator(frame:CGRect.zero)
    var featuredDoctors: [FeaturedDoctor]?
    var filterData = [FeaturedDoctor]()
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    var isSearching = false
    //@IBOutlet weak var rightTopButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        // Do any additional setup after lfoading the view.
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        myTableView.delegate = self
        myTableView.dataSource = self
        
        myTableView.estimatedRowHeight = 70
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        title = "Bác sĩ nổi bật"
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(ListMoreFeaturedDoctorVC.btBack(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        self.featuredDoctors = [FeaturedDoctor]()
        
        let nib = UINib(nibName: "DoctorInfoCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        
        // create right bar button item
        searchOn = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(isSearchOn))
        searchOff = UIBarButtonItem(title: "Hủy", style: .plain, target: self, action: #selector(isSearchOff))
        
        self.navigationItem.rightBarButtonItems = [self.searchOn]
        // create search bar
        self.tabBarController?.tabBar.isHidden = true
        searchBar.placeholder = "Tìm kiếm.."
        searchBar.tintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        searchBar.isHidden = true
        searchBar.delegate = self
        footerScrollView.loadingMoreDescription = "Tải thêm"
        self.myTableView.es.addInfiniteScrolling(animator:footerScrollView) { [weak self] in
            if let weakSelf = self {
                weakSelf.pageNumber = weakSelf.pageNumber + 1
                FeaturedDoctor().getDoctorRatedHomeFixByPage(pageNumber:weakSelf.pageNumber, success: { (data) in
                    weakSelf.myTableView.es.stopLoadingMore()
                    if data.count > 0 {
                        self?.featuredDoctors?.append(contentsOf: data)
                        self?.myTableView.reloadData()
                    } else {
                        self?.footerScrollView.loadingMoreDescription = "Đã hết dữ liệu"
                    }
                }, fail: { (error, response) in
                    
                })
            }
        }
        
        self.getData()
    }
    
    @IBAction func btBack(sender: UIBarButtonItem) {
         _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    func isSearchOn() {
        self.navigationItem.setRightBarButtonItems([self.searchOff], animated: false)
        self.navigationItem.titleView = searchBar
        searchBar.isHidden = false
        searchBar.becomeFirstResponder()
    }
    
    func isSearchOff() {
        searchBar.text = nil
        self.navigationItem.setRightBarButtonItems([self.searchOn], animated: false)
        self.navigationItem.titleView = nil
        if searchBar.text == "" {
            isSearching = false
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
    }
    
    // get data from user login
    func getData() {
        //let progress = self.showProgress()
        FeaturedDoctor().getDoctorRatedHomeFixByPage(pageNumber: 1, success: { (data) in
            if data.count > 0 {
                DispatchQueue.main.async(execute: {
                    self.featuredDoctors?.append(contentsOf: data)
                    self.myTableView.reloadData()
                    //self.hideProgress(progress)
                    Loader.addLoaderTo(self.myTableView)
                    Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(ListMoreFeaturedDoctorVC.loaded), userInfo: nil, repeats: true)
                })
            }else{
                Loader.removeLoaderFrom(self.myTableView)
            }
        }, fail: { (error, response) in
            //self.hideProgress(progress)
            Loader.removeLoaderFrom(self.myTableView)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func loaded()
    {
        Loader.removeLoaderFrom(self.myTableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching {
            return filterData.count
        }
        return self.featuredDoctors!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! DoctorInfoCell
        cell.layer.cornerRadius = 4.0
        cell.layer.masksToBounds = true
        cell.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        cell.layer.borderWidth = 1.0
        cell.isOnlineBtn.layer.cornerRadius = 10.0
        cell.isOnlineBtn.layer.masksToBounds = true
        cell.isOnlineBtn.layer.borderWidth = 2.0
        cell.isOnlineBtn.layer.borderColor = UIColor.white.cgColor
        var featuredDoctor:FeaturedDoctor? = nil
        if isSearching{
            featuredDoctor = self.filterData[indexPath.row]
        }else{
            featuredDoctor = self.featuredDoctors?[indexPath.row]
        }
        DispatchQueue.main.async(execute: {
            if let userId = featuredDoctor?.users?.userID {
                self.databaseReference.child("Helpers").child("Users").child("\(userId)").observe(.value, with: { (snapshot) in
                    if snapshot.exists() {
                        if let dict = snapshot.value as? [String : Any] {
                            let isOnline = dict["isOnline"] as! Bool
                            if isOnline == true {
                                cell.isOnlineBtn.backgroundColor = UIColor.init(hex: "25D366")
                            } else {
                                cell.isOnlineBtn.backgroundColor = UIColor.init(hex: "AAAAAA")
                            }
                        }
                    } else {
                        cell.isOnlineBtn.backgroundColor = UIColor.init(hex: "AAAAAA")
                    }
                })
            }
            
            if let avatar = featuredDoctor?.users?.avatar, let userId = featuredDoctor?.users?.userID {
                cell.imgAvatar.contentMode = .scaleToFill
                let urlString = avatar.trimmingCharacters(in: .whitespaces)
                if avatar != "" || !avatar.isEmpty{
                    let url = API.baseURLImage + "\(API.iCNMImage)" + "\(userId)" + "/\(urlString)"
                    cell.imgAvatar.loadImageUsingUrlString(urlString: url)
                }else{
                    cell.imgAvatar.image = UIImage(named: "avatar_default")
                }
            }else{
                cell.imgAvatar.image = UIImage(named: "avatar_default")
            }
            
            if let rating = featuredDoctor?.doctor?.rating{
                cell.cosmosStar.rating = rating
            }
            if let doctorName = featuredDoctor?.users?.name{
                cell.lbUser.text = doctorName
                cell.lbUser.lineBreakMode = .byWordWrapping
                cell.lbUser.numberOfLines = 0
                cell.lbUser.adjustsFontSizeToFitWidth = true
            }
            if featuredDoctor?.specialists?.count != 0{
                if let specialists = featuredDoctor?.specialists![0].name{
                    if IS_IPHONE_5{
                        cell.lbSpecialist.text = "CK: \(specialists)"
                    }else{
                        cell.lbSpecialist.text = "Chuyên khoa: \(specialists)"
                    }
                    cell.lbSpecialist.lineBreakMode = .byWordWrapping
                    cell.lbSpecialist.numberOfLines = 0
                }
            }else{
                if IS_IPHONE_5{
                    cell.lbSpecialist.text = "CK: Chưa cập nhật"
                }else{
                    cell.lbSpecialist.text = "Chuyên khoa: Chưa cập nhật"
                }
            }
        })
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var featuredDoctor:FeaturedDoctor? = nil
        if isSearching{
            featuredDoctor = self.filterData[indexPath.row]
        }else{
            featuredDoctor = self.featuredDoctors?[indexPath.row]
        }
        let profileDoctorVC = ProfileDoctorVC(nibName: "ProfileDoctorVC", bundle: nil)
        profileDoctorVC.featuredDoctor = featuredDoctor!
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(profileDoctorVC, animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            isSearching = false
            view.endEditing(true)
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
        else {
            isSearching = true
            filterContentForSearchText(searchText)
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filterData = (self.featuredDoctors?.filter({( lookUp : FeaturedDoctor) -> Bool in
            let doctorName = lookUp.users?.name.folding(options: .diacriticInsensitive, locale: .autoupdatingCurrent)
            let keySearch = searchText.folding(options: .diacriticInsensitive, locale: .autoupdatingCurrent)
            return doctorName!.lowercased().contains(keySearch.lowercased())
            }
        ))!
    }
}
