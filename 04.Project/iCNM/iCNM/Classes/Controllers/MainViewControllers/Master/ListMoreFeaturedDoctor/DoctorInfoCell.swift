//
//  DoctorInfoCell.swift
//  iCNM
//
//  Created by Mac osx on 7/14/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Cosmos

class DoctorInfoCell: UITableViewCell {
    
    // outlet connect
    @IBOutlet weak var imgAvatar: CustomImageView!
    @IBOutlet weak var isOnlineBtn: UIButton!
    @IBOutlet weak var lbUser: UILabel!
    @IBOutlet weak var lbSpecialist: UILabel!
    @IBOutlet weak var cosmosStar: CosmosView!
    @IBOutlet weak var lbTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imgAvatar.layer.cornerRadius = 30
        imgAvatar.layer.masksToBounds = true
        imgAvatar.layer.borderWidth = 2.5
        imgAvatar.layer.borderColor = UIColor.init(hex:"1D6EDC").cgColor
        
        isOnlineBtn.layer.cornerRadius = 8.0
        isOnlineBtn.layer.masksToBounds = true
        isOnlineBtn.layer.borderWidth = 2.0
        isOnlineBtn.layer.borderColor = UIColor.white.cgColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

