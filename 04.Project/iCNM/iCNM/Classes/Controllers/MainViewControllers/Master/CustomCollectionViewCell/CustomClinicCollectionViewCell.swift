//
//  CustomClinicCollectionViewCell.swift
//  iCNM
//
//  Created by Quang Hung on 7/7/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Cosmos
class CustomClinicCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var viewWidthConstr: NSLayoutConstraint!
    
    @IBOutlet weak var viewHeightConstr: NSLayoutConstraint!
    @IBOutlet weak var imgHeightConstr: NSLayoutConstraint!
    @IBOutlet weak var imageThumb: CustomImageView!
    @IBOutlet weak var lblDoctorName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
