//
//  CustomDictMedicalCollectionViewCell.swift
//  iCNM
//
//  Created by Quang Hung on 7/7/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class CustomDictMedicalCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageThumb: CustomImageView!
    @IBOutlet weak var lblMedicalName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
