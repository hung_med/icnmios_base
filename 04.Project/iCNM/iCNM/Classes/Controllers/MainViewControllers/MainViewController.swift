//
//  MainViewController.swift
//  iCNM
//
//  Created by Medlatec on 5/11/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import GoogleSignIn
import SideMenu
import StoreKit
import Firebase
import FirebaseDatabase

class MainViewController: BaseViewController ,UIViewControllerTransitioningDelegate, UITableViewDataSource, UITableViewDelegate, LookupResultVCDelegate, DetailLookUpResultVCDelegate, ListLookUpResultVCDelegate, CustomMenuTableViewDelegate,UIPopoverPresentationControllerDelegate, UIScrollViewDelegate, CustomSlideTableViewCellDelegate {
    @IBOutlet weak var myTableView: UITableView!
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    let databaseReference = Database.database().reference()
    
    var banners: [Banner]?
    var featuredNews: [FeaturedNews]?
    var answerQuestions: [QuestionAnswer]?
    var featuredDoctors: [FeaturedDoctor]?
    var linkClinics: [LinkClinics]?
    
    var listTitle:[String] = ["", "", "Chương trình ưu đãi", "Hỏi đáp cùng bác sĩ", "Bác sĩ nổi bật", "Từ điển y tế", "Tin tức mới"]
    var listImages:[String]?
    var descPictures:[String]?
    var listServiceID:[Int]?
    var dictMedicalImages:[String] = ["ic_tudien_xetnghiem","ic_tudien_benh","ic_tudien_dichvu", "ic_tudien_thuoc"]
    var dictMedicalNames:[String] = ["Từ điển Xét nghiệm","Từ điển bệnh","Từ điển Dịch vụ"]
    var storedOffsets = [Int: CGFloat]()
    var userDefaults = UserDefaults.standard
    var btnMenu:UIButton?
    var userOnline = [Int]()
    var isOnline:Bool = false
    
    //var cgsize:CGFloat? = 0.0
    @IBOutlet weak var menuAction: UIBarButtonItem!
    let supportFeedBackVC = StoryboardScene.Main.supportCategoriesMenuNavigationController.instantiate()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let realm = try! Realm()
        currentUser = realm.currentUser()
        menuAction.action = #selector(revealToggle(sender:))
        //self.navigationController?.navigationBar.barStyle = UIColor(hexColor: 0x0288D1)
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        myTableView.delegate = self
        myTableView.dataSource = self
        
        myTableView.estimatedRowHeight = 200
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        self.listImages = [String]()
        self.descPictures = [String]()
        self.listServiceID = [Int]()
        self.banners = [Banner]()
        self.featuredNews = [FeaturedNews]()
        self.linkClinics = [LinkClinics]()
        self.answerQuestions = [QuestionAnswer]()
        self.featuredDoctors = [FeaturedDoctor]()
        
        let nib = UINib(nibName: "CustomTableViewCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        let nib3 = UINib(nibName: "CustomSlideTableViewCell", bundle: nil)
        myTableView.register(nib3, forCellReuseIdentifier: "Cell3")
        
        myTableView.register(UINib(nibName: "CustomTableViewHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "headerCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleLoginSuccess), name: Constant.NotificationMessage.loginSuccess, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleLogout), name: Constant.NotificationMessage.logout, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.methodOfReceivedNotification(notification:)), name: Notification.Name("hideMenuComplete"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleNotification), name: Constant.NotificationMessage.handleNotification, object: nil)
        
        if userDefaults.object(forKey: "receiveNotification") != nil{
            self.tabBarController?.selectedIndex = 4
        }
        
        // Do any additional setup after loading the view.
        layoutFAB()
        self.checkForAppUpdate(deviceType: 2)
        DispatchQueue.global(qos: .background).async {
            self.getAllBanner()
            self.getClinicHome()
            self.getDoctorRatedHome()
            self.getAnswerQuestionRated()
            self.getRatedNewsCategories()
        }
        
        if let currentUser = self.currentUser{
            self.getBannerScholarships(userID: currentUser.id)
        }
    }
    
    func getBannerScholarships(userID:Int){
        Charity().getBannerCharity(userID:userID, success: { (data) in
            if !(data?.isEmpty)! {
                let result:String = data!
                let urlString = API.baseURLImage + "" + API.iCNMImage + "BannerImage/" + result
                self.checkScholarships(urlPopupBanner:urlString)
            } else {
                
            }
        }, fail: { (error, response) in
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func checkScholarships(urlPopupBanner:String){
        let popupBannerVC = PopupBannerViewController(nibName: "PopupBannerViewController", bundle: nil)
        popupBannerVC.urlPopupBanner = urlPopupBanner
        popupBannerVC.currentUser = self.currentUser
        popupBannerVC.modalPresentationStyle = .custom
        popupBannerVC.transitioningDelegate = self
        self.present(popupBannerVC, animated: true, completion: nil)
    }
    
    func checkForAppUpdate(deviceType:Int){
        Version().getVersionApp(deviceType:deviceType, success: { (data) in
            if data != nil {
                let minAppVersion:String = data!
                let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
                //Compare App Versions
                let minAppVersionComponents:NSArray = minAppVersion.components(separatedBy: ".") as NSArray
                let appVersionComponents:NSArray = appVersion!.components(separatedBy: ".") as NSArray
                var needToUpdate = false
                
                for i in 0..<min(minAppVersionComponents.count, appVersionComponents.count)
                {
                    let minAppVersionComponent = minAppVersionComponents.object(at: i) as! String
                    let appVersionComponent = appVersionComponents.object(at: i) as! String
                    let minApp: String = minAppVersionComponent
                    let appVer: String = appVersionComponent
                    if (minApp != appVer)
                    {
                        needToUpdate = (appVer < minApp)
                        break
                    }
                }
                
                if (needToUpdate)
                {
                    let alert = UIAlertController(title: "Cập nhật ứng dụng", message: "iCNM đã có bản cập nhật mới trên App Store. Quý khách hàng vui lòng cập nhật phần mềm để trải nghiệm ứng dụng tiện ích và hiệu quả hơn.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Cập nhật ngay", style: UIAlertActionStyle.default, handler: { alertAction in
                        UIApplication.shared.openURL(NSURL(string : Constant.linkAppStore)! as URL)
                        alert.dismiss(animated: true, completion: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                }
            } else {
                
            }
        }, fail: { (error, response) in
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    // handleNotification
    func handleNotification(aNotification:Notification) {
        self.tabBarController?.selectedIndex = 4
    }
   
    func revealToggle(sender: UIBarButtonItem) {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    @IBAction func test(_ sender: Any) {
        
    }
    
    @IBAction func touchOnChangeSupportFeedback(_ sender: Any) {
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    func methodOfReceivedNotification(notification: Notification){
        layoutFAB()
    }
    
    func layoutFAB() {
        let window = UIApplication.shared.keyWindow!
        if IS_IPHONE_X{
            btnMenu = UIButton(frame: CGRect(x: screenSizeWidth-80, y: screenSizeHeight-160, width: 50, height: 50))
        }else{
            btnMenu = UIButton(frame: CGRect(x: screenSizeWidth-70, y: screenSizeHeight-110, width: 50, height: 50))
        }
        
        btnMenu?.setImage(UIImage(named: "menu_icon"), for: .normal)
        
        btnMenu?.addTarget(self, action:#selector(handleMenu), for: .touchUpInside)
        btnMenu?.backgroundColor = UIColor.red
        btnMenu?.layer.cornerRadius = 25
        btnMenu?.layer.masksToBounds = true
        if btnMenu != nil{
            btnMenu?.removeFromSuperview()
        }
        window.addSubview(btnMenu!)
        btnMenu?.isHidden = true
    }
    
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController){
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        btnMenu?.isHidden = false
        self.view.alpha = 1.0
    }
    
    func handleMenu(sender: UIButton){
        btnMenu?.isHidden = true
        if let presented = self.presentedViewController {
            presented.removeFromParentViewController()
        }
        self.view.alpha = 0.7
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0xFF4500, alpha: 1.0)
        let customMenuVC = CustomMenuTableView(nibName: "CustomMenuTableView", bundle: nil)
        customMenuVC.delegate = self
        customMenuVC.view.layer.cornerRadius = 4
        customMenuVC.view.layer.masksToBounds = true
        customMenuVC.modalPresentationStyle = UIModalPresentationStyle.popover
        customMenuVC.preferredContentSize = CGSize(width:200, height:135)
        let popover = customMenuVC .popoverPresentationController
        customMenuVC.view.backgroundColor = .red
        popover?.sourceView = sender as UIButton
        popover!.delegate = self
        if IS_IPHONE_X{
            popover?.sourceRect = CGRect(x:0, y:20, width: 200, height: 135)
        }else{
            popover?.sourceRect = CGRect(x:0, y:0, width: 200, height: 135)
        }
        
        popover?.permittedArrowDirections = UIPopoverArrowDirection.up
        self .present(customMenuVC, animated: true, completion: {
            
        })
    }

    func goBackMainVC(index:Int){
        btnMenu?.isHidden = true
        if index == 0{
            let parent = self.parent!
            parent.dismiss(animated: true, completion: {
                self.view.alpha = 1.0
            })
            self.tabBarController?.tabBar.isHidden = true
            showListResultVC()
        }else if index == 1{
            self.view.alpha = 1.0
            self.tabBarController?.tabBar.isHidden = true
            navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
            let questionVC = StoryboardScene.Main.createQuestionVC.instantiate()
            self.navigationController?.pushViewController(questionVC, animated: true)
        }else{
            self.view.alpha = 1.0
            let scheduleVC = StoryboardScene.Main.scheduleAppointmentVC.instantiate()
            self.tabBarController?.tabBar.isHidden = true
            navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
            self.navigationController?.pushViewController(scheduleVC, animated: true)
        }
    }
    
    func showListResultVC(){
        if let user = self.currentUser{
            if user.userDoctor?.userInfo?.userName != nil {
                var optionMenu:UIAlertController? = nil
                if IS_IPAD{
                    optionMenu = UIAlertController(title: nil, message: "Chọn loại tài khoản", preferredStyle: .alert)
                }else{
                    optionMenu = UIAlertController(title: nil, message: "Chọn loại tài khoản", preferredStyle: .actionSheet)
                }
                
                let action1 = UIAlertAction(title: "Tra cứu kết quả", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    self.showLookupResultVC()
                })
                let action2 = UIAlertAction(title: "Xem kết quả cho bác sĩ", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    if let user = self.currentUser{
                        let userWeb = user.userDoctor?.userInfo?.userName
                        let passWeb = user.userDoctor?.userInfo?.userPass
                        self.getDoctorIDUnit(userWeb: userWeb!, passWed: passWeb!, organizeID: 5)
                    }
                    
                })
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
                    (alert: UIAlertAction!) -> Void in
                })
                
                let image =  UIImage.resizeImage(image: UIImage(named: "user-circle")!, toWidth: 20)?.maskWithColor(color: UIColor(hexColor: 0x1976D2, alpha: 1.0))
                let image2 =  UIImage.resizeImage(image: UIImage(named: "user-md")!, toWidth: 20)?.maskWithColor(color: UIColor(hexColor: 0xFF1E17, alpha: 1.0))
                
                action1.setValue(image?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), forKey: "image")
                action2.setValue(image2?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), forKey: "image")
                
                optionMenu?.addAction(action1)
                optionMenu?.addAction(action2)
                optionMenu?.addAction(cancelAction)
                
                // show action sheet
                optionMenu?.popoverPresentationController?.sourceView = self.view
                self.present(optionMenu!, animated: true, completion: nil)
            }else{
                
            }
        }
        
        self.showLookupResultVC()
    }
    
    func showLookupResultVC(){
        btnMenu?.isHidden = true
        let lookupResultVC = LookupResultVC(nibName: "LookupResultVC", bundle: nil)
        if let currUser = self.currentUser{
            lookupResultVC.currentUser = currUser
        }
        lookupResultVC.delegate = self
        self.tabBarController?.tabBar.isHidden = true
        lookupResultVC.modalPresentationStyle = .custom
        lookupResultVC.transitioningDelegate = self
        lookupResultVC.view.frame = CGRect(x: screenSizeWidth/2-(screenSizeWidth-94)/2, y: screenSizeHeight/2-(screenSizeHeight - 336)/2, width: screenSizeWidth-94, height: screenSizeHeight - 336)
        
        self.present(lookupResultVC, animated: true, completion: nil)
    }
    
    func getDoctorIDUnit(userWeb:String, passWed:String, organizeID:Int){
        let progress = self.showProgress()
        LoginDoctorUnit().getDoctorIDUnit(userWeb:userWeb, passWeb:passWed, organizeID:organizeID, success: { (data) in
            if data != nil {
                self.hideProgress(progress)
                let date = Date()
                let calendar = Calendar.current
                let month = calendar.component(.month, from: date)
                let year = calendar.component(.year, from: date)
                if let maBS = data?.maBS{
                     self.showListLookUpUnit(user: "", maBS: maBS, currentMonth: String(month), currentYear: String(year), organizeID: "5")
                }
            } else {
                self.hideProgress(progress)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController!, sourceViewController source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController: presented, presenting: presentingViewController)
    }

    func gobackMainView() {
        self.tabBarController?.tabBar.isHidden = false
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        btnMenu?.isHidden = false
    }
    
    func dismissResultVC() {
        confirmLogin(myView: self)
        return
    }
    
    func showListLookUpResultbyPID(pid: String, organizeID:String) {
        let listLookUpResultVC = ListLookUpResultVC(nibName: "ListLookUpResultVC", bundle: nil)
        listLookUpResultVC.delegate = self
        self.tabBarController?.tabBar.isHidden = true
        listLookUpResultVC.getListLookUpResultByPID(pid: pid, organizeID:organizeID)
        self.navigationController?.pushViewController(listLookUpResultVC, animated: true)
    }
    
    func showListLookUpResultBySID(pid: String, sid: String, organizeID:String) {
        if let viewController = UIStoryboard(name: "DetailLookUp", bundle: nil).instantiateViewController(withIdentifier: "DetailLookUpResultVC") as? DetailLookUpResultVC {
            viewController.delegate = self
            self.tabBarController?.tabBar.isHidden = true
            viewController.getListLookUpResultBySID(sid: sid, pID: pid, organizeID: organizeID)
            self.navigationController!.pushViewController(viewController, animated: true)
        }
    }
    
    func showListLookUpResultbyPhone(phone: String) {
        if let viewController = UIStoryboard(name: "DetailLookUp", bundle: nil).instantiateViewController(withIdentifier: "DetailLookUpResultVC") as? DetailLookUpResultVC {
            viewController.delegate = self
            self.tabBarController?.tabBar.isHidden = true
            viewController.getListLookUpResultByPhone(phone: phone)
            self.navigationController!.pushViewController(viewController, animated: true)
        }
    }
    
    func showListLookUpUnit(user:String, maBS:String, currentMonth:String, currentYear:String, organizeID:String){
        let listLookUpResultVC = ListLookUpResultVC(nibName: "ListLookUpResultVC", bundle: nil)
        listLookUpResultVC.getListLookUpResultUnit(user: user, maBS: maBS, currentMonth: currentMonth, currentYear:currentYear, organizeID: organizeID)
        listLookUpResultVC.delegate = self
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(listLookUpResultVC, animated: true)
    }

    func gobackLookupVC(){
        self.tabBarController?.tabBar.isHidden = true
        btnMenu?.isHidden = true
        self.showListResultVC()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        btnMenu?.isHidden = false
        //right menu
        SideMenuManager.default.menuRightNavigationController = supportFeedBackVC
        self.tabBarController?.tabBar.isHidden = false
        //  self.hidesBottomBarWhenPushed = false;
       // self.title = "Trang chủ"
        requestData()
        
        // call video
        guard let id = currentUser?.userDoctor?.userInfo?.id else {return}
        NotificationCenter.default.post(name: NSNotification.Name("UserDidLoginNotification"), object: nil, userInfo: ["userId": "\(String(describing: id))"])
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        btnMenu?.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool){
       // self.tabBarController?.tabBar.isHidden = true
        btnMenu?.isHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        btnMenu?.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func requestData() {
        if let user = currentUser {
            //let progress = showProgress()
            user.requestUserInformation(success: { (newUserInfo) in
                //self.hideProgress(progress)
                NotificationCenter.default.post(name: Constant.NotificationMessage.profileEdited, object: self)
            }, fail: { (error, response) in
                //self.hideProgress(progress)
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }
    }
    
    func handleLoginSuccess(aNotification:Notification) {
        let realm = try! Realm()
        currentUser = realm.currentUser()
        let userDefault = UserDefaults.standard
        userDefault.set(true, forKey: "agreedTerms")
        userDefault.synchronize()
        
        let databaseReference = Database.database().reference()
        guard let id = currentUser?.userDoctor?.userInfo?.id else {return}
        let date = Date()
        let time = Int((date.timeIntervalSince1970) * 1000)
        let value = ["userID": id as Any, "type": "Doctor", "time": time, "isOnline": true] as [String : Any]
        databaseReference.child("Helpers").child("Users").child("\(id)").setValue(value)
    }
    
    func handleLogout(aNotification:Notification) {
        currentUser = nil
        let userDefault = UserDefaults.standard
        if userDefault.bool(forKey: "agreedTerms") {
             userDefault.removeObject(forKey: "agreedTerms")
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func getAllBanner(){
        let progress = self.showProgress()
        //self.banners = [Banner]()
        Banner().getAllBanner(success: { (data) in
            if data.count > 0 {
                DispatchQueue.main.async(execute: {
                    self.banners?.append(contentsOf: data)
                    for i in 0..<Int((self.banners?.count)!){
                        let banner = self.banners![i]
                        self.listImages?.append(banner.imgStr)
                        self.descPictures?.append(banner.des)
                        self.listServiceID?.append(banner.dataID)
                        //self.myTableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: .automatic)
                    }
                })
                self.hideProgress(progress)
            } else {
                self.hideProgress(progress)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func getRatedNewsCategories(){
        let progress = self.showProgress()
        //self.featuredNews = [FeaturedNews]()
        FeaturedNews.getAllFeaturedNews(success: { (data) in
            if data.count > 0 {
                DispatchQueue.main.async(execute: {
                    self.featuredNews?.append(contentsOf: data)
                    self.hideProgress(progress)
                    self.myTableView.reloadData()
                })
            } else {
                self.hideProgress(progress)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func getAnswerQuestionRated(){
        let progress = self.showProgress()
        //self.answerQuestions = [QuestionAnswer]()
        AnswerQuestion.getAllAnswerQuestion(success: { (data) in
            if data.count > 0 {
                DispatchQueue.main.async(execute: {
                    self.answerQuestions?.append(contentsOf: data)
                    self.hideProgress(progress)
                    //self.myTableView.reloadData()
                })
            } else {
                self.hideProgress(progress)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func getClinicHome(){
        let progress = self.showProgress()
        //self.linkClinics = [LinkClinics]()
        LinkClinics.getAllLinkClinics(success: { (data) in
            if data.count > 0 {
                DispatchQueue.main.async(execute: {
                    self.linkClinics?.append(contentsOf: data)
                    self.hideProgress(progress)
                    //self.myTableView.reloadSections(NSIndexSet(index: 2) as IndexSet, with: .automatic)
                })
            } else {
                self.hideProgress(progress)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func getDoctorRatedHome(){
        let progress = self.showProgress()
        //self.featuredDoctors = [FeaturedDoctor]()
        FeaturedDoctor.getAllFeaturedDoctor(success: { (data) in
            if data.count > 0 {
                DispatchQueue.main.async(execute: {
                    self.featuredDoctors?.append(contentsOf: data)
                    self.hideProgress(progress)
                })
            } else {
                 self.hideProgress(progress)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return listTitle.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerCell") as! CustomTableViewHeader
        headerView.titleHeader.text = listTitle[section]
        headerView.titleHeader.font = UIFont.boldSystemFont(ofSize: 18.0)
        switch section{
        case 0:
            headerView.imgIcon.image = UIImage(named: "")
        case 1:
            headerView.imgIcon.image = UIImage(named: "")
        case 2:
            headerView.imgIcon.image = UIImage(named: "calendar-plus-o")?.maskWithColor(color: UIColor(hexColor: 0x1976D2, alpha: 1.0))
        case 3:
            headerView.imgIcon.image = UIImage(named: "comment")?.maskWithColor(color: UIColor(hexColor: 0x1976D2, alpha: 1.0))
        case 4:
            headerView.imgIcon.image = UIImage(named: "user-md")?.maskWithColor(color: UIColor(hexColor: 0x1976D2, alpha: 1.0))
        case 5:
            headerView.imgIcon.image = UIImage(named: "medkit")?.maskWithColor(color: UIColor(hexColor: 0x1976D2, alpha: 1.0))
        case 6:
            headerView.imgIcon.image = UIImage(named: "rss-square")?.maskWithColor(color: UIColor(hexColor: 0x1976D2, alpha: 1.0))
        default:
            headerView.imgIcon.image = UIImage(named: "user-md")?.maskWithColor(color: UIColor(hexColor: 0x1976D2, alpha: 1.0))
        }
        
        if section == 3 || section == 4 || section == 6{
            headerView.imageArrow.isHidden = false
            headerView.imageArrow.image = UIImage(named: "icon_rightarrow")?.maskWithColor(color: UIColor(hexColor: 0x1976D2, alpha: 1.0))
        }else{
            headerView.imageArrow.isHidden = true
        }
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainViewController.directQuickVC(_:)))
        headerView.imageArrow.tag = section
        headerView.imageArrow.isUserInteractionEnabled = true
        headerView.imageArrow.addGestureRecognizer(tapGestureRecognizer)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3", for: indexPath) as! CustomSlideTableViewCell
            let widthConstraint = NSLayoutConstraint(
                item: cell.footerView,
                attribute: NSLayoutAttribute.height,
                relatedBy: NSLayoutRelation.equal,
                toItem: nil,
                attribute: NSLayoutAttribute.height,
                multiplier: 1,
                constant: 28
            )
            self.view.addConstraint(widthConstraint)
            
            cell.delegate = self
                cell.countImage = (self.listImages?.count)!
                if cell.countImage > 0{
                   // cell.activityIndicator.startAnimating()
                    cell.listServiceID = listServiceID
                    cell.showSlideAnimation(isCheck: false, listImages: self.listImages!, desc: self.descPictures!)
                }
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomTableViewCell
            switch indexPath.section {
            case 1:
                cell.collectionView.register(UINib(nibName: "CustomMenuCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MenuCell")
                cell.collectionView.viewWithTag(1)
                if #available(iOS 10, *) {
                    // use an api that requires the minimum version to be 10
                } else {
                    
                    cell.collectionView.collectionViewLayout.invalidateLayout()
                }
            case 2:
                cell.collectionView.register(UINib(nibName: "CustomClinicCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ClinicCell")
                cell.collectionView.viewWithTag(2)
                if #available(iOS 10, *) {
                    // use an api that requires the minimum version to be 10
                } else {
                    cell.collectionView.collectionViewLayout.invalidateLayout()
                }
            case 3:
                cell.collectionView.register(UINib(nibName: "CustomQACollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "QACell")
                cell.collectionView.viewWithTag(3)
                if #available(iOS 10, *) {
                    // use an api that requires the minimum version to be 10
                } else {
                    cell.collectionView.collectionViewLayout.invalidateLayout()
                }
            case 4:
                cell.collectionView.register(UINib(nibName: "CustomDoctorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DoctorCell")
                cell.collectionView.viewWithTag(4)
                if #available(iOS 10, *) {
                    // use an api that requires the minimum version to be 10
                } else {
                    cell.collectionView.collectionViewLayout.invalidateLayout()
                }
            case 5:
                cell.collectionView.register(UINib(nibName: "CustomDictMedicalCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MedicalCell")
                cell.collectionView.viewWithTag(5)
                if #available(iOS 10, *) {
                    // use an api that requires the minimum version to be 10
                } else {
                    cell.collectionView.collectionViewLayout.invalidateLayout()
                }
            case 6:
                cell.collectionView.register(UINib(nibName: "CustomNewsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NewsCell")
                cell.collectionView.viewWithTag(6)
                if #available(iOS 10, *) {
                    // use an api that requires the minimum version to be 10
                } else {
                    cell.collectionView.collectionViewLayout.invalidateLayout()
                }
            default:
                cell.collectionView.register(UINib(nibName: "CustomNewsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NewsCell")
                cell.collectionView.viewWithTag(6)
                if #available(iOS 10, *) {
                    // use an api that requires the minimum version to be 10
                } else {
                    cell.collectionView.collectionViewLayout.invalidateLayout()

                }
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            return cell
        }
    }
    
    // Set the spacing between sections
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if section == 0 || section == 1{
            return 0
        }
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 5 {
            //let detailNewsVC = NewsDetailViewController(nibName: "NewsDetailViewController", bundle: nil)
            //self.tabBarController?.tabBar.isHidden = true
            //self.navigationController?.pushViewController(detailNewsVC, animated: true)
        }else if indexPath.section == 3{
            //self.tabBarController?.selectedIndex = 1
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        switch indexPath.section{
        case 0:
            if banners!.count > 0{
                if IS_IPAD{
                    return screenSizeHeight/3.5
                }else{
                    return screenSizeHeight/4.0
                }
            }else{
                return 0
            }
        
        case 1:
            if IS_IPAD{
                return 386
            }else{
                if IS_IPHONE_XS{
                    return screenSizeHeight/3.55
                }else{
                    if IS_IPHONE_X{
                        return screenSizeHeight/3.57
                    }else{
                        if IS_IPHONE_6 || IS_IPHONE_8{
                            return screenSizeHeight/2.94
                        }else if IS_IPHONE_5{
                            return screenSizeHeight/2.98
                        }
                        return screenSizeHeight/2.91
                    }
                }
            }
        case 2:
            if IS_IPAD{
                return 386
            }else{
                if linkClinics!.count > 0{
                    return 185
                }else{
                    return 0
                }
            }
        case 3:
            if IS_IPAD{
                return screenSizeHeight/2.768
            }else{
                if answerQuestions!.count > 0{
                    return 375
                }else{
                    return 0
                }
            }
        case 4:
            if IS_IPAD{
                return screenSizeHeight/4
            }else{
                if featuredDoctors!.count > 0{
                    return 215
                }else{
                    return 0
                }
            }
        case 5:
            if IS_IPHONE_XS{
                return screenSizeHeight/4
            }else{
                if IS_IPAD{
                    return screenSizeHeight/4
                }else{
                    if IS_IPHONE_X{
                        return screenSizeHeight/5.2+15
                    }else{
                        return screenSizeHeight/4+15
                    }
                }
            }
        case 6:
            if featuredNews!.count > 0{
                let value = Int(screenSizeHeight/4.05714)
                if IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_8{
                    return CGFloat(240 + value*(featuredNews!.count/2)-140)
                }else{
                    return CGFloat(240 + value*(featuredNews!.count/2)+5)
                }
            }else{
                return 0
            }
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? CustomTableViewCell else { return }
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section)
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? CustomTableViewCell else { return }
        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        let contentOffset = scrollView.contentOffset.y
        if contentOffset > screenSizeHeight{
            btnMenu?.isHidden = false
            if btnMenu != nil{
                btnMenu?.removeFromSuperview()
            }
            let window = UIApplication.shared.keyWindow!
            window.addSubview(btnMenu!)
        }else{
            btnMenu?.isHidden = true
        }
    }
    
    func presentServicePackage(serviceID:Int, serviceName:String){
        let servicePackageDetailVC = ServicePackageDetailVC(nibName: "ServicePackageDetailVC", bundle: nil)
        servicePackageDetailVC.isCheck = true
        servicePackageDetailVC.serviceName = serviceName
        servicePackageDetailVC.getAllServiceDetailGroups(serviceID:serviceID)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(servicePackageDetailVC, animated: true)
    }
}

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
//        case 0:
//            return 1
        case 1:
            collectionView.collectionViewLayout.invalidateLayout()
            return Int(Constant.totalItem)
        case 2:
            collectionView.collectionViewLayout.invalidateLayout()
            return linkClinics!.count
        case 3:
            collectionView.collectionViewLayout.invalidateLayout()
            return answerQuestions!.count
        case 4:
            collectionView.collectionViewLayout.invalidateLayout()
            return featuredDoctors!.count
        case 5:
            collectionView.collectionViewLayout.invalidateLayout()
            return dictMedicalNames.count
        case 6:
            collectionView.collectionViewLayout.invalidateLayout()
            return featuredNews!.count
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        collectionView.collectionViewLayout.invalidateLayout()
        switch collectionView.tag {
//        case 0:
//            return CGSize(width: screenSizeWidth, height: 200)
        case 1:
            if IS_IPAD {
                return CGSize(width: collectionView.bounds.width / 3, height: 180)
            } else {
                let itemWidth = getItemWidth(boundWidth: collectionView.bounds.size.width, column:  3)
                return CGSize(width: itemWidth-1, height: itemWidth-14)
            }
        case 2:
            if IS_IPAD{
                return CGSize(width: screenSizeWidth/2, height: 386)
            }else{
                return CGSize(width: 180, height: 170)
            }
        case 3:
            if IS_IPAD{
                return CGSize(width: 280, height: screenSizeHeight/2.768)
            }else{
                return CGSize(width: 255, height: 360)
            }
        case 4:
            if IS_IPAD{
                return CGSize(width: screenSizeWidth/3.84, height: screenSizeHeight/4)
            }else{
                return CGSize(width: 120, height: 200)
            }
        case 5:
            if IS_IPHONE_XS{
                return CGSize(width: screenSizeWidth/3.0, height: screenSizeHeight/4.5)
            }else{
                if IS_IPAD{
                    return CGSize(width: screenSizeWidth/3.18, height: screenSizeHeight/4)
                }else{
                    if IS_IPHONE_X{
                        return CGSize(width: screenSizeWidth/3.2, height: screenSizeHeight/5.2)
                    }else{
                        return CGSize(width: screenSizeWidth/3.3, height: screenSizeHeight/4)
                    }
                }
            }
        case 6:
            let itemWidth = getItemWidth(boundWidth: collectionView.bounds.size.width, column:2)
            let value = Int(screenSizeHeight/4.05714)
            return CGSize(width: itemWidth, height: value)
            
        default:
            return CGSize(width: 0, height: 0)
        }
    }
    
    func getItemWidth(boundWidth: CGFloat, column:CGFloat) -> Int {
        let totalWidth = boundWidth - (Constant.offset) - (column - 1) * Constant.minItemSpacing
        return Int(totalWidth / column)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.collectionViewLayout.invalidateLayout()
        switch collectionView.tag {
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCell", for: indexPath) as! CustomMenuCollectionViewCell
            if IS_IPAD {
                cell.viewWidthConstr.constant = self.view.bounds.size.width / 3
                cell.viewHeightConstr.constant = 120
                cell.imgWidthConstr.constant = 120
                cell.imgHeightConstr.constant = 120
                cell.lblTitle.font = UIFont.systemFont(ofSize: 25)
            }else if IS_IPHONE_5 {
                cell.imgWidthConstr.constant = 50
                cell.imgHeightConstr.constant = 50
                cell.lblBottomConstr.constant = 0
            }
            
            cell.myView.layer.cornerRadius = 4.0
            cell.myView.layer.masksToBounds = true
            cell.myView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            cell.myView.layer.borderWidth = 1.0
            
            cell.myView.layer.shadowOffset = CGSize(width:1, height:10)
            cell.myView.layer.shadowColor = UIColor.groupTableViewBackground.cgColor
            cell.myView.layer.shadowOpacity = 1
            cell.myView.clipsToBounds = false
            let shadowFrame: CGRect = (cell.layer.bounds)
            let shadowPath: CGPath = UIBezierPath(rect: shadowFrame).cgPath
            cell.myView.layer.shadowPath = shadowPath
            DispatchQueue.main.async(execute: {
                switch indexPath.row{
                case 0:
                    cell.imageThumb.image = UIImage(named: "menu0")
                    cell.lblTitle.text = "Tra cứu"
                case 1:
                    cell.imageThumb.image = UIImage(named: "menu4")
                    cell.lblTitle.text = "Hỏi đáp"
                case 2:
                    cell.imageThumb.image = UIImage(named: "menu2")
                    cell.lblTitle.text = "Gói khám"
                case 3:
                    cell.imageThumb.image = UIImage(named: "menu5")
                    cell.lblTitle.text = "Đặt lịch"
                case 4:
                    cell.imageThumb.image = UIImage(named: "menu3")
                    cell.lblTitle.text = "Từ điển"
                case 5:
                    cell.imageThumb.image = UIImage(named: "menu6")
                    cell.lblTitle.text = "Hồ sơ"
                default:
                    cell.imageThumb.image = UIImage(named: "menu5")
                    cell.lblTitle.text = "Tra cứu"
                }
            })
            
            return cell
        
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ClinicCell", for: indexPath) as! CustomClinicCollectionViewCell
            cell.layer.cornerRadius = 4.0
            cell.layer.masksToBounds = true
            cell.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            cell.layer.borderWidth = 1.0
            
            if IS_IPAD {
                cell.viewWidthConstr.constant = self.view.bounds.size.width / 2
                cell.viewHeightConstr.constant = 386
                cell.imgHeightConstr.constant = 300
                cell.lblDoctorName.font = UIFont.systemFont(ofSize: 25)
            }
            
            let linkClinicObj = linkClinics?[indexPath.row]
            DispatchQueue.main.async(execute: {
                if let image = linkClinicObj?.image, let serviceID = linkClinicObj?.serviceID{
                    cell.imageThumb.contentMode = .scaleToFill
                    let urlString = image.trimmingCharacters(in: .whitespaces)
                    if image != "" || !image.isEmpty{
                        let url = API.baseURLImage + "\(API.iCNMImage)" + "\(API.serviceImage)" + "\(serviceID)" + "/\(urlString)"
                        cell.imageThumb.loadImageUsingUrlString(urlString: url)
                    }else{
                        cell.imageThumb.image = UIImage(named: "default-thumbnail")
                    }
                }else{
                    cell.imageThumb.image = UIImage(named: "default-thumbnail")
                }
                
                if let organizeName = linkClinicObj?.organizeName{
                    cell.lblDoctorName.text = organizeName
                    cell.lblDoctorName.lineBreakMode = .byWordWrapping
                    cell.lblDoctorName.numberOfLines = 0
                    //cell.lblDoctorName.adjustsFontSizeToFitWidth = true
                }
            })
            
            return cell
        case 3:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QACell", for: indexPath) as! CustomQACollectionViewCell
            cell.layer.cornerRadius = 4.0
            cell.layer.masksToBounds = true
            cell.layer.borderColor = UIColor.groupTableViewBackground.cgColor;
            cell.layer.borderWidth = 1.0
            cell.view_Title.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
            cell.userAvatar.contentMode = .scaleToFill
            cell.userAvatar.layer.cornerRadius = 25
            cell.userAvatar.layer.masksToBounds = true
            cell.doctorAvatar.contentMode = .scaleToFill
            cell.doctorAvatar.layer.cornerRadius = 25
            cell.doctorAvatar.layer.masksToBounds = true
            cell.doctorAvatar.layer.borderColor = UIColor(hexColor: 0x1D6EDC, alpha: 1.0).cgColor
            cell.doctorAvatar.layer.borderWidth = 2.0
            cell.userAvatar.layer.borderColor = UIColor(hexColor: 0x1D6EDC, alpha: 1.0).cgColor
            cell.userAvatar.layer.borderWidth = 2.0
            
            let answerQuestion = answerQuestions?[indexPath.row]
            DispatchQueue.main.async(execute: {
                if let questionTitle = answerQuestion?.question?.title{
                    cell.lblQuestionTitle.text = questionTitle
                }
                
                if let userFullName = answerQuestion?.question?.fullname{
                    cell.lblFullname.text = userFullName
                }
                
                if let avatar = answerQuestion?.userAsk?.avatar, let userId = answerQuestion?.question?.userID {
                    if avatar != "" || !avatar.isEmpty{
                        let urlString = avatar.trimmingCharacters(in: .whitespaces)
                        let url = API.baseURLImage + "\(API.iCNMImage)" + "\(userId)" + "/\(urlString)"
                        
                        cell.userAvatar.loadImageUsingUrlString(urlString: url)
                    }else{
                        cell.userAvatar.image = UIImage(named: "avatar_default")
                    }
                }else{
                    cell.userAvatar.image = UIImage(named: "avatar_default")
                }
                
                if let dateCreateStr = answerQuestion?.question?.dateCreate{
                    //get datetime server
                    let date = dateCreateStr.timeAgoSinceNow()
                    cell.lblCreateDate.text = "Đã hỏi \(date)"
                }
                
                if let questionContent = try! answerQuestion?.question?.content?.convertHtmlSymbols() {
                    cell.lblQuestionContent.text = questionContent
                    cell.lblQuestionContent.textColor = UIColor.darkGray
                    cell.lblQuestionContent.lineBreakMode = .byWordWrapping
                    cell.lblQuestionContent.numberOfLines = 0
                }
                
                if let avatar_doctor = answerQuestion?.userOfDoctor?.avatar, let doctorID = answerQuestion?.userOfDoctor?.id {
                    if avatar_doctor != "" || !avatar_doctor.isEmpty{
                        let urlString = avatar_doctor.trimmingCharacters(in: .whitespaces)
                        let url = API.baseURLImage + "\(API.iCNMImage)" + "\(doctorID)" + "/\(urlString)"
                        cell.doctorAvatar.loadImageUsingUrlString(urlString: url)
                    }else{
                        cell.doctorAvatar.image = UIImage(named: "avatar_default")
                    }
                }else{
                    cell.doctorAvatar.image = UIImage(named: "avatar_default")
                }
                
                if let doctorName = answerQuestion?.userOfDoctor?.name{
                    cell.userNameDoctor.text = "\(doctorName)"
                }
                
                if let dateCreate_doctor = answerQuestion?.answer?.dateCreate{
                    //get datetime server
                    let date = dateCreate_doctor.timeAgoSinceNow()
                    cell.lblCreateDate2.text = "Đã trả lời \(date)"
                }
                
                if let answer = answerQuestion?.answer?.content{
                    let decodeContent = String.htmlDecode(answer)
                    cell.lblDoctorAnswer.text = decodeContent()
                    cell.lblDoctorAnswer.textColor = UIColor.white
                    cell.lblDoctorAnswer.lineBreakMode = .byWordWrapping
                    cell.lblDoctorAnswer.numberOfLines = 0
                }
                
                if let specialName = answerQuestion?.specialist?.name{
                    cell.lblSpecialName.text = specialName
                    cell.lblSpecialName.layer.borderColor = UIColor.groupTableViewBackground.cgColor
                    cell.lblSpecialName.layer.borderWidth = 1.0
                }
            })
            
            return cell
        case 4:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DoctorCell", for: indexPath) as! CustomDoctorCollectionViewCell
            cell.layer.cornerRadius = 4.0
            cell.layer.masksToBounds = true
            cell.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            cell.layer.borderWidth = 1.0
            cell.isOnlineBtn.layer.cornerRadius = 10.0
            cell.isOnlineBtn.layer.masksToBounds = true
            cell.isOnlineBtn.layer.borderWidth = 2.0
            cell.isOnlineBtn.layer.borderColor = UIColor.white.cgColor
            
            DispatchQueue.main.async(execute: {
                let featuredDoctor = self.featuredDoctors?[indexPath.row]
                if let userId = featuredDoctor?.users?.userID {
                    self.databaseReference.child("Helpers").child("Users").child("\(userId)").observe(.value, with: { (snapshot) in
                        if snapshot.exists() {
                            if let dict = snapshot.value as? [String : Any] {
                                let isOnline = dict["isOnline"] as! Bool
                                if isOnline == true {
                                    cell.isOnlineBtn.backgroundColor = UIColor.init(hex: "25D366")
                                } else {
                                    cell.isOnlineBtn.backgroundColor = UIColor.init(hex: "AAAAAA")
                                }
                            }
                        } else {
                            cell.isOnlineBtn.backgroundColor = UIColor.init(hex: "AAAAAA")
                        }
                    })
                }
                
                if let avatar = featuredDoctor?.users?.avatar, let userId = featuredDoctor?.users?.userID {
                    cell.imageThumb.contentMode = .scaleToFill
                    let urlString = avatar.trimmingCharacters(in: .whitespaces)
                    if avatar != "" || !avatar.isEmpty{
                        let url = API.baseURLImage + "\(API.iCNMImage)" + "\(userId)" + "/\(urlString)"
                        cell.imageThumb.loadImageUsingUrlString(urlString: url)
                    }else{
                        cell.imageThumb.image = UIImage(named: "avatar_default")
                    }
                }else{
                    cell.imageThumb.image = UIImage(named: "avatar_default")
                }
                
                if let rating = featuredDoctor?.doctor?.rating{
                    cell.cosmosStar.rating = rating
                }
                if let doctorName = featuredDoctor?.users?.name{
                    cell.lblDoctorName.text = doctorName
                    cell.lblDoctorName.lineBreakMode = .byWordWrapping
                    cell.lblDoctorName.numberOfLines = 0
                    //cell.lblDoctorName.adjustsFontSizeToFitWidth = true
                }
                if featuredDoctor?.specialists?.count != 0{
                    if let specialists = featuredDoctor?.specialists![0].name{
                        cell.lbSpecialize.text = specialists
                        cell.lbSpecialize.lineBreakMode = .byWordWrapping
                        cell.lbSpecialize.numberOfLines = 0
                    }
                }else{
                    cell.lbSpecialize.text = ""
                }
            })
            
            return cell
            
        case 5:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MedicalCell", for: indexPath) as! CustomDictMedicalCollectionViewCell
            cell.layer.cornerRadius = 4.0
            cell.layer.masksToBounds = true
            cell.layer.borderColor = UIColor.groupTableViewBackground.cgColor;
            cell.layer.borderWidth = 1.0
            DispatchQueue.main.async(execute: {
                cell.imageThumb.image = UIImage(named: self.dictMedicalImages[indexPath.row])
                cell.lblMedicalName.text = self.dictMedicalNames[indexPath.row]
                cell.lblMedicalName.lineBreakMode = .byWordWrapping
                cell.lblMedicalName.numberOfLines = 0
            })
            return cell
            
        case 6:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCell", for: indexPath) as! CustomNewsCollectionViewCell
            cell.layer.cornerRadius = 4.0
            cell.layer.masksToBounds = true
            cell.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            cell.layer.borderWidth = 1.0
            
            let featuredNew = featuredNews?[indexPath.row]
            DispatchQueue.main.async(execute: {
                if let tmpImageName = featuredNew?.img {
                    cell.imageThumb.contentMode = .scaleToFill
                    let urlBase = tmpImageName.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
                    cell.imageThumb.loadImageUsingUrlString(urlString: urlBase!)
                }
                if let tmpTitle = featuredNew?.title{
                    cell.lblTitleNews.text = tmpTitle
                    cell.lblTitleNews.lineBreakMode = .byWordWrapping
                    cell.lblTitleNews.numberOfLines = 0
                    cell.lblTitleNews.adjustsFontSizeToFitWidth = true
                }
                if let categoryName = featuredNew?.categoryName, let tmpDateTime = featuredNew?.newsModel.datePhan{
                    //get datetime server
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    var date = dateFormatter.date(from: tmpDateTime)
                    if date == nil {
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                        date = dateFormatter.date(from: tmpDateTime)
                    }
                    
                    let dateTime = date?.timeAgoSinceNow()
                    if let currentDateTime = dateTime{
                        cell.lblDepartment.text = "\(categoryName) - \(currentDateTime)"
                        cell.lblDepartment.textColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
                    }
                }
            })
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DoctorCell", for: indexPath) as! CustomDoctorCollectionViewCell
            cell.layer.cornerRadius = 4.0
            cell.layer.masksToBounds = true
            cell.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            cell.layer.borderWidth = 1.0
            return cell
        }
    }
    
    func directQuickVC(_ sender:UITapGestureRecognizer) {
        if sender.view?.tag == 3{
            self.tabBarController?.selectedIndex = 1
        }else if sender.view?.tag == 4{
            let featuredDoctorVC = ListMoreFeaturedDoctorVC(nibName: "ListMoreFeaturedDoctorVC", bundle: nil)
            //featuredDoctorVC.databaseReference = databaseReference
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(featuredDoctorVC, animated: true)
        }else if sender.view?.tag == 6{
            self.tabBarController?.selectedIndex = 3
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection view at row \(collectionView.tag) selected index path \(indexPath)")
        if collectionView.tag == 6{
            let newsDetailVC = StoryboardScene.Main.newsDetailViewController.instantiate()
            
            if let featuredNewsObj = featuredNews?[indexPath.row] {
                let newsCategory = NewsCategory()
                newsCategory.id = featuredNewsObj.categoryID
                newsCategory.name = featuredNewsObj.categoryName
                newsDetailVC.newsCategoryInput = newsCategory
                let model =  featuredNewsObj.newsModel
                newsDetailVC.newsModelInput = model
                self.navigationController?.pushViewController(newsDetailVC, animated: true)
            }
        }else if collectionView.tag == 1{
            switch indexPath.row{
            case 0:
                showListResultVC()
            case 1:
                self.tabBarController?.selectedIndex = 1
            case 2:
                let servicePackageVC = ServicePackageVC(nibName: "ServicePackageVC", bundle: nil)
                self.tabBarController?.tabBar.isHidden = true
                self.navigationController?.pushViewController(servicePackageVC, animated: true)
            case 3:
                /*
                if currentUser == nil{
                    confirmLogin(myView: self)
                }
                let scheduleAppointmentVC = StoryboardScene.Main.scheduleAppointmentVC.instantiate()
                self.tabBarController?.tabBar.isHidden = true
                self.navigationController?.pushViewController(scheduleAppointmentVC, animated: true)
                */
                self.presentScheduleAppointmentVC()
            case 4:
                var optionMenu:UIAlertController? = nil
                if IS_IPAD{
                    optionMenu = UIAlertController(title: nil, message: "Từ điển", preferredStyle: .alert)
                }else{
                    optionMenu = UIAlertController(title: nil, message: "Từ điển", preferredStyle: .actionSheet)
                }
                
                 let action1 = UIAlertAction(title: "Từ điển Xét nghiệm", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    
                    let listDictMedicalVC = ListDictMedicalVC(nibName: "ListDictMedicalVC", bundle: nil)
                    self.tabBarController?.tabBar.isHidden = true
                    self.navigationController?.pushViewController(listDictMedicalVC, animated: true)
                 })
                 let action2 = UIAlertAction(title: "Từ điển Bệnh", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    let dictDiseaseVC = ListDictDiseaseVC(nibName: "ListDictDiseaseVC", bundle: nil)
                    self.tabBarController?.tabBar.isHidden = true
                    self.navigationController?.pushViewController(dictDiseaseVC, animated: true)
                 })
                 let action3 = UIAlertAction(title: "Từ điển Dịch vụ", style: .default, handler: {
                    (alert: UIAlertAction!) -> Void in
                    let dictServiceVC = ListDictServiceVC(nibName: "ListDictServiceVC", bundle: nil)
                    self.tabBarController?.tabBar.isHidden = true
                    dictServiceVC.title = "Từ điển Dịch vụ"
                    self.navigationController?.pushViewController(dictServiceVC, animated: true)
                 })
                 
                 let cancelAction = UIAlertAction(title: "Huỷ", style: .cancel, handler: {
                    (alert: UIAlertAction!) -> Void in
                 })
                 
                 action1.setValue(UIImage(named: "icon_tudien_xetnghiem")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), forKey: "image")
                 action2.setValue(UIImage(named: "icon_tudien_benh")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), forKey: "image")
                 action3.setValue(UIImage(named: "icon_tudien_dichvu")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), forKey: "image")
                 optionMenu?.addAction(action1)
                 optionMenu?.addAction(action2)
                 optionMenu?.addAction(action3)
                 optionMenu?.addAction(cancelAction)
                 
                 // show action sheet
                 optionMenu?.popoverPresentationController?.sourceView = self.view
                 self.present(optionMenu!, animated: true, completion: nil)
            case 5:
                if currentUser != nil {
                    if let user = currentUser {
                        let healthRecordsVC = HealthRecordsVC(nibName: "HealthRecordsVC", bundle: nil)
                        healthRecordsVC.getMedicalProfileByUserID(userID:user.id)
                        self.tabBarController?.tabBar.isHidden = true
                        self.navigationController?.pushViewController(healthRecordsVC, animated: true)
                    }
                }else{
                    confirmLogin(myView: self)
                }
            default:
                showListResultVC()
            }
        }else if collectionView.tag == 2{
            let linkClinicObj = linkClinics?[indexPath.row]
            if let serviceID = linkClinicObj?.serviceID, let serviceName = linkClinicObj?.serviceName{
                self.presentServicePackage(serviceID: serviceID, serviceName: serviceName)
            }
        }
        else if collectionView.tag == 4 {
            let profileDoctorVC = ProfileDoctorVC(nibName: "ProfileDoctorVC", bundle: nil)
            let featuredDoctor = self.featuredDoctors?[indexPath.row]
            profileDoctorVC.featuredDoctor = featuredDoctor!
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(profileDoctorVC, animated: true)
        }else if collectionView.tag == 5{
            if indexPath.row == 0 {
                // Tu dien XN
                let listDictMedicalVC = ListDictMedicalVC(nibName: "ListDictMedicalVC", bundle: nil)
                self.tabBarController?.tabBar.isHidden = true
                self.navigationController?.pushViewController(listDictMedicalVC, animated: true)
            } else if indexPath.row == 1{
                // Tu dien Benh
                let dictDiseaseVC = ListDictDiseaseVC(nibName: "ListDictDiseaseVC", bundle: nil)
                self.tabBarController?.tabBar.isHidden = true
                self.navigationController?.pushViewController(dictDiseaseVC, animated: true)
            } else if indexPath.row == 2{
                // Tu dien DV
                let dictServiceVC = ListDictServiceVC(nibName: "ListDictServiceVC", bundle: nil)
                self.tabBarController?.tabBar.isHidden = true
                dictServiceVC.title = "Từ điển Dịch vụ"
                self.navigationController?.pushViewController(dictServiceVC, animated: true)
            }else{
                //showAlertView(title: "Chức năng này đang được xây dựng", view: self)
                //return
            }
        } else if collectionView.tag == 3{
            let qADetailVC = self.storyboard?.instantiateViewController(withIdentifier: "QADetail") as! QADetailViewController
            self.tabBarController?.tabBar.isHidden = true
            //pass here object
            let answerQuestions = self.answerQuestions?[indexPath.row]
            qADetailVC.questionAnswer = answerQuestions
            qADetailVC.isCheck = true
            self.navigationController?.pushViewController(qADetailVC, animated: true)
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5, animations: {
            self.tabBarController?.tabBar.isHidden = true
        }, completion: {
            finished in
            self.tabBarController?.tabBar.isHidden = false
        })
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5, animations: {
            self.tabBarController?.tabBar.isHidden = false
        }, completion: {
            finished in
            self.tabBarController?.tabBar.isHidden = true
        })
    }
    
    func presentScheduleAppointmentVC(){
        if currentUser == nil{
            confirmLogin(myView: self)
        }
        
        var optionMenu:UIAlertController? = nil
        if IS_IPAD{
            optionMenu = UIAlertController(title: nil, message: "Tuỳ chọn", preferredStyle: .alert)
        }else{
            optionMenu = UIAlertController(title: nil, message: "Tuỳ chọn", preferredStyle: .actionSheet)
        }
        
        let action1 = UIAlertAction(title: "Đặt lịch hẹn khám", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let sAppointmentVC = StoryboardScene.Main.scheduleAppointmentVC.instantiate()
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(sAppointmentVC, animated: true)
        })
        let action2 = UIAlertAction(title: "Đặt lịch với Bác sĩ", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let scheduleAppointmentVC = FormScheduleAppointmentVC(nibName: "FormScheduleAppointmentVC", bundle: nil)
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(scheduleAppointmentVC, animated: true)
        })
        
        let cancelAction = UIAlertAction(title: "Huỷ", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        let image_doctor =  UIImage.resizeImage(image: UIImage(named: "user-circle")!, toWidth: 20)?.maskWithColor(color: UIColor(hexColor: 0x1976D2, alpha: 1.0))
        let image_normal =  UIImage.resizeImage(image: UIImage(named: "user-md")!, toWidth: 20)?.maskWithColor(color: UIColor(hexColor: 0xFF1E17, alpha: 1.0))
        action2.setValue(image_normal?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), forKey: "image")
        action1.setValue(image_doctor?.withRenderingMode(UIImageRenderingMode.alwaysOriginal), forKey: "image")
        
        optionMenu?.addAction(action1)
        optionMenu?.addAction(action2)
        optionMenu?.addAction(cancelAction)
        
        // show action sheet
        optionMenu?.popoverPresentationController?.sourceView = self.view
        self.present(optionMenu!, animated: true, completion: nil)
    }
}

class HalfSizePresentationController : UIPresentationController {
    override var frameOfPresentedViewInContainerView: CGRect {
        return CGRect(x: 0, y: 0, width: 300, height: 300)
    }
}


