//
//  ExampleData.swift
//  ios-swift-collapsible-table-section
//
//  Created by Yong Su on 8/1/17.
//  Copyright © 2017 Yong Su. All rights reserved.
//

import Foundation

//
// MARK: - Section Data Structure
//
public struct ItemInSection {
    var name: String
    
    public init(name: String) {
        self.name = name
    }
}

public struct SectionGroup {
    var name: String
    var items: [ItemInSection]
    var collapsed: Bool
    
    public init(name: String, items: [ItemInSection], collapsed: Bool = true) {
        self.name = name
        self.items = items
        self.collapsed = collapsed
    }
}
