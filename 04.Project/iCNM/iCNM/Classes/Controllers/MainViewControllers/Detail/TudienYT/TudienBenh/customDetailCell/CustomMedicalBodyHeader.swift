//
//  CustomMedicalBodyHeader.swift
//  iCNM
//
//  Created by Mac osx on 8/14/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
protocol CustomMedicalBodyHeaderDelegate {
    func toggleSection(_ header: CustomMedicalBodyHeader, section: Int)
}

class CustomMedicalBodyHeader: UITableViewHeaderFooterView
{
    @IBOutlet weak var lblNameSection: UILabel!
    @IBOutlet weak var lblArrowSection: UILabel!
    var delegate: CustomMedicalBodyHeaderDelegate?
    var section: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(CustomMedicalBodyHeader.tapHeader(_:))))
    }
    
    //
    // Trigger toggle section when tapping on the header
    //
    func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? CustomMedicalBodyHeader else {
            return
        }
        
        delegate?.toggleSection(self, section: cell.section)
    }
    
    func setCollapsed(_ collapsed: Bool) {
        self.lblArrowSection.rotate(collapsed ? 0 : .pi/2)
    }
}
