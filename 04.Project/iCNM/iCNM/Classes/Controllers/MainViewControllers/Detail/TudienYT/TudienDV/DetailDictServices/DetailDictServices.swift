//
//  ListLookUpResultVC.swift
//  iCNM
//
//  Created by Mac osx on 7/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class DetailDictServices: BaseTableViewController {

    @IBOutlet weak var myTableView: UITableView!
    var medicalDictDetails:[MedicalDictionaryDetail]?
    var sectionsData: [SectionGroup]?
    var titleServices:String? = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(DetailDictServices.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        title = titleServices
        myTableView.delegate = self
        myTableView.dataSource = self
        
        myTableView.estimatedRowHeight = 70
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        let nib = UINib(nibName: "DetailMedicalBodyCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        
        let nib_header = UINib(nibName: "CustomMedicalBodyHeader", bundle: nil)
        myTableView.register(nib_header, forHeaderFooterViewReuseIdentifier: "headerCell")
        
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func getDetailDictServicesGroup(specialistServicesObj:SpecialistServicesGroup){
        sectionsData = [SectionGroup]()
        
        self.sectionsData?.append(SectionGroup(name: "", items: [
            ItemInSection(name: specialistServicesObj.name)]))
        
        self.sectionsData?.append(SectionGroup(name: "Ý nghĩa chung", items: [
                ItemInSection(name: specialistServicesObj.meanSocial)]))
            
        self.sectionsData?.append(SectionGroup(name: "Ý nghĩa chuyên sâu", items: [
                    ItemInSection(name: specialistServicesObj.meanSpecial)]))
    }
    
    // MARK: UITableViewDelegate
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerCell") as? CustomMedicalBodyHeader ?? CustomMedicalBodyHeader(reuseIdentifier: "headerCell")
        
        headerView.lblNameSection.text = sectionsData?[section].name
        headerView.lblNameSection.textColor = UIColor.white
        //headerView.setCollapsed((sectionsData?[section].collapsed)!)
        headerView.backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        
        headerView.section = section
       // headerView.delegate = self
        return headerView
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sectionsData!.count
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if section == 0{
            return 0
        }
        return 40
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionsData![section].items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item: ItemInSection = sectionsData![indexPath.section].items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DetailMedicalBodyCell
        if indexPath.section == 0{
            cell.lblDesc.font = UIFont.boldSystemFont(ofSize: 16.0)
        }
        if let name = try! item.name.convertHtmlSymbols(){
            cell.lblDesc.text = name
        }

        cell.lblDesc.lineBreakMode = .byWordWrapping
        cell.lblDesc.numberOfLines = 0
        cell.lblDesc.adjustsFontSizeToFitWidth = true
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension//Choose your custom row height
    }
}


