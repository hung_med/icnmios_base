//
//  AddNewResultVC.swift
//  iCNM
//
//  Created by Thanh Huyen on 4/11/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
class AddNewResultVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var newBackButton:UIBarButtonItem? = nil
    let imagePicker = UIImagePickerController()
    var arrayXquang = [Image]()
    var arraySieuAm = [Image]()
    var arrayXetNghiem = [Image]()
    var arrayNoiSoi = [Image]()
    var imageListUpload = [ImageResult]()
    var listResultCustom = [ResultCustom]()
    var patientResultCustom = [[String: Any]]()
    var addNewResult = AddNewResult()
    var arrayAddNewResult = [AddNewResult]()
    var medicalProfileObj:MedicalProfile?
    var tagButton = 0
    var delButton = 0
    var imgBase64String = ""
    var imgName = ""
    private var currentUploadImageRequest:Request?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Thêm kết quả khám"
        createBackButton()
        
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        registerThongTinCell()
        registerDichVuCell()
        getNumberOfService()
    }
    
    func getNumberOfService() {
        addNewResult.getAllResultType(success: { (result) in
            for data in result {
                self.arrayAddNewResult.append(data)
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }, fail: { (error, response) in
            return
        })
    }
    
    // xib thong tin cell
    func registerThongTinCell() {
        let nib_thongtin = UINib(nibName: "FormThongTinCell", bundle: nil)
        tableView.register(nib_thongtin, forCellReuseIdentifier: "thongtinCell")
    }
    
    // xib up dich vu cell
    func registerDichVuCell() {
        let nib_sieuam = UINib(nibName: "SieuAmCell", bundle: nil)
        tableView.register(nib_sieuam, forCellReuseIdentifier: "sieuamCell")
        
        let nib_xquang = UINib(nibName: "XQuangCell", bundle: nil)
        tableView.register(nib_xquang, forCellReuseIdentifier: "xquangCell")
        
        let nib_xetngiem = UINib(nibName: "XetNgiemCell", bundle: nil)
        tableView.register(nib_xetngiem, forCellReuseIdentifier: "xetngiemCell")
        
        let nib_noisoi = UINib(nibName: "NoiSoiCell", bundle: nil)
        tableView.register(nib_noisoi, forCellReuseIdentifier: "noisoiCell")
    }
    
    // create back button
    func createBackButton() {
        self.navigationItem.hidesBackButton = true
        let button: UIButton = UIButton(type: .system)
        button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
        let backTitle = String.fontAwesomeIcon(name: .angleLeft)
        button.setTitle(backTitle, for: .normal)
        button.addTarget(self, action: #selector(backView), for: UIControlEvents.touchUpInside)
        newBackButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    func backView() {
        self.navigationController?.popViewController(animated: true)
    }
    
    // create resultPatient
    func createResutlPatient(result: String, conclusion: String, id: String, code: String, name: String) -> ResultPatient {
        let resultPatient = ResultPatient()
        resultPatient.resultText = result
        resultPatient.conclusion = conclusion
        resultPatient.resultTypeID = id
        resultPatient.testCode = code
        resultPatient.testName = name
        return resultPatient
    }
    
    @IBAction func btnSaveResult(_ sender: Any) {
        tableView.reloadData()

        let cell = tableView.dequeueReusableCell(withIdentifier: "thongtinCell") as! FormThongTinCell
        let sieuamCell = tableView.dequeueReusableCell(withIdentifier: "sieuamCell") as! SieuAmCell
        let xquangCell = tableView.dequeueReusableCell(withIdentifier: "xquangCell") as! XQuangCell
        let xetngiemCell = tableView.dequeueReusableCell(withIdentifier: "xetngiemCell") as! XetNgiemCell
        let noisoiCell = tableView.dequeueReusableCell(withIdentifier: "noisoiCell") as! NoiSoiCell
        
        if cell.txtLyDoKham.text != "" && cell.txtKetLuan.text != "" && cell.txtDiaDiemKham.text != "" {
            
            let resultCustom = ResultCustom()
            resultCustom.imageResult = imageListUpload
            
            // call function resultPatient and hard assign
            if sieuamCell.txtKetQua.text != "" || sieuamCell.txtKetLuan.text != "" {
                if let result = sieuamCell.txtKetQua.text,
                    let conclution = sieuamCell.txtKetLuan.text {
                    let resultPatient = createResutlPatient(result: result, conclusion: conclution, id: "1", code: "001SA", name: "Siêu Âm")
                    resultCustom.resultPatient = resultPatient
                    listResultCustom.append(resultCustom)
                }
            }
            
            if xquangCell.txtKetQua.text != "" || xquangCell.txtKetLuan.text != "" {
                if let result = xquangCell.txtKetQua.text,
                    let conclution = xquangCell.txtKetLuan.text {
                    let resultPatient = createResutlPatient(result: result, conclusion: conclution, id: "2", code: "001XQ", name: "X quang")
                    resultCustom.resultPatient = resultPatient
                    listResultCustom.append(resultCustom)
                }
            }
            
            if xetngiemCell.txtKetQua.text != "" || xetngiemCell.txtKetLuan.text != "" {
                if let result = xetngiemCell.txtKetQua.text,
                    let conclution = xetngiemCell.txtKetLuan.text {
                    let resultPatient = createResutlPatient(result: result, conclusion: conclution, id: "3", code: "001XN", name: "Xét nghiệm")
                    resultCustom.resultPatient = resultPatient
                    listResultCustom.append(resultCustom)
                }
            }
            
            if noisoiCell.txtKetQua.text != "" || noisoiCell.txtKetLuan.text != "" {
                if let result = noisoiCell.txtKetQua.text,
                    let conclution = noisoiCell.txtKetLuan.text {
                    let resultPatient = createResutlPatient(result: result, conclusion: conclution, id: "4", code: "001NS", name: "Nội Soi")
                    resultCustom.resultPatient = resultPatient
                    listResultCustom.append(resultCustom)
                }
            }

            // assign for patient
            let patient = Patient()
            if let medicalProfile = self.medicalProfileObj {
                patient.Active = medicalProfile.active
                patient.Email = medicalProfile.email
                patient.Gender = medicalProfile.gender
                patient.MedicalProfileID = medicalProfile.medicalProfileID
                patient.PID = medicalProfile.pID
                patient.SidCode = ""
                patient.PatientName = medicalProfile.patientName
                patient.Phone = medicalProfile.mobilePhone
                patient.Note = cell.txtGhiChu.text!
                patient.Place = cell.txtDiaDiemKham.text!
                patient.Conclude = cell.txtKetLuan.text!
                patient.Reason = cell.txtLyDoKham.text!
                // yyyy-MM-dd
                let dateFormater = DateFormatter()
                patient.InTime = dateFormater.string(from: cell.dateNgayKham.currentDate)
            }
            
            let patientResultCustom = PatientResultCustom()
            patientResultCustom.resultCustom = listResultCustom
            patientResultCustom.patient = patient
            
            currentUploadImageRequest = AddNewResult.addPatientResult(parameterDic: patientResultCustom.toJSON(), success: { (responseString) in
                if responseString.contains("Done") {
                    // self.hideProgress(progress)
                } else {
                    let alertViewController = UIAlertController(title: "Thông báo", message: "Có Lỗi xảy ra!", preferredStyle: .alert)
                    let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                    alertViewController.addAction(noAction)
                    self.present(alertViewController, animated: true, completion: nil)
                }
            }, fail: { (error, response) in
                // self.hideProgress(progress)
                //self.handleNetworkError(error: error, responseData: response.data)
            })
        } else {
            let alertViewController = UIAlertController(title: "Thông báo", message: "Bạn cần nhập đầy đủ các trường", preferredStyle: .alert)
            let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
            alertViewController.addAction(noAction)
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
}

extension AddNewResultVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayAddNewResult.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "thongtinCell") as! FormThongTinCell
            return cell
            
        case 1:
            let sieuamCell = tableView.dequeueReusableCell(withIdentifier: "sieuamCell") as! SieuAmCell
            let nib_sieuamcell = UINib(nibName: "UpAnhCell", bundle: nil)
            sieuamCell.collectionView.register(nib_sieuamcell, forCellWithReuseIdentifier: "upAnhCell")
            sieuamCell.collectionView.delegate = self
            sieuamCell.collectionView.dataSource = self
            sieuamCell.collectionView.tag = 1
            if arraySieuAm.count != 0 {
                if tagButton == 1 {
                    sieuamCell.heightCollectionView.constant = 100
                    sieuamCell.collectionView.reloadData()
                }
            } else {
                sieuamCell.heightCollectionView.constant = 0
            }
            sieuamCell.btnTaiThem.tag = indexPath.row
            sieuamCell.btnTaiThem.addTarget(self, action: #selector(uploadImage), for: .touchUpInside)
            return sieuamCell
        
        case 2:
            let xquangCell = tableView.dequeueReusableCell(withIdentifier: "xquangCell") as! XQuangCell
            let nib_xquangcell = UINib(nibName: "UpAnhCell", bundle: nil)
            xquangCell.collectionView.register(nib_xquangcell, forCellWithReuseIdentifier: "upAnhCell")
            xquangCell.collectionView.delegate = self
            xquangCell.collectionView.dataSource = self
            xquangCell.collectionView.tag = 2
            if arrayXquang.count != 0 {
                if tagButton == 2 {
                    xquangCell.heightCollectionView.constant = 100
                    xquangCell.collectionView.reloadData()
                }
            } else {
                xquangCell.heightCollectionView.constant = 0
            }
            xquangCell.btnTaiThem.tag = indexPath.row
            xquangCell.btnTaiThem.addTarget(self, action: #selector(uploadImage), for: .touchUpInside)
            return xquangCell
        
        case 3:
            let xetngiemCell = tableView.dequeueReusableCell(withIdentifier: "xetngiemCell") as! XetNgiemCell
            let nib_xetngiemCell = UINib(nibName: "UpAnhCell", bundle: nil)
            xetngiemCell.collectionView.register(nib_xetngiemCell, forCellWithReuseIdentifier: "upAnhCell")
            xetngiemCell.collectionView.delegate = self
            xetngiemCell.collectionView.dataSource = self
            xetngiemCell.collectionView.tag = 3
            if arrayXetNghiem.count != 0 {
                if tagButton == 3 {
                    xetngiemCell.heightCollectionView.constant = 100
                    xetngiemCell.collectionView.reloadData()
                }
            } else {
                xetngiemCell.heightCollectionView.constant = 0
            }
            
            xetngiemCell.btnTaiThem.tag = indexPath.row
            xetngiemCell.btnTaiThem.addTarget(self, action: #selector(uploadImage), for: .touchUpInside)
            return xetngiemCell
            
        case 4:
            let noisoiCell = tableView.dequeueReusableCell(withIdentifier: "noisoiCell") as! NoiSoiCell
            let nib_noisoiCell = UINib(nibName: "UpAnhCell", bundle: nil)
            noisoiCell.collectionView.register(nib_noisoiCell, forCellWithReuseIdentifier: "upAnhCell")
            noisoiCell.collectionView.delegate = self
            noisoiCell.collectionView.dataSource = self
            noisoiCell.collectionView.tag = 4
            if arrayNoiSoi.count != 0 {
                if tagButton == 4 {
                    noisoiCell.heightCollectionView.constant = 100
                    noisoiCell.collectionView.reloadData()
                }
            } else {
                noisoiCell.heightCollectionView.constant = 0
            }
            
            noisoiCell.btnTaiThem.tag = indexPath.row
            noisoiCell.btnTaiThem.addTarget(self, action: #selector(uploadImage), for: .touchUpInside)
            return noisoiCell
            
        default:
            let cell = UITableViewCell()
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 360
            
        case 1:
            if arraySieuAm.count != 0 {
                return 320
            } else {
                return 220
            }

        case 2:
            if arrayXquang.count != 0 {
                return 320
            } else {
                return 220
            }

        case 3:
            if arrayXetNghiem.count != 0 {
                return 320
            } else {
                return 220
            }

        case 4:
            if arrayNoiSoi.count != 0 {
                return 320
            } else {
                return 220
            }
        default:
            return 0
        }
    }
    
    func uploadImage(_ sender: UIButton) {
        self.imagePicker.delegate = self
        tagButton = sender.tag
        let alert = UIAlertController(title: "Chọn ảnh", message: nil, preferredStyle: .actionSheet)
        let action1 = UIAlertAction(title: "Chụp ảnh mới", style: .default) { (action1) in
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let action2 = UIAlertAction(title: "Chọn ảnh từ thư viện", style: .default) { (action2) in
            self.imagePicker.sourceType = .photoLibrary
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let action3 = UIAlertAction(title: "Huỷ", style: .cancel, handler: nil)
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        self.present(alert, animated: true, completion: nil)
    }
}

extension AddNewResultVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(tagButton)
        if tagButton == 1 {
            return arraySieuAm.count
        } else if tagButton == 2 {
            return arrayXquang.count
        } else if tagButton == 3 {
            return arrayXetNghiem.count
        } else if tagButton == 4 {
            return arrayNoiSoi.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if tagButton == 1 {
            let sieuamCell = collectionView.dequeueReusableCell(withReuseIdentifier: "upAnhCell", for: indexPath) as! UpAnhCell
            sieuamCell.imgAnh.image = arraySieuAm[indexPath.row]
            sieuamCell.btnDelete.tag = 1
            sieuamCell.btnDelete.addTarget(self, action: #selector(deleteImage), for: .touchUpInside)
            delButton = indexPath.row
            return sieuamCell
        } else if tagButton == 2 {
            let xquangCell = collectionView.dequeueReusableCell(withReuseIdentifier: "upAnhCell", for: indexPath) as! UpAnhCell
            xquangCell.imgAnh.image = arrayXquang[indexPath.row]
            xquangCell.btnDelete.tag = 2
            xquangCell.btnDelete.addTarget(self, action: #selector(deleteImage), for: .touchUpInside)
            delButton = indexPath.row
            return xquangCell
        } else if tagButton == 3 {
            let xetngiemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "upAnhCell", for: indexPath) as! UpAnhCell
            xetngiemCell.imgAnh.image = arrayXetNghiem[indexPath.row]
            xetngiemCell.btnDelete.tag = 3
            xetngiemCell.btnDelete.addTarget(self, action: #selector(deleteImage), for: .touchUpInside)
            delButton = indexPath.row
            return xetngiemCell
        } else if tagButton == 4 {
            let noisoiCell = collectionView.dequeueReusableCell(withReuseIdentifier: "upAnhCell", for: indexPath) as! UpAnhCell
            noisoiCell.imgAnh.image = arrayNoiSoi[indexPath.row]
            print("indexpath \(indexPath.row)")
            noisoiCell.btnDelete.tag = 4
            noisoiCell.btnDelete.addTarget(self, action: #selector(deleteImage), for: .touchUpInside)
            delButton = indexPath.row
            return noisoiCell
        } else {
            return UICollectionViewCell()
        }
    }
    
    func deleteImage(_ sender: UIButton) {
        if sender.tag == 1 {
            arraySieuAm.remove(at: delButton)
        } else if sender.tag == 2 {
            arrayXquang.remove(at: delButton)
        } else if sender.tag == 3 {
            arrayXetNghiem.remove(at: delButton)
        } else if sender.tag == 4 {
            arrayNoiSoi.remove(at: delButton)
        }
        self.tableView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
    }
}

extension AddNewResultVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if let image_tmp = resizeImage(image: image , toWidth: 512), let imageData = UIImageJPEGRepresentation(image_tmp,1.0)  {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyyMMdd_hhmmss"
                let imageName = "patient_" + dateFormatter.string(from: Date())
                
                let imageResult = ImageResult()
                imageResult.name = imageName
                imageResult.base64String = imageData.base64EncodedString()
                self.imageListUpload.append(imageResult)
            }
            
            if tagButton == 1 {
                arraySieuAm.append(image)
            }
            
            else if tagButton == 2 {
                arrayXquang.append(image)
            }
            
            else if tagButton == 3 {
                arrayXetNghiem.append(image)
            }
            
            else if tagButton == 4 {
                arrayNoiSoi.append(image)
            }
        }
        self.tableView.reloadData()
        dismiss(animated: true, completion: nil)
    }
    
    func resizeImage(image:UIImage, toWidth:CGFloat)->UIImage? {
        let ratio = toWidth / image.size.width
        let newHeight = image.size.height * ratio
        UIGraphicsBeginImageContext(CGSize(width: toWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: toWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
}
