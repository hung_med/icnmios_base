//
//  FormThongTinCell.swift
//  iCNM
//
//  Created by Thanh Huyen on 4/11/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class FormThongTinCell: UITableViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var txtLyDoKham: FWFloatingLabelTextField!
    @IBOutlet weak var txtDiaDiemKham: FWFloatingLabelTextField!
    @IBOutlet weak var txtKetLuan: FWFloatingLabelTextField!
    @IBOutlet weak var txtGhiChu: FWFloatingLabelTextField!
    @IBOutlet weak var viewThongTin: UIView!
    @IBOutlet weak var dateNgayKham: FWDatePicker!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewThongTin.layer.cornerRadius = 8
        viewThongTin.layer.masksToBounds = true
        txtLyDoKham.delegate = self
        txtDiaDiemKham.delegate = self
        txtKetLuan.delegate = self
        txtGhiChu.delegate = self
        
        dateNgayKham.maximumDate = Date()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == txtLyDoKham {
            txtLyDoKham.resignFirstResponder()
        }
        return true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
