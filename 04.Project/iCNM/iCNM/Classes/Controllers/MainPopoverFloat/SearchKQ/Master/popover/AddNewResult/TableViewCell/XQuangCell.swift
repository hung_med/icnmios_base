//
//  XQuangCell.swift
//  iCNM
//
//  Created by Thanh Huyen on 4/16/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import UIKit

class XQuangCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var txtKetQua: FWFloatingLabelTextField!
    @IBOutlet weak var txtKetLuan: FWFloatingLabelTextField!
    @IBOutlet weak var btnTaiThem: UIButton!
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!
    @IBOutlet weak var viewXQuang: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewXQuang.layer.cornerRadius = 8
        viewXQuang.layer.masksToBounds = true
        
        btnTaiThem.layer.cornerRadius = 8
        btnTaiThem.layer.masksToBounds = true
        btnTaiThem.layer.borderWidth = 1
        btnTaiThem.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
