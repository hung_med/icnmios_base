//
//  DetailLookUpResultVC.swift
//  iCNM
//
//  Created by Mac osx on 7/14/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import PhotoSlider
import FontAwesome_swift

protocol DetailLookUpResultVCDelegate {
    func gobackLookupVC()
}
class DetailLookUpResultVC: BaseViewControllerNoSearchBar, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
   
    var detailLookUpResults: [DetailLookUpResultBySID]?
    var detailLookUpResults2: [DetailLookUpResultBySID]?
    
    var filteredNames:[DetailLookUpResultBySID]?
    var delegate:DetailLookUpResultVCDelegate?
    @IBOutlet var myTableView: UITableView!
    @IBOutlet var btnSupport: UIButton!
    @IBOutlet var bottomView: UIView!
    var headerTitle = ["Kết quả xét nghiệm", "Kết quả hình ảnh"]
    var listImageURL:[Int: [String]] = [:]
    var listTestName:[String]? = nil
    var lastY: CGFloat = 0.0
    var _pID = ""
    var storedOffsets = [Int: CGFloat]()
    var currentOffset_Y: CGFloat = 0.0
    var searchBarButton:UIBarButtonItem? = nil
    var newBackButton:UIBarButtonItem? = nil
    var isSearch:Bool? = false
    var titleNav:String? = nil
    
    var searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 250, height: 18))
    var searchOn : UIBarButtonItem!
    var searchOff : UIBarButtonItem!
  
    let searchController = UISearchController(searchResultsController: nil)
    override func viewDidLoad() {
        super.viewDidLoad()
        self.definesPresentationContext = true
        self.tabBarController?.tabBar.isHidden = true
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.estimatedRowHeight = 70
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        let nib = UINib(nibName: "DetailLookUpCustomCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
       
        myTableView.register(UINib(nibName: "DetailLookUpCustomHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "Cell")
        myTableView.bounces = false
        self.navigationItem.hidesBackButton = true
        self.listTestName = [String]()
        self.listImageURL = [Int: [String]]()
        
        // Setup the Search Controller
        if #available(iOS 11.0, *) {
            self.navigationItem.setHidesBackButton(true, animated: false)
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(DetailLookUpResultVC.back(sender:)), for: UIControlEvents.touchUpInside)
            newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
        }else{
            let button: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
            button.setImage(UIImage(named: "angle-left"), for: UIControlState.normal)
            button.addTarget(self, action: #selector(DetailLookUpResultVC.back(sender:)), for: UIControlEvents.touchUpInside)
            newBackButton = UIBarButtonItem(customView: button)
        }
        
        self.navigationItem.leftBarButtonItem = newBackButton
        
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        
        // create right bar button item
        searchOn = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(isSearchOn))
        searchOff = UIBarButtonItem(title: "Hủy", style: .plain, target: self, action: #selector(isSearchOff))
        self.navigationItem.rightBarButtonItems = [self.searchOn]
        
        // create search bar
        searchBar.placeholder = "Tìm kiếm tên xét nghiệm..."
        searchBar.tintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        searchBar.isHidden = true
        searchBar.delegate = self
        
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
        self.delegate?.gobackLookupVC()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if searchBar.text != "" {
            isSearchOn()
        }

        self.btnSupport.titleLabel?.font = UIFont.fontAwesome(ofSize: 18.0)
        let title = String.fontAwesomeIcon(name: .phoneSquare) + "  Bạn muốn được tư vấn?"
        self.btnSupport.setTitle(title, for: .normal)
        self.btnSupport.addTarget(self, action: #selector(centerSupportHandler), for: .touchUpInside)
    }
    
    func centerSupportHandler(sender: UIButton) {
        Constant.HOTLINE.makeCallPhone()
    }
        
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if searchBar.text != "" {
            isSearchOn()
        }
    }
    
    func isSearchOn() {
        self.navigationItem.setRightBarButtonItems([self.searchOff], animated: false)
        self.navigationItem.titleView = searchBar
        searchBar.isHidden = false
        searchBar.becomeFirstResponder()
    }
    
    func isSearchOff() {
        searchBar.text = nil
        self.navigationItem.setRightBarButtonItems([self.searchOn], animated: false)
        self.navigationItem.titleView = nil
        if searchBar.text == "" {
            isSearch = false
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            isSearch = false
            view.endEditing(true)
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
        else {
            isSearch = true
            filterContentForSearchText(searchText)
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
    }
    
    func getListLookUpResultBySID(sid:String, pID:String, organizeID:String){
        let progress = self.showProgress()
        _pID = pID
        self.detailLookUpResults = [DetailLookUpResultBySID]()
        self.detailLookUpResults2 = [DetailLookUpResultBySID]()
        DetailLookUpResultBySID.getListDetailLookUpResultBySID(sID: sid, organizeID: organizeID, success: { (data) in
            if data.count > 0 {
                for i in 0..<data.count{
                    let detailLookUpResults = data[i]
                    let imgUrl = detailLookUpResults.imgUrl
                    let testName = detailLookUpResults.testName
                    if (imgUrl == "" || imgUrl == "null") && testName != ""{
                        self.detailLookUpResults?.append(detailLookUpResults)
                    }else{
                        self.detailLookUpResults2?.append(detailLookUpResults)
                    }
                }

                self.listImageURL = self.resultListImageURL(lookUpObj: self.detailLookUpResults2!)
                
                self.myTableView.reloadData()
                self.hideProgress(progress)
            } else {
                //self?.tableView.es_noticeNoMoreData()
                self.hideProgress(progress)
                let lblWarning = createUILabel()
                self.myTableView.addSubview(lblWarning)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func getListLookUpResultByPhone(phone:String){
        let progress = self.showProgress()
        _pID = ""
        self.detailLookUpResults = [DetailLookUpResultBySID]()
        self.detailLookUpResults2 = [DetailLookUpResultBySID]()
        DetailLookUpResultBySID.getListDetailLookUpResultByPhone(phone: phone, success: { (data) in
            if data.count > 0 {
                for i in 0..<data.count{
                    let detailLookUpResults = data[i]
                    let imgUrl = detailLookUpResults.imgUrl
                    let testName = detailLookUpResults.testName
                    if (imgUrl == "" || imgUrl == "null") && testName != ""{
                        self.detailLookUpResults?.append(detailLookUpResults)
                    }else{
                        self.detailLookUpResults2?.append(detailLookUpResults)
                    }
                }
                
                self.listImageURL = self.resultListImageURL(lookUpObj: self.detailLookUpResults2!)
                
                self.myTableView.reloadData()
                self.hideProgress(progress)
            } else {
                self.hideProgress(progress)
                let lblWarning = createUILabel()
                self.myTableView.addSubview(lblWarning)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func resultListImageURL(lookUpObj:[DetailLookUpResultBySID])->[Int: [String]]{
        var result = [Int: [String]]()
        for i in 0..<Int((lookUpObj.count)){
            let urlString = lookUpObj[i].imgUrl.components(separatedBy: ";")
            for url in urlString {
                if !url.isEmpty || url != ""{
                    let url = "\(Constant.baseURLResult)\(url)"
                    if result[i] == nil {
                        result[i] = [String]()
                    }
                    result[i]?.append(url)
                }else{
                    result[i]?.append("")
                }
            }
        }
        return result
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       // let frame = CGRect(x: 0, y: 0, width: myTableView.frame.size.width, height: myTableView.frame.size.height)
       // myTableView.frame = frame
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchBar.text != "" {
            if self.filteredNames!.count == 0{
                return 0
            }
            return 1
        }else{
           return headerTitle.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "Cell") as! DetailLookUpCustomHeader
        
        if searchBar.text != "" {
            headerView.lblHeaderMain.text = headerTitle[0]
        }else{
            headerView.lblHeaderMain.text = headerTitle[section]
        }
        
        headerView.lblHeaderTestName.isHidden = false
        headerView.lblHeaderResultTest.isHidden = false
        headerView.lblHeaderNormalRange.isHidden = false
        headerView.lineView1.isHidden = false
        headerView.lineView2.isHidden = false
        headerView.lineView3.isHidden = false
        
        headerView.lblHeaderTestName.lineBreakMode = .byWordWrapping
        headerView.lblHeaderTestName.numberOfLines = 0
        headerView.lblHeaderTestName.adjustsFontSizeToFitWidth = true
        headerView.lblHeaderTestName.sizeToFit()
        
        
        if section == 0{
            headerView.lblHeaderTestName.text = "Tên xét nghiệm"
            headerView.contentView.backgroundColor = UIColor.groupTableViewBackground
            headerView.lblHeaderTestName.textColor = UIColor.black
            headerView.lblHeaderResultTest.text = "Kết quả"
            headerView.lblHeaderNormalRange.text = "Khoảng bình thường"
            headerView.lblHeaderNormalRange.lineBreakMode = .byWordWrapping
            headerView.lblHeaderNormalRange.numberOfLines = 0
            headerView.lblHeaderNormalRange.adjustsFontSizeToFitWidth = true
            headerView.lblHeaderNormalRange.sizeToFit()
            return headerView
        } else {
            headerView.contentView.backgroundColor = UIColor.groupTableViewBackground
            //headerView.lblHeaderTestName.backgroundColor = UIColor.white
            headerView.lblHeaderTestName.text = ""
            headerView.lblHeaderTestName.textColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
            headerView.lblHeaderResultTest.isHidden = true
            headerView.lblHeaderNormalRange.isHidden = true
            headerView.lineView1.isHidden = true
            headerView.lineView2.isHidden = true
            headerView.lineView3.isHidden = true
            return headerView
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 86
        }else{
            return 40
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchBar.text != "" {
            return (self.filteredNames?.count)!
        }else{
            if section == 0{
                return detailLookUpResults!.count
            }else{
                return detailLookUpResults2!.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let detailLookUpObject: DetailLookUpResultBySID?
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DetailLookUpCustomCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            if indexPath.row % 2 == 0 {
                cell.backgroundColor = UIColor.white
            } else{
                cell.backgroundColor = UIColor(hexColor: 0xE1F5FE, alpha: 1.0)
            }
            
            if searchBar.text != "" {
                detailLookUpObject = (self.filteredNames?[indexPath.row])!
            }else{
                detailLookUpObject = self.detailLookUpResults?[indexPath.row]
            }
            
            //update title navigation controler
            if let sID = detailLookUpObject?.sID{
                title = "\(sID)"
                titleNav = title
            }
            if let testNameTmp = detailLookUpObject?.testName{
                cell.lblTestName.text = testNameTmp
                cell.lblTestName.lineBreakMode = .byWordWrapping
                cell.lblTestName.numberOfLines = 0
                cell.lblTestName.adjustsFontSizeToFitWidth = true
                cell.lblTestName.sizeToFit()
                cell.lblTestName.tag = indexPath.row
                let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
                cell.lblTestName.isUserInteractionEnabled=true
                cell.lblTestName.addGestureRecognizer(tapGesture)
            }
            if let normalRangeTmp = detailLookUpObject?.normalRange{
                if let unit = detailLookUpObject?.unit.trim(){
                    if !unit.isEmpty{
                        cell.lblNormalRange.text = normalRangeTmp + "\n" + "(\(unit))"
                    }else{
                        cell.lblNormalRange.text = normalRangeTmp
                    }
                }else{
                    cell.lblNormalRange.text = normalRangeTmp
                }
                
                cell.lblNormalRange.lineBreakMode = .byWordWrapping
                cell.lblNormalRange.numberOfLines = 0
            }
            
            if indexPath.row == 0{
                cell.lblResultTest.textAlignment = NSTextAlignment.right
            }
            
            if let testHead = detailLookUpObject?.testHead{
                if testHead == 1{
                    cell.lblTestName.font = UIFont.boldSystemFont(ofSize: 14.0)
                }else{
                    cell.lblTestName.font = UIFont.systemFont(ofSize: 14.0)
                }
            }
            
            if let conclusion = detailLookUpObject?.conclusion{
                if conclusion.lowercased().trimmingCharacters(in: .whitespaces) == "tăng" || conclusion.lowercased().trimmingCharacters(in: .whitespaces) == "cao" || conclusion.lowercased().trimmingCharacters(in: .whitespaces) == "dương tính" || conclusion.lowercased().trimmingCharacters(in: .whitespaces) == "to"{
                    cell.lblResultTest.textColor = UIColor.red
                    cell.lblResultTest.font = UIFont.boldSystemFont(ofSize: 14.0)
                    cell.lblResultTest.textAlignment = NSTextAlignment.right
                }else if conclusion.lowercased().trimmingCharacters(in: .whitespaces) == "giảm" || conclusion.lowercased().trimmingCharacters(in: .whitespaces) == "thấp" || conclusion.lowercased().trimmingCharacters(in: .whitespaces) == "âm tính" || conclusion.lowercased().trimmingCharacters(in: .whitespaces) == "nhỏ"{
                    cell.lblResultTest.textColor = UIColor.blue
                    cell.lblResultTest.textAlignment = NSTextAlignment.left
                    cell.lblResultTest.font = UIFont.boldSystemFont(ofSize: 14.0)
                }else if conclusion == "null" || conclusion.isEmpty{
                    cell.lblResultTest.textColor = UIColor.black
                    cell.lblResultTest.textAlignment = NSTextAlignment.center
                    cell.lblResultTest.font = UIFont.systemFont(ofSize: 14.0)
                }
            }
        
                if let resultTextTmp = detailLookUpObject?.resultText{
                    cell.lblResultTest.text = resultTextTmp.htmlDecode()
                    cell.lblResultTest.lineBreakMode = .byWordWrapping
                    cell.lblResultTest.numberOfLines = 0
                    if !resultTextTmp.isEmpty{
                        var hightLimitValue = 0.0
                        if let hightLimit = detailLookUpObject?.hightLimit
                        {
                            hightLimitValue = Double(hightLimit)
                        }
                        
                        var lowLimitValue = 0.0
                        if let lowLimit = detailLookUpObject?.lowLimit
                        {
                            lowLimitValue = Double(lowLimit)
                        }
                        
                        if lowLimitValue == -1 || (lowLimitValue == 0 && hightLimitValue == 0) || _pID == "" || _pID.trimmingCharacters(in: .whitespaces).isEmpty {
                            cell.imgChart.image = UIImage(named: "")
                            cell.constraint.constant = -24
                        }else{
                            cell.imgChart.image = UIImage(named: "bar-chart")
                            cell.constraint.constant = 8
                        }
                        
                        let decimalCharacters = CharacterSet.decimalDigits
                        let decimalRange = resultTextTmp.rangeOfCharacter(from: decimalCharacters)
                        if decimalRange == nil {
                            cell.imgChart.image = UIImage(named: "")
                            cell.constraint.constant = -24
                        }
                        
                    }else{
                        cell.imgChart.image = UIImage(named: "")
                        cell.constraint.constant = -24
                    }
                }
            return cell
            
        }else{
            let nib = UINib(nibName: "CustomPhotoCell", bundle: nil)
            myTableView.register(nib, forCellReuseIdentifier: "Cell2")
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath) as! CustomPhotoCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
            cell.myCollection.register(UINib(nibName: "CustomPhotoCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Cell")
            cell.myCollection.viewWithTag(indexPath.section)
            //cell.selectionStyle = UITableViewCellSelectionStyle.blue
            cell.backgroundColor = UIColor.white
            if searchController.isActive && searchController.searchBar.text != "" {
                detailLookUpObject = (self.filteredNames?[indexPath.row])!
            }else{
                detailLookUpObject = self.detailLookUpResults2?[indexPath.row]
            }
           // detailLookUpObject = self.detailLookUpResults2?[indexPath.row]
            if let sID = detailLookUpObject?.sID{
                title = "\(sID)"
                titleNav = title
            }
            if let existImageURL = detailLookUpObject?.imgUrl{
                if existImageURL == "" || existImageURL == "null"{
                    cell.myCollection.isHidden = true
                    cell.lblContent.isHidden = true
                    cell.lblDoctorName.isHidden = true
                    cell.lblDateTime.isHidden = true
                }else{
                    cell.myCollection.isHidden = false
                    cell.lblContent.isHidden = false
                    cell.lblDoctorName.isHidden = false
                    cell.lblDateTime.isHidden = false
                }
                
                if let testName = detailLookUpObject?.testName{
                    cell.lblTestName.text = testName
                    cell.lblTestName.lineBreakMode = .byWordWrapping
                    cell.lblTestName.numberOfLines = 0
                    cell.lblTestName.adjustsFontSizeToFitWidth = true
                    cell.lblTestName.sizeToFit()
                    cell.lblTestName.tag = indexPath.row
                    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
                    cell.lblTestName.isUserInteractionEnabled=true
                    cell.lblTestName.addGestureRecognizer(tapGesture)
                }

                if let conclusion = detailLookUpObject?.conclusion, let content = detailLookUpObject?.resultText{
                    cell.lblContent.text = ("Kết quả\n\n\(content)\n" + "\n\nKết luận\n\n\(conclusion)\n")
                    cell.lblContent.lineBreakMode = .byWordWrapping
                    cell.lblContent.numberOfLines = 0
                    cell.lblContent.adjustsFontSizeToFitWidth = true
                    cell.lblContent.sizeToFit()
                }
                
                if let doctorName = detailLookUpObject?.doctorName{
                    cell.lblDoctorName.text = doctorName
                }
                
                if let sid = detailLookUpObject?.sID{
                    let splitStr = sid.components(separatedBy: "-")[0]
                    if splitStr.count > 5{
                        //200917
                        let day = splitStr.prefix(2)
                        let month = splitStr.suffix(4).prefix(2)
                        let year = splitStr.suffix(4).suffix(2)
                        let formatDate = "Hà nội: Ngày \(day) tháng \(month) năm 20\(year)"
                        cell.lblDateTime.text = formatDate
                    }else{
                        
                    }
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let graphDataVC = GraphDataViewController(nibName: "GraphDataViewController", bundle: nil)
        if indexPath.section == 0{
            if _pID == "null" || _pID == "" || _pID.trimmingCharacters(in: .whitespaces).isEmpty{
                return
            }
            
            let detailLookUpObject: DetailLookUpResultBySID?
            if searchController.isActive && searchController.searchBar.text != ""{
                detailLookUpObject = (self.filteredNames?[indexPath.row])!
                isSearch = true
            }else{
                isSearch = false
                detailLookUpObject = self.detailLookUpResults?[indexPath.row]
            }
            
            if let resultTextTmp = detailLookUpObject?.resultText{
                if !resultTextTmp.isEmpty{
                    var hightLimitValue = 0.0
                    if let hightLimit = detailLookUpObject?.hightLimit
                    {
                        hightLimitValue = Double(hightLimit)
                    }
                    
                    var lowLimitValue = 0.0
                    if let lowLimit = detailLookUpObject?.lowLimit
                    {
                        lowLimitValue = Double(lowLimit)
                    }
                    
                    if lowLimitValue == -1 || (lowLimitValue == 0 && hightLimitValue == 0) || _pID == "" || _pID.trimmingCharacters(in: .whitespaces).isEmpty {
                        return
                    }
                    
                    if let testCode = detailLookUpObject?.testCode{
                        graphDataVC.getDataChartDetailLookUp(pid: _pID, testCode: testCode)
                    }
                    
                    self.navigationItem.titleView = nil
                    graphDataVC.currentTitle = detailLookUpObject?.testName
                    self.navigationController?.pushViewController(graphDataVC, animated: true)
                }else{
                    return
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if self.detailLookUpResults?.count == 0 && self.detailLookUpResults2?.count == 0{
            return 0
        }else{
            if self.detailLookUpResults?.count == 0{
                return UITableViewAutomaticDimension
            }
            if indexPath.section == 0{
                let detailLookUpObject = self.detailLookUpResults?[indexPath.row]
                if let existImageURL = detailLookUpObject?.imgUrl{
                    if existImageURL == "null" || existImageURL == ""{
                        return UITableViewAutomaticDimension
                    }else{
                        return 0
                    }
                }
                
            } else{
                let detailLookUpObject = self.detailLookUpResults2?[indexPath.row]
                if let existImageURL = detailLookUpObject?.imgUrl{
                    if existImageURL == "null" || existImageURL == ""{
                        return 0
                    }else{
                        return UITableViewAutomaticDimension
                    }
                }
            }
        }
        
    
       return UITableViewAutomaticDimension
        //Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            guard let tableViewCell = cell as? CustomPhotoCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        }
    }
    
    func handleTap(sender: UITapGestureRecognizer) {
        let tag = (sender.view as? UILabel)?.tag
        var detailLookUpObject:DetailLookUpResultBySID? = nil
        if searchBar.text != "" {
            detailLookUpObject = (self.filteredNames?[tag!])
        }else{
            detailLookUpObject = self.detailLookUpResults?[tag!]
        }
        if (detailLookUpObject?.testCode) != nil{
            let testCode = detailLookUpObject?.testCode.components(separatedBy: " ")[0]
            self.getUserFollowAction(testCode: testCode!)
        }
    }
    
    func getUserFollowAction(testCode:String){
        let progress = self.showProgress()
        Meanning().getMeaningByCode(testCode:testCode, success: { (data) in
            if data != nil{
                self.hideProgress(progress)
                let descLookUpResultVC = DescLookUpResultVC(nibName: "DescLookUpResultVC", bundle: nil)
                if let testName = try! data?.testName.convertHtmlSymbols(){
                    descLookUpResultVC.testName = testName
                }
                if let meaning1 = try! data?.meaning1.convertHtmlSymbols(){
                    descLookUpResultVC.meaning1 = meaning1
                }
                if let meaning2 = try! data?.meaning2.convertHtmlSymbols(){
                    descLookUpResultVC.meaning2 = meaning2
                }
                descLookUpResultVC.modalPresentationStyle = .custom
                descLookUpResultVC.view.frame = CGRect(x: screenSizeWidth/2-(screenSizeWidth-60)/2, y: screenSizeHeight/2-(screenSizeHeight - 300)/2, width: screenSizeWidth-60, height: screenSizeHeight - 300)
                self.present(descLookUpResultVC, animated: true, completion: nil)
            } else {
                self.hideProgress(progress)
                let descLookUpResultVC = DescLookUpResultVC(nibName: "DescLookUpResultVC", bundle: nil)
                descLookUpResultVC.testName = "Đang cập nhật dữ liệu"
                descLookUpResultVC.meaning1 = "Đang cập nhật dữ liệu"
                descLookUpResultVC.meaning2 = "Đang cập nhật dữ liệu"
                descLookUpResultVC.modalPresentationStyle = .custom
                descLookUpResultVC.view.frame = CGRect(x: screenSizeWidth/2-(screenSizeWidth-60)/2, y: screenSizeHeight/2-(screenSizeHeight - 300)/2, width: screenSizeWidth-60, height: screenSizeHeight - 300)
                self.present(descLookUpResultVC, animated: true, completion: nil)
            }
            
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            guard let tableViewCell = cell as? CustomPhotoCell else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentY = scrollView.contentOffset.y
        currentOffset_Y = currentY
        let currentBottomY = scrollView.frame.size.height + currentY
        if currentY > lastY {
            //"scrolling down"
            myTableView.bounces = true
        } else {
            //"scrolling up"
            // Check that we are not in bottom bounce
            if currentBottomY < scrollView.contentSize.height + scrollView.contentInset.bottom {
                myTableView.bounces = true
            }
        }
        lastY = scrollView.contentOffset.y
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredNames = detailLookUpResults?.filter({( lookUpResult : DetailLookUpResultBySID) -> Bool in
            let testName = lookUpResult.testName.folding(options: .diacriticInsensitive, locale: .autoupdatingCurrent)
            let keySearch = searchText.folding(options: .diacriticInsensitive, locale: .autoupdatingCurrent)
            return testName.lowercased().contains(keySearch.lowercased())
        })
        myTableView.reloadData()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5, animations: {
            self.bottomView.alpha = 1
        }, completion: {
            finished in
            self.bottomView.isHidden = false
        })
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
        UIView.animate(withDuration: 0.5, animations: {
            self.bottomView.alpha = 0
        }, completion: {
            finished in
            self.bottomView.isHidden = true
        })
    }
}

extension DetailLookUpResultVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let tag = collectionView.tag
        if let result = self.listImageURL[tag]{
            return result.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 220, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CustomPhotoCollectionViewCell
        let tag = collectionView.tag
        let listURL = listImageURL[tag]
        DispatchQueue.main.async(execute: {
            let urlString = listURL![indexPath.row]
            cell.layer.cornerRadius = 4.0
            cell.layer.masksToBounds = true
            cell.layer.borderColor = UIColor.groupTableViewBackground.cgColor
            cell.layer.borderWidth = 2.0
            
            cell.imageThumb.contentMode = .scaleAspectFill
            let url = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            cell.imageThumb.loadImageUsingUrlString(urlString: url!)
        })
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if listImageURL.count == 0{
            return
        }
        let tag = collectionView.tag
        if tag > listImageURL.count{
            return
        }
        let urlString = listImageURL[tag]![indexPath.row]
//        let imageView = CustomImageView()
        let url = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
//        imageView.loadImageUsingUrlString(urlString: url!)
        if let imgUrl = URL(string: url!) {
            //                imageView.loadImageUsingUrlString(urlString: url! + ".jpg")
            let photoSlider = PhotoSlider.ViewController(imageURLs: [imgUrl])
            photoSlider.modalPresentationStyle = .overCurrentContext
            photoSlider.modalTransitionStyle = .crossDissolve
            present(photoSlider, animated: true, completion: nil)
        }
    }
    
    // Use to back from full mode
    func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let view = sender.view!
        view.removeFromSuperview()
        self.navigationController?.navigationBar.layer.zPosition = 0
    }
}
