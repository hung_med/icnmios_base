//
//  CustomPhotoCell.swift
//  iCNM
//
//  Created by Mac osx on 7/18/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class CustomPhotoCell: UITableViewCell {

    @IBOutlet weak var myCollection: UICollectionView!
    @IBOutlet weak var lblTestName: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblDateTime: UILabel!
    @IBOutlet weak var lblDoctorName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.separatorInset = .zero
        self.preservesSuperviewLayoutMargins = false
        self.layoutMargins = .zero
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension CustomPhotoCell {
    
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        myCollection.delegate = dataSourceDelegate
        myCollection.dataSource = dataSourceDelegate
        myCollection.tag = row
        myCollection.layer.cornerRadius = 4.0
        myCollection.backgroundColor = UIColor.white
        myCollection.setContentOffset(myCollection.contentOffset, animated:false) // Stops collection view if it was scrolling.
        
        myCollection.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { myCollection.contentOffset.x = newValue }
        get { return myCollection.contentOffset.x }
    }
    
}


