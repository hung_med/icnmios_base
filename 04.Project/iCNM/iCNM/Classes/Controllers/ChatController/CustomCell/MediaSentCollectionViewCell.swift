//
//  MediaSentCollectionViewCell.swift
//  iCNM
//
//  Created by Thanh Huyen on 4/29/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class MediaSentCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgMedia: UIImageView!
    @IBOutlet weak var lbNumber: UILabel!
}
