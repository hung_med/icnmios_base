//
//  MyQuestionThumbCell.swift
//  iCNM
//
//  Created by Quang Hung on 7/7/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class MyQuestionThumbCell: UICollectionViewCell {
    var identity = CGAffineTransform.identity
    @IBOutlet weak var imageThumb: CustomImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
