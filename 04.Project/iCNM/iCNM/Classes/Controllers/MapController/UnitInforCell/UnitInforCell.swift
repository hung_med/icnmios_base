//
//  ClinicCell.swift
//  iCNM
//
//  Created by Nguyen Van Dung on 7/24/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class UnitInforCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lbMedicalUnitName: UILabel!
    @IBOutlet weak var lbCountLike: UILabel!
    @IBOutlet weak var lbAddress: UILabel!
    @IBOutlet weak var lbDistance: UILabel!
    
    @IBOutlet weak var imgStar1: UIImageView!
    @IBOutlet weak var imgStar2: UIImageView!
    @IBOutlet weak var imgStar3: UIImageView!
    @IBOutlet weak var imgStar4: UIImageView!
    @IBOutlet weak var imgStar5: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        let f = contentView.frame
        let fr = UIEdgeInsetsInsetRect(f, UIEdgeInsetsMake(15, 15, 15, 15))
        contentView.frame = fr
        addShadow(cell: self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func addShadow(cell:UITableViewCell) {
        cell.layer.cornerRadius = 4
        cell.layer.masksToBounds = true
        
        cell.layer.masksToBounds = false
        cell.layer.shadowOffset = CGSize(width: 0, height:0)
        cell.layer.shadowColor = UIColor.darkGray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowRadius = 2
    }
    
    /*
     * Set text comment count
     */
    func setCommentCount(locationID: Int) -> Void {
        UserRating.getUserRating(locationID: locationID) { (data, error) in
            self.lbCountLike.text = "(\((data?.count)!) Đánh giá)"
        }
    }
    
}
