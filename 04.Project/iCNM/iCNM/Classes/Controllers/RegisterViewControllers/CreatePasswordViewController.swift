//
//  CreatePasswordViewController.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift

class CreatePasswordViewController: BaseViewControllerNoSearchBar,UITextFieldDelegate {
    var userRegister:UserRegister!
    @IBOutlet weak var scrollView: FWCustomScrollView!
    
    @IBOutlet weak var passwordTextField: FWFloatingLabelTextField!
    private var currentTextField:UITextField?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem?.title = " "
        passwordTextField.addRegx(strRegx: "^.{6,}$", errorMsg: "Mật khẩu không được ít hơn 6 ký tự")
        passwordTextField.addRegx(strRegx: "^\\S*$", errorMsg: "Mật khẩu không được chứa khoảng trắng")
        registerForNotifications()
        passwordTextField.becomeFirstResponder()
        passwordTextField.delegate = self
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Thoát", style: .plain, handler: { (button) in
            let alertViewController = UIAlertController(title: "Thông báo", message: "Hủy bỏ quá trình đăng ký", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Đồng ý", style: UIAlertActionStyle.default, handler: { (action) in
                if let array = self.navigationController?.viewControllers, array.count > 1 {
                    self.navigationController?.popToViewController(array[1], animated: true)
                }
            })
            let noAction = UIAlertAction(title: "Huỷ bỏ", style: UIAlertActionStyle.default, handler:nil)
            alertViewController.addAction(noAction)
            alertViewController.addAction(yesAction)
            self.present(alertViewController, animated: true, completion: nil)
        })
        // Do any additional setup after loading the view.
    }
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func touchScreen(_ sender: Any) {
        currentTextField?.resignFirstResponder()
    }
    
    @IBAction func touchContinueButton(_ sender: Any) {
        if passwordTextField.validate() {
            userRegister.password = passwordTextField.text!
            let progress = showProgress()
            userRegister.register(success: { (userInfo) in
                User.doLogin(phoneNumber: self.userRegister.phoneNumber, password: self.userRegister.password, success: { (response) in
                    self.hideProgress(progress)
                    if response == "null" {
                        let alertViewController = UIAlertController(title: "Đăng Nhập", message: "Thông tin đăng nhập không chính xác", preferredStyle: .alert)
                        let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                        alertViewController.addAction(noAction)
                        self.present(alertViewController, animated: true, completion: nil)
                    } else if let userDoctor = UserDoctor(JSONString:response) {
                        if userDoctor.id == 0 {
                            let alertViewController = UIAlertController(title: "Thông báo", message: "Số điện thoại không tồn tại!", preferredStyle: .alert)
                            let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                            alertViewController.addAction(noAction)
                            self.present(alertViewController, animated: true, completion: nil)
                        } else {
                            autoreleasepool {
                                do {
                                    let realm = try Realm()
                                    let user = User()
                                    user.id = userDoctor.id
                                    if let doctorID = userDoctor.userInfo?.doctorID {
                                        user.doctorID =  doctorID
                                    }
                                    user.userDoctor = userDoctor
                                    try realm.write {
                                        realm.add(user, update: true)
                                    }
                                    NotificationCenter.default.post(name: Constant.NotificationMessage.loginSuccess, object: nil)
                                    //self.navigationController?.dismiss(animated: true, completion: nil)
                                    if let window = UIApplication.shared.keyWindow {
                                        let mainViewController =  StoryboardScene.Main.mainTabBarController.instantiate()
                                        window.rootViewController = mainViewController
                                    }

                                } catch let error {
                                    let alertViewController = UIAlertController(title: "Thông báo", message: "Không thể lưu thông tin đăng nhập:" + error.localizedDescription, preferredStyle: .alert)
                                    let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                                    alertViewController.addAction(noAction)
                                    self.present(alertViewController, animated: true, completion: nil)
                                }
                            }
                        }
                    }
                }, fail: { (error, response) in
                    self.hideProgress(progress)
                    self.handleNetworkError(error: error, responseData: response.data)
                })
            }, fail: { (error, response) in
                self.hideProgress(progress)
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }
    }
    
    @objc private func keyboardWillShow(aNotification:Notification) {
        if let curTextField = currentTextField {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            scrollView.contentInset = contentInset
            scrollView.scrollIndicatorInsets = contentInset
            let rect = curTextField.frame
            let test = view.bounds.height - (rect.size.height + rect.origin.y + scrollView.contentInset.top) - 40.0
            let different = keyboardFrame.size.height - test
            print(different)
            if different > 0 {
                scrollView.contentOffset.y = different - self.scrollView.contentInset.top
            }
        }
        
    }
    
    @objc private func keyboardWillHide(aNotification:Notification) {
        var contentInset:UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = 0
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentTextField = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
