//
//  OtherServiceCollectionViewCell.swift
//  iCNM
//
//  Created by Quang Hung on 7/7/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Cosmos

class OtherServiceCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageThumb: CustomImageView!
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblSaleOff: UILabel!
    @IBOutlet weak var lblDisCount: UILabel!
    @IBOutlet weak var cosmosRateStar: CosmosView!
    @IBOutlet weak var lblCost: UILabel!
    @IBOutlet weak var lblNumberComment: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
