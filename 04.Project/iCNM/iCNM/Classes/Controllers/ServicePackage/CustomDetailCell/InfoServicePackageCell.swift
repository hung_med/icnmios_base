//
//  InfoServicePackageCell.swift
//  iCNM
//
//  Created by Mac osx on 11/7/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Cosmos

class InfoServicePackageCell: UITableViewCell {
    @IBOutlet weak var lblServiceName: UILabel!
    @IBOutlet weak var lblServiceDes: UILabel!
    @IBOutlet weak var cosmosRate: CosmosView!
    @IBOutlet weak var lblDisCount: UILabel!
    @IBOutlet weak var lblDisCountFe: UILabel!
    @IBOutlet weak var lblCost: UILabel!
    @IBOutlet weak var lblStar: UILabel!
    @IBOutlet weak var lblNumberComment: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
