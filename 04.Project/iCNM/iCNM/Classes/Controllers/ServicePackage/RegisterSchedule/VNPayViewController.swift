//
//  VNPayViewController.swift
//  iCNM
//
//  Created by Mac osx on 2/1/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class VNPayViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var myWebView: UIWebView!
    var service:Service? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        title = "Thanh toán trực tuyến"
        myWebView.delegate = self
        if let serviceID = service?.serviceID, let serviceName = service?.serviceName, let price = service?.costFe{
            let param = API.baseURL_VNPay+"?serviceID=\(serviceID)&serviceName=\(serviceName)&price=\(price)"
            let urlString = param.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            if let instagramURL = URL(string: urlString!){
                let requestObj = URLRequest(url: instagramURL as URL)
                myWebView.loadRequest(requestObj)
            }
        }
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        print(webView.stringByEvaluatingJavaScript(from: "window.location.href")!)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        let str = request.url?.absoluteString
        NSLog("str:" ,str!)
        return true
    }
}
