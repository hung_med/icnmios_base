//
//  CustomSearchResultCell.swift
//  iCNM
//
//  Created by Mac osx on 3/28/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class CustomSearchResultCell: UITableViewCell {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    //@IBOutlet weak var lbDistance: UILabel!
    @IBOutlet weak var imagThumb: CustomImageView!
    @IBOutlet weak var titleConstraint: NSLayoutConstraint!
    @IBOutlet weak var descConstraint: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
