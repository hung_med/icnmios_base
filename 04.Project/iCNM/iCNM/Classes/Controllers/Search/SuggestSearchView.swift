//
//  SuggestSearchView.swift
//  iCNM
//
//  Created by Hoang Van Trung on 8/19/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class SuggestSearchView: UIView, UIGestureRecognizerDelegate {
    @IBOutlet var view: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var keySearchList = [String]()
    var executeSearchBlock : ((_ keySearch: String) -> Void)?
    var hiddenKeyboardBlock: (() -> Void)?
    var suggestSearch = [SuggestSearch]()
    var arrSearchHistory = [String]()
    var arrKeySearch = [String]()
    var keySearch: String = ""
    
    let defaults:UserDefaults = UserDefaults.standard
    
    @IBOutlet weak var heighOfSuggest: NSLayoutConstraint!
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("SuggestSearchView", owner: self, options: nil)
        self.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view": view]))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: ["view": view]))
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.layoutIfNeeded()
        self.isHidden = true
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
//        self.alpha = 0.5
        
        arrKeySearch.reserveCapacity(10)
        if defaults.stringArray(forKey: "historySearch") != nil{
            let arrStrHis: [String] = defaults.stringArray(forKey: "historySearch") ?? [String]()
            arrKeySearch = arrStrHis.reversed()
        }
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.cancelSearch))
        recognizer.delegate = self
        self.addGestureRecognizer(recognizer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func hiddenView() {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0.0
        }, completion: {(_) in
            self.isHidden = true
        })
    }
    
    func showView(keySearch: String = "") {
        self.keySearch = keySearch.trim()
        self.isHidden = false
        self.alpha = 1
        self.view.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        UIView.animate(withDuration: 0.3, animations: {
            
        }, completion: {(_) in
            if !keySearch.isEmpty {
                let paramDic: [String: Any] = ["charStr":keySearch.trim()]
                self.getSuggestSearch(param: paramDic)
            } else {
                self.getHistorySearch()
            }
        })
    }
    
    func getSuggestSearch(param: [String: Any]) {
        let suggestSearch = SuggestSearch()
        suggestSearch.getSuggestSearch(param:param,success: { (result) in
            self.suggestSearch = result
            self.tableView.reloadData()
        }) { (error, response) in
            
        }
    }
    
    func getHistorySearch() {
        arrSearchHistory = defaults.stringArray(forKey: "historySearch") ?? [String]()
//        self.heighOfSuggest.constant = CGFloat(arrSearchHistory.count * 40)
        tableView.reloadData()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: tableView))! {
            return false
        }
        return true
    }
    
    func cancelSearch() {
        self.hiddenView()
        if let block = hiddenKeyboardBlock {
            block()
        }
    }
}

extension SuggestSearchView: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    }
    internal func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var keysearch: String = ""
        if self.keySearch.isEmpty {
            keysearch = arrSearchHistory[indexPath.row].trim()
        } else {
            keysearch = suggestSearch[indexPath.row].searchContent.trim()
        }
        if let block = executeSearchBlock {
            block(keysearch)
        }
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
    // MARK: - UITableViewDatasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.keySearch.isEmpty {
            if arrSearchHistory.count > 3 {
                heighOfSuggest.constant = 120.0
                return 3
            }
            heighOfSuggest.constant = CGFloat(arrSearchHistory.count * 40)
            return arrSearchHistory.count
        } else {
            heighOfSuggest.constant = CGFloat(suggestSearch.count * 40)
            return suggestSearch.count //> 3 ? 3 : suggestSearch.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        var cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: cellIdentifier)
        }
        if self.keySearch.isEmpty {
            cell?.textLabel?.text = arrSearchHistory[indexPath.row].trim()
            cell?.textLabel?.font = UIFont.systemFont(ofSize: 14.0)
            cell?.imageView?.image = UIImage(named:"ic_history")
        } else {
            cell?.textLabel?.text = suggestSearch[indexPath.row].searchContent.trim()
            cell?.textLabel?.font = UIFont.boldSystemFont(ofSize: 14.0)
            let tmpDateTime = suggestSearch[indexPath.row].dateCreate
                //get time to server
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            var date = dateFormatter.date(from: tmpDateTime)
            if date == nil {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                date = dateFormatter.date(from: tmpDateTime)
            }
            let dateTime = date?.timeAgoSinceNow()
            cell?.detailTextLabel?.text  = "\(dateTime!)"
            cell?.detailTextLabel?.font = UIFont.systemFont(ofSize: 13.0)
            
            cell?.detailTextLabel?.textColor = UIColor.gray
        }
        return cell!
    }
}
