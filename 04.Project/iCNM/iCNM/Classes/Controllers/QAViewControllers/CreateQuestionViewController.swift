//
//  CreateQuestionViewController.swift
//  iCNM
//
//  Created by Medlatec on 7/4/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import FontAwesome_swift

class CreateQuestionViewController: BaseViewControllerNoSearchBar, FWComboBoxDelegate, UIImagePickerControllerDelegate,UICollectionViewDataSource, UICollectionViewDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate {
    @IBOutlet weak var askPersonComboBox: FWComboBox!
    @IBOutlet weak var askPersonAvatar: PASImageView!
    @IBOutlet weak var phoneNumberTextField: FWFloatingLabelTextField!
    @IBOutlet weak var emailTextField: FWFloatingLabelTextField!
    @IBOutlet weak var addressTextField: FWFloatingLabelTextField!
    @IBOutlet weak var genderTextField: FWFloatingLabelTextField!
    @IBOutlet weak var yearTextField: FWFloatingLabelTextField!
    @IBOutlet weak var maleRadioButton: ISRadioButton!
    @IBOutlet weak var femaleRadioButton: ISRadioButton!
    @IBOutlet weak var nameTextField: UILabel!
    @IBOutlet weak var questionForTextField: UILabel!
    @IBOutlet weak var contentRequireLabel: UILabel!
    @IBOutlet weak var btnSendQuestion: UIButton!
    @IBOutlet weak var specialistComboBox: FWComboBox!
    @IBOutlet weak var questionTitleTextField: FWFloatingLabelTextField!
    @IBOutlet weak var questionContentTextView: UITextView!
    @IBOutlet weak var imageCollectionView: FWCollectionView!
    @IBOutlet weak var imageCollectionViewTopSpaceConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: FWCustomScrollView!
    private var currentTextField:UITextField?
    private var currentTextView:UITextView?
    
    private var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    var imageListUpload = [[String: Any]]()
    private var uploadPhotos = [UIImage]()
    private var uploadPhotoNames = [String]()
    private let imagePicker = UIImagePickerController()
    private var token : NotificationToken?
    private var specialistNotificationToken:NotificationToken?
    private var currentRequest:Request?
    private var currentAddQuestionRequest:Request?
    private var currentUploadImageRequest:Request?
    private var currentSpecialist:Specialist?
    private let specialistResult:Results<Specialist> = {
        let realm = try! Realm()
        return realm.objects(Specialist.self).filter("active == true")
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        // Setup the Search Controller
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(CreateQuestionViewController.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        if currentUser == nil {
            self.navigationController?.dismiss(animated: true, completion: nil)
            return
        }
        
        if let nvc = self.navigationController {
            if nvc.viewControllers.count == 1 {
                self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Đóng", style: UIBarButtonItemStyle.plain, target: self, action: #selector(touchCloseButton(_:)));
            }
        }
        
        token = currentUser?.observe({[weak self] (change) in
            switch change {
            case .change:
                print("Thay doi")
            case .error(let error):
                print("Co loi \(error)")
            case .deleted:
                print("bi xoa")
                self?.navigationController?.dismiss(animated: true, completion: nil)
            }
        })
        
        questionForTextField?.font = UIFont.fontAwesome(ofSize: 15.0)
        let labelText = String.fontAwesomeIcon(name: .userCircle) + "  Câu hỏi cho"
        questionForTextField.text = labelText
        askPersonComboBox.layer.cornerRadius = 4.0
        askPersonComboBox.layer.masksToBounds = true
        askPersonComboBox.layer.borderColor = UIColor.lightGray.cgColor
        askPersonComboBox.layer.borderWidth = 0.5
        questionContentTextView.layer.cornerRadius = 4.0
        questionContentTextView.layer.masksToBounds = true
        questionContentTextView.layer.borderColor = UIColor.lightGray.cgColor
        questionContentTextView.layer.borderWidth = 0.5
        
        specialistComboBox.layer.cornerRadius = 4.0
        specialistComboBox.layer.masksToBounds = true
        specialistComboBox.layer.borderColor = UIColor.lightGray.cgColor
        specialistComboBox.layer.borderWidth = 0.5
        
        askPersonComboBox.dataSource = ["Tôi", "Người khác"]
        askPersonComboBox.selectRow(at: 0)
        askPersonComboBox.delegate = self
        askPersonComboBox.comboTextAlignment = .center
        specialistComboBox.delegate = self
        
        let btnButton =  UIButton(type: .custom)
        btnButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 15.0)
        let buttonName = String.fontAwesomeIcon(name: .send)
        btnButton.setTitle(buttonName, for: .normal)
        
        btnButton.addTarget(self, action: #selector(CreateQuestionViewController.touchSendQuestionButton), for: .touchUpInside)
        btnButton.frame = CGRect(x: screenSizeWidth-20, y: 0, width: 25, height:25)
        let sendBtnItem = UIBarButtonItem(customView: btnButton)
        self.navigationItem.setRightBarButtonItems([sendBtnItem], animated: true)
        
        if let avatarURL = currentUser?.userDoctor?.userInfo?.getAvatarURL() {
            askPersonAvatar.imageURL(URL: avatarURL)
        }
    
        phoneNumberTextField.text = currentUser?.userDoctor?.userInfo?.phone
        emailTextField.text = currentUser?.userDoctor?.userInfo?.email
        addressTextField.text = currentUser?.userDoctor?.userInfo?.address
        nameTextField.text = currentUser?.userDoctor?.userInfo?.name
        let birthday = currentUser?.userDoctor?.userInfo?.birthday
        let year = birthday?.toString(withFormat: "yyyy")
        yearTextField.text = year
        //maleRadioButton.isUserInteractionEnabled = false
        //femaleRadioButton.isUserInteractionEnabled = false
        nameTextField.isUserInteractionEnabled = false
        if let gender = currentUser?.userDoctor?.userInfo?.gender {
            if gender == "M" {
                genderTextField.text = "Nam"
                maleRadioButton.isSelected = true
            } else {
                genderTextField.text = "Nữ"
                femaleRadioButton.isSelected = true
            }
        } else {
            genderTextField.text = ""
            maleRadioButton.isSelected = false
            femaleRadioButton.isSelected = false
        }
        imagePicker.delegate = self
        imageCollectionView.delegate = self
        imageCollectionView.dataSource = self
        imageCollectionView.collectionViewLayout = QuestionImageCollectionViewFlowLayout()
        if uploadPhotos.count == 0 {
            imageCollectionViewTopSpaceConstraint.constant = 0
        }
        let contentRequireString = " Vui lòng nhập đầy đủ nội dung câu hỏi để được tư vấn tốt nhất từ các bác sĩ chuyên môn. Nội dung phải lớn hơn 50 ký tự và không vượt quá 1000 ký tự. Xin cảm ơn !"
        let labelFont = UIFont.systemFont(ofSize: 14.0)
        let fontAwesomeFont = FontFamily.FontAwesome.regular.font(size: 14.0)
        let attributedString = NSMutableAttributedString(string: contentRequireString)
        attributedString.addAttributes([NSFontAttributeName:fontAwesomeFont ?? labelFont,NSForegroundColorAttributeName:UIColor(hex:"ef4c4c")], range: NSMakeRange(0, 1))
        attributedString.addAttributes([NSFontAttributeName:fontAwesomeFont ?? labelFont,NSForegroundColorAttributeName:UIColor(hex:"9d9d9d")], range: NSMakeRange(1, contentRequireString.count-1))
        contentRequireLabel.attributedText = attributedString
        specialistNotificationToken = specialistResult.observe({[weak self] (changes) in
            switch changes {
            case .initial:
                // Results are now populated and can be accessed without blocking the UI
                self?.setupSpecialistComboBox()
                break
            case .update:
                self?.setupSpecialistComboBox()
                break
            case .error(let error):
                // An error occurred while opening the Realm file on the background worker thread
                fatalError("\(error)")
                break
            }
        })
        phoneNumberTextField.delegate = self
        emailTextField.delegate = self
        addressTextField.delegate = self
        questionTitleTextField.delegate = self
        questionContentTextView.delegate = self
        yearTextField.delegate = self
        registerForNotifications()
        emailTextField.addRegx(strRegx: "[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}", errorMsg: "Địa chỉ email không hợp lệ")
        phoneNumberTextField.addRegx(strRegx: "[0-9]{10,11}", errorMsg: "Số điện thoại không đúng định dạng")
        phoneNumberTextField.messageForValidatingLength = "Số điện thoại không được bỏ trống"
        //addressTextField.messageForValidatingLength = "Địa chỉ không được bỏ trống"
        yearTextField.messageForValidatingLength = "Năm sinh không được bỏ trống"
        genderTextField.messageForValidatingLength = "Bạn chưa chọn giới tính"
        requestData()
        // Do any additional setup after loading the view.
        self.checkTrackingFeature()
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func checkTrackingFeature(){
        if currentUser != nil{
            let userDefaults = UserDefaults.standard
            let token = userDefaults.object(forKey: "token") as! String
            TrackingFeature().checkTrackingFeature(fcm:token, featureName:Constant.CREATE_QUESTION, categoryID: 0, success: {(result) in
                if result != nil{
                    print("flow track done:", Constant.CREATE_QUESTION)
                }else{
                    print("flow track fail:", Constant.CREATE_QUESTION)
                }
            }, fail: { (error, response) in
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }else{
            return
        }
    }
    
    @IBAction func touchGenderRadioButton(_ sender: ISRadioButton) {
        if sender == maleRadioButton {
            genderTextField.text = "Nam"
        } else {
            genderTextField.text = "Nữ"
        }
    }
    
    private func setupSpecialistComboBox() {
        specialistComboBox.dataSource = specialistResult.map({ (specialist) -> String in
            return specialist.name ?? ""
        })
        if let curSpecialist = currentSpecialist {
            let index = specialistResult.index(where: { (specialist) -> Bool in
                specialist.id == curSpecialist.id
            })
            specialistComboBox.selectRow(at: index)
        }
    }
    
    override func requestData() {
        currentRequest?.cancel()
        let progressHUD = self.showProgress()
        currentRequest = Specialist.getAllSpecialist(success: { (data) in
            self.hideProgress(progressHUD)
        }) { (error, response) in
            self.handleNetworkError(error: error, responseData: response.data)
            self.hideProgress(progressHUD)
        }
    }
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Đặt câu hỏi")
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fwComboBoxWillShow(comboBox:FWComboBox) {
        currentTextField?.resignFirstResponder()
        currentTextView?.resignFirstResponder()
    }
    
    func fwComboBox(comboBox: FWComboBox, didSelectAtIndex index: Int) {
        if comboBox == askPersonComboBox {
            if index == 0{
                if let avatarURL = currentUser?.userDoctor?.userInfo?.getAvatarURL(){
                    askPersonAvatar.imageURL(URL: avatarURL)
                }
                phoneNumberTextField.text = currentUser?.userDoctor?.userInfo?.phone
                addressTextField.text = currentUser?.userDoctor?.userInfo?.address
                nameTextField.text = currentUser?.userDoctor?.userInfo?.name
                emailTextField.text = currentUser?.userDoctor?.userInfo?.email
                nameTextField.text = currentUser?.userDoctor?.userInfo?.name
                let birthday = currentUser?.userDoctor?.userInfo?.birthday
                let year = birthday?.toString(withFormat: "yyyy")
                yearTextField.text = year
                //maleRadioButton.isUserInteractionEnabled = false
                //femaleRadioButton.isUserInteractionEnabled = false
                if let gender = currentUser?.userDoctor?.userInfo?.gender {
                    if gender == "M" {
                        genderTextField.text = "Nam"
                        maleRadioButton.isSelected = true
                    } else {
                        genderTextField.text = "Nữ"
                        femaleRadioButton.isSelected = true
                    }
                }
                
            } else {
                askPersonAvatar.reset()
                emailTextField.text = ""
                phoneNumberTextField.text = ""
                genderTextField.text = ""
                femaleRadioButton.isSelected = false
                maleRadioButton.isSelected = false
                addressTextField.text = ""
                nameTextField.text = ""
                yearTextField.text = ""
                //maleRadioButton.isUserInteractionEnabled = true
                //femaleRadioButton.isUserInteractionEnabled = true
            }
        } else if comboBox == specialistComboBox {
            currentSpecialist = specialistResult[index]
        }
    }
    
    @IBAction func touchCloseButton(_ sender: Any) {
//        self.navigationController?.popViewController(animated: false)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func touchSelectPhotoButton(_ sender: UIButton) {
        if (uploadPhotos.count >= 3) {
            let alertViewController = UIAlertController(title: "Thông báo", message: "Bạn không thể tải lên quá 3 ảnh", preferredStyle: .alert)
            let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
            alertViewController.addAction(noAction)
            self.present(alertViewController, animated: true, completion: nil)
            return;
        }

        var avatarActionSheet:UIAlertController? = nil
        if IS_IPAD{
            avatarActionSheet = UIAlertController(title: nil, message: "Thêm ảnh mô tả", preferredStyle: .alert)
        }else{
            avatarActionSheet = UIAlertController(title: nil, message: "Thêm ảnh mô tả", preferredStyle: .actionSheet)
        }
        
        let cameraAction = UIAlertAction(title: "Chụp ảnh mới", style: .default) { (action) in
            self.imagePicker.sourceType = .camera
            self.imagePicker.cameraCaptureMode = .photo
            self.imagePicker.modalPresentationStyle = .fullScreen
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let galleryAction = UIAlertAction(title: "Chọn ảnh từ thư viện", style: .default) { (action) in
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Đóng", style: .cancel, handler: nil)
        avatarActionSheet?.addAction(cameraAction)
        avatarActionSheet?.addAction(galleryAction)
        avatarActionSheet?.addAction(cancelAction)
        
        present(avatarActionSheet!, animated: true, completion: nil)
    }
    
    private func resizeImage(image:UIImage, toWidth:CGFloat)->UIImage? {
        let ratio = toWidth / image.size.width
        let newHeight = image.size.height * ratio
        UIGraphicsBeginImageContext(CGSize(width: toWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: toWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    // UIImagePickerController Delegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let chosenImage = info[UIImagePickerControllerOriginalImage]{
            if let curUser = currentUser, let image = resizeImage(image: chosenImage as! UIImage, toWidth: 512), let imageData = UIImageJPEGRepresentation(image,1.0)  {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyyMMdd_hhmmss"
                let questionImageName = "question_\(curUser.id)_image_" + dateFormatter.string(from: Date())
                self.imageListUpload.append(
                    ["Name": questionImageName,
                     "Base64String": imageData.base64EncodedString()]
                )
                
                self.uploadPhotos.append(image)
                self.uploadPhotoNames.append(questionImageName)
                if self.uploadPhotos.count > 0 {
                    self.imageCollectionViewTopSpaceConstraint.constant = 8
                }
                self.imageCollectionView.reloadData()
            }
            dismiss(animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //UICollectionView Datasource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return uploadPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! QuestionImageCollectionViewCell
        let index = indexPath.row
        let image = uploadPhotos[index]
        cell.imageView.image = image
        cell.removeButton.tag = index
        cell.removeButton.addTarget(self, action: #selector(touchRemoveImageButton(sender:)), for: .touchUpInside)
        return cell
    }
    
    func touchRemoveImageButton(sender:UIButton) {
        uploadPhotos.remove(at: sender.tag)
        uploadPhotoNames.remove(at: sender.tag)
        if uploadPhotos.count == 0 {
            imageCollectionViewTopSpaceConstraint.constant = 0
        }
        imageCollectionView.reloadData()
        
    }
    
    deinit {
        specialistNotificationToken?.invalidate()
        token?.invalidate()
        NotificationCenter.default.removeObserver(self)
    }
    
    //Keyboard handle
    @objc private func keyboardWillShow(aNotification:Notification) {
        if let curTextField = currentTextField {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            scrollView.contentInset = contentInset
            scrollView.scrollIndicatorInsets = contentInset
            if let rect = curTextField.superview?.convert(curTextField.frame, to: curTextField.superview?.superview) {
                let test = view.bounds.height - (rect.size.height + rect.origin.y + scrollView.contentInset.top) - 60.0
                let different = keyboardFrame.size.height - test
                if different > 0 {
                    scrollView.contentOffset.y = different - self.scrollView.contentInset.top
                }
            }
        } else if let curTextView = currentTextView {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            scrollView.contentInset = contentInset
            scrollView.scrollIndicatorInsets = contentInset
            if let rect = curTextView.superview?.convert(curTextView.frame, to: curTextView.superview?.superview) {
                let test = view.bounds.height - (rect.size.height + rect.origin.y + scrollView.contentInset.top) - 60.0
                let different = keyboardFrame.size.height - test
                if different > 0 {
                    scrollView.contentOffset.y = different - self.scrollView.contentInset.top
                }
            }
        }
        
    }
    
    @objc private func keyboardWillHide(aNotification:Notification) {
        var contentInset:UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = 0
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentTextField = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        currentTextField = nil
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        currentTextField = nil
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == yearTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == emailTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == addressTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == phoneNumberTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 12
        }else if textField == questionTitleTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else{
            return true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = questionContentTextView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }

        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        return changedText.count <= 400
    }
    
    @IBAction func touchScreen(_ sender: Any) {
        currentTextField?.resignFirstResponder()
        currentTextView?.resignFirstResponder()
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        currentTextView = textView
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        currentTextView = nil
        return true
    }
    
    @IBAction func touchSendQuestionButton(_ sender: Any) {
        currentTextField?.resignFirstResponder()
        currentTextView?.resignFirstResponder()
        if specialistComboBox.currentRow == nil {
            let alertViewController = UIAlertController(title: "Thông báo", message: "Bạn chưa chọn nhóm bệnh", preferredStyle: .alert)
            let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
            alertViewController.addAction(noAction)
            self.present(alertViewController, animated: true, completion: nil)
            return;
        } else if questionContentTextView.text.count <= 50 || questionContentTextView.text.count > 1000 {
            let alertViewController = UIAlertController(title: "Thông báo", message: "Nội dung câu hỏi phải có nhiều hơn 50 ký tự và không quá 1000 ký tự", preferredStyle: .alert)
            let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
            alertViewController.addAction(noAction)
            self.present(alertViewController, animated: true, completion: nil)
            return;
        }
        
        if emailTextField.validate() && addressTextField.validate() && phoneNumberTextField.validate() && genderTextField.validate() && yearTextField.validate() && questionTitleTextField.validate() && yearTextField.validate(){
            if yearTextField.text?.count != 4{
                showAlertView(title: "Năm sinh không đúng định dạng", view: self)
                return
            }else{
                let date = Date()
                let calendar = Calendar.current
                let currentYear = Int(calendar.component(.year, from: date))
                
                if let birthDay = Int(yearTextField.text!) {
                    if birthDay > currentYear{
                        showAlertView(title: "Năm sinh không lớn hơn năm hiện tại", view: self)
                        return
                    }
                }
            }
            
            let question = Question()
            question.title = questionTitleTextField.text!.trim()
            let components = questionContentTextView.text?.components(separatedBy: "\n\n").filter { $0 != "" }
            question.content = components?.joined(separator: "\n")
            let currentSpecialist = specialistResult[specialistComboBox.currentRow!]
            question.specialistID = currentSpecialist.id
            question.userID = (currentUser?.userDoctor?.userInfo?.id)!
            question.fullname = currentUser?.userDoctor?.userInfo?.name
            question.phone = phoneNumberTextField.text?.trim()
            question.email = emailTextField.text?.trim()
            question.address = currentUser?.userDoctor?.userInfo?.address?.trim()
            question.gender = maleRadioButton.isSelected ? "M":"F"
            question.doctorID = 0
            question.expireTime = nil
            question.questionImage = uploadPhotoNames.joined(separator: ";")
           // question.birthday = yearTextField.text?.trim()
            
            let parameter: [String : Any] =
                ["Question": question.toJSON(),
                 "ImageUploads": self.imageListUpload]
            
            self.btnSendQuestion.isUserInteractionEnabled = false
            currentAddQuestionRequest?.cancel()
            let progress = self.showProgress()
            currentUploadImageRequest = Question.uploadPhoto(paramDic: parameter, success: { (resultString) in
                self.hideProgress(progress)
                if resultString == "\"ok\"" {
                    self.btnSendQuestion.isUserInteractionEnabled = true
                    let alert = UIAlertController(title: "Đặt câu hỏi thành công", message: "Bác sĩ trên hệ thống iCNM sẽ sớm phản hồi cho bạn. Để biết trạng thái câu hỏi vui lòng truy cập tính năng 'Câu hỏi của tôi' trong menu ứng dụng", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Câu hỏi của tôi", style: UIAlertActionStyle.default, handler: { alertAction in
                        self.btnSendQuestion.isUserInteractionEnabled = true
                        self.questionTitleTextField.text = ""
                        self.questionContentTextView.text = ""
                        self.specialistComboBox.selectRow(at: nil)
                        self.uploadPhotos.removeAll()
                        self.uploadPhotoNames.removeAll()
                        self.imageCollectionView.reloadData()
                        self.imageListUpload.removeAll()
                        
                        let myQuestionVC = MyQuestionVC(nibName: "MyQuestionVC", bundle: nil)
                        self.tabBarController?.tabBar.isHidden = true
                        self.navigationController?.pushViewController(myQuestionVC, animated: true)
                    }))
                    alert.addAction(UIAlertAction(title: "Đóng", style: UIAlertActionStyle.cancel, handler: { alertAction in
                        self.btnSendQuestion.isUserInteractionEnabled = true
                        self.questionTitleTextField.text = ""
                        self.questionContentTextView.text = ""
                        self.specialistComboBox.selectRow(at: nil)
                        self.uploadPhotos.removeAll()
                        self.uploadPhotoNames.removeAll()
                        self.imageCollectionView.reloadData()
                        self.imageListUpload.removeAll()
                    }))
                    self.present(alert, animated: true, completion: nil)
                    } else {
                        self.btnSendQuestion.isUserInteractionEnabled = true
                        let alertViewController = UIAlertController(title: "Thông báo", message: "Có lỗi khi tạo câu hỏi", preferredStyle: .alert)
                        let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                            alertViewController.addAction(noAction)
                        self.present(alertViewController, animated: true, completion: nil)
                    }
            }, fail: { (error, response) in
                self.btnSendQuestion.isUserInteractionEnabled = true
                self.hideProgress(progress)
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if let flowLayout = imageCollectionView.collectionViewLayout as? QuestionImageCollectionViewFlowLayout {
            flowLayout.invalidateLayout()
        }
    }
}
