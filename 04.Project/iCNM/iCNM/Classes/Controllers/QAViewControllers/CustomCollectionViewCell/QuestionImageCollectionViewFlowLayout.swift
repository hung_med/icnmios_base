//
//  QuestionImageCollectionViewFlowLayout.swift
//  iCNM
//
//  Created by Medlatec on 7/27/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class QuestionImageCollectionViewFlowLayout: UICollectionViewFlowLayout {
    override init() {
        super.init()
        setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    func setupLayout() {
        minimumInteritemSpacing = 1
        minimumLineSpacing = 1
        scrollDirection = .vertical
    }

    override var itemSize: CGSize {
        set {
            
        }
        get {
            let numberOfColumns: CGFloat = 3
            
            let itemWidth = (self.collectionView!.bounds.width - (numberOfColumns - 1)) / numberOfColumns
            return CGSize(width: floor(itemWidth), height: floor(itemWidth))
        }
    }
}
