//
//  QATableViewController.swift
//  iCNM
//
//  Created by Medlatec on 6/19/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import SideMenu
import RealmSwift
import ESPullToRefresh
import Alamofire
import Social
import FontAwesome_swift
import Toaster

class QATableViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    @IBOutlet weak var myTableView: UITableView!
    var notificationToken: NotificationToken? = nil
    let selectedSpecialists:Results<Specialist> = {
        let realm = try! Realm()
        return realm.objects(Specialist.self).filter("selected == true")
    }()
    
    private var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    private var pageNumber = 1
    
    var questionData = [QuestionAnswer]()
    
    private var selectedIndex = -1
    private var isCheck:Bool? = false
    private var headerScrollView = ESRefreshHeaderAnimator(frame:CGRect.zero)
    private var footerScrollView = ESRefreshFooterAnimator(frame:CGRect.zero)
    
    let specialistSelectVC = StoryboardScene.Main.specialistMenuNavigationController.instantiate()
    
    private var currentRequest:Request?
    private var currentQARequest:Request?
    @IBOutlet weak var scopeSelected: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        if #available(iOS 11.0, *) {
//            tableView.contentInsetAdjustmentBehavior = .never
//        }
        let attributes = [
            NSFontAttributeName : UIFont.fontAwesome(ofSize: 14.0),
            NSForegroundColorAttributeName : UIColor.white
        ]
        scopeSelected.setTitleTextAttributes(attributes, for: UIControlState.normal)
        scopeSelected.subviews[0].backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        scopeSelected.subviews[1].backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        scopeSelected.subviews[2].backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        scopeSelected.subviews[3].backgroundColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        scopeSelected.layer.cornerRadius = 4.0
        scopeSelected.layer.masksToBounds = true
        let title_th = String.fontAwesomeIcon(name: .leaf) + "   Tiêu hoá"
        scopeSelected.setTitle(title_th, forSegmentAt: 0)
        let title_nk = String.fontAwesomeIcon(name: FontAwesome(rawValue: "\u{f1ae}")!) + "   Nhi khoa"
        scopeSelected.setTitle(title_nk, forSegmentAt: 1)
        let title_tk = String.fontAwesomeIcon(name: .lastFMSquare) + "   Thần kinh"
        scopeSelected.setTitle(title_tk, forSegmentAt: 2)
        let title_tm = String.fontAwesomeIcon(name: .heartbeat) + "   Tim mạch"
        scopeSelected.setTitle(title_tm, forSegmentAt: 3)
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        let realm = try! Realm()
        currentUser = realm.currentUser()
        NotificationCenter.default.addObserver(self, selector: #selector(handleLoginSuccess), name: Constant.NotificationMessage.loginSuccess, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleLogout), name: Constant.NotificationMessage.logout, object: nil)
        myTableView.estimatedRowHeight = 100
        myTableView.rowHeight = UITableViewAutomaticDimension
        NotificationCenter.default.addObserver(self, selector: #selector(updateSelected), name: Constant.NotificationMessage.specialistSelectedChanged, object: nil)
        //        notificationToken = selectedSpecialists.addNotificationBlock { [weak self] (changes) in
        //            switch changes {
        //            case .initial:
        //                break;
        ////                self?.getQuestionAnswer()
        //            case .update:
        //                // Query results have changed, so apply them to the UITableView
        //                self?.getQuestionAnswer()
        //            case .error(let error):
        //                fatalError("\(error)")
        //            }
        //        }
        headerScrollView.pullToRefreshDescription = "Kéo để làm mới"
        headerScrollView.releaseToRefreshDescription = "Thả để làm mới"
        headerScrollView.loadingDescription = "Đang tải..."
        footerScrollView.loadingDescription = "Đang tải..."
        footerScrollView.loadingMoreDescription = "Tải thêm"
        footerScrollView.noMoreDataDescription = "Đã hết dữ liệu"
        
        self.myTableView.es.addInfiniteScrolling(animator:footerScrollView) { [weak self] in
            if let weakSelf = self {
                weakSelf.pageNumber = weakSelf.pageNumber + 1
                let specialistIDs:[Int] = weakSelf.selectedSpecialists.map { $0.id }
                QuestionAnswer.getAllQuestionAnswer(pageNumber: weakSelf.pageNumber, specialistIDs: specialistIDs, success: { (data) in
                    self?.myTableView.es.stopLoadingMore()
                    if data.count > 0 {
                        self?.questionData.append(contentsOf: data)
                        self?.myTableView.reloadData()
                    } else {
                        self?.myTableView.es.noticeNoMoreData()
                    }
                }) { (error, response) in
                    self?.myTableView.es.stopLoadingMore()
                    self?.handleNetworkError(error: error, responseData: response.data)
                }
            }
        }
        self.myTableView.es.addPullToRefresh(animator: headerScrollView) { [weak self] in
            if let weakSelf = self {
                self?.pageNumber = 1
                if self?.selectedSpecialists.count == 0 {
                    self?.questionData = [QuestionAnswer]()
                } else {
                    let specialistIDs:[Int] = weakSelf.selectedSpecialists.map { $0.id }
                    self?.currentQARequest?.cancel()
                    self?.currentQARequest = QuestionAnswer.getAllQuestionAnswer(pageNumber: weakSelf.pageNumber, specialistIDs: specialistIDs, success: { (data) in
                        
                        self?.questionData = data
                        self?.myTableView.reloadData()
                        self?.myTableView.es.stopPullToRefresh()
                    }) { (error, response) in
                        self?.myTableView.es.stopPullToRefresh()
                        self?.handleNetworkError(error: error, responseData: response.data)
                    }
                }
            }
        }
        myTableView.tableFooterView = UIView()
        requestData()
        
        self.checkTrackingFeature()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    @IBAction func selectItemInSpecialists(_ sender: Any) {
        var currentIndex = 0
        if scopeSelected.selectedSegmentIndex == 0{
            currentIndex = 5
        }else if scopeSelected.selectedSegmentIndex == 1{
            currentIndex = 7
        }else if scopeSelected.selectedSegmentIndex == 2{
            currentIndex = 25
        }else{
            currentIndex = 35
        }
        scopeSelected.selectedSegmentIndex = -1
        let topQAVC = self.storyboard?.instantiateViewController(withIdentifier: "TopQAVC") as! TopQATableViewController
        self.tabBarController?.tabBar.isHidden = true
        topQAVC.currentIndex = currentIndex
        self.navigationController?.pushViewController(topQAVC, animated: true)
    }
    
    func checkTrackingFeature(){
        if currentUser != nil{
            let userDefaults = UserDefaults.standard
            let token = userDefaults.object(forKey: "token") as! String
            TrackingFeature().checkTrackingFeature(fcm:token, featureName:Constant.QUESTION_ANSWER, categoryID: 0, success: {(result) in
                if result != nil{
                    print("flow track done:", Constant.QUESTION_ANSWER)
                }else{
                    print("flow track fail:", Constant.QUESTION_ANSWER)
                }
            }, fail: { (error, response) in
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }else{
            return
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Hỏi đáp"
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Hỏi đáp")
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
        
        SideMenuManager.default.menuRightNavigationController = specialistSelectVC
        if selectedIndex > -1 {
            self.myTableView.reloadRows(at: [IndexPath(row: 0, section: selectedIndex)], with: UITableViewRowAnimation.fade)
            selectedIndex = -1
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        SideMenuManager.menuRightNavigationController = nil
    }
    
    override func requestData() {
        currentRequest?.cancel()
        //let progressHUD = self.showProgress()
        currentRequest = Specialist.getAllSpecialist(success: { (data) in
            //self.hideProgress(progressHUD)
            self.getQuestionAnswer()
        }) { (error, response) in
            self.handleNetworkError(error: error, responseData: response.data)
            //self.hideProgress(progressHUD)
        }
    }
    
    @objc func updateSelected(aNotification: Notification) {
        getQuestionAnswer()
    }
    
    func getQuestionAnswer() {
        currentQARequest?.cancel()
        pageNumber = 1
        if selectedSpecialists.count == 0 {
            self.questionData = [QuestionAnswer]()
        } else {
            let specialistIDs:[Int] = selectedSpecialists.map { $0.id }
            //let progress = showProgress()
            currentQARequest = QuestionAnswer.getAllQuestionAnswer(pageNumber: pageNumber, specialistIDs: specialistIDs, success: { (data) in
                //self.hideProgress(progress)
                self.questionData = data
                self.myTableView.reloadData()
                Loader.addLoaderTo(self.myTableView)
                Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(QATableViewController.loaded), userInfo: nil, repeats: true)
            }) { (error, response) in
                //self.hideProgress(progress)
                Loader.removeLoaderFrom(self.myTableView)
                self.handleNetworkError(error: error, responseData: response.data)
            }
        }
    }
    
    func loaded()
    {
        Loader.removeLoaderFrom(self.myTableView)
    }
    
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return questionData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuestionCell", for: indexPath) as! QuestionTableViewCell
        cell.avatarImageView.reset()
        cell.nameLabel.text = ""
        cell.timeLabel.text = ""
        cell.specialistTag.text = ""
        cell.questionLabel.text = ""
        cell.questionTitleLabel.text = ""
        let questionAnswer = questionData[indexPath.section]
        
        if let askUser = questionAnswer.userAsk, let avatarURL = askUser.getAvatarURL() {
            cell.avatarImageView.imageURL(URL: avatarURL)
        }
        
        if let sumRate = questionAnswer.answer?.sumRate{
            if sumRate != 0{
                cell.moreButton.isHidden = false
                cell.moreButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 18.0)
                let titleBtn = String.fontAwesomeIcon(name: .star)
                cell.moreButton.setTitle(titleBtn, for: UIControlState.normal)
                cell.moreButton.setTitleColor(UIColor(red: 1, green: 149/255, blue: 0, alpha: 1), for: UIControlState.normal)
            }else{
                cell.moreButton.isHidden = true
            }
        }else{
            cell.moreButton.isHidden = true
        }
        
        if let question = questionAnswer.question {
            cell.nameLabel.text = question.fullname
            cell.questionTitleLabel.text = question.title
            cell.questionLabel.text = question.content
            if let createdDate = question.dateCreate {
                let timeString = "\u{f017} " + createdDate.timeAgoSinceNow()
                let timeAttributedString = NSMutableAttributedString(string: timeString)
                timeAttributedString.addAttributes([NSFontAttributeName:UIFont(font: FontFamily.FontAwesome.regular, size: 14.0)], range: NSMakeRange(0, 1))
                if  questionAnswer.answer != nil {
                    let space = "   "
                    let answered = space + "\u{f058}  Đã trả lời"
                    let answerAttributedString = NSMutableAttributedString(string: answered)
                    answerAttributedString.addAttributes([NSForegroundColorAttributeName:UIColor(hex:"39b54a"), NSFontAttributeName:UIFont(font: FontFamily.FontAwesome.regular, size: 14.0)], range: NSMakeRange(space.count, 1))
                    timeAttributedString.append(answerAttributedString)
                }
                cell.timeLabel.attributedText = timeAttributedString
            }
        }
        if let specialistName = questionAnswer.specialist?.name {
            cell.specialistTag.text = specialistName
        }
        
        let countComment = questionAnswer.question?.countComment ?? 0
        let countLike = questionAnswer.question?.countLike ?? 0
        let countShare = questionAnswer.question?.countShare ?? 0
        cell.shareButton.setTitle("  Chia sẻ" + (countShare>0 ? " (\(countShare))":""), for: .normal)
        cell.commentButton.setTitle("  Bình luận" + (countComment>0 ? " (\(countComment))":""), for: .normal)
        if let user = self.currentUser{
            if user.id == questionAnswer.userAsk?.id || user.doctorID == questionAnswer.doctor?.id{
                cell.commentButton.isHidden = false
            }else{
                cell.commentButton.isHidden = true
            }
        }else{
            cell.commentButton.isHidden = true
        }
        cell.commentButton.tag = indexPath.section
        cell.commentButton.addTarget(self, action: #selector(self.handleUserComment(_:)), for: .touchUpInside)
        cell.likeBUtton.tag = indexPath.section
        cell.shareButton.tag = indexPath.section
        
        if IS_IPHONE_5{
            cell.moreButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 13.0)
            cell.likeBUtton.titleLabel?.font = UIFont.fontAwesome(ofSize: 13.0)
            cell.shareButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 13.0)
        }
        
        if questionAnswer.liked == false {
            cell.likeBUtton.setAttributedTitle(nil, for: .normal)
            cell.likeBUtton.setTitleColor(UIColor(hex:"9D9D9D"), for: .normal)
            cell.likeBUtton.setTitle("  Thích" + (countLike>0 ? " (\(countLike))":""), for: .normal)
        } else {
            let attributedString = NSMutableAttributedString(string: "  Thích" + (countLike>0 ? " (\(countLike))":""))
            attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor(hex:"1d6edc"), range: NSMakeRange(0, 1))
            cell.likeBUtton.setAttributedTitle(attributedString, for: .normal)
        }
        
        if let userID = currentUser?.id, questionAnswer.userInteractionInfoLoaded == false && questionAnswer.isLoadingUserInteractionInfo == false {
            questionAnswer.getUserInteractionInfo(userID: userID, type: .Question, success: {
                if cell.likeBUtton.tag == indexPath.section {
                    if questionAnswer.liked {
                        let cLike = questionAnswer.question?.countLike ?? 0
                        let attributedString = NSMutableAttributedString(string: "  Thích" + (cLike>0 ? " (\(cLike))":""))
                        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor(hex:"1d6edc"), range: NSMakeRange(0, 1))
                        cell.likeBUtton.setAttributedTitle(attributedString, for: .normal)
                    }
                }
            }, fail: { (error, response) in
                
            })
        }

        return cell
    }
    
    @objc func handleUserComment(_ sender: UIButton){
        let questionAnswer = questionData[sender.tag]
        selectedIndex = sender.tag
        let questionComment = StoryboardScene.Main.qaComment.instantiate()
        questionComment.questionAnswer = questionAnswer
        self.navigationController?.pushViewController(questionComment, animated: true)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        self.title = ""
        if segue.identifier == StoryboardSegue.Main.showQuestionAnswerDetail.rawValue {
            if let destinationVC = segue.destination as? QADetailViewController {
                if let cell = sender as? QuestionTableViewCell {
                    if let indexPath = myTableView.indexPath(for: cell) {
                        let questionAnswer = questionData[indexPath.section]
                        destinationVC.questionAnswer = questionAnswer
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0.00001
        }
        return 5.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5.0
    }
    
    
    @IBAction func touchSpecialistSelect(_ sender: Any) {
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    deinit {
        notificationToken?.invalidate()
    }
    @IBAction func touchLikeButton(_ sender: Any) {
        if let userID = currentUser?.id, !currentUser!.isInvalidated {
            let index = (sender as! UIButton).tag
            let question = questionData[index]
            question.toggleLikeThank(userID: userID, type: .Question, success: {
                let indexPath = IndexPath(item: 0, section: index)
                self.myTableView.reloadRows(at: [indexPath], with: .none)
            }, fail: { (error, response) in
                
            })
        } else {
            confirmLogin(myView: self)
        }
    }
    @IBAction func touchShareButton(_ sender: Any) {
        let index = (sender as! UIButton).tag
        let question = questionData[index]
        if let composeVC = SLComposeViewController(forServiceType: SLServiceTypeFacebook), let title = question.question?.title, let questionID = question.question?.id{
            let result = title.replacingOccurrences(of: " ", with: "-")
            let urlString = Constant.baseURLWeb + "/\(questionID)" + "/\(result).aspx"
            let urlApp = URL(string: urlString)
            composeVC.add(urlApp)
            composeVC.setInitialText("Viết gì đó...")
            self.present(composeVC, animated: true, completion: nil)
        }
    }
    
    func handleLoginSuccess(aNotification:Notification) {
        let realm = try! Realm()
        currentUser = realm.currentUser()
        myTableView.reloadData()
    }
    
    func handleLogout(aNotification:Notification) {
        currentUser = nil
        myTableView.reloadData()
    }
    
    @IBAction func touchReveal(_ sender: Any) {
       present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5, animations: {
            self.tabBarController?.tabBar.isHidden = true
        }, completion: {
            finished in
            self.tabBarController?.tabBar.isHidden = false
        })
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        UIView.animate(withDuration: 0.5, animations: {
            self.tabBarController?.tabBar.isHidden = false
        }, completion: {
            finished in
            self.tabBarController?.tabBar.isHidden = true
        })
    }
}
