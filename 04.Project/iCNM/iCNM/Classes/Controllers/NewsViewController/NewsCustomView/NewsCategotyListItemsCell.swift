//
//  NewsCategotyListItemsCell.swift
//  iCNM
//
//  Created by Len Pham on 8/2/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class NewsCategotyListItemsCell: UITableViewCell {
    @IBOutlet weak var thumbImageView: CustomImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timesLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    @IBOutlet weak var viewForTap: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
