//
//  NewsHomeCollectionViewCell.swift
//  iCNM
//
//  Created by Len Pham on 7/30/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class NewsHomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var viewForTap: UIView!
    @IBOutlet weak var thumbImageView: CustomImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timesLabel: UILabel!
}
