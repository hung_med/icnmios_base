//
//  NewsDetailWebView.swift
//  iCNM
//
//  Created by Len Pham on 8/17/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class NewsDetailWebView: UIView, UIWebViewDelegate{

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var webViewHeightConstraint: NSLayoutConstraint!
    
    public var htmlContent: String = String() {
        didSet {
           
            webView.loadHTMLString(htmlContent,
                                             baseURL: nil)
        }
    }
    

    override func layoutSubviews() {
        super.layoutSubviews()
    }

    // MARK: init methods
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonSetup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonSetup()
    }
    
    // MARK: setup view
    
    private func loadViewFromNib() -> UIView {
        let viewBundle = Bundle(for: type(of: self))
        //  An exception will be thrown if the xib file with this class name not found,
        let view = viewBundle.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)?[0]
        return view as! UIView
    }
    
    private func commonSetup() {
        let nibView = loadViewFromNib()
        nibView.frame = bounds
        // the autoresizingMask will be converted to constraints, the frame will match the parent view frame
        nibView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // Adding nibView on the top of our view
        addSubview(nibView)
        webView.delegate = self
    }
}
