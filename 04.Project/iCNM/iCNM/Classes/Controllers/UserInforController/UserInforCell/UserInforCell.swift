//
//  UserInforCell.swift
//  iCNM
//
//  Created by ngvdung on 8/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class UserInforCell: UITableViewCell {
    
    @IBOutlet weak var imgCellPresenter: UIImageView!
    @IBOutlet weak var lbCellContent: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
