//
//  UserInforViewController.swift
//  iCNM
//
//  Created by ngvdung on 8/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import SDWebImage

class UserInforViewController: UIViewController {
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var userInfo: UserInfo!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgAvatar.layer.cornerRadius = 40
        imgAvatar.layer.masksToBounds = true
        imgAvatar.layer.borderWidth = 2
        imgAvatar.layer.borderColor = UIColor.white.cgColor

        tableView.register(UINib.init(nibName: "UserInforCell", bundle: Bundle.main), forCellReuseIdentifier: "UserInforCell")
        tableView.register(UINib.init(nibName: "JoinedDateCell", bundle: Bundle.main), forCellReuseIdentifier: "JoinedDateCell")
        
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableViewAutomaticDimension
        
        if (userInfo != nil) {
            self.setUserInfo(userInfo: self.userInfo)
        }
    }
    
    func setUserInfo(userInfo: UserInfo){
        //imgAvatar.s  //dynamic var avatar:String?
        if userInfo.avatar != nil{
            let avatarStr = userInfo.avatar!.trimmingCharacters(in: .whitespaces)
           // let avatarFullStringUrl = Constant.imageBaseURL + "\(self.userInfo.id)/\(avatarStr)"
            let avatarFullStringUrl = API.baseURLImage + "\(API.iCNMImage)" + "\(self.userInfo.id)/\(avatarStr)"
            self.imgAvatar.sd_setImage(with: URL(string: avatarFullStringUrl), placeholderImage: UIImage(named: "avatar_default"))
        }
    
        lbName.text = userInfo.name
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

// MARK: TableView Delegate, Datasource
extension UserInforViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserInforCell", for: indexPath) as! UserInforCell
        let cell2 = tableView.dequeueReusableCell(withIdentifier: "JoinedDateCell", for: indexPath) as!  JoinedDateCell
        
        switch indexPath.row {
        case 0:
            cell.imgCellPresenter.image = UIImage(named: "call.png")
            
            if (self.userInfo.phone != nil) {
                cell.lbCellContent.text = self.userInfo.phone
            } else {
                cell.lbCellContent.text = "Chưa cập nhật thông tin"
            }
            
            break
        case 1:
            cell.imgCellPresenter.image = UIImage(named: "icon_email.png")
            
            if (self.userInfo.email != nil) {
                cell.lbCellContent.text = self.userInfo.email
            } else {
                cell.lbCellContent.text = "Chưa cập nhật thông tin"
            }
            
            break
        case 2:
            cell.imgCellPresenter.image = UIImage(named: "ic_birthday.png")
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            if (userInfo.birthday != nil) {
               cell.lbCellContent.text = formatter.string(from: userInfo.birthday!)
            } else {
                cell.lbCellContent.text = "Chưa cập nhật thông tin"
            }
            
            break
        case 3:
            cell.imgCellPresenter.image = UIImage(named: "ic_gender")
            if self.userInfo.gender == "M"{
                cell.lbCellContent.text = "Nam"
            }else{
                cell.lbCellContent.text = "Nữ"
            }
            
            break
        case 4:
            cell.imgCellPresenter.image = UIImage(named: "placeholder.png")
            
            if (self.userInfo.address != nil) {
               cell.lbCellContent.text = self.userInfo.address
            } else {
                cell.lbCellContent.text = "Chưa cập nhật thông tin"
            }
            
            break
        case 5:
            cell.imgCellPresenter.image = UIImage(named: "ic_job.png")
            
            if (self.userInfo.job != nil) {
                cell.lbCellContent.text = self.userInfo.job
            } else {
                cell.lbCellContent.text = "Chưa cập nhật thông tin"
            }
            
            break
        default:
            cell2.lbContent1.text = "\(self.userInfo.name!) đã tham gia iCNM vào"
            
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            
            if (self.userInfo.createdDate != nil) {
                cell2.lbContent2.text = formatter.string(from: self.userInfo.createdDate!)
            }
            
            break
        }
       
        
        if (indexPath.row != 6) {
            return cell
        } else {
            return cell2
        }
    }
    
}

