//
//  CommentsCell.swift
//  iCNM
//
//  Created by Nguyen Van Dung on 7/29/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import SDWebImage
import DZNEmptyDataSet

protocol CommentsCellDelegate {
    func reloadCommentList(numberOfComment: Int)
}

class CommentsCell: UITableViewCell {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnViewAllComments: UIButton!
    @IBOutlet weak var heightConstraintBtnViewAllComment: NSLayoutConstraint!
    
    var locationID:Int = 0
    var listUserComment:[UserRating]!
    var delegate:CommentsCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        listUserComment = [UserRating]()
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        tableView.tableFooterView = UIView()
        
        tableView.register(UINib(nibName: "UserCommentCell", bundle: nil), forCellReuseIdentifier: "UserCommentCell")
        
        tableView.separatorColor = UIColor.clear
        tableView.estimatedRowHeight = 110
        tableView.rowHeight = UITableViewAutomaticDimension
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    // MARK: Set data
    func fillDataComment(locationID: Int) -> Void {
        UserRating.getUserRating(locationID: locationID) { (data, error) in
            self.listUserComment = data
            
            if (self.listUserComment.count == 0) {
                self.heightConstraintBtnViewAllComment.constant = 0
            } else {
                self.btnViewAllComments.setTitle("Xem tất cả đánh giá (\(self.listUserComment.count))", for:.normal)
                self.tableView.reloadData()
            }
        }
    }
    
    @IBAction func viewAllCommentAction(_ sender: Any) {
        self.heightConstraintBtnViewAllComment.constant = 0
        self.delegate?.reloadCommentList(numberOfComment: self.listUserComment.count)
    }
    
}

extension CommentsCell: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listUserComment.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let userCommentCell = tableView.dequeueReusableCell(withIdentifier: "UserCommentCell", for: indexPath) as! UserCommentCell
        userCommentCell.selectionStyle = .none
        
        let userRating = self.listUserComment[indexPath.row]
        
        userCommentCell.userInfo = userRating.user
        self.fillDataForUserCommentCell(cell: userCommentCell, userRating: userRating)
        
        /*
        let avatarStr = userRating.user.avatar!.trimmingCharacters(in: .whitespaces)
        let avatarFullStringUrl = Constant.imageBaseURL + "\(userRating.user.id)/\(avatarStr)"
       
        userCommentCell.imgUserAvatar.sd_setImage(with: URL(string: avatarFullStringUrl), placeholderImage: UIImage(named: "no_thumbnail.png"))
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        
        //user
        userCommentCell.lbNameOfUser.text = userRating.user.name
        
        //star
        Common.sharedInstance.setRatingStar(imgViewStar1: userCommentCell.imgStar1, imgViewStar2: userCommentCell.imgStar2, imgViewStar3: userCommentCell.imgStar3, imgViewStar4: userCommentCell.imgStar4, imgViewStar5: userCommentCell.imgStar5, ratingScore: Float(userRating.rating.sumRate))
        
        //rating
        userCommentCell.lbTimeOpen.text = formatter.string(from: userRating.rating.dateCreate!)
        userCommentCell.lbCommentContent.text = userRating.rating.rateContent
         */
        
        return userCommentCell
    }
    
    /*
     * Fill data for user comment
     */
    func fillDataForUserCommentCell(cell: UserCommentCell, userRating: UserRating) {
        if userRating.user.avatar != nil{
            let avatarStr = userRating.user.avatar!.trimmingCharacters(in: .whitespaces)
            let avatarFullStringUrl = API.baseURLImage + "\(API.iCNMImage)" + "\(userRating.user.id)/\(avatarStr)"
            
            cell.imgUserAvatar.sd_setImage(with: URL(string: avatarFullStringUrl), placeholderImage: UIImage(named: "avatar_default"))
        }else{
            cell.imgUserAvatar.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "avatar_default"))
        }
        
        //user
        cell.lbNameOfUser.text = userRating.user.name
        
        //star
        Common.sharedInstance.setRatingStar(imgViewStar1: cell.imgStar1, imgViewStar2: cell.imgStar2, imgViewStar3: cell.imgStar3, imgViewStar4: cell.imgStar4, imgViewStar5: cell.imgStar5, ratingScore: userRating.rating.sumRate)
        
        //rating
        cell.lbTimeOpen.text = userRating.rating.dateCreate!.timeAgoSinceNow()
        cell.lbCommentContent.text = userRating.rating.rateContent.trim()
    }
}

extension CommentsCell: DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    func title(forEmptyDataSet scrollView: UIScrollView) -> NSAttributedString? {
        let str = "Không có đánh giá nào"
        let attrs = [NSFontAttributeName: UIFont.preferredFont(forTextStyle: UIFontTextStyle.headline)]
        return NSAttributedString(string: str, attributes: attrs)
    }
    
    func backgroundColor(forEmptyDataSet scrollView: UIScrollView) -> UIColor  {
        return UIColor.white
    }
    
}
