//
//  CommonInforCell.swift
//  MedlatecDemo
//
//  Created by Nguyen Van Dung on 7/26/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import SDWebImage
import GoogleMaps

protocol CommonInforCellDelegate {
    func performLikeAction()
    func performRatingAction()
    func openMapDirection()
}

class CommonInforCell: UITableViewCell {

    @IBOutlet weak var imgMedicalUnit: UIImageView!
    @IBOutlet weak var lbMedicalName: UILabel!
    @IBOutlet weak var lbRating: UILabel!
    @IBOutlet weak var lbCountRating: UILabel!
    
    @IBOutlet weak var imgStar1: UIImageView!
    @IBOutlet weak var imgStar2: UIImageView!
    @IBOutlet weak var imgStar3: UIImageView!
    @IBOutlet weak var imgStar4: UIImageView!
    @IBOutlet weak var imgStar5: UIImageView!
    
    @IBOutlet weak var lbAddress: UILabel!
    @IBOutlet weak var lbPhoneNumber: UILabel!
    @IBOutlet weak var lbEmail: UILabel!
    @IBOutlet weak var lbWebsite: UILabel!
    @IBOutlet weak var lbTimeOpen: UILabel!
    @IBOutlet weak var lbIntro: UILabel!
    
    @IBOutlet weak var btnSchedule: UIButton!
    @IBOutlet weak var btnCallPhone: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var lbDistance: UILabel!
    @IBOutlet weak var btnLike: UIButton!
    
    @IBOutlet weak var imageBtnLike: UIImageView!
    @IBOutlet weak var imageBtnReview: UIImageView!
    
    
    //var
    var delegate:CommonInforCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //init map
        mapView.settings.zoomGestures = false
        mapView.settings.scrollGestures = false
        
        lbDistance.layer.borderColor = UIColor.gray.cgColor
        lbDistance.layer.masksToBounds = true
        lbDistance.layer.borderWidth = 0.5
        lbDistance.layer.cornerRadius = 3
        
        //Notification
        // Register to receive notification
        let changeImageButtonLikeNoti = Notification.Name("ChangeImageButtonLike")
        let changeImageButtonUnLikeNoti = Notification.Name("ChangeImageButtonUnLike")
        let changeImageRatedNoti = Notification.Name("ChangeImageRated")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeImageButtonLiked), name: changeImageButtonLikeNoti, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(self.changeImageButtonUnLike), name: changeImageButtonUnLikeNoti, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.changeImageRate), name: changeImageRatedNoti, object: nil)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: Set data
    func setCountRating(locationID: Int) -> Void {
        UserRating.getUserRating(locationID: locationID) { (data, error) in
            self.lbCountRating.text = "\((data?.count)!) Đánh giá"
        }
    }
        
    //Set marker for map
    func setMarkerForMap(lat: Float, long: Float, name: String, locationTypeID: Int) -> Void {
        
        let position = CLLocationCoordinate2D(latitude: CLLocationDegrees(lat), longitude: CLLocationDegrees(long))
        
        let camera = GMSCameraPosition.camera(withLatitude: CLLocationDegrees(lat),
                                              longitude: CLLocationDegrees(long),
                                              zoom: 16)
        mapView.animate(to: camera)

        let imgTypeDoctor = UIImage(named: "map_icon_doctor")
        let imgTypeHospital = UIImage(named: "map_icon_hospital")
        let imgTypeDrugstore = UIImage(named: "map_icon_medicin")
        
        let marker = GMSMarker()
        marker.position = position
        
        //set image
        if (locationTypeID == 1) {
            marker.icon = imgTypeDoctor
        } else if (locationTypeID == 2 || locationTypeID == 3 || locationTypeID == 4) {
            marker.icon = imgTypeHospital
        } else if (locationTypeID == 5) {
            marker.icon = imgTypeDrugstore
        }
        
        marker.map = mapView
    }
    
    @IBAction func btnLikeAction(_ sender: Any) {
        
        self.delegate?.performLikeAction()
    }
    
    @IBAction func btnRatingAction(_ sender: Any) {
        self.delegate?.performRatingAction()
    }
    
    @IBAction func openMapDirection(_ sender: Any) {
        self.delegate?.openMapDirection()
    }
    
    // MARK: ----
    func changeImageButtonLiked() {
        imageBtnLike.image = UIImage(named: "ic_btn_favorited")
    }
    
    func changeImageButtonUnLike() {
        imageBtnLike.image = UIImage(named: "like_btn_60")
    }
    
    func changeImageRate(notification: Notification) {
        imageBtnReview.image = UIImage(named: "ic_btn_rated.png")
    }
}
