//
//  CollectionNearByUnitCell.swift
//  iCNM
//
//  Created by Nguyen Van Dung on 8/17/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class CollectionNearByUnitCell: UICollectionViewCell {

    @IBOutlet weak var imgUnit: UIImageView!
    @IBOutlet weak var lbUnitName: UILabel!
    @IBOutlet weak var lbLocationTypeName: UILabel!
    
    @IBOutlet weak var imgStar1: UIImageView!
    @IBOutlet weak var imgStar2: UIImageView!
    @IBOutlet weak var imgStar3: UIImageView!
    @IBOutlet weak var imgStar4: UIImageView!
    @IBOutlet weak var imgStar5: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
