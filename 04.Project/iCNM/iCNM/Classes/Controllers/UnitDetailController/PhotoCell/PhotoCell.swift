//
//  PhotoCell.swift
//  MedlatecDemo
//
//  Created by Nguyen Van Dung on 7/26/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class PhotoCell: UITableViewCell {
    
    @IBOutlet weak var numPhotos: UILabel!
    @IBOutlet weak var lblWarning: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    var listPhoto:[String]? = nil
    var locationID:Int = 0
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        listPhoto = [String]()
        lblWarning.isHidden = false
        self.collectionView.register(UINib(nibName: "PhotoCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "PhotoCollectionViewCell")
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        self.collectionView.collectionViewLayout = layout
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //Get data
    func showListImages(images:[String], locationID:Int) -> Void {
        self.listPhoto = images
        self.locationID = locationID
        if !images[0].isEmpty{
            self.numPhotos.text = "(\(images.count) Hình ảnh)"
        }else{
            self.numPhotos.text = "(0 Hình ảnh)"
        }
        self.collectionView.reloadData()
    }
}

extension PhotoCell: UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (self.listPhoto?.count)!
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath) as! PhotoCollectionViewCell
        let imageStr = self.listPhoto![indexPath.row]
        
        var urlImage = API.baseURLImage + "iCNMImage/" + "LocationImage/" +
            "\(self.locationID)/" + imageStr
        let result = Int(splitImageLocationToString(str: imageStr))
        
        if result != self.locationID{
            let url = urlImage.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            cell.imgPhoto.loadImageUsingUrlString(urlString: url!)
            lblWarning.isHidden = true
        }else{
            urlImage = API.baseURLImage + "iCNMImage/" + "LocationImage/" + "\(self.locationID)/" + imageStr
            let realImageUrl = urlImage.components(separatedBy: ";")[0]
            let url = realImageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            cell.imgPhoto.loadImageUsingUrlString(urlString: url!)
            lblWarning.isHidden = true
        }
        
//
//
//
//        if result != medicalUnit.locationID{
//            urlImage = API.baseURLImage + "iCNMImage/" + "LocationImage/" + medicalUnit.imageStr
//            let realImageUrl = urlImage.components(separatedBy: ";")[0]
//            cell.imgMedicalUnit.sd_setImage(with: URL(string: realImageUrl), placeholderImage: UIImage(named: "default-thumbnail.png") )
//        }else{
//            cell.imgMedicalUnit.sd_setImage(with: URL(string: realImageUrl), placeholderImage: UIImage(named: "default-thumbnail.png"))
//        }
//
//
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if self.listPhoto![0] != ""{
            return CGSize(width: 150, height: 150)
        }else{
            return CGSize(width: 0, height: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView,
                                 layout collectionViewLayout: UICollectionViewLayout,
                                 minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
}
