//
//  ManagerQuestionDoctorVC.swift
//  iCNM
//
//  Created by Mac osx on 11/6/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import FontAwesome_swift
import Toaster
import ESPullToRefresh
import PhotoSlider

class ManagerQuestionDoctorVC: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate, DetailAnswerDoctorVCVCDelegate, UIViewControllerTransitioningDelegate {
    var scopeSelected = ScrollableSegmentedControl()
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var myView: UIView!
    var storedOffsets = [Int: CGFloat]()
    var questionAnswerForDoctors: [QuestionAnswerForDoctor]?
    var questionReceivedForDoctors: [QuestionAnswerForDoctor]?
    var questionNoAnswerForDoctor: [QuestionAnswerForDoctor]?
    var serviceNames: [String]?
    var scopeBar:UIView? = nil
    var currentIndex:Int = 0
    var resultTime:Int? = 0
    var listStatusTime:[Bool]?
    var listImages:[Int: [String]] = [:]
    var listImages_0:[Int: [String]] = [:]
    var listImages_1:[Int: [String]] = [:]
    var currentOffset_Y: CGFloat = 0.0
    var lastY: CGFloat = 0.0
    var isChecked:Bool? = false
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    let userDefaults = UserDefaults.standard
    fileprivate var pageNumber = 1
    fileprivate var footerScrollView = ESRefreshFooterAnimator(frame:CGRect.zero)
    let segmented = YSSegmentedControl(frame: .zero, titles: [])
    let scrollView = UIScrollView()
    var infoButtonBar:UIBarButtonItem?
    
    @IBOutlet weak var topConstr: NSLayoutConstraint!
    
    //    private var specialListSelect = [SpecialistModel]()
//    private var specialistSelected = [SpecialistModel]()
//    var arrChuyenKhoa = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        title = "Câu hỏi bác sĩ"
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(ManagerQuestionDoctorVC.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            //assign button to navigationbar
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        let buttonRight: UIButton = UIButton(type: .system)
        buttonRight.titleLabel?.font = UIFont.fontAwesome(ofSize: 16.0)
        let btn = String.fontAwesomeIcon(name: .dollar)
        buttonRight.setTitle(btn, for: .normal)
        buttonRight.addTarget(self, action: #selector(ManagerQuestionDoctorVC.infoSummaryBarButtonHandler(sender:)), for: UIControlEvents.touchUpInside)
        infoButtonBar = UIBarButtonItem(customView: buttonRight)
        serviceNames = ["CÂU HỎI MỚI", "ĐANG CHỜ TRẢ LỜI", "ĐÃ TRẢ LỜI"]
        self.questionAnswerForDoctors = [QuestionAnswerForDoctor]()
        self.questionReceivedForDoctors = [QuestionAnswerForDoctor]()
        self.questionNoAnswerForDoctor = [QuestionAnswerForDoctor]()
        
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.estimatedRowHeight = 200
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        let nib1 = UINib(nibName: "Tab1CustomCell", bundle: nil)
        myTableView.register(nib1, forCellReuseIdentifier: "Tab1Cell")
        let nib2 = UINib(nibName: "Tab2CustomCell", bundle: nil)
        myTableView.register(nib2, forCellReuseIdentifier: "Tab2Cell")
        let nib3 = UINib(nibName: "Tab3CustomCell", bundle: nil)
        myTableView.register(nib3, forCellReuseIdentifier: "Tab3Cell")
        
        self.reloadSegmentController(names: self.serviceNames!, currentTab: 0)
        footerScrollView.loadingMoreDescription = "Tải thêm"
        self.myTableView.es.addInfiniteScrolling(animator:footerScrollView) { [weak self] in
            if let weakSelf = self {
                let index = self?.scopeSelected.selectedSegmentIndex
                if let user = self?.currentUser{
                    if index == 0{
                        weakSelf.pageNumber = weakSelf.pageNumber + 1
                        QuestionAnswerForDoctor().getQuestionNoAnswerForDoctor(doctorID:user.doctorID, pageNumber:weakSelf.pageNumber, success: { (data) in
                            weakSelf.myTableView.es.stopLoadingMore()
                            if data.count > 0 {
                                self?.questionNoAnswerForDoctor?.append(contentsOf: data)
                                self?.listImages_0 = (self?.resultListImageURL(questionAnswer: (self?.questionNoAnswerForDoctor!)!))!
                                self?.myTableView.reloadData()
                            } else {
                                self?.footerScrollView.loadingMoreDescription = "Đã hết dữ liệu"
                            }
                        }, fail: { (error, response) in
                            
                        })
                    }else if index == 1{
                        weakSelf.pageNumber = weakSelf.pageNumber + 1
                        QuestionAnswerForDoctor().getQuestionRecivedForDoctor(doctorID:user.doctorID, pageNumber:weakSelf.pageNumber, success: { (data) in
                            weakSelf.myTableView.es.stopLoadingMore()
                            if data.count > 0 {
                                self?.questionReceivedForDoctors?.append(contentsOf: data)
                                self?.listStatusTime = self?.checkStatusTime(questionReceivedForDoctors: self?.questionReceivedForDoctors)
                                self?.listImages = (self?.resultListImageURL(questionAnswer: (self?.questionReceivedForDoctors!)!))!
                                self?.myTableView.reloadData()
                            } else {
                                self?.footerScrollView.loadingMoreDescription = "Đã hết dữ liệu"
                            }
                        }, fail: { (error, response) in
                            
                        })
                    }else{
                        weakSelf.pageNumber = weakSelf.pageNumber + 1
                        QuestionAnswerForDoctor().getQuestionAnswerForDoctor(doctorID:user.doctorID, pageNumber:weakSelf.pageNumber, success: { (data) in
                            weakSelf.myTableView.es.stopLoadingMore()
                            if data.count > 0 {
                                self?.questionAnswerForDoctors?.append(contentsOf: data)
                                self?.listImages_1 = (self?.resultListImageURL(questionAnswer: (self?.questionAnswerForDoctors!)!))!
                                self?.myTableView.reloadData()
                            } else {
                                self?.footerScrollView.loadingMoreDescription = "Đã hết dữ liệu"
                            }
                        }, fail: { (error, response) in
                            
                        })
                    }
                }
            }
        }
        
        //get time to server
        self.getCurrentDateTime()
        self.setupSwipAction()
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func infoSummaryBarButtonHandler(sender: UIBarButtonItem)
    {
        if self.questionAnswerForDoctors?.count == 0{
            showAlertView(title: "Bạn chưa có trả lời nào trong chuyên khoa đã đăng kí", view: self)
            return
        }else{
            var totalNumber = 0
            for questionAnswerObj in self.questionAnswerForDoctors!{
                if questionAnswerObj.approver!{
                    totalNumber += 1
                }
            }
            
            if totalNumber == 0{
                showAlertView(title: "Bạn chưa có câu trả lời nào được duyệt! Hệ thống đang chờ duyệt câu trả lời và sẽ thông báo cho bạn sớm nhất. Trân trọng cảm ơn", view: self)
                return
            }else{
                let sumMoney = 60*totalNumber
                let msg = "iCNM chúc mừng bác sĩ trả lời thành công và được duyệt \(totalNumber) câu hỏi. Kinh phí bác sĩ được hưởng là \(sumMoney).000 đ . Trân trọng cảm ơn sự hợp tác của bác sĩ!"
                showAlertView(title: msg, view: self)
                return
            }
        }
    }
    
    /*
     * Set up swipe gesture for view
     */
    func setupSwipAction() -> Void {
        let swipeLeft = UISwipeGestureRecognizer.init(target: self, action: #selector(self.swipeLeftAndRight))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.myTableView.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer.init(target: self, action: #selector(self.swipeLeftAndRight))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.myTableView.addGestureRecognizer(swipeRight)
    }
    
    func swipeLeftAndRight(gestureRecognizer: UISwipeGestureRecognizer) {
        if gestureRecognizer.direction == .right{
            currentIndex -= 1
            if currentIndex < 0{
                currentIndex = 0
                return
            }
        }else{
            currentIndex += 1
            if currentIndex >= (self.serviceNames?.count)!{
                currentIndex = (self.serviceNames?.count)!
                return
            }
        }
        
        scopeSelected.selectedSegmentIndex = currentIndex
    }
    
    func checkStatusTime(questionReceivedForDoctors: [QuestionAnswerForDoctor]?)->[Bool]{
        var result:[Bool]? = []
       // listStatusLabel = [String]()
        if let count = questionReceivedForDoctors?.count{
            for i in 0..<count{
                let questionReceivedForDoctorObj = questionReceivedForDoctors![i]
                if let expireTime = questionReceivedForDoctorObj.expireTime{
                    let currentSecond = Int(expireTime.millisecondsSince1970)
                    let diff = self.resultTime! - currentSecond
                    if (diff > 60 * 60 * 1000) {
                        //qua han
                        result?.append(true)
                        //listStatusLabel?.append("Đã quá hạn trả")
                    }else{
                        //da nhan
                        result?.append(false)
                        //let strTime = expireTime.timeAgoSinceNow()
                        //listStatusLabel?.append(strTime)
                    }
                }
            }
        }
        return result!
        
    }
    
    func getCurrentDateTime(){
        DatetimeServer().getCurrentDateTime(success: { (data) in
            if data != nil {
                let second = Int((data?.currentDateTime?.millisecondsSince1970)!)
                self.resultTime = second
            } else {
                
            }
        }, fail: { (error, response) in
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func getQuestionNoAnswerForDoctor(doctorID:Int, pageNumber:Int){
        let progress = self.showProgress()
        QuestionAnswerForDoctor().getQuestionNoAnswerForDoctor(doctorID:doctorID, pageNumber:pageNumber, success: { (data) in
            if data.count > 0 {
                self.questionNoAnswerForDoctor?.append(contentsOf: data)
                self.listImages_0 = self.resultListImageURL(questionAnswer: (self.questionNoAnswerForDoctor!))
                self.myTableView.reloadData()
                self.hideProgress(progress)
            } else {
                self.hideProgress(progress)
                let index = self.scopeSelected.selectedSegmentIndex
                if index == 0{
                    Toast(text: "Không có câu hỏi mới nào trong chuyên khoa của bạn").show()
                }
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func getQuestionReceivedForDoctor(doctorID:Int, pageNumber:Int){
        let progress = self.showProgress()
        QuestionAnswerForDoctor().getQuestionRecivedForDoctor(doctorID:doctorID, pageNumber:pageNumber, success: { (data) in
            if data.count > 0 {
                self.questionReceivedForDoctors?.append(contentsOf: data)
                self.listStatusTime = self.checkStatusTime(questionReceivedForDoctors: self.questionReceivedForDoctors)
                self.listImages = self.resultListImageURL(questionAnswer: (self.questionReceivedForDoctors!))
                self.myTableView.reloadData()
                self.hideProgress(progress)
            } else {
                self.hideProgress(progress)
                Toast(text: "Không nhận có câu trả lời nào đang chờ nào trong chuyên khoa của bạn").show()
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func getQuestionAnswerForDoctor(doctorID:Int, pageNumber:Int){
        let progress = self.showProgress()
        QuestionAnswerForDoctor().getQuestionAnswerForDoctor(doctorID:doctorID, pageNumber:pageNumber, success: { (data) in
            if data.count > 0 {
                self.questionAnswerForDoctors?.append(contentsOf: data)
                self.listImages_1 = self.resultListImageURL(questionAnswer: (self.questionAnswerForDoctors!))
                self.myTableView.reloadData()
                self.hideProgress(progress)
            } else {
                self.hideProgress(progress)
                Toast(text: "Không có câu đã trả lời nào trong chuyên khoa của bạn").show()
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if IS_IPAD{
            topConstr.constant = 0
        }else{
            if #available(iOS 11.0, *) {
                
            }
            else{
                self.myView.frame = CGRect(x: 0, y: 0, width: myView.frame.size.width, height: myView.frame.size.height)
                self.myTableView.frame = CGRect(x: 0, y: 40, width: myTableView.frame.size.width, height: myTableView.frame.size.height+76)
            }
            
            if IS_IPHONE_X || IS_IPHONE_XS{
                self.myView.frame = CGRect(x: 0, y: 20, width: myView.frame.size.width, height: myView.frame.size.height)
                self.myTableView.frame = CGRect(x: 0, y: 108, width: myTableView.frame.size.width, height: myTableView.frame.size.height)
            }
        }
    }
    
    func reloadSegmentController(names:[String], currentTab:Int){
        // let totalSize = self.calculateLongestTextWidth(names: names)
        if #available(iOS 11.0, *) {
            scopeSelected.frame = CGRect(x: 0, y: 38, width: screenSizeWidth/3*CGFloat(names.count), height: 38)
        }
        else{
            scopeSelected.frame = CGRect(x: 0, y: 34, width: screenSizeWidth/3*CGFloat(names.count), height: 30)
        }
        
        var indexCate: Int = 0
        for copeTitle in names {
            scopeSelected.insertSegment(withTitle: copeTitle, at: indexCate)
            indexCate += 1
        }
        scopeSelected.addTarget(self, action: #selector(ManagerQuestionDoctorVC.reloadQuestionForDoctor), for: .valueChanged)
        scopeSelected.segmentContentColor = UIColor.groupTableViewBackground
        scopeSelected.segmentContentColor = UIColor.lightGray
        scopeSelected.underlineSelected = true
        scopeSelected.segmentStyle = .textOnly
        scopeSelected.tintColor = UIColor(hex:"1976D2")
        // add it in a scrollView, because ill got too much categories here. Just if you need that:
        
        // add it in a scrollView, because ill got too much categories here. Just if you need that:
        let scrollView  = UIScrollView()
        if #available(iOS 11.0, *) {
            scrollView.frame = CGRect(x:0, y: 34, width: screenSizeWidth, height: 100)
        }
        else{
            scrollView.frame = CGRect(x:0, y: 45, width: screenSizeWidth, height: 100)
        }
        
        scrollView.contentSize = CGSize(width: screenSizeWidth/3*CGFloat(names.count), height: scopeSelected.frame.size.height-1)
        scrollView.showsHorizontalScrollIndicator = false
        // set first category
        scopeSelected.selectedSegmentIndex = currentTab
        scrollView.addSubview(scopeSelected)
        myView?.addSubview(scrollView)
    }
    
    private func calculateLongestTextWidth(names:[String]?)->CGFloat {
        var totalSize:CGFloat = 0
        for str in names!{
            let fontAttributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 14)]
            let size = (str as NSString).size(attributes: fontAttributes)
            totalSize += size.width
        }
        return totalSize
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        title = "Quản trị câu hỏi bác sĩ"
        super.viewDidAppear(animated)
        if IS_IPAD{
            topConstr.constant = 0
        }else{
            if IS_IPHONE_X || IS_IPHONE_XS{
                self.myView.frame = CGRect(x: 0, y: 20, width: myView.frame.size.width, height: myView.frame.size.height)
                self.myTableView.frame = CGRect(x: 0, y: 108, width: myTableView.frame.size.width, height: myTableView.frame.size.height)
            }
        }
        if isChecked!{
            remindQuestionAnswer()
        }
        
    }
    
    func switchChanged(_ sender: ScrollableSegmentedControl){
        // currentIndex = sender.selectedSegmentIndex
        self.myTableView .reloadData()
    }
    
    func remindQuestionAnswer(){
        currentIndex = 1
        scopeSelected.selectedSegmentIndex = currentIndex
    }
    
    func reloadQuestionForDoctor(_ sender: ScrollableSegmentedControl){
        let index = scopeSelected.selectedSegmentIndex
        if let user = currentUser{
            if index == 0{
                pageNumber = 1
                 self.navigationItem.setRightBarButton(nil, animated: true)
                questionReceivedForDoctors = [QuestionAnswerForDoctor]()
                questionNoAnswerForDoctor = [QuestionAnswerForDoctor]()
                self.myTableView.reloadData()
                self.getQuestionNoAnswerForDoctor(doctorID: user.doctorID, pageNumber: 1)
            }else if index == 1{
                pageNumber = 1
                 self.navigationItem.setRightBarButton(nil, animated: true)
                questionAnswerForDoctors = [QuestionAnswerForDoctor]()
                questionNoAnswerForDoctor = [QuestionAnswerForDoctor]()
                self.myTableView.reloadData()
                self.getQuestionReceivedForDoctor(doctorID: user.doctorID, pageNumber: 1)
            }else{
                pageNumber = 1
                self.navigationItem.setRightBarButton(infoButtonBar, animated: true)
                questionAnswerForDoctors = [QuestionAnswerForDoctor]()
                questionReceivedForDoctors = [QuestionAnswerForDoctor]()
                self.myTableView.reloadData()
                self.getQuestionAnswerForDoctor(doctorID: user.doctorID, pageNumber: 1)
            }
        }
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let index = scopeSelected.selectedSegmentIndex
        if index == 0{
            return (self.questionNoAnswerForDoctor?.count)!
        }else if index == 1{
            return (self.questionReceivedForDoctors?.count)!
        }else{
            return (self.questionAnswerForDoctors?.count)!
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = scopeSelected.selectedSegmentIndex
        switch index {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Tab1Cell", for: indexPath) as! Tab1CustomCell
            cell.collectionView.register(UINib(nibName: "MyQuestionThumbCell", bundle: nil), forCellWithReuseIdentifier: "ThumbCell")
            cell.collectionView.viewWithTag(indexPath.row)
            let layout = UICollectionViewFlowLayout()
            cell.collectionView.collectionViewLayout = layout
            if let listImagesArr = self.listImages_0[indexPath.row]{
                for item in listImagesArr{
                    if item.isEmpty{
                        cell.heightConstraint.constant = 0.0
                    }else{
                        cell.heightConstraint.constant = 65.0
                    }
                }
            }else{
                cell.heightConstraint.constant = 0.0
            }
            
            let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTapAvatar))
            gestureRecognizer.numberOfTapsRequired = 1
            cell.avatarImageView.isUserInteractionEnabled = true
            cell.avatarImageView.tag = indexPath.row
            cell.avatarImageView.accessibilityHint = "\(index)"
            cell.avatarImageView.addGestureRecognizer(gestureRecognizer)
            
            let questionNoAnswerForDoctorObj = self.questionNoAnswerForDoctor![indexPath.row]
            cell.registerBtn.layer.borderColor = UIColor.lightGray.cgColor
            cell.registerBtn.layer.borderWidth = 1.0
            if self.currentIndex == questionNoAnswerForDoctorObj.id{
                cell.answerBtn.isHidden = false
                cell.registerBtn.isHidden = true
                cell.answerBtn.tag = indexPath.row
                cell.answerBtn.addTarget(self, action: #selector(btnAnswerHandler), for: .touchUpInside)
            }else{
                cell.answerBtn.isHidden = true
                cell.registerBtn.isHidden = false
                cell.registerBtn.tag = indexPath.row
                cell.registerBtn.addTarget(self, action: #selector(btnRegisterHandler), for: .touchUpInside)
            }
            
            if let title = questionNoAnswerForDoctorObj.title?.trim()
            {
                cell.questionTitleLabel.text = title
            }
            if let content = questionNoAnswerForDoctorObj.content?.trim()
            {
                cell.questionLabel.text = content.components(separatedBy: "\n\n").filter { $0 != "" }.joined(separator: "\n")
                cell.questionLabel.lineBreakMode = .byWordWrapping
                cell.questionLabel.numberOfLines = 0
            }
            if let fullname = questionNoAnswerForDoctorObj.fullname
            {
                cell.nameLabel.text = fullname
            }
            if let dateCreate = questionNoAnswerForDoctorObj.dateCreate
            {
                let strDatetime = dateCreate.timeAgoSinceNow()
                cell.timeLabel.text = "Đã hỏi \(strDatetime)"
            }
            
            if let specialName = questionNoAnswerForDoctorObj.specialName
            {
                cell.specialistTag.text = specialName
            }
            
            if let avatar = questionNoAnswerForDoctorObj.avatar {
                let id = questionNoAnswerForDoctorObj.userID
                let urlString = avatar.trimmingCharacters(in: .whitespaces)
                if urlString.isEmpty{
                    cell.avatarImageView.update(image: UIImage(named:"avatar_default"), animated: true)
                }else{
                    let avatarURL = API.baseURLImage + "\(API.iCNMImage)" + "\(id)" + "/\(urlString)"
                    if let otherAvatarURL = URL(string: avatarURL) {
                        cell.avatarImageView.imageURL(URL: otherAvatarURL)
                    }
                }
            }else{
                cell.avatarImageView.update(image: UIImage(named:"avatar_default"), animated: true)
            }
            
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Tab2Cell", for: indexPath) as! Tab2CustomCell
            cell.collectionView.register(UINib(nibName: "MyQuestionThumbCell", bundle: nil), forCellWithReuseIdentifier: "ThumbCell")
            cell.collectionView.viewWithTag(indexPath.row)
            cell.collectionView.reloadData()
            if let listImagesArr = self.listImages[indexPath.row]{
                for item in listImagesArr{
                    if item.isEmpty{
                        cell.heightConstraint.constant = 0.0
                    }else{
                        cell.heightConstraint.constant = 65.0
                    }
                }
            }else{
                cell.heightConstraint.constant = 0.0
            }
            
            let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTapAvatar))
            gestureRecognizer.numberOfTapsRequired = 1
            cell.avatarImageView.isUserInteractionEnabled = true
            cell.avatarImageView.tag = indexPath.row
            cell.avatarImageView.accessibilityHint = "\(index)"
            cell.avatarImageView.addGestureRecognizer(gestureRecognizer)
            
            let questionReceivedForDoctorObj = self.questionReceivedForDoctors![indexPath.row]
            cell.warningLabel?.font = UIFont.fontAwesome(ofSize: 13.0)
            var warningStr = ""
            let isCheck = self.listStatusTime![indexPath.row]
            // let strStatusTime = self.listStatusLabel![indexPath.row]
            // if self.currentUser?.id == questionReceivedForDoctorObj.userID{
            if isCheck{
                warningStr = String.fontAwesomeIcon(name: .exclamationCircle) + "  Đã quá hạn trả lời"
                cell.warningLabel.text = warningStr
                cell.warningLabel.textColor = UIColor.red
                cell.answerBtn.alpha = 0.5
            }else{
                if let dateCreate = questionReceivedForDoctorObj.expireTime
                {
                    let strDatetime = dateCreate.timeAgoSinceNow()
                    warningStr = String.fontAwesomeIcon(name: .checkCircle) + "  Đã nhận " + "\(strDatetime)"
                    cell.answerBtn.tag = indexPath.row
                    cell.answerBtn.alpha = 1
                    cell.answerBtn.addTarget(self, action: #selector(btnAnswerHandler), for: .touchUpInside)
                    cell.warningLabel.text = warningStr
                    cell.warningLabel.textColor = UIColor(hex:"1976D2")
                }
            }
            //   }
            if let title = questionReceivedForDoctorObj.title?.trim()
            {
                cell.questionTitleLabel.text = title
            }
            if let content = questionReceivedForDoctorObj.content?.trim()
            {
                cell.questionLabel.text = content.components(separatedBy: "\n\n").filter { $0 != "" }.joined(separator: "\n")
                cell.questionLabel.lineBreakMode = .byWordWrapping
                cell.questionLabel.numberOfLines = 0
            }
            if let fullname = questionReceivedForDoctorObj.fullname
            {
                cell.nameLabel.text = fullname
            }
            if let dateCreate = questionReceivedForDoctorObj.dateCreate
            {
                let strDatetime = dateCreate.timeAgoSinceNow()
                cell.timeLabel.text = "Đã hỏi \(strDatetime)"
            }
            
            if let specialName = questionReceivedForDoctorObj.specialName
            {
                cell.specialistTag.text = specialName
            }
            
            if let avatar = questionReceivedForDoctorObj.avatar {
                let id = questionReceivedForDoctorObj.userID
                let urlString = avatar.trimmingCharacters(in: .whitespaces)
                let avatarURL = API.baseURLImage + "\(API.iCNMImage)" + "\(id)" + "/\(urlString)"
                if let otherAvatarURL = URL(string: avatarURL) {
                    cell.avatarImageView.imageURL(URL: otherAvatarURL)
                }
            }else{
                cell.avatarImageView.update(image: UIImage(named:"avatar_default"), animated: true)
            }
            return cell
            
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "Tab3Cell", for: indexPath) as! Tab3CustomCell
            cell.collectionView.register(UINib(nibName: "MyQuestionThumbCell", bundle: nil), forCellWithReuseIdentifier: "ThumbCell")
            cell.collectionView.viewWithTag(indexPath.row)
            if let listImagesArr = self.listImages_1[indexPath.row]{
                for item in listImagesArr{
                    if item.isEmpty{
                        cell.heightConstraint.constant = 0.0
                    }else{
                        cell.heightConstraint.constant = 65.0
                    }
                }
            }else{
                cell.heightConstraint.constant = 0.0
            }
            
            let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.handleTapAvatar))
            gestureRecognizer.numberOfTapsRequired = 1
            cell.avatarImageView.isUserInteractionEnabled = true
            cell.avatarImageView.tag = indexPath.row
            cell.avatarImageView.accessibilityHint = "\(index)"
            cell.avatarImageView.addGestureRecognizer(gestureRecognizer)
            
            let questionAnswerForDoctorObj = self.questionAnswerForDoctors![indexPath.row]
            cell.warningLabel?.font = UIFont.fontAwesome(ofSize: 13.0)
            if let approver = questionAnswerForDoctorObj.approver{
                if approver{
                    let approverTime = questionAnswerForDoctorObj.approverTime?.timeAgoSinceNow()
                    if let str = approverTime{
                        let warningStr = String.fontAwesomeIcon(name: .checkCircle) + "  Đã duyệt " + str
                        cell.warningLabel.text = warningStr
                        cell.warningLabel.textColor = UIColor.blue
                    }else{
                        let warningStr = String.fontAwesomeIcon(name: .checkCircle) + "  Đã duyệt "
                        cell.warningLabel.text = warningStr
                        cell.warningLabel.textColor = UIColor.blue
                    }
                }else{
                    let warningStr = String.fontAwesomeIcon(name: .exclamationCircle) + "  Chưa duyệt"
                    cell.warningLabel.text = warningStr
                    cell.warningLabel.textColor = UIColor.red
                }
            }
            
            if let title = questionAnswerForDoctorObj.title?.trim()
            {
                cell.questionTitleLabel.text = title
            }
            if let content = questionAnswerForDoctorObj.content?.trim()
            {
                cell.questionLabel.text = content.components(separatedBy: "\n\n").filter { $0 != "" }.joined(separator: "\n")
                cell.questionLabel.lineBreakMode = .byWordWrapping
                cell.questionLabel.numberOfLines = 0
            }
            if let fullname = questionAnswerForDoctorObj.fullname
            {
                cell.nameLabel.text = fullname
            }
            if let dateCreate = questionAnswerForDoctorObj.dateCreate
            {
                let strDatetime = dateCreate.timeAgoSinceNow()
                cell.timeLabel.text = "Đã hỏi \(strDatetime)"
            }
            
            if let specialName = questionAnswerForDoctorObj.specialName
            {
                cell.specialistTag.text = specialName
            }
            
            if let avatar = questionAnswerForDoctorObj.avatar {
                let id = questionAnswerForDoctorObj.userID
                let urlString = avatar.trimmingCharacters(in: .whitespaces)
                let avatarURL = API.baseURLImage + "\(API.iCNMImage)" + "\(id)" + "/\(urlString)"
                if let otherAvatarURL = URL(string: avatarURL) {
                    cell.avatarImageView.imageURL(URL: otherAvatarURL)
                }
            }else{
                cell.avatarImageView.update(image: UIImage(named:"avatar_default"), animated: true)
            }
            
            if let answerDoctor = try! questionAnswerForDoctorObj.answerContent?.convertHtmlSymbols(){
                cell.answerDoctorLabel.text = answerDoctor
                cell.answerDoctorLabel.lineBreakMode = .byWordWrapping
                cell.answerDoctorLabel.numberOfLines = 0
            }
            
            if let answerDoctor = questionAnswerForDoctorObj.answerDateCreate{
                let strDatetime = answerDoctor.timeAgoSinceNow()
                cell.timeDoctorLabel.text = "Đã trả lời \(strDatetime)"
            }
            
            if let doctorName = currentUser?.userDoctor?.userInfo?.name{
                cell.nameDoctorLabel.text = "Bác sĩ \(doctorName)"
            }
            
            if let avatar_doctor = currentUser?.userDoctor?.userInfo?.avatar, let id = currentUser?.id {
                let urlString = avatar_doctor.trimmingCharacters(in: .whitespaces)
                let avatarURL = API.baseURLImage + "\(API.iCNMImage)" + "\(id)" + "/\(urlString)"
                if let otherAvatarURL = URL(string: avatarURL) {
                    cell.avatarDoctorImageView.imageURL(URL: otherAvatarURL)
                }
            }
            
            return cell
        default:
            return UITableViewCell.init()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        let index = scopeSelected.selectedSegmentIndex
        if index == 0{
            if self.questionNoAnswerForDoctor!.count > 0{
                return UITableViewAutomaticDimension
            }else{
                return 0
            }
        }else if index == 1{
            if self.questionReceivedForDoctors!.count > 0{
                return UITableViewAutomaticDimension
            }else{
                return 0
            }
        }else{
            if self.questionAnswerForDoctors!.count > 0{
                return UITableViewAutomaticDimension
            }else{
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = scopeSelected.selectedSegmentIndex
        if index == 0{
            
        }else if index == 1{
            
        }else{
            let questionAnswerForDoctorObj = self.questionAnswerForDoctors![indexPath.row]
            let detailAnswerDoctorVC = DetailAnswerDoctorVC(nibName: "DetailAnswerDoctorVC", bundle: nil)
            detailAnswerDoctorVC.currentTag = index
            detailAnswerDoctorVC.resultTime = self.resultTime!
            detailAnswerDoctorVC.delegate = self
            detailAnswerDoctorVC.questionAnswerForDoctors = questionAnswerForDoctorObj
            //self.navigationController?.pushViewController(detailAnswerDoctorVC, animated: true)
            detailAnswerDoctorVC.modalPresentationStyle = .custom
            detailAnswerDoctorVC.transitioningDelegate = self
            self.present(detailAnswerDoctorVC, animated: true, completion: nil)
        }
    }
    
    func btnAnswerHandler(sender: UIButton) {
        let index = sender.tag
        var questionAnswerForDoctorObj:QuestionAnswerForDoctor? = nil
        if scopeSelected.selectedSegmentIndex == 0{
            questionAnswerForDoctorObj = questionNoAnswerForDoctor![index]
        }else if scopeSelected.selectedSegmentIndex == 1{
            questionAnswerForDoctorObj = questionReceivedForDoctors![index]
        }
        
        let detailAnswerDoctorVC = DetailAnswerDoctorVC(nibName: "DetailAnswerDoctorVC", bundle: nil)
        detailAnswerDoctorVC.currentTag = scopeSelected.selectedSegmentIndex
        detailAnswerDoctorVC.resultTime = self.resultTime!
        detailAnswerDoctorVC.delegate = self
        detailAnswerDoctorVC.questionAnswerForDoctors = questionAnswerForDoctorObj
        //self.navigationController?.pushViewController(detailAnswerDoctorVC, animated: true)
        detailAnswerDoctorVC.modalPresentationStyle = .custom
        detailAnswerDoctorVC.transitioningDelegate = self
        self.present(detailAnswerDoctorVC, animated: true, completion: nil)
    }
    
//    func btnMessageWarning(sender: UIButton) {
//        showAlertView(title: "Đã quá hạn trả lời câu hỏi này", view: self)
//        return
//    }
    
    func btnRegisterHandler(sender: UIButton) {
        var optionMenu:UIAlertController? = nil
        if IS_IPAD{
            optionMenu = UIAlertController(title: "Thông báo", message: "Bạn có chắc chắn muốn trả lời câu hỏi này không?", preferredStyle: .alert)
        }else{
            optionMenu = UIAlertController(title: "Thông báo", message: "Bạn có chắc chắn muốn trả lời câu hỏi này không?", preferredStyle: .alert)
        }
        optionMenu?.addAction(UIAlertAction(title: "Đồng ý", style: .default, handler: { _ in
            let index = sender.tag
            let questionNoAnswerForDoctorObj = self.questionNoAnswerForDoctor![index]
            if let user = self.currentUser{
                let questionID = questionNoAnswerForDoctorObj.id
                self.registerAnswerQuestion(index:index, questionID:questionID, doctorID:user.doctorID, questionNoAnswerForDoctor:questionNoAnswerForDoctorObj)
            }
        }))
        optionMenu?.addAction(UIAlertAction(title: "Huỷ", style: UIAlertActionStyle.cancel, handler: {(action:UIAlertAction!) in
            
        }))
        
        optionMenu?.popoverPresentationController?.sourceView = self.view
        self.present(optionMenu!, animated: true, completion: nil)
    }
    
    func registerAnswerQuestion(index: Int, questionID:Int, doctorID:Int, questionNoAnswerForDoctor:QuestionAnswerForDoctor){
        let progress = self.showProgress()
        QuestionAnswerForDoctor().registerAnswerQuestion(questionID:questionID, doctorID:doctorID, success: { (data) in
            if data != nil {
                    self.hideProgress(progress)
                    self.currentIndex = questionID
                    let indexPath = IndexPath(item: index, section: 0)
                    self.myTableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
                    var alert:UIAlertController? = nil
                    if IS_IPAD{
                        alert = UIAlertController(title: "Thông báo", message: "Bạn đã đăng ký nhận câu trả lời thành công. Thời gian trả lời cho câu hỏi này tối đa 60 phút. Xin cảm ơn!", preferredStyle: .alert)
                    }else{
                        alert = UIAlertController(title: "Thông báo", message: "Bạn đã đăng ký nhận câu trả lời thành công. Thời gian trả lời cho câu hỏi này tối đa 60 phút. Xin cảm ơn!", preferredStyle: .actionSheet)
                    }
                
                alert?.addAction(UIAlertAction(title: "Để sau", style: UIAlertActionStyle.cancel, handler: {    (action:UIAlertAction!) in
                    
                    }))
                alert?.addAction(UIAlertAction(title: "Trả lời ngay", style: .default, handler: { action in
                        self.myTableView.reloadData()
                        let detailAnswerDoctorVC = DetailAnswerDoctorVC(nibName: "DetailAnswerDoctorVC", bundle: nil)
                        detailAnswerDoctorVC.currentTag = 0
                        detailAnswerDoctorVC.resultTime = 0
                        detailAnswerDoctorVC.delegate = self
                        detailAnswerDoctorVC.questionAnswerForDoctors = questionNoAnswerForDoctor
                        detailAnswerDoctorVC.modalPresentationStyle = .custom
                        detailAnswerDoctorVC.transitioningDelegate = self
                        self.present(detailAnswerDoctorVC, animated: true, completion: nil)
                    }))

                    
                self.present(alert!, animated: true, completion: nil)
            } else {
                self.hideProgress(progress)
                showAlertView(title: "Đăng ký nhận câu trả lời thất bại", view: self)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func reloadDataAnswerDoctor(currentTag:Int){
        if currentTag == 2{
            if let user = currentUser{
                questionAnswerForDoctors = [QuestionAnswerForDoctor]()
                self.getQuestionAnswerForDoctor(doctorID:user.doctorID, pageNumber:1)
            }
        }else{
            scopeSelected.selectedSegmentIndex = 2
        }
        showAlertView(title: "Bạn đã trả lời câu hỏi thành công. Bạn có thể chỉnh sửa trong khoảng thời gian trả lời câu hỏi. Xin cảm ơn!", view: self)
    }
    
    func handleTapAvatar(_ sender: UITapGestureRecognizer) {
        let tab:Int = Int(sender.view!.accessibilityHint!)!
        let index = sender.view?.tag
        var questionAnswerForDoctorObj:QuestionAnswerForDoctor? = nil
        if tab == 0{
            questionAnswerForDoctorObj = self.questionNoAnswerForDoctor![index!]
        }else if tab == 1{
            questionAnswerForDoctorObj = self.questionReceivedForDoctors![index!]
        }else{
            questionAnswerForDoctorObj = self.questionAnswerForDoctors![index!]
        }
        
        let userInfo = UserInfo()
        userInfo.id = (questionAnswerForDoctorObj?.userID)!
        userInfo.name = questionAnswerForDoctorObj?.fullname
        userInfo.address = questionAnswerForDoctorObj?.address
        userInfo.email = questionAnswerForDoctorObj?.email
        userInfo.avatar = questionAnswerForDoctorObj?.avatar
        userInfo.phone = questionAnswerForDoctorObj?.phone
        self.showProfileCurrentUser(user: userInfo)
    }
    
    func showProfileCurrentUser(user:UserInfo){
        let profileDoctorVC = ProfileDoctorVC(nibName: "ProfileDoctorVC", bundle: nil)
        let featuredDoctor = FeaturedDoctor()
        featuredDoctor.userInfo = user
        featuredDoctor.doctor = nil
        featuredDoctor.specialists = nil
        profileDoctorVC.featuredDoctor = featuredDoctor
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(profileDoctorVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let index = scopeSelected.selectedSegmentIndex
        if index == 0{
            guard let tableViewCell = cell as? Tab1CustomCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        }else if index == 1{
            guard let tableViewCell = cell as? Tab2CustomCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        }else{
            guard let tableViewCell = cell as? Tab3CustomCell else { return }
            tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
            tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let index = scopeSelected.selectedSegmentIndex
        if index == 0{
            guard let tableViewCell = cell as? Tab1CustomCell else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        }else if index == 1{
            guard let tableViewCell = cell as? Tab2CustomCell else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        }else{
            guard let tableViewCell = cell as? Tab3CustomCell else { return }
            storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentY = scrollView.contentOffset.y
        currentOffset_Y = currentY
        let currentBottomY = scrollView.frame.size.height + currentY
        if currentY > lastY {
            //"scrolling down"
            myTableView.bounces = true
        } else {
            //"scrolling up"
            // Check that we are not in bottom bounce
            if currentBottomY < scrollView.contentSize.height + scrollView.contentInset.bottom {
                myTableView.bounces = false
            }
        }
        lastY = scrollView.contentOffset.y
    }
    
    func resultListImageURL(questionAnswer:[QuestionAnswerForDoctor])->[Int: [String]]{
        var result = [Int: [String]]()
        for i in 0..<Int(questionAnswer.count){
            let urlString = questionAnswer[i].questionImage?.components(separatedBy: ";")
            if urlString != nil{
                for url in urlString! {
                    if !url.isEmpty || url != ""{
                        let urlString = url.trimmingCharacters(in: .whitespaces)
                        let questionID = questionAnswer[i].id
                        let urlBase = API.baseURLImage + "\(API.iCNMImage)" +
                                "QuestionImage/\(questionID)/" + "\(urlString)"
                        if result[i] == nil {
                            result[i] = [String]()
                        }
                        result[i]?.append(urlBase)
                        
                    }
                }
            }
        }
        return result
    }
}

extension ManagerQuestionDoctorVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        let index = scopeSelected.selectedSegmentIndex
        let tag = collectionView.tag
        switch index {
        case 0:
            if tag >= self.listImages_0.count{
                return 0
            }
            if let result = self.listImages_0[tag]{
                return result.count
            }
            return 0
            
        case 1:
            if tag >= self.listImages.count{
                return 0
            }
            if let result = self.listImages[tag]{
                return result.count
            }
            return 0
        case 2:
            if tag >= self.listImages_1.count{
                return 0
            }
            if let result = self.listImages_1[tag]{
                return result.count
            }
            return 0
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.collectionViewLayout.invalidateLayout()
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThumbCell", for: indexPath) as! MyQuestionThumbCell
        cell.contentView.layer.cornerRadius = 4.0
        cell.contentView.layer.masksToBounds = true
        cell.contentView.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        cell.contentView.layer.borderWidth = 1.0
        let tag = collectionView.tag
        let index = scopeSelected.selectedSegmentIndex
        cell.imageThumb.contentMode = .scaleToFill
        
        if index == 0{
            let imgStr = self.listImages_0[tag]![indexPath.row]
            if imgStr.range(of:".jpg") != nil {
                cell.imageThumb.loadImageUsingUrlString(urlString: imgStr)
            }else{
                cell.imageThumb.loadImageUsingUrlString(urlString: imgStr + ".jpg")
            }
        }else if index == 1{
            let imgStr = self.listImages[tag]![indexPath.row]
            if imgStr.range(of:".jpg") != nil {
                cell.imageThumb.loadImageUsingUrlString(urlString: imgStr)
            }else{
                cell.imageThumb.loadImageUsingUrlString(urlString: imgStr + ".jpg")
            }
        }else if index == 2{
            let imgStr = self.listImages_1[tag]![indexPath.row]
            if imgStr.range(of:".jpg") != nil {
                cell.imageThumb.loadImageUsingUrlString(urlString: imgStr)
            }else{
                cell.imageThumb.loadImageUsingUrlString(urlString: imgStr + ".jpg")
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let index = scopeSelected.selectedSegmentIndex
        let tag = collectionView.tag
        var images:[String]? = nil
        if index == 0{
            images = self.listImages_0[tag]! as [String]
        }else if index == 1{
            images = self.listImages[tag]! as [String]
        }else{
            images = self.listImages_1[tag]! as [String]
        }
        
        let urlString = images![indexPath.row]
//        let imageView = CustomImageView()
        let url = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if url?.range(of:".jpg") != nil {
            if let imgUrl = URL(string: url!) {
                //                imageView.loadImageUsingUrlString(urlString: url!)
                let photoSlider = PhotoSlider.ViewController(imageURLs: [imgUrl])
                photoSlider.modalPresentationStyle = .overCurrentContext
                photoSlider.modalTransitionStyle = .crossDissolve
                present(photoSlider, animated: true, completion: nil)
            }
        } else{
            if let imgUrl = URL(string: url! + ".jpg") {
                //                imageView.loadImageUsingUrlString(urlString: url! + ".jpg")
                let photoSlider = PhotoSlider.ViewController(imageURLs: [imgUrl])
                photoSlider.modalPresentationStyle = .overCurrentContext
                photoSlider.modalTransitionStyle = .crossDissolve
                present(photoSlider, animated: true, completion: nil)
            }
        }
        
//        let frame = CGRect(x: 0, y: 0, width: screenSizeWidth, height: screenSizeHeight)
//        imageView.frame = frame
//        imageView.contentMode = .scaleAspectFit
//        imageView.backgroundColor = .black
//        imageView.isUserInteractionEnabled = true
//        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
//        imageView.addGestureRecognizer(tap)
//        var height:CGFloat = 0.0
//        if IS_IPHONE_X{
//            height = 76.0
//        }else{
//            height = CGFloat((self.navigationController?.navigationBar.frame.size.height)!)
//        }
//        let imageClose = UIImageView(frame: CGRect(x: screenSizeWidth-40, y: height+25, width: 25, height: 25))
//        let image = UIImage(named: "icon_close")?.maskWithColor(color: UIColor(hexColor: 0xFF1E17, alpha: 1.0))
//        imageClose.image = image
//        imageView.addSubview(imageClose)
//        self.navigationController?.navigationBar.layer.zPosition = -1
//        self.view .addSubview(imageView)
    }
    
    // Use to back from full mode
    func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        let view = sender.view!
        view .removeFromSuperview()
        self.navigationController?.navigationBar.layer.zPosition = 0
    }
}
