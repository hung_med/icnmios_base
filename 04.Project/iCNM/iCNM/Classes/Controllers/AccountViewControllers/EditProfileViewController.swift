//
//  EditProfileTableViewController.swift
//  iCNM
//
//  Created by Medlatec on 5/12/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import FontAwesome_swift

class EditProfileViewController: BaseViewController, FWComboBoxDelegate, UITableViewDelegate, UITableViewDataSource, SpecialistPopupViewControllerDelegate,WorkExperiencePopupViewControllerDelegate, EducationPopupViewControllerDelegate,PrizePopupViewControllerDelegate, UIScrollViewDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, FWDatePickerDelegate, CropViewControllerDelegate {
    
    @IBOutlet weak var buttonTopSpaceForCommonUser: NSLayoutConstraint!
    
    @IBOutlet weak var buttonTopSpaceForDoctor: NSLayoutConstraint!
    
    @IBOutlet weak var specialistLabel: UILabel!
    
    @IBOutlet weak var addSpecialistButton: UIButton!
    
    @IBOutlet weak var workExperienceLabel: UILabel!
    
    @IBOutlet weak var addWorkExperienceButton: UIButton!
    
    @IBOutlet weak var educationLabel: UILabel!
    
    @IBOutlet weak var addEducationButton: UIButton!
    
    @IBOutlet weak var prizeLabel: UILabel!
    
    @IBOutlet weak var addPrizeButton: UIButton!
    @IBOutlet weak var updateAvatarButton: UIButton!
    @IBOutlet weak var updatePassButton: UIButton!
    
    var imageView = UIImageView()
    private var image: UIImage?
    private var croppingStyle = CropViewCroppingStyle.default
    private var croppedRect = CGRect.zero
    private var croppedAngle = 0
    
    private var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    private var provinces:[Province]? {
        didSet {
            populateProvinceComboBox()
        }
    }
    
    private var districts:[District]? {
        didSet {
            populateDistrictComboBox()
        }
    }
    
    private var specialistSelect=[SpecialistModel]()
    private var specialistSelected=[SpecialistModel]()
    
    private var workExperiences = [WorkExperience]()
    private var educations = [Education]()
    private var prizes = [Prize]()
    
    private var currentProvinceIndex:Int?
    private var currentDistrictIndex:Int?
    private var currentTextField:UITextField?
    
    
    @IBOutlet weak var scrollView: FWCustomScrollView!
    
    @IBOutlet weak var avatarImageView: PASImageView!
    @IBOutlet weak var nameTextField: FWFloatingLabelTextField!
    
    @IBOutlet weak var phoneTextField: FWFloatingLabelTextField!
    
    @IBOutlet weak var emailTextField: FWFloatingLabelTextField!
    
    @IBOutlet weak var birthdayButton: FWDatePicker!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var maleRadioButton: ISRadioButton!
    @IBOutlet weak var femaleRadioButton: ISRadioButton!
    @IBOutlet weak var addressTextField: FWFloatingLabelTextField!
    @IBOutlet weak var provinceComboBox: FWComboBox!
    @IBOutlet weak var districtComboBox: FWComboBox!
    @IBOutlet weak var jobTextField: FWFloatingLabelTextField!
    
    @IBOutlet weak var workplaceTextField: FWFloatingLabelTextField!
    @IBOutlet weak var educationTextField: FWFloatingLabelTextField!
    
    @IBOutlet weak var aboutMeTextField: FWFloatingLabelTextField!
    
    @IBOutlet weak var specialistTableView: FWTableView!
    @IBOutlet weak var workExperiencesTableView: FWTableView!
    
    @IBOutlet weak var educationTableView: FWTableView!
    @IBOutlet weak var prizeTableView: FWTableView!
    
    @IBAction func touchScreen(_ sender: Any) {
        currentTextField?.resignFirstResponder()
    }
    
    let picker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerForNotifications()
        scrollView.delegate = self
        picker.delegate = self
        provinceComboBox.delegate = self
        districtComboBox.delegate = self
        districtComboBox.defaultTitle = "Chọn Quận Huyện"
        provinceComboBox.defaultTitle = "Chọn tỉnh thành phố"
        birthdayButton.maximumDate = Date()
        birthdayButton.delegate = self
        //btnUpdate.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0.0).isActive = true

        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        }
        if let doctorID = currentUser?.doctorID, doctorID > 0 {
            specialistTableView.dataSource = self
            specialistTableView.delegate = self
            
            workExperiencesTableView.dataSource = self
            workExperiencesTableView.delegate = self
            workExperiencesTableView.estimatedRowHeight = 44
            workExperiencesTableView.rowHeight = UITableViewAutomaticDimension
            
            educationTableView.dataSource = self
            educationTableView.delegate = self
            educationTableView.estimatedRowHeight = 44
            educationTableView.rowHeight = UITableViewAutomaticDimension
            
            prizeTableView.dataSource = self
            prizeTableView.delegate = self
            prizeTableView.estimatedRowHeight = 44
            prizeTableView.rowHeight = UITableViewAutomaticDimension
        } else {
            specialistLabel.isHidden = true
            specialistTableView.isHidden = true
            addSpecialistButton.isHidden = true
            workExperienceLabel.isHidden = true
            workExperiencesTableView.isHidden = true
            addWorkExperienceButton.isHidden = true
            prizeLabel.isHidden = true
            prizeTableView.isHidden = true
            addPrizeButton.isHidden = true
            educationLabel.isHidden = true
            addEducationButton.isHidden = true
        }
        
        //emailTextField.addRegx(strRegx: "[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}", errorMsg: "Địa chỉ email không hợp lệ")
        nameTextField.delegate = self
        phoneTextField.delegate = self
        emailTextField.delegate = self
        addressTextField.delegate = self
        workplaceTextField.delegate = self
        jobTextField.delegate = self
        educationTextField.delegate = self
        aboutMeTextField.delegate = self
        
        if let verifyEmail = currentUser?.userDoctor?.userInfo?.verifyEmail, verifyEmail == true {
            emailTextField.isEnabled = false
        }
        if let verifyPhone = currentUser?.userDoctor?.userInfo?.verifyPhone, verifyPhone == true {
            phoneTextField.isEnabled = false
        }
        
        let selectedSpecialistIDs = currentUser?.doctorInformation?.doctorSpecialists.map({ (doctorSpecialist) -> Int in
            return doctorSpecialist.specialID
        })
        if let specialists = currentUser?.doctorInformation?.specialists {
            for specialist in specialists {
                specialistSelect.append(SpecialistModel(title: specialist.name ?? "", isSelected: selectedSpecialistIDs?.contains(specialist.id) ?? false, isUserSelectEnable: true, specialist: specialist))
            }
        }
        specialistSelected = specialistSelect.filter({ (specialistModel) -> Bool in
            return specialistModel.isSelected
        })
        
        currentUser?.doctorInformation?.workExperiences.forEach({ (workExprience) in
            self.workExperiences.append(WorkExperience(value:workExprience))
        })
        
        currentUser?.doctorInformation?.educateds.forEach({ (workExprience) in
            self.educations.append(Education(value:workExprience))
        })
        
        currentUser?.doctorInformation?.prizes.forEach({ (prize) in
            self.prizes.append(Prize(value:prize))
        })
        
        updateAvatarButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 14.0)
        updatePassButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 14.0)
        let avatarBtnTitle = String.fontAwesomeIcon(name: .plus) + "  Cập nhật ảnh đại diện"
        let passBtnTitle = String.fontAwesomeIcon(name: .lock) + "  Đổi mật khẩu"
        updateAvatarButton.setTitle(avatarBtnTitle, for: .normal)
        updatePassButton.setTitle(passBtnTitle, for: .normal)
        populateData()
        requestData()
        self.searchBar.isHidden = true
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
    }
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.layoutImageView()
        if let doctorID = currentUser?.doctorID, doctorID > 0 {
            buttonTopSpaceForDoctor.isActive = true
            buttonTopSpaceForCommonUser.isActive = false
        } else {
            buttonTopSpaceForDoctor.isActive = false
            buttonTopSpaceForCommonUser.isActive = true
        }
    }
    
    override func requestData() {
        let activityIndicator = self.showProgress()
        Province.requestAllProvinces(success: { (provinces) in
            self.provinces = provinces
            if let currentProvinceID = self.currentUser?.userDoctor?.userInfo?.provinceID {
                if let province = self.provinces?.filter({ (province) -> Bool in
                    return province.id == currentProvinceID
                }).first {
                    province.requestAllDistrict(success: { (districts) in
                        self.districts = districts
                        self.hideProgress(activityIndicator)
                        self.populateDistrictComboBox()
                    }, fail: { (error, response) in
                        self.hideProgress(activityIndicator)
                        self.handleNetworkError(error: error, responseData: response.data)
                    })
                } else {
                    self.hideProgress(activityIndicator)
                }
            } else {
                self.hideProgress(activityIndicator)
            }
        }) { (error, response) in
            self.hideProgress(activityIndicator)
            self.handleNetworkError(error: error, responseData: response.data)
        }
    }
    
    func requestDistricts() {
        if let provinceIndex = currentProvinceIndex {
            if let provincesList = self.provinces {
                let province = provincesList[provinceIndex]
                let activityIndicator = self.showProgress()
                province.requestAllDistrict(success: { (districts) in
                    self.districts = districts
                    self.hideProgress(activityIndicator)
                    self.populateDistrictComboBox()
                }, fail: { (error, response) in
                    self.hideProgress(activityIndicator)
                    self.handleNetworkError(error: error, responseData: response.data)
                })
            }
        }
    }
    
    private func populateProvinceComboBox() {
        if let provinces = self.provinces {
            let activeProvinces = provinces.filter({ (province) -> Bool in
                return province.active
            })
            provinceComboBox.dataSource = activeProvinces.map({ (province) -> String in
                return province.name ?? ""
            })
            if let currentProvinceID = currentUser?.userDoctor?.userInfo?.provinceID, currentProvinceIndex == nil {
                for (index, province) in activeProvinces.enumerated() {
                    if province.id == currentProvinceID {
                        currentProvinceIndex = index
                        break
                    }
                }
            }
            provinceComboBox.selectRow(at: currentProvinceIndex)
        } else {
            provinceComboBox.selectRow(at: nil)
            provinceComboBox.dataSource = [String]()
        }
    }
    
    private func populateDistrictComboBox() {
        if let districts = self.districts {
            districtComboBox.dataSource = districts.map({ (district) -> String in
                return district.name ?? ""
            })
            if let currentDistrictID = currentUser?.userDoctor?.userInfo?.districtID, currentDistrictIndex == nil {
                for (index, district) in districts.enumerated() {
                    if district.id == currentDistrictID {
                        currentDistrictIndex = index
                        break
                    }
                }
            }
            districtComboBox.selectRow(at: currentDistrictIndex)
        } else {
            districtComboBox.selectRow(at: nil)
            districtComboBox.dataSource = [String]()
        }
    }
    
    private func populateData() {
        
        if let userInfo = currentUser?.userDoctor?.userInfo {
            nameTextField.text = userInfo.name
            if let avatarURL = userInfo.getAvatarURL() {
                avatarImageView.imageURL(URL:avatarURL)
            }
            if let phoneNumber = userInfo.phone {
                phoneTextField.text = phoneNumber
            }
            if let emailAddress = userInfo.email {
                emailTextField.text = emailAddress
            }
            if let birthday = userInfo.birthday {
                birthdayButton.currentDate = birthday
            }
            if let gender = userInfo.gender {
                if gender == "M" {
                    genderLabel.text = "Nam"
                    maleRadioButton.isSelected = true
                } else {
                    genderLabel.text = "Nữ"
                    femaleRadioButton.isSelected = true
                }
            } else {
                genderLabel.text = ""
                maleRadioButton.isSelected = false
                femaleRadioButton.isSelected = false
            }
            if let address = userInfo.address {
                addressTextField.text = address
            }
            
            if let job = userInfo.job {
                jobTextField.text = job.trimmingCharacters(in: .whitespacesAndNewlines)
            }
            
            if let workplace = userInfo.workplace {
                workplaceTextField.text = workplace.trimmingCharacters(in: .whitespacesAndNewlines)
            }
            
            if let education = userInfo.education {
                educationTextField.text = education.trimmingCharacters(in: .whitespacesAndNewlines)
            }
            
            if let aboutMe = userInfo.aboutMe {
                aboutMeTextField.text = aboutMe.trimmingCharacters(in: .whitespacesAndNewlines)
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func touchGenderRadioButton(_ sender: ISRadioButton) {
        if sender == maleRadioButton {
            genderLabel.text = "Nam"
        } else {
            genderLabel.text = "Nữ"
        }
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == StoryboardSegue.Profile.showAddSpecialistPopup.rawValue {
            let specialistPopup = segue.destination as! SpecialistPopupViewController
            specialistPopup.dataArray = self.specialistSelect
            specialistPopup.delegate = self
        } else if segue.identifier == StoryboardSegue.Profile.showEditWorkExperiencePopup.rawValue {
            if let cell = sender as? UITableViewCell, let indexPath = self.workExperiencesTableView.indexPath(for: cell) {
                let workExperience = workExperiences[indexPath.section]
                let desitinationVC = segue.destination as! WorkExperiencePopupViewController
                desitinationVC.workExperience = workExperience
                desitinationVC.delegate = self
                
            }
        } else if segue.identifier == StoryboardSegue.Profile.showEditEducation.rawValue {
            if let cell = sender as? UITableViewCell, let indexPath = self.educationTableView.indexPath(for: cell) {
                let education = educations[indexPath.section]
                let desitinationVC = segue.destination as! EducationPopupViewController
                desitinationVC.education = education
                desitinationVC.delegate = self
            }
        } else if segue.identifier == StoryboardSegue.Profile.showEditPrize.rawValue {
            if let cell = sender as? UITableViewCell, let indexPath = self.prizeTableView.indexPath(for: cell) {
                let prize = prizes[indexPath.section]
                let desitinationVC = segue.destination as! PrizePopupViewController
                desitinationVC.prize = prize
                desitinationVC.delegate = self
                
            }
        } else if segue.identifier == StoryboardSegue.Profile.showAddWorkExperiencePopup.rawValue {
            let desitinationVC = segue.destination as! WorkExperiencePopupViewController
            desitinationVC.delegate = self
        } else if segue.identifier == StoryboardSegue.Profile.showAddEducation.rawValue {
            let desitinationVC = segue.destination as! EducationPopupViewController
            desitinationVC.delegate = self
        } else if segue.identifier == StoryboardSegue.Profile.showAddPrize.rawValue {
            let desitinationVC = segue.destination as! PrizePopupViewController
            desitinationVC.delegate = self
        }
    }
    
    
    @IBAction func touchBackButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func touchUpdatePassWord(_ sender: Any) {
        let changePasswordVC = ChangePasswordVC(nibName: "ChangePasswordVC", bundle: nil)
        //changePasswordVC.delegate = self
        changePasswordVC.modalPresentationStyle = .custom
        changePasswordVC.view.frame = CGRect(x: screenSizeWidth/2-310/2, y: screenSizeHeight/2-376/2, width: 310, height: 376)
        self.present(changePasswordVC, animated: true, completion: nil)
    }
    
    func fwComboBox(comboBox: FWComboBox, didSelectAtIndex index: Int) {
        if comboBox == provinceComboBox {
            currentProvinceIndex = index
            currentDistrictIndex = nil
            requestDistricts()
        } else if comboBox == districtComboBox {
            currentDistrictIndex = index
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == nameTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == phoneTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 12
        }else if textField == addressTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 100
        }else if textField == emailTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == workplaceTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 100
        }else if textField == jobTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 100
        }else if textField == aboutMeTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 100
        }else{
            return true
        }
    }
    
    //MARK: - FWTableView Datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == specialistTableView {
            return specialistSelected.count
        } else if tableView == workExperiencesTableView {
            return workExperiences.count
        } else if tableView == educationTableView {
            return educations.count
        } else if tableView == prizeTableView {
            return prizes.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == specialistTableView {
            //SpecialistCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "SpecialistCell", for: indexPath)
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15.0)
            cell.textLabel?.text = specialistSelected[indexPath.section].title
            return cell
        } else if tableView == workExperiencesTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "WorkExperienceCell", for: indexPath) as! EditProfileTableViewCell
            let workExperience = workExperiences[indexPath.section]
            var workExperiencesString = ""
            if let position = workExperience.position {
                workExperiencesString += " \t" + position + " "
            }
            if let company = workExperience.company {
                workExperiencesString += "tại " + company
            }
            if let startDate = workExperience.startDate {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy"
                workExperiencesString += "\n \tTừ " + dateFormatter.string(from: startDate)
            }
            if let endDate = workExperience.endDate {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy"
                workExperiencesString += " đến " + dateFormatter.string(from: endDate)
            }
            if let summary = workExperience.summary {
                workExperiencesString += "\n \t"+summary
            }
            cell.contentLabel.text = workExperiencesString
            cell.removeButton.tag = indexPath.section
            cell.removeButton.addTarget(self, action: #selector(removeWorkExperience), for: .touchUpInside)
            return cell
        } else if tableView == educationTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EducationCell", for: indexPath) as! EditProfileTableViewCell
            let education = educations[indexPath.section]
            var educationString = ""
            if let major = education.major {
                educationString += " \tHọc " + major + " "
            }
            if let schoolName = education.schoolName {
                educationString += "tại " + schoolName
            }
            if let startDate = education.startDate {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy"
                educationString += "\n \tTừ " + dateFormatter.string(from: startDate)
            }
            if let endDate = education.endDate {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy"
                educationString += " đến " + dateFormatter.string(from: endDate)
            }
            if let summary = education.summary {
                educationString += "\n \t"+summary
            }
            cell.contentLabel.text = educationString
            cell.removeButton.tag = indexPath.section
            cell.removeButton.addTarget(self, action: #selector(removeEducation), for: .touchUpInside)
            return cell
            
        } else if tableView == prizeTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PrizeCell", for: indexPath) as! EditProfileTableViewCell
            let prize = prizes[indexPath.section]
            var prizeString = ""
            if let name = prize.name {
                prizeString += " \tĐạt được " + name + " "
            }
            if let prizePlace = prize.prizePlace {
                prizeString += "\n \tTại " + prizePlace
            }
            if let date = prize.prizeTime {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy"
                prizeString += "\n \t" + dateFormatter.string(from: date)
            }
            cell.contentLabel.text = prizeString
            cell.removeButton.tag = indexPath.section
            cell.removeButton.addTarget(self, action: #selector(removePrize), for: .touchUpInside)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    //Delegate from popup select specialist
    func specialistDidUpdate(specialistPopup: SpecialistPopupViewController) {
        specialistSelected = specialistSelect.filter({ (specialistModel) -> Bool in
            return specialistModel.isSelected
        })
        if specialistSelected.count == 0{
            showAlertView(title: "Bạn vui lòng chọn chuyên khoa", view: self)
            return
        }
        self.specialistTableView.reloadData()
    }
    
    //Reload data for update workExperience, prize, education
    func workExperienceDidUpdate(specialistPopup: WorkExperiencePopupViewController, workExperience: WorkExperience?) {
        if let workExperience = workExperience {
            workExperience.doctorID = currentUser!.doctorID
            workExperiences.append(workExperience)
            workExperiencesTableView.reloadData()
        } else {
            workExperiencesTableView.reloadData()
        }
    }
    
    func educationDidUpdate(educationPopup: EducationPopupViewController, education: Education?) {
        if let education = education {
            education.doctorID = currentUser!.doctorID
            educations.append(education)
            educationTableView.reloadData()
        } else {
            educationTableView.reloadData()
        }
    }
    
    func prizePopupDidUpdate(prizePopup: PrizePopupViewController, prize: Prize?) {
        if let prize = prize {
            prize.doctorID = currentUser!.doctorID
            prizes.append(prize)
            prizeTableView.reloadData()
        } else {
            prizeTableView.reloadData()
        }
    }
    
    // remove Work Experience
    func removeWorkExperience(sender:UIButton) {
        let alertViewController = UIAlertController(title: "Thông báo", message: "Bạn có chắc chắn muốn xoá hay không?", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Có", style: UIAlertActionStyle.cancel, handler: { (action) in
            self.workExperiences.remove(at: sender.tag)
            self.workExperiencesTableView.reloadData()
        })
        alertViewController.addAction(yesAction)
        let noAction = UIAlertAction(title: "Không", style: UIAlertActionStyle.default, handler:nil)
        alertViewController.addAction(noAction)
        self.present(alertViewController, animated: true, completion: nil)
    }
    
    func removeEducation(sender:UIButton) {
        let alertViewController = UIAlertController(title: "Thông báo", message: "Bạn có chắc chắn muốn xoá hay không?", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Có", style: UIAlertActionStyle.cancel, handler: { (action) in
            self.educations.remove(at: sender.tag)
            self.educationTableView.reloadData()
        })
        alertViewController.addAction(yesAction)
        let noAction = UIAlertAction(title: "Không", style: UIAlertActionStyle.default, handler:nil)
        alertViewController.addAction(noAction)
        self.present(alertViewController, animated: true, completion: nil)
    }
    
    func removePrize(sender:UIButton) {
        let alertViewController = UIAlertController(title: "Thông báo", message: "Bạn có chắc chắn muốn xoá hay không?", preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Có", style: UIAlertActionStyle.cancel, handler: { (action) in
            self.prizes.remove(at: sender.tag)
            self.prizeTableView.reloadData()
        })
        alertViewController.addAction(yesAction)
        let noAction = UIAlertAction(title: "Không", style: UIAlertActionStyle.default, handler:nil)
        alertViewController.addAction(noAction)
        self.present(alertViewController, animated: true, completion: nil)
    }
    
    @IBAction func touchUpdateProfileButton(_ sender: UIButton) {
        if (nameTextField.validate() && phoneTextField.validate() && addressTextField.validate()) {
            if let currentUserInfo = currentUser?.userDoctor?.userInfo {
                let user = User()
                let userInfo = UserInfo(value: currentUserInfo)
                userInfo.name = nameTextField.text?.trim()
                userInfo.phone = phoneTextField.text?.trim()
                userInfo.email = emailTextField.text?.trim()
                userInfo.birthday = birthdayButton.currentDate
                userInfo.gender = maleRadioButton.isSelected ? "M":"F"
                userInfo.address = addressTextField.text?.trim()
                if let provinceIndex = currentProvinceIndex {
                    if let province = provinces?[provinceIndex] {
                        userInfo.provinceID = province.id
                    }
                } else {
                    userInfo.provinceID = nil
                }
                
                if let districtIndex = currentDistrictIndex {
                    if let district = districts?[districtIndex] {
                        userInfo.districtID = district.id
                    }
                } else {
                    userInfo.districtID = nil
                }
                userInfo.job = jobTextField.text?.trim()
                userInfo.education = educationTextField.text?.trim()
                userInfo.workplace = workplaceTextField.text?.trim()
                userInfo.aboutMe = aboutMeTextField.text?.trim()
                let userDoctor = UserDoctor()
                userDoctor.userInfo = userInfo
                if let doctor = currentUser?.userDoctor?.doctor {
                    userDoctor.doctor = Doctor(value: doctor)
                }
                user.userDoctor = userDoctor
                if let doctorID = currentUser?.doctorID, doctorID > 0 {
                    let doctorInformation = DoctorInformation()
                    var doctorSpecialists = [DoctorSpecialist] ()
                    for specialist in specialistSelected {
                        let doctorSpecialist = DoctorSpecialist()
                        doctorSpecialist.doctorID = doctorID
                        doctorSpecialist.specialID = specialist.specialist.id
                        doctorSpecialists.append(doctorSpecialist)
                    }
                    doctorInformation.doctorSpecialists.append(objectsIn: doctorSpecialists)
                    doctorInformation.educateds.append(objectsIn: educations)
                    doctorInformation.workExperiences.append(objectsIn: workExperiences)
                    doctorInformation.prizes.append(objectsIn: prizes)
                    user.doctorInformation = doctorInformation
                }
                
                let activityIndicator = self.showProgress()
                user.updateUser(success: {
                    self.hideProgress(activityIndicator)
                    let userTypeActionSheet = UIAlertController(title: "Thông báo", message: "Cập nhật thành công", preferredStyle: .alert)
                    let personalAccount = UIAlertAction(title: "Đóng", style: .default) { (action) in
                        self.navigationController?.popViewController(animated: true)
                    }
                    userTypeActionSheet.addAction(personalAccount)
                    self.present(userTypeActionSheet, animated: true, completion: nil)
                }, fail: { (error, response) in
                    self.hideProgress(activityIndicator)
                    self.handleNetworkError(error: error, responseData: response.data)
                })
                
            }
        }
    }
    
    
    //Keyboard handle
    @objc private func keyboardWillShow(aNotification:Notification) {
        if let curTextField = currentTextField {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            scrollView.contentInset = contentInset
            scrollView.scrollIndicatorInsets = contentInset
            let rect = curTextField.frame
            let test = view.bounds.height - (rect.size.height + rect.origin.y + scrollView.contentInset.top) - 60.0
            let different = keyboardFrame.size.height - test
            if different > 0 {
                scrollView.contentOffset.y = different - self.scrollView.contentInset.top
            }
        }
        
    }
    
    @objc private func keyboardWillHide(aNotification:Notification) {
        var contentInset:UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = 0
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentTextField = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        currentTextField = nil
        return true
    }
    
    @IBAction func updateAvatar(_ sender: Any) {
        var avatarActionSheet:UIAlertController? = nil
        if IS_IPAD{
            avatarActionSheet = UIAlertController(title: nil, message: "Ảnh đại diện", preferredStyle: .alert)
        }else{
            avatarActionSheet = UIAlertController(title: nil, message: "Ảnh đại diện", preferredStyle: .actionSheet)
        }
        
        let cameraAction = UIAlertAction(title: "Chụp ảnh mới", style: .default) { (action) in
            self.picker.sourceType = .camera
            self.picker.cameraCaptureMode = .photo
            self.picker.modalPresentationStyle = .fullScreen
            self.present(self.picker, animated: true, completion: nil)
        }
        let galleryAction = UIAlertAction(title: "Chọn ảnh từ thư viện", style: .default) { (action) in
            self.picker.sourceType = .photoLibrary
            self.picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.present(self.picker, animated: true, completion: nil)
        }
        let cancelAction = UIAlertAction(title: "Hủy", style: .cancel, handler: nil)
        avatarActionSheet?.addAction(cameraAction)
        avatarActionSheet?.addAction(galleryAction)
        avatarActionSheet?.addAction(cancelAction)
        
        present(avatarActionSheet!, animated: true, completion: nil)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        self.croppedRect = cropRect
        self.croppedAngle = angle
        updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    public func updateImageViewWithImage(_ image: UIImage, fromCropViewController cropViewController: CropViewController) {
        imageView.image = image
        layoutImageView()
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        if cropViewController.croppingStyle != .circular {
            imageView.isHidden = true
            
            cropViewController.dismissAnimatedFrom(self, withCroppedImage: image,
                                                   toView: imageView,
                                                   toFrame: CGRect.zero,
                                                   setup: { self.layoutImageView() },
                                                   completion: { self.imageView.isHidden = false })
            
            let progress = showProgress()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyyMMdd_hhmmss"
            if let curUser = currentUser, let avatar = UIImage.resizeImage(image: imageView.image!, toWidth: 512), let imageData = UIImagePNGRepresentation(avatar) {
                let avatarName = "avatar_\(curUser.id)_" + dateFormatter.string(from: Date())
                let parameterDic:[String:Any] = [
                    "UserID":curUser.id,
                    "Name": avatarName,
                    "Base64String": imageData.base64EncodedString()
                ]
                curUser.updateAvatar(parameterDic: parameterDic, success: { (responseString) in
                    self.hideProgress(progress)
                    if responseString.contains("Done") {
                        let realm = try! Realm()
                        try! realm.write {
                            curUser.userDoctor?.userInfo?.avatar = avatarName+".jpg"
                        }
                        if let avatarURL = curUser.userDoctor?.userInfo?.getAvatarURL() {
                            self.avatarImageView.reset()
                            self.avatarImageView.imageURL(URL:avatarURL)
                        }
                    } else {
                        let alertViewController = UIAlertController(title: "Thông báo", message: "Có Lỗi xảy ra!", preferredStyle: .alert)
                        let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                        alertViewController.addAction(noAction)
                        self.present(alertViewController, animated: true, completion: nil)
                    }
                }, fail: { (error, response) in
                    self.hideProgress(progress)
                    self.handleNetworkError(error: error, responseData: response.data)
                })
            }
            //dismiss(animated: true, completion: nil)
        }
        else {
            self.imageView.isHidden = false
            cropViewController.dismiss(animated: true, completion: nil)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let image = (info[UIImagePickerControllerOriginalImage] as? UIImage) else { return }
        let cropController = CropViewController(croppingStyle: croppingStyle, image: image)
        cropController.delegate = self
        self.image = image
        
        //If profile picture, push onto the same navigation stack
        if croppingStyle == .circular {
            picker.pushViewController(cropController, animated: true)
        }
        else { //otherwise dismiss, and then present from the main controller
            picker.dismiss(animated: true, completion: {
                self.present(cropController, animated: true, completion: nil)
                //self.navigationController!.pushViewController(cropController, animated: true)
            })
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func willShow(_ picker: FWDatePicker) {
        currentTextField?.resignFirstResponder()
    }
    
    func fwComboBoxWillShow(comboBox: FWComboBox) {
        currentTextField?.resignFirstResponder()
    }
    
    public func layoutImageView() {
        guard imageView.image != nil else { return }
        let padding: CGFloat = 20.0
        
        var viewFrame = self.view.bounds
        viewFrame.size.width -= (padding * 2.0)
        viewFrame.size.height -= ((padding * 2.0))
        
        var imageFrame = CGRect.zero
        imageFrame.size = imageView.image!.size;
        
        if imageView.image!.size.width > viewFrame.size.width || imageView.image!.size.height > viewFrame.size.height {
            let scale = min(viewFrame.size.width / imageFrame.size.width, viewFrame.size.height / imageFrame.size.height)
            imageFrame.size.width *= scale
            imageFrame.size.height *= scale
            imageFrame.origin.x = (self.view.bounds.size.width - imageFrame.size.width) * 0.5
            imageFrame.origin.y = (self.view.bounds.size.height - imageFrame.size.height) * 0.5
            imageView.frame = imageFrame
        }
        else {
            self.imageView.frame = imageFrame;
            self.imageView.center = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
        }
    }
}
