//
//  ConfirmCodePasswordVC.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import MessageUI
class ConfirmCodePasswordVC: BaseViewControllerNoSearchBar,UITextFieldDelegate {
    var userRegister:UserRegister! = nil
    var userID:Int = 0
    var countTimer:Int = 0
    var timer:Timer? = nil
    private var verifyCode:String = ""
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var timeWaiting: UILabel!
    @IBOutlet weak var scrollView: FWCustomScrollView!
    @IBOutlet weak var verifyCodeTextField: FWFloatingLabelTextField!
    private var currentTextField:UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        //self.navigationItem.hidesBackButton = true
        verifyCodeTextField.becomeFirstResponder()
        verifyCodeTextField.delegate = self
        descriptionLabel.text = "Hãy cho iCNM biết số điện thoại này là của bạn. Nhập mã trong SMS đã gửi đến\n84" + (userRegister.phoneNumber.hasPrefix("0") ? userRegister.phoneNumber.substring(from: userRegister.phoneNumber.index(userRegister.phoneNumber.startIndex, offsetBy:1)) : userRegister.phoneNumber)
        verifyCodeTextField.addTarget(self, action: #selector(verifyCodeTextFieldChanged), for: .editingChanged)
        registerForNotifications()
        sendSMS()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        // Do any additional setup after loading the view.
        countTimer = 300 * 1000
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ConfirmCodePasswordVC.updateRealTimer), userInfo: nil, repeats: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.title = "Lấy lại mật khẩu"
    }
    
    func updateRealTimer() {
        if(countTimer > 0) {
            countTimer -= 1000
            let result = Common.sharedInstance.stringFromTimeInterval2(interval: countTimer)
            timeWaiting.text = "\(result)"
            timeWaiting.textColor = UIColor(hex:"1976D2")
            verifyCodeTextField.isUserInteractionEnabled = true
        }else{
            timeWaiting.text = "Hết hạn"
            timeWaiting.textColor = UIColor.red
            timer?.invalidate()
            timer = nil
            verifyCodeTextField.isUserInteractionEnabled = false
        }
    }

    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func continueButtonTouch(_ sender: Any) {
        if verifyCodeTextField.validate() {
            self.navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
                let confirmPasswordVC = StoryboardScene.LoginRegister.confirmPasswordVC.instantiate()
                confirmPasswordVC.userID = self.userID
                self.navigationController?.pushViewController(confirmPasswordVC, animated: true)
        }else{

        }
    }

    @IBAction func resendSMSButtonTouch(_ sender: Any) {
        sendSMS()
    }
    
    @IBAction func changePhoneNumberButtonTouch(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func sendSMS() {
        let progress = self.showProgress()
        let code = 1000 + arc4random_uniform(8000)
        verifyCodeTextField.removeAllRegx()
        verifyCodeTextField.addRegx(strRegx: "^\(code)$", errorMsg: "Mã xác nhận không chính xác")
        verifyCode = "\(code)"
        let verifyMessage = "Mã xác nhận iCNM của bạn là:\(code)"
        userRegister.sendVerifyPhone(code:verifyMessage,areaCode: "84",success: { (string) in
            self.hideProgress(progress)
            if string.range(of: "Successfull") == nil {
                if string == "" || string == "null" {
                    let alertViewController = UIAlertController(title: "Thông báo", message: "Có lỗi từ máy chủ gửi tin nhắn", preferredStyle: .alert)
                    let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                    alertViewController.addAction(noAction)
                    self.present(alertViewController, animated: true, completion: nil)
                } else {
                    let alertViewController = UIAlertController(title: "Thông báo", message: string, preferredStyle: .alert)
                    let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                    alertViewController.addAction(noAction)
                    self.present(alertViewController, animated: true, completion: nil)
                }
            }
        }) { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        }
    }
    
    @objc private func keyboardWillShow(aNotification:Notification) {
        if let curTextField = currentTextField {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            scrollView.contentInset = contentInset
            scrollView.scrollIndicatorInsets = contentInset
            let rect = curTextField.frame
            let test = view.bounds.height - (rect.size.height + rect.origin.y + scrollView.contentInset.top) - 40.0
            let different = keyboardFrame.size.height - test
            if different > 0 {
                scrollView.contentOffset.y = different - self.scrollView.contentInset.top
            }
        }
        
    }
    
    @objc private func keyboardWillHide(aNotification:Notification) {
        var contentInset:UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = 0
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentTextField = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == verifyCodeTextField {
            textField.resignFirstResponder()
        }
        return true
    }
    
    @IBAction func verifyCodeTextFieldChanged(_ sender: Any) {
        if let textField = sender as? FWFloatingLabelTextField {
            if textField.validate() {
                self.navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
                let confirmPasswordVC = StoryboardScene.LoginRegister.confirmPasswordVC.instantiate()
                confirmPasswordVC.userID = self.userID
                self.navigationController?.pushViewController(confirmPasswordVC, animated: true)
            }else{
                
            }
        }
    }
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
//        if segue.identifier == StoryboardSegue.LoginRegister.showNameInputViewController.rawValue {
//            userRegister.verifyPhone = true
//            let destinationVC = segue.destination as! NameInputViewController
//            destinationVC.userRegister = self.userRegister
//        }
     }
    
    @IBAction func touchScreen(_ sender: Any) {
        currentTextField?.resignFirstResponder()
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}

