//
//  WorkExperienceViewController.swift
//  iCNM
//
//  Created by Medlatec on 5/23/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

@objc protocol WorkExperiencePopupViewControllerDelegate : class {
    @objc optional func workExperienceDidUpdate(specialistPopup:WorkExperiencePopupViewController, workExperience:WorkExperience?)
}

class WorkExperiencePopupViewController: UIViewController, UITextFieldDelegate {

    var workExperience:WorkExperience?
    weak var delegate:WorkExperiencePopupViewControllerDelegate?
    
    @IBOutlet weak var companyTextField: FWFloatingLabelTextField!
    @IBOutlet weak var positionTextField: FWFloatingLabelTextField!
    @IBOutlet weak var summaryTextField: FWFloatingLabelTextField!
    @IBOutlet weak var startYearTextField: FWFloatingLabelTextField!
    @IBOutlet weak var endYearTextField: FWFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        companyTextField.delegate = self
        positionTextField.delegate = self
        summaryTextField.delegate = self
        startYearTextField.delegate = self
        endYearTextField.delegate = self
        
        if let wExperience = workExperience {
            companyTextField.text = wExperience.company
            positionTextField.text = wExperience.position
            summaryTextField.text = wExperience.summary
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy"
            startYearTextField.text = wExperience.startDate != nil ? dateFormatter.string(from: wExperience.startDate!) : ""
            endYearTextField.text = wExperience.endDate != nil ? dateFormatter.string(from: wExperience.endDate!) : ""
        }
        
        startYearTextField.addRegx(strRegx: "^[0-9]{4}$", errorMsg: "Năm bạn nhập chưa đúng định dạng")
        endYearTextField.addRegx(strRegx: "^[0-9]{4}$", errorMsg: "Năm bạn nhập chưa đúng định dạng")
        startYearTextField.validateBlock = {
            [weak self]() -> (String?) in
            if let startText = self?.startYearTextField.text, let value = Int(startText) {
                let currentYear = Calendar.current.component(.year, from: Date())
                if value > currentYear || value < 1951 {
                    return "Bạn chỉ được nhập năm trong khoảng từ 1951 đến \(currentYear)"
                }
                
                if let endText = self?.endYearTextField.text, let value2 = Int(endText) {
                    if value > value2 {
                        return "Năm bắt đầu không được lớn hơn năm kết thúc"
                    }
                }
                
                return nil
            } else {
                return "Năm bạn nhập chưa đúng định dạng"
            }
        }
        
        endYearTextField.validateBlock = {
            [weak self]() -> (String?) in
            if let endText = self?.endYearTextField.text, let value = Int(endText) {
                let currentYear = Calendar.current.component(.year, from: Date())
                if value > currentYear || value < 1951 {
                    return "Bạn chỉ được nhập năm trong khoảng từ 1951 đến \(currentYear)"
                }
                
                if let startText = self?.startYearTextField.text, let value2 = Int(startText) {
                    if value < value2 {
                        return "Năm kết thúc không được nhỏ hơn năm bắt đầu"
                    }
                }
                
                return nil
            } else {
                return "Năm bạn nhập chưa đúng định dạng"
            }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func touchDoneButton(_ sender: Any) {
        if (companyTextField.validate() && positionTextField.validate() && summaryTextField.validate() && startYearTextField.validate() && endYearTextField.validate()) {
        if let workExperience = workExperience {
            workExperience.company = companyTextField.text?.trim()
            workExperience.position = positionTextField.text?.trim()
            workExperience.summary = summaryTextField.text?.trim()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy"
            workExperience.startDate = dateFormatter.date(from: startYearTextField.text!.trim())
            workExperience.endDate = dateFormatter.date(from: endYearTextField.text!.trim())
            delegate?.workExperienceDidUpdate?(specialistPopup: self, workExperience: nil)
        } else {
            let newWorkExperience = WorkExperience()
            newWorkExperience.company = companyTextField.text?.trim()
            newWorkExperience.position = positionTextField.text?.trim()
            newWorkExperience.summary = summaryTextField.text?.trim()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy"
            newWorkExperience.startDate = dateFormatter.date(from: startYearTextField.text!.trim())
            newWorkExperience.endDate = dateFormatter.date(from: endYearTextField.text!.trim())
            delegate?.workExperienceDidUpdate?(specialistPopup: self, workExperience: newWorkExperience)
        }
        dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func touchCancleButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == companyTextField {
            companyTextField.resignFirstResponder()
        }
        
        if textField == positionTextField {
            positionTextField.resignFirstResponder()
        }
        
        if textField == summaryTextField {
            summaryTextField.resignFirstResponder()
        }
        
        if textField == startYearTextField {
            startYearTextField.resignFirstResponder()
        }
        
        if textField == endYearTextField {
            endYearTextField.resignFirstResponder()
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == startYearTextField {
            endYearTextField.validate()
        }
        
        if textField == endYearTextField {
            startYearTextField.validate()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == companyTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 100
        }else if textField == positionTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == summaryTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 200
        }else if textField == startYearTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else if textField == endYearTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 4
        }else{
            return true
        }
    }
    
    @IBAction func touchPopupBackground(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func touchScreen(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
