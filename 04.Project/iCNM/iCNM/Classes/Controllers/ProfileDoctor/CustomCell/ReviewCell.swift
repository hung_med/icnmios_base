//
//  ReviewCell.swift
//  iCNM
//
//  Created by Mac osx on 10/6/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Cosmos
class ReviewCell: UITableViewCell {

    @IBOutlet weak var cosmosViewCamNhanChung: CosmosView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var imageAvatar:PASImageView!
    @IBOutlet weak var lblReview:UILabel!
    @IBOutlet weak var lblReviewNumber:UILabel!
    @IBOutlet weak var lblCreateDate:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageAvatar.layer.cornerRadius = 25
        imageAvatar.layer.masksToBounds = true
        cosmosViewCamNhanChung.isUserInteractionEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
