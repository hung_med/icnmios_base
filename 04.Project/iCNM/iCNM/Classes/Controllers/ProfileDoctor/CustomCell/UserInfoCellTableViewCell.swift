//
//  UserInfoCellTableViewCell.swift
//  iCNM
//
//  Created by Mac osx on 10/6/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Cosmos
class UserInfoCellTableViewCell: UITableViewCell {

    @IBOutlet weak var cosmosStar: CosmosView!
    @IBOutlet weak var lblFullName:UILabel!
    @IBOutlet weak var profileImage:PASImageView!
    @IBOutlet weak var lblReviewNumber:UILabel!
    @IBOutlet weak var lblFollow:UILabel!
    @IBOutlet weak var lblFollowing:UILabel!
    @IBOutlet weak var btnFollowed:UIButton!
    @IBOutlet weak var btnMessage:UIButton!
    @IBOutlet weak var view1:UIView!
    @IBOutlet weak var view2:UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        profileImage.layer.cornerRadius = 35
        profileImage.layer.masksToBounds = true
        btnFollowed.layer.borderColor = UIColor.white.cgColor
        btnMessage.layer.borderColor = UIColor.white.cgColor
        btnMessage.layer.borderWidth = 1.0
        btnFollowed.layer.borderWidth = 1.0
        btnFollowed.layer.cornerRadius = 4
        btnMessage.layer.cornerRadius = 4
        btnMessage.isHidden = true
        btnFollowed.isHidden = true
        view1.isHidden = true
        view2.isHidden = true
        cosmosStar.isHidden = true
        cosmosStar.isUserInteractionEnabled = false
        view1.isUserInteractionEnabled = false
        view2.isUserInteractionEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
