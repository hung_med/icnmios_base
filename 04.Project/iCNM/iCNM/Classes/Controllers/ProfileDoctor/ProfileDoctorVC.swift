//
//  ProfileDoctorVC.swift
//  iCNM
//
//  Created by Mac osx on 10/6/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import Cosmos
import PopupDialog
import FontAwesome_swift
import Toaster
import ESPullToRefresh
import Firebase
import FirebaseDatabase
import GoogleSignIn

class ProfileDoctorVC: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate, UIViewControllerTransitioningDelegate, RatingVCDelegate {
    
    var sectionsData:[String]? = [String]()
    @IBOutlet weak var myTableView: UITableView!
    var buttonMessage:UIBarButtonItem?
    var buttonFollowed:UIBarButtonItem?
    let defaults:UserDefaults = UserDefaults.standard
    private var specialistSelect:[SpecialistModel]?
    private var specialistSelected:[SpecialistModel]?
    private var workExperiences:[WorkExperience]?
    private var educations:[Education]?
    private var prizes:[Prize]?
    var featuredDoctor:FeaturedDoctor? = nil
    var profileDoctors: [ProfileDoctor]?
    var listUserComment:[UserRating]!
    var followed:Int? = 0

    var userInfo:[String]? = nil
    var listIcon:[String]? = nil
    var currentAvatar:String? = nil
    var isCheck:Bool?=false
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    let databaseReference = Database.database().reference()
    
    fileprivate var pageNumber = 1
    fileprivate var footerScrollView = ESRefreshFooterAnimator(frame:CGRect.zero)
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.estimatedRowHeight = 70
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white, NSFontAttributeName:UIFont(name:"HelveticaNeue", size: 20)!]
        title = "Danh sách đánh giá"
        let button: UIButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        button.setImage(UIImage(named: "angle-left"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(ProfileDoctorVC.back(sender:)), for: UIControlEvents.touchUpInside)
        let newBackButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.leftBarButtonItem = newBackButton
        
        if let titleNav = featuredDoctor?.userInfo?.name{
            title = titleNav
        }

        let nib_header = UINib(nibName: "CustomMedicalBodyHeader", bundle: nil)
        myTableView.register(nib_header, forHeaderFooterViewReuseIdentifier: "headerCell")

        let nib = UINib(nibName: "UserInfoCellTableViewCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "UserInformationCell")
        
        let nib1 = UINib(nibName: "DetailUserInfoCell", bundle: nil)
        myTableView.register(nib1, forCellReuseIdentifier: "DetailUserCell")
        
        let nib2 = UINib(nibName: "ReviewCell", bundle: nil)
        myTableView.register(nib2, forCellReuseIdentifier: "ReviewCell")
        
        let nib3 = UINib(nibName: "ReviewOtherCell", bundle: nil)
        myTableView.register(nib3, forCellReuseIdentifier: "ReviewOtherCell")
      //  ratings = [Rating]()
        self.requestData()
        
        footerScrollView.loadingMoreDescription = "Tải thêm"
        self.myTableView.es.addInfiniteScrolling(animator:footerScrollView) { [weak self] in
            
            if let weakSelf = self {
            weakSelf.pageNumber = weakSelf.pageNumber + 1
                if self?.featuredDoctor?.doctor != nil{
                    UserRating.getUserDoctorByID(doctorID: (self?.featuredDoctor?.doctor?.id)!, pageNumber:weakSelf.pageNumber) { (data, error) in
                        weakSelf.myTableView.es.stopLoadingMore()
                        if let result = data {
                            if result.count > 0{
                                for item in result {
                                    self?.listUserComment.append(item)
                                    self?.myTableView.reloadData()
                                }
                            }else{
                                self?.footerScrollView.loadingMoreDescription = "Đã hết dữ liệu"
                                if self?.listUserComment.count == 0{
                                    self?.footerScrollView.loadingMoreDescription = "Không có bình luận nào"
                                }
                            }
                        }
                    }
                }else{
                    weakSelf.myTableView.es.stopLoadingMore()
                }
            }
        }
    }
    
    override func requestData() {
        listUserComment = [UserRating]()
        self.profileDoctors = [ProfileDoctor]()
        self.specialistSelect = [SpecialistModel]()
        self.specialistSelected = [SpecialistModel]()
        self.workExperiences = [WorkExperience]()
        self.educations = [Education]()
        self.prizes = [Prize]()
        self.userInfo = [String]()
        self.listIcon = [String]()
        
        DispatchQueue.main.async{
            if self.currentUser != nil {
                if let user = self.currentUser {
                    if user.id == self.featuredDoctor?.userInfo?.id{
                        self.sectionsData = ["", "Thông tin", "Đánh giá khác"]
                        self.getUserDoctorByID(userID:(self.featuredDoctor?.userInfo?.id)!, userViewID: 0)
                        if self.featuredDoctor?.doctor != nil{
                            self.getRatingDoctorByPage(doctorID: (self.featuredDoctor?.doctor?.id)!, pageNumber: 1)
                        }
                    }else{
                        self.sectionsData = ["", "Thông tin", "Đánh giá của bạn", "Đánh giá khác"]
                        if let user = self.currentUser {
                            self.getUserDoctorByID(userID:(self.featuredDoctor?.userInfo?.id)!, userViewID: user.id)
                            
                            if self.featuredDoctor?.doctor != nil{
                                self.getRatingDoctorByPage(doctorID: (self.featuredDoctor?.doctor?.id)!, pageNumber: 1)
                            }
                        }
                    }
                }
            }else{
                self.sectionsData = ["", "Thông tin", "Đánh giá khác"]
                self.getUserDoctorByID(userID:(self.featuredDoctor?.userInfo?.id)!, userViewID: 0)
                if self.featuredDoctor?.doctor != nil{
                    self.getRatingDoctorByPage(doctorID: (self.featuredDoctor?.doctor?.id)!, pageNumber: 1)
                }
            }
        }
    }
    
    func back(sender: UIBarButtonItem) {
//        let myView = self.navigationController?.view
//        let progress = iCNM.showProgress(view: myView!)
//        self.hideProgress(progress)
        self.view.isUserInteractionEnabled = true
        self.navigationItem.rightBarButtonItem?.isEnabled = true
        self.buttonMessage?.isEnabled = true
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
    }
    
    // MARK: Set data
    func getRatingDoctorByPage(doctorID:Int, pageNumber:Int) -> Void {
        let progress = self.showProgress()
        //listUserComment = [UserRating]()
        UserRating.getUserDoctorByID(doctorID: doctorID, pageNumber:pageNumber) { (data, error) in
            self.listUserComment = data
            if self.listUserComment.count > 0{
                self.myTableView.reloadData()
            }else{
                self.footerScrollView.loadingMoreDescription = "Không có dữ liệu"
            }
            
            self.hideProgress(progress)
        }
    }
    
    func getUserDoctorByID(userID:Int, userViewID:Int){
        let progress = self.showProgress()
        ProfileDoctor().getUserDoctorByID(userID: userID, userViewID: userViewID, success: { (data) in
            if let profileDoctorObj = data {
                
                DispatchQueue.main.async(execute: {
                    self.profileDoctors?.append(profileDoctorObj)
                    
                    let userInfoObj = profileDoctorObj.userInfo
                    if let phone = userInfoObj?.phone{
                        self.userInfo?.append(phone)
                        self.listIcon?.append("phone-square.png")
                    }
                    if let email = userInfoObj?.email{
                        self.userInfo?.append(email)
                        self.listIcon?.append("email.png")
                    }
                    if let birthday = userInfoObj?.birthday {
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateStyle = .short
                        dateFormatter.timeStyle = .none
                        dateFormatter.dateFormat = "dd/MM/yyyy"
                        let convertToStr = dateFormatter.string(from: birthday)
                        self.userInfo?.append(convertToStr)
                        self.listIcon?.append("birthday.png")
                    }
                    
                    if let gender = userInfoObj?.gender{
                        var sex = ""
                        if gender == "F"{
                            sex = "Nữ"
                            self.listIcon?.append("female.png")
                        }else{
                            sex = "Nam"
                            self.listIcon?.append("male.png")
                        }
                        self.userInfo?.append(sex)
                    }
                    if let address = userInfoObj?.address{
                        self.userInfo?.append(address)
                         self.listIcon?.append("home.png")
                    }
                    if let workplace = userInfoObj?.workplace{
                        self.userInfo?.append(workplace)
                        self.listIcon?.append("hospital.png")
                    }
                    
                    let selectedSpecialistIDs = profileDoctorObj.doctorInformation?.doctorSpecialists.map({ (doctorSpecialist) -> Int in
                        return doctorSpecialist.specialID
                    })
                    
                    if (profileDoctorObj.doctorInformation?.specialists.count)! > 0{
                        var specialistsString = ""
                        if let specialists = profileDoctorObj.doctorInformation?.specialists {
                            for specialist in specialists {
                                let specialistObj:SpecialistModel = SpecialistModel(title: specialist.name ?? "", isSelected: selectedSpecialistIDs?.contains(specialist.id) ?? false, isUserSelectEnable: true, specialist: specialist)
                                specialistsString += specialistObj.specialist.name! + "\n"
                            }
                        }
                        self.userInfo?.append(specialistsString)
                        self.listIcon?.append("medkit.png")
                    }
                    
                    if let avatar = userInfoObj?.avatar{
                        self.currentAvatar = avatar
                    }
                    
                    if (profileDoctorObj.doctorInformation?.educateds.count)! > 0{
                        var educationString = ""
                        profileDoctorObj.doctorInformation?.educateds.forEach({ (education) in
                            if let major = education.major {
                                educationString += "Đã học " + major + " "
                            }
                            if let schoolName = education.schoolName {
                                educationString += "tại " + schoolName
                            }
                            if let startDate = education.startDate {
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "yyyy"
                                educationString += " vào " + dateFormatter.string(from: startDate)
                            }
                            if let endDate = education.endDate {
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "yyyy"
                                educationString += " đến " + dateFormatter.string(from: endDate)
                            }
                            if let summary = education.summary {
                                educationString += " (\(summary))\n"
                            }
                        })
                        self.userInfo?.append(educationString)
                        self.listIcon?.append("graduation.png")
                    }
                    
                    if (profileDoctorObj.doctorInformation?.workExperiences.count)! > 0{
                        var workExperiencesString = ""
                        profileDoctorObj.doctorInformation?.workExperiences.forEach({ (workExperience) in
                            if let position = workExperience.position {
                                workExperiencesString += "Đã làm " + position + " "
                            }
                            if let company = workExperience.company {
                                workExperiencesString += " tại " + company
                            }
                            if let startDate = workExperience.startDate {
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "yyyy"
                                workExperiencesString += " từ " + dateFormatter.string(from: startDate)
                            }
                            if let endDate = workExperience.endDate {
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "yyyy"
                                workExperiencesString += " đến " + dateFormatter.string(from: endDate)
                            }
                            if let summary = workExperience.summary {
                                workExperiencesString += " (\(summary))\n"
                            }
                        })
                        
                        self.userInfo?.append(workExperiencesString)
                        self.listIcon?.append("street-view.png")
                    }
                    
                    if (profileDoctorObj.doctorInformation?.prizes.count)! > 0{
                        var prizeString = ""
                        profileDoctorObj.doctorInformation?.prizes.forEach({ (prize) in
                            if let name = prize.name {
                                prizeString += "Đạt được " + name + " "
                            }
                            if let prizePlace = prize.prizePlace {
                                prizeString += "tại " + prizePlace
                            }
                            if let date = prize.prizeTime {
                                let dateFormatter = DateFormatter()
                                dateFormatter.dateFormat = "yyyy"
                                prizeString += " " + dateFormatter.string(from: date) + "\n"
                            }
                        })
                        self.userInfo?.append(prizeString)
                        self.listIcon?.append("trophy.png")
                    }
                    
                    if let aboutMe = userInfoObj?.aboutMe{
                        self.userInfo?.append(aboutMe)
                        self.listIcon?.append("")
                    }
                    
                    if let startTime = profileDoctorObj.userInfo?.createdDate{
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateStyle = .short
                        dateFormatter.timeStyle = .none
                        dateFormatter.dateFormat = "dd/MM/yyyy"
                        let convertToStr = dateFormatter.string(from: startTime)
                        self.userInfo?.append("Đã tham gia iCNM vào \(convertToStr)")
                        self.listIcon?.append("")
                    }
                    
                    var image1:UIImage? = nil
                    var image2:UIImage? = nil
                    if self.profileDoctors?[0] != nil{
                        if let follow = self.profileDoctors?[0].userFollow{
                            self.isCheck = true
                            if self.currentUser?.id == follow.userID{
                                image1  =  UIImage(named: "comment-o.png")?.maskWithColor(color: UIColor.white)
                                image2  =  UIImage(named: "user-circle")?.maskWithColor(color: UIColor.white)
                            }
                        }else{
                            self.isCheck = false
                            image1  =  UIImage(named: "comment-o.png")?.maskWithColor(color: UIColor.white)
                            image2  =  UIImage(named: "user-plus")?.maskWithColor(color: UIColor.white)
                        }
                        
                        self.buttonMessage = UIBarButtonItem(image: image1, style: .plain, target: self, action: #selector(self.handleMessage))
                        self.buttonFollowed = UIBarButtonItem(image: image2, style: .plain, target: self, action: #selector(self.handleFollowed))
                        
                        self.navigationItem.setRightBarButtonItems([self.buttonFollowed!, self.buttonMessage!], animated: true)
                    }
                    self.myTableView.reloadData()
                    self.hideProgress(progress)
                })
                
            } else {
                self.hideProgress(progress)
            }
            
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return (self.sectionsData?.count)!
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if currentUser != nil {
            if let user = currentUser {
                if user.id == featuredDoctor?.userInfo?.id{
                    if section == 0{
                        return 1
                    }else if section == 1{
                        return self.userInfo!.count
                    }else{
                        return self.listUserComment.count
                    }
                }else{
                    if section == 0{
                        return 1
                    }else if section == 1{
                        return self.userInfo!.count
                    }else if section == 2{
                        return 1
                    }else{
                        return self.listUserComment.count
                    }
                }
            }
            
        }else{
            if section == 0{
                return 1
            }else if section == 1{
                return self.userInfo!.count
            }else{
                return self.listUserComment.count
            }
        }
        return 0
    }
    
    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "headerCell") as? CustomMedicalBodyHeader
        headerView?.contentView.backgroundColor = UIColor.lightGray
        headerView?.lblNameSection.text = sectionsData?[section]
        headerView?.lblNameSection.textColor = UIColor.white
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        if section == 0{
            return 0
        }
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserInformationCell", for: indexPath) as! UserInfoCellTableViewCell
        
            if (self.profileDoctors?.count)! > 0{
                cell.view1.isHidden = false
                cell.view2.isHidden = false
                cell.btnFollowed.isHidden = false
                cell.btnMessage.isHidden = false
                cell.cosmosStar.isHidden = false
                if currentUser != nil {
                    if currentUser?.id == featuredDoctor?.userInfo?.id{
                        cell.btnFollowed.isHidden = true
                        cell.btnMessage.isHidden = true
                        self.navigationItem.setRightBarButtonItems(nil, animated: true)
                    }else{
                        cell.btnFollowed.isHidden = false
                        cell.btnMessage.isHidden = false
                        cell.btnFollowed.titleLabel?.font = UIFont.fontAwesome(ofSize: 14.0)
                        if isCheck!{
                            let title = String.fontAwesomeIcon(name: .userCircle) + "   Đã Theo dõi"
                            cell.btnFollowed.setTitle(title, for: .normal)
                        }else{
                            let title = String.fontAwesomeIcon(name: .userPlus) + "   Theo dõi"
                            cell.btnFollowed.setTitle(title, for: .normal)
                        }
                        cell.btnMessage.titleLabel?.font = UIFont.fontAwesome(ofSize: 14.0)
                        let titleMessage = String.fontAwesomeIcon(name: .commentO) + "   Nhắn tin"
                        cell.btnMessage.setTitle(titleMessage, for: .normal)
                        
                        cell.btnFollowed.addTarget(self, action:#selector(handleFollowed), for: .touchUpInside)
                        cell.btnMessage.addTarget(self, action:#selector(handleMessage), for: .touchUpInside)
                    }
                }else{
                    cell.btnFollowed.isHidden = true
                    cell.btnMessage.isHidden = true
                    self.navigationItem.setRightBarButtonItems(nil, animated: true)
                }
                
                if let doctor = self.profileDoctors?[0].doctor{
                    cell.cosmosStar.rating = doctor.rating
                    if IS_IPAD{
                        cell.cosmosStar.starSize = Double(screenSizeWidth/25.0)
                        if #available(iOS 9.0, *) {
                            cell.cosmosStar.leftAnchor.constraint(equalTo: cell.contentView.leftAnchor, constant: 180).isActive = true
                            cell.lblFullName.leftAnchor.constraint(equalTo: cell.contentView.leftAnchor, constant: screenSizeWidth/2 - cell.lblFullName.frame.width/2).isActive = true
                        } else {
                            // Fallback on earlier versions
                        }
                        
                    }else{
                        cell.cosmosStar.starSize = Double(screenSizeWidth/16.0)
                    }
                    
                }
                if let followNum = self.profileDoctors?[0].followNum{
                    if self.followed == 1{
                        cell.lblFollow.text = "\(followNum+1) người theo dõi"
                    }else{
                        cell.lblFollow.text = "\(followNum) người theo dõi"
                    }
                    
                }
                if let followedNum = self.profileDoctors?[0].followedNum{
                    cell.lblFollowing.text = "\(followedNum) đang theo dõi"
                }
                if let rateNum = self.profileDoctors?[0].rateNum, let doctor = self.profileDoctors?[0].doctor{
                    cell.lblReviewNumber.text = "\(String(format: "%.1f", doctor.rating))(\(rateNum) đánh giá)"
                }
            }
            if let titleNav = featuredDoctor?.userInfo?.name{
                cell.lblFullName.text = titleNav
            }
            if let userId = featuredDoctor?.userInfo?.id{
                if currentAvatar != nil{
                    let avatarURL = API.baseURLImage + "\(API.iCNMImage)" + "\(userId)" + "/\(self.currentAvatar!)"
                    let urlString = avatarURL.trimmingCharacters(in: .whitespaces)
                    let url = NSURL(string: urlString)
                    cell.profileImage.imageURL(URL: url! as URL)
                    
                }
            }
                
            return cell
        }else if indexPath.section == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailUserCell", for: indexPath) as! DetailUserInfoCell
            if let userInfo = self.userInfo?[indexPath.row]{
                cell.lblName.text = userInfo
                cell.lblName.lineBreakMode = .byWordWrapping
                cell.lblName.numberOfLines = 0
            }
            
            if let icon = self.listIcon?[indexPath.row]{
                let image  =  UIImage(named: icon)?.maskWithColor(color: UIColor(hexColor: 0x1976D2, alpha: 1.0))
                cell.iconImage.image = image
            }
            
            return cell
        }else if indexPath.section == 2 && currentUser != nil && currentUser?.id != featuredDoctor?.userInfo?.id{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
            cell.selectionStyle = UITableViewCellSelectionStyle.none
                if let user = currentUser {
                    if let avatar = user.userDoctor?.userInfo?.avatar{
                        let avatarURL = API.baseURLImage + "\(API.iCNMImage)" + "\(user.id)" + "/\(avatar)"
                        let urlString = avatarURL.trimmingCharacters(in: .whitespaces)
                        let url = NSURL(string: urlString)
                        cell.imageAvatar.imageURL(URL: url! as URL)
                    }
                    
                    cell.lblName.text = user.userDoctor?.userInfo?.name
                    var isExistRate:Bool? = false
                    for listComment in listUserComment{
                        if let userInfo = listComment.user{
                            if userInfo.id == user.id{
                                isExistRate = true
                                let rating = listComment.rating
                                if let rateContent = rating?.rateContent{
                                    cell.lblReview.text = rateContent
                                }
                                if let sumRate = rating?.sumRate{
                                    cell.cosmosViewCamNhanChung.rating = sumRate
                                    cell.lblReviewNumber.text = "(\(sumRate))"
                                }
                                    
                                if let dateTime = rating?.dateCreate2{
                                    //get time to server
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                                    var date = dateFormatter.date(from: dateTime)
                                        
                                    if date == nil {
                                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                                        date = dateFormatter.date(from: dateTime)
                                    }
                                        
                                    let dateTime = date?.timeAgoSinceNow()
                                    cell.lblCreateDate.text = "\(dateTime!)"
                                    }
                                }
                            }
                        }
                        if !isExistRate!{
                            cell.lblName.text = user.userDoctor?.userInfo?.name
                            cell.lblReview.text = "Nói cho mọi người biết trải nghiệm của bạn"
                            cell.lblReviewNumber.text = ""
                            cell.cosmosViewCamNhanChung.rating = 0.0
                            cell.lblCreateDate.text = ""
                        }
                }
                return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewOtherCell", for: indexPath) as! ReviewOtherCell
            if (self.listUserComment?.count)! > 0{
                let rating = self.listUserComment[indexPath.row].rating
                let userInfo = self.listUserComment[indexPath.row].user
                if let username = userInfo?.name{
                    cell.lblName.text = username
                }
                if let userId = userInfo?.id{
                    if let avatar = userInfo?.avatar{
                        let urlString = avatar.trimmingCharacters(in: .whitespaces)
                        let avatarURL = API.baseURLImage + "\(API.iCNMImage)" + "\(userId)" + "/\(urlString)"
                        let url = NSURL(string: avatarURL)
                        cell.imageAvatar.imageURL(URL: url! as URL)
                    }else{
                        cell.imageAvatar.update(image: UIImage(named:"avatar_default"), animated: true)
                    }
                }
                
                if let rating = rating?.sumRate{
                    cell.cosmosViewCamNhanChung.rating = rating
                    cell.lblReviewNumber.text = "(\(rating))"
                }
                if let rateContent = rating?.rateContent{
                    cell.lblReview.text = rateContent
                    cell.lblReview.lineBreakMode = .byWordWrapping
                    cell.lblReview.numberOfLines = 0
                }
                
                if let dateCreate = rating?.dateCreate2{
                    //get time to server
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                    var date = dateFormatter.date(from: dateCreate)
                    
                    if date == nil {
                        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                        date = dateFormatter.date(from: dateCreate)
                    }
                    
                    let dateTime = date?.timeAgoSinceNow()
                    cell.lblCreateDate.text = "\(dateTime!)"
                }
            }else{
                
            }
            return cell
        }
    }
   
    func handleFollowed(sender: UIButton){
        if !isCheck!{
            if currentUser != nil {
                if let user = currentUser {
                    self.getUserFollowAction(userID: (featuredDoctor?.userInfo?.id)!, userIDFllow: user.id, followID: 1)
                }
            }
        }else{
            let alert = UIAlertController(title: "Thông báo", message: "Bạn có muốn bỏ theo dõi người này không?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Bỏ theo dõi", style: .default, handler: { action in
                if self.currentUser != nil {
                    if let user = self.currentUser {
                        self.getUserFollowAction(userID: (self.featuredDoctor?.userInfo?.id)!, userIDFllow: user.id, followID: 0)
                    }
                }
            }))
            
            alert.addAction(UIAlertAction(title: "Thoát", style: UIAlertActionStyle.cancel, handler: {    (action:UIAlertAction!) in
                return
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func handleMessage(sender: UIButton) {
        
        // post method
        let chatVC = StoryboardScene.Chat.chatVc.instantiate()
        
        guard let id = currentUser?.userDoctor?.userInfo?.id else {return}
        guard let userName = currentUser?.userDoctor?.userInfo?.name else {return}
        guard let keySearch = currentUser?.userDoctor?.userInfo?.keySearchName else {return}
        let fromAvatar = currentUser?.userDoctor?.userInfo?.avatar
        let fromPhone = currentUser?.userDoctor?.userInfo?.phone
        
        if fromAvatar != nil && fromPhone != nil {
            if let userAvatar = fromAvatar, let userPhone = fromPhone {
                databaseReference.child("Users").child("\(id)").setValue(["UserId": id as Any,"Name": userName as Any,"Avatar": "\(userAvatar)" as Any, "KeySearch" : keySearch as Any, "Phone" : "\(userPhone)" as Any])
            }
        } else if fromAvatar != nil && fromPhone == nil {
            if let userAvatar = fromAvatar {
                databaseReference.child("Users").child("\(id)").setValue(["UserId": id as Any,"Name": userName as Any,"Avatar": "\(userAvatar)" as Any, "KeySearch" : keySearch as Any, "Phone" : "nil" as Any])
            }
        } else if fromAvatar == nil && fromPhone != nil {
            if let userPhone = fromPhone {
                databaseReference.child("Users").child("\(id)").setValue(["UserId": id as Any,"Name": userName as Any,"Avatar": "nil" as Any, "KeySearch" : keySearch as Any, "Phone" : "\(userPhone)" as Any])
            }
        } else {
            databaseReference.child("Users").child("\(id)").setValue(["UserId": id as Any,"Name": userName as Any,"Avatar": "nil" as Any, "KeySearch" : keySearch as Any, "Phone" : "nil" as Any])
        }
        
        
        guard let toId = featuredDoctor?.userInfo?.id else {return}
        guard let toUserName = featuredDoctor?.userInfo?.name else {return}
        guard let toKeySearch = featuredDoctor?.userInfo?.keySearchName else {return}
        let toAvatar = featuredDoctor?.userInfo?.avatar
        let toPhone = featuredDoctor?.userInfo?.phone
        
        if toAvatar != nil && toPhone != nil {
            if let toUserAvatar = toAvatar, let toUserPhone = toPhone {
                databaseReference.child("Users").child("\(toId)").setValue(["UserId": toId as Any,"Name": toUserName as Any,"Avatar": "\(toUserAvatar)" as Any, "KeySearch" : toKeySearch as Any, "Phone" : "\(toUserPhone)" as Any])
            }
        } else if toAvatar != nil && toPhone == nil {
            if let toUserAvatar = toAvatar {
                databaseReference.child("Users").child("\(toId)").setValue(["UserId": toId as Any,"Name": toUserName as Any,"Avatar": "\(toUserAvatar)" as Any, "KeySearch" : toKeySearch as Any, "Phone" : "nil" as Any])
            }
        } else if toAvatar == nil && toPhone != nil {
            if let toUserPhone = toPhone {
                databaseReference.child("Users").child("\(toId)").setValue(["UserId": toId as Any,"Name": toUserName as Any,"Avatar": "nil" as Any, "KeySearch" : toKeySearch as Any, "Phone" : "\(toUserPhone)" as Any])
            }
        } else {
            databaseReference.child("Users").child("\(toId)").setValue(["UserId": toId as Any,"Name": toUserName as Any,"Avatar": "nil" as Any, "KeySearch" : toKeySearch as Any, "Phone" : "nil" as Any])
        }
        
        chatVC.receiverId = featuredDoctor?.userInfo?.id
        chatVC.receiverName = featuredDoctor?.userInfo?.name
        chatVC.receiverAvatar = featuredDoctor?.userInfo?.avatar
        chatVC.receiverKeySearch = (featuredDoctor?.userInfo?.keySearchName)!
        
    
        NotificationCenter.default.post(name: NSNotification.Name("UserDidLoginNotification"), object: nil, userInfo: ["userId": "\(String(describing: id))"])
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(chatVC, animated: true)
    }
    
    func getUserFollowAction(userID:Int, userIDFllow:Int, followID:Int){
        let progress = self.showProgress()
        UserIDFollow().getUserFollowAction(userID: userID, userIDFllow: userIDFllow, followID:followID, success: { (data) in
            if data != nil{
                var image1:UIImage? = nil
                var image2:UIImage? = nil
                self.followed = followID
                if followID == 1{
                    self.isCheck = true
                    Toast(text: "Theo dõi thành công").show()
                    image1  =  UIImage(named: "comment-o.png")?.maskWithColor(color: UIColor.white)
                    image2  =  UIImage(named:"user-circle")?.maskWithColor(color: UIColor.white)
                }else{
                    self.isCheck = false
                    Toast(text: "Bỏ theo dõi thành công").show()
                    image1  =  UIImage(named: "comment-o.png")?.maskWithColor(color: UIColor.white)
                    image2  =  UIImage(named:"user-plus")?.maskWithColor(color: UIColor.white)
                }
                
                self.buttonMessage = UIBarButtonItem(image: image1, style: .plain, target: self, action: #selector(self.handleMessage))
                self.buttonFollowed = UIBarButtonItem(image: image2, style: .plain, target: self, action: #selector(self.handleFollowed))
                
                self.navigationItem.setRightBarButtonItems([self.buttonFollowed!, self.buttonMessage!], animated: true)
            
                self.myTableView.reloadData()
                self.hideProgress(progress)
            } else {
                self.hideProgress(progress)
            }
            
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    //Rating
    func performRatingAction() {
        let ratingVC = RatingVC(nibName: "RatingVC", bundle: nil)
        ratingVC.delegate = self
        ratingVC.listUserComment = self.listUserComment
        ratingVC.modalPresentationStyle = .custom
        ratingVC.transitioningDelegate = self
        self.present(ratingVC, animated: true, completion: nil)
    }
    
    func reloadRatingUser(general: Double, ratingTechnique: Double, medicalEthics: Double, noidung: String, sumRate: Double) {
        let progress = self.showProgress()
        if self.currentUser != nil {
            if let user = self.currentUser {
                user.requestUserInformation(success: { (newUserInfo) in
                    let userID = user.id
                    let doctorID = self.featuredDoctor?.doctor?.id
                    if doctorID == nil{
                        Toast(text: "Bạn không thể đánh giá tài khoản cá nhân").show()
                        self.hideProgress(progress)
                        return
                    }
                    var isUpdate:Bool = false
                    var rateID:Int = 0
                    for listComment in self.listUserComment{
                        if let userInfo = listComment.user{
                            if userInfo.id == user.id{
                                isUpdate = true
                                rateID = listComment.rating.rateID
                            }
                        }
                    }
                    
                    UserRating.addEditRatingUserDoctor(isUpdate: isUpdate, rateID:rateID, rateContent: noidung, technique: ratingTechnique, medicalEthics: medicalEthics, general:general, sumRate: Float(sumRate), doctorID: doctorID!, userID: userID, success: { (data) in
                        if data != nil {
                            self.pageNumber = 1
                            if self.featuredDoctor?.doctor != nil{
                                self.getRatingDoctorByPage(doctorID: (self.featuredDoctor?.doctor?.id)!, pageNumber: self.pageNumber)
                            }
                            Toast(text: "Đánh giá thành công").show()
                            self.requestData()
                            self.hideProgress(progress)
                        }else{
                            self.hideProgress(progress)
                        }
                        
                    }, fail: { (error, response) in
                        
                    })
                    
                    
                }, fail: { (error, response) in
                    
                })
            }
            
        }
        
        self.goBackProfileDoctor()
    }
    
    func goBackProfileDoctor()
    {
        self .dismiss(animated: true) {
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0{
            return 253
        }else if indexPath.section == 1{
            return UITableViewAutomaticDimension
        }else if indexPath.section == 2{
            if currentUser != nil {
                if currentUser?.id == featuredDoctor?.userInfo?.id{
                    return 146
                }else{
                    return 162
                }
            }else{
                return 146
            }
        }else{
            if (self.listUserComment?.count)! > 0{
                return UITableViewAutomaticDimension
            }else{
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if currentUser != nil {
            if indexPath.section == 2{
                self.performRatingAction()
            }else{
                self.myTableView .deselectRow(at: indexPath, animated: true)
            }
        }
    }
}
