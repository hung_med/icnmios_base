//
//  SADoctorCell.swift
//  iCNM
//
//  Created by Mac osx on 8/7/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import Cosmos
class SADoctorCell: UITableViewCell {

    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var cosmosStar: CosmosView!
    @IBOutlet weak var avatarDoctor: CustomImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblSpecialistName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblNumberRating: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblScheduleTime: UILabel!
    @IBOutlet weak var btnscheduleTime: UIButton!
    @IBOutlet weak var myCollectionView: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let f = contentView.frame
        let fr = UIEdgeInsetsInsetRect(f, UIEdgeInsetsMake(10, 10, 10, 15))
        contentView.frame = fr
        addShadow(cell: self)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func addShadow(cell:UITableViewCell) {
        cell.layer.cornerRadius = 4
        cell.layer.masksToBounds = true
        
        cell.layer.masksToBounds = false
        cell.layer.shadowOffset = CGSize(width: 0, height:5)
        cell.layer.shadowColor = UIColor.groupTableViewBackground.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowRadius = 2
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension SADoctorCell {
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        myCollectionView.delegate = dataSourceDelegate
        myCollectionView.dataSource = dataSourceDelegate
        myCollectionView.tag = row
        myCollectionView.layer.cornerRadius = 4.0
        myCollectionView.layer.cornerRadius = 4.0
        myCollectionView.backgroundColor = UIColor.white
        myCollectionView.setContentOffset(myCollectionView.contentOffset, animated:false) // Stops collection view if it was scrolling.
        myCollectionView.reloadData()
    }
    
    var collectionViewOffset: CGFloat {
        set { myCollectionView.contentOffset.x = newValue }
        get { return myCollectionView.contentOffset.x }
    }
}
