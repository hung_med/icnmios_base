//
//  FormScheduleAppointmentVC.swift
//  iCNM
//
//  Created by Mac osx on 11/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import FontAwesome_swift

class FormScheduleAppointmentVC: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, FWComboBoxDelegate {
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var lblNearUnit: UILabel!
    @IBOutlet weak var btnRequire: UIButton!
    @IBOutlet weak var myView: UIView!
    @IBOutlet weak var specialistComboBox: FWComboBox!
    private var currentSpecialistRequest:Request?
    private var currentOrganizeRequest:Request?
    private var currentSpecialist:Specialist?
    private var specialistNotificationToken:NotificationToken?
    private let specialistResult:Results<Specialist> = {
        let realm = try! Realm()
        return realm.objects(Specialist.self).filter("active == true")
    }()
    
    let searchController = UISearchController(searchResultsController: nil)
    var searchBarButton:UIBarButtonItem? = nil
    var isSearch:Bool? = false
    var storedOffsets = [Int: CGFloat]()
    var currentRequest:Request?
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    var isSearching = false
    var listOrganizes:[Organize] = [Organize]()
    var filteredOrganizeNames:[Organize] = [Organize]()
    var listSpecialistObj:[Specialist]? = nil
    var newBackButton:UIBarButtonItem? = nil
    var searchBar = UISearchBar(frame: CGRect(x: 0, y: 0, width: 250, height: 18))
    var searchOn : UIBarButtonItem!
    var searchOff : UIBarButtonItem!
    @IBOutlet weak var spaceTopHeightConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if ScreenSize.SCREEN_MAX_LENGTH >= 812{
            spaceTopHeightConstraint.constant = 95.0
        }else{
            spaceTopHeightConstraint.constant = 80.0
        }
        self.tabBarController?.tabBar.isHidden = true
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(FormScheduleAppointmentVC.back(sender:)), for: UIControlEvents.touchUpInside)
            newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }
            
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        title = "ĐẶT LỊCH BÁC SĨ"
        self.myView.layer.borderWidth = 0.5
        self.myView.layer.borderColor = UIColor.lightGray.cgColor
        self.btnRequire.layer.cornerRadius = 4.0
        self.btnRequire.layer.masksToBounds = true
        self.btnRequire.addTarget(self, action: #selector(FormScheduleAppointmentVC.handlerConsulting(_:)), for: UIControlEvents.touchUpInside)
        
        self.lblNearUnit.text = "Đơn vị khám gần nhất (0)"
       // self.myTableView2.isHidden = true
        // create right bar button item
        searchOn = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(isSearchOn))
        searchOff = UIBarButtonItem(title: "Hủy", style: .plain, target: self, action: #selector(isSearchOff))
        self.navigationItem.rightBarButtonItems = [self.searchOn]
        
        // create search bar
        self.tabBarController?.tabBar.isHidden = true
        searchBar.placeholder = "Tìm kiếm.."
        searchBar.tintColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1)
        searchBar.isHidden = true
        searchBar.delegate = self
        
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.estimatedRowHeight = 70
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        self.listSpecialistObj = [Specialist]()
        let nib = UINib(nibName: "MedicalUnitCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "MedUnitCell")
        
        specialistComboBox.delegate = self
        specialistComboBox.layer.borderWidth = 1
        specialistComboBox.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        specialistComboBox.layer.cornerRadius = 4
        specialistComboBox.layer.masksToBounds = true
        //specialistComboBox.defaultTitle = "Tất cả"
        specialistNotificationToken = specialistResult.observe({[weak self] (changes) in
            switch changes {
            case .initial:
                // Results are now populated and can be accessed without blocking the UI
                self?.setupSpecialistComboBox()
                break
            case .update:
                // Query results have changed, so apply them to the UITableView
                self?.setupSpecialistComboBox()
                break
            case .error(let error):
                // An error occurred while opening the Realm file on the background worker thread
                fatalError("\(error)")
                break
            }
        })
        
        requestData()
    }
    
    deinit {
        specialistNotificationToken?.invalidate()
        currentSpecialistRequest?.cancel()
        NotificationCenter.default.removeObserver(self)
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func handlerConsulting(_ sender: Any) {
        Constant.HOTLINE.makeCallPhone()
    }
        
    override func requestData() {
        let progress = self.showProgress()
        currentSpecialistRequest?.cancel()
        currentSpecialistRequest = Specialist.getAllSpecialist(success: { (data) in
            self.currentOrganizeRequest = Organize.getAllOrganizes(success: { (organizes) in
                self.listOrganizes = organizes
                self.myTableView.reloadData()
                self.hideProgress(progress)
            }) { (error, response) in
                self.handleNetworkError(error: error, responseData: response.data)
                self.hideProgress(progress)
            }
        }, fail: { (error, response) in
            self.handleNetworkError(error: error, responseData: response.data)
            self.hideProgress(progress)
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if searchBar.text != "" {
            isSearchOn()
        }
    }
    
    private func setupSpecialistComboBox() {
        specialistComboBox.dataSource = specialistResult.map({ (specialist) -> String in
            return specialist.name ?? ""
        })
        if let curSpecialist = currentSpecialist {
            let index = specialistResult.index(where: { (specialist) -> Bool in
                specialist.id == curSpecialist.id
            })
            specialistComboBox.selectRow(at: index)
        }
    }
    
    func fwComboBox(comboBox: FWComboBox, didSelectAtIndex index: Int) {
        if comboBox == specialistComboBox {
            currentSpecialist = specialistResult[index]
            if let specialistID = currentSpecialist?.id{
                self.listOrganizes = [Organize]()
                currentOrganizeRequest?.cancel()
                let progress = self.showProgress()
                self.currentOrganizeRequest = Organize.getAllOrganizeFollowSpecialistID(specialistID: specialistID, success: { (organizes) in
                    self.listOrganizes = organizes
                    self.myTableView.reloadData()
                    self.hideProgress(progress)
                }) { (error, response) in
                    self.hideProgress(progress)
                    self.handleNetworkError(error: error, responseData: response.data)
                }
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if searchBar.text != "" {
            return self.filteredOrganizeNames.count
        }
        return self.listOrganizes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MedUnitCell", for: indexPath) as! MedicalUnitCell
       
        DispatchQueue.main.async(execute: {
            var organizeObj:Organize? = nil
            if self.isSearching {
                organizeObj = self.filteredOrganizeNames[indexPath.row]
            }else{
                organizeObj = self.listOrganizes[indexPath.row]
            }
            if let avatar = organizeObj?.imgString{
                cell.medicalUnitThumbnail.contentMode = .scaleToFill
                let urlString = avatar.trimmingCharacters(in: .whitespaces)
                if avatar != "" || !avatar.isEmpty{
                    let urlString = API.baseURLImage + "\(API.iCNMImage)" + "/\(API.organizeImage)" + "\(urlString)"
                    cell.medicalUnitThumbnail.loadImageUsingUrlString(urlString: urlString)
                }else{
                    cell.medicalUnitThumbnail.image = UIImage(named: "avatar_default")
                }
            }else{
                cell.medicalUnitThumbnail.image = UIImage(named: "avatar_default")
            }
            
            if let organizeName = organizeObj?.organizeName{
                cell.medicalUnitName.text = organizeName
                cell.medicalUnitName.lineBreakMode = .byWordWrapping
                cell.medicalUnitName.numberOfLines = 0
            }
            if let organizeAddress = organizeObj?.organizeAddress{
                cell.medicalUnitLocation.text = organizeAddress
                cell.medicalUnitLocation.lineBreakMode = .byWordWrapping
                cell.medicalUnitLocation.numberOfLines = 0
            }
        })
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var currentOrganize:Organize? = nil
        if self.filteredOrganizeNames.count != 0 {
            currentOrganize = self.filteredOrganizeNames[indexPath.row]
        }else{
            if self.listOrganizes.count != 0{
                currentOrganize = self.listOrganizes[indexPath.row]
            }
        }

        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let currentDate = formatter.string(from: date)
        var specialistID:Int = 0
        if currentSpecialist != nil{
            specialistID = (currentSpecialist?.id)!
        }else{
            specialistID = 0
        }
        
        if let organizeID = currentOrganize?.id{
            let sADoctorVC = ScheduleAppointmentDoctorVC(nibName: "ScheduleAppointmentDoctorVC", bundle: nil)
            sADoctorVC.currentOrganize = currentOrganize
            sADoctorVC.currentSpecialist = self.currentSpecialist
            sADoctorVC.getDoctorFollowSpecialist(organizeID: organizeID, specialistID: specialistID, currentDate: currentDate)
            self.navigationController?.pushViewController(sADoctorVC, animated: true)
        }else{
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!)
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
        
        if searchController.isActive{
            self.navigationItem.leftBarButtonItem = nil
        }else{
            self.navigationItem.leftBarButtonItem = newBackButton
        }
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredOrganizeNames = listOrganizes.filter({( organize : Organize) -> Bool in
            return ((organize.organizeName?.lowercased().contains(searchText.lowercased()))! || (organize.imgString?.lowercased().contains(searchText.lowercased()))!)
        })
        
        if filteredOrganizeNames.count != 0 && !searchText.isEmpty{
            self.myTableView.reloadData()
        }else{
            self.filteredOrganizeNames = [Organize]()
            self.myTableView.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar:UISearchBar) {
        self.navigationItem.titleView = nil
        self.navigationItem.setRightBarButtonItems([searchBarButton!], animated: true)
        self.navigationItem.leftBarButtonItem = newBackButton
        self.filteredOrganizeNames = [Organize]()
        self.myTableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == "" {
            isSearching = false
            view.endEditing(true)
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
        else {
            isSearching = true
            filterContentForSearchText(searchText)
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
    }
    
    func didPresentSearchController(searchController: UISearchController) {
        searchController.searchBar.becomeFirstResponder()
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        searchController.searchBar.tintColor = UIColor.gray
        return true
    }
    
    func isSearchOn() {
        self.navigationItem.setRightBarButtonItems([self.searchOff], animated: false)
        self.navigationItem.titleView = searchBar
        searchBar.isHidden = false
        searchBar.becomeFirstResponder()
    }
    
    func isSearchOff() {
        searchBar.text = nil
        self.navigationItem.setRightBarButtonItems([self.searchOn], animated: false)
        self.navigationItem.titleView = nil
        if searchBar.text == "" {
            isSearching = false
            DispatchQueue.main.async {
                self.myTableView.reloadData()
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBar.resignFirstResponder()
    }
}
