//
//  ChoiceSpecialistCell.swift
//  iCNM
//
//  Created by Mac osx on 8/7/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit

class MedicalUnitCell: UITableViewCell {
    @IBOutlet weak var medicalUnitThumbnail: CustomImageView!
    @IBOutlet weak var medicalUnitName: UILabel!
    @IBOutlet weak var medicalUnitLocation: UILabel!
    @IBOutlet weak var myGroupView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let f = contentView.frame
        let fr = UIEdgeInsetsInsetRect(f, UIEdgeInsetsMake(10, 10, 10, 15))
        contentView.frame = fr
        addShadow(cell: self)
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func addShadow(cell:UITableViewCell) {
        cell.layer.cornerRadius = 4
        cell.layer.masksToBounds = true
        
        cell.layer.masksToBounds = false
        cell.layer.shadowOffset = CGSize(width: 0, height:5)
        cell.layer.shadowColor = UIColor.groupTableViewBackground.cgColor
        cell.layer.shadowOpacity = 0.2
        cell.layer.shadowRadius = 2
    }
    
}
