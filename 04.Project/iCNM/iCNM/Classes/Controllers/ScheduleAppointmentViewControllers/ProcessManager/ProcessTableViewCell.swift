//
//  ProcessTableViewCell.swift
//  iCNM
//
//  Created by Thanh Huyen on 8/21/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import UIKit
import Cosmos

class ProcessTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgProcess: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var coverView: UIView!
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var sumRating: CosmosView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if IS_IPHONE_5{
            sumRating.starSize = 20.0
        }else{
            sumRating.starSize = 25.0
        }
        sumRating.isHidden = true
        imgProcess.layer.cornerRadius = 25
        imgProcess.layer.masksToBounds = true
        imgProcess.layer.borderColor = UIColor.init(hex:"1D6EDC").cgColor
        imgProcess.layer.borderWidth = 2.5
        
        coverView.layer.borderColor = UIColor.init(hex:"1D6EDC").cgColor
        coverView.layer.borderWidth = 1
        coverView.layer.cornerRadius = 8
        coverView.layer.masksToBounds = true
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
