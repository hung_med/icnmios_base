//
//  SAViewController.swift
//  iCNM
//
//  Created by Medlatec on 7/27/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import FontAwesome_swift

class SAViewController: BaseViewControllerNoSearchBar, FWComboBoxDelegate, UITextFieldDelegate, UITextViewDelegate, FWDatePickerDelegate, PointSymptonVCDelegate, UIScrollViewDelegate {
  
    @IBOutlet weak var organizeLabel: UILabel!
    @IBOutlet weak var specialistLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var selectDateLabel: UILabel!
    @IBOutlet weak var timeRangeLabel: UILabel!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblRelation: UILabel!
    
    @IBOutlet weak var avatarImageView: PASImageView!
    @IBOutlet weak var personComboBox: FWComboBox!
    @IBOutlet weak var relationComboBox: FWComboBox!
    @IBOutlet weak var doctorIDTextField: FWFloatingLabelTextField!
    @IBOutlet weak var phoneNumberTextField: FWFloatingLabelTextField!
    @IBOutlet weak var addressTextField: FWFloatingLabelTextField!
    @IBOutlet weak var fullNameTextField: FWFloatingLabelTextField!
//    @IBOutlet weak var birthdayTextField: FWFloatingLabelTextField!
    @IBOutlet weak var birthdayDatePicker: FWDatePicker!
    @IBOutlet weak var genderTextField: FWFloatingLabelTextField!
    @IBOutlet weak var maleRadioButton: ISRadioButton!
    @IBOutlet weak var femaleRadioButton: ISRadioButton!
    @IBOutlet weak var organizeComboBox: FWComboBox!
    @IBOutlet weak var specialistComboBox: FWComboBox!
    @IBOutlet weak var listScheduleTimeComboBox: FWComboBox!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var pointSymptomsTextView: UITextView!
    @IBOutlet weak var btnSelectPointMedical: UIButton!
    @IBOutlet weak var appointmentDatePicker: FWDatePicker!
    //@IBOutlet weak var timeRangeCollectionView: UICollectionView!
    @IBOutlet weak var scrollView: FWCustomScrollView!
    private var currentTextField:UITextField?
    private var currentTextView:UITextView?
    private var currentOrganizeRequest:Request?
    private var currentBookingRequest:Request?
    private var currentSpecialistRequest:Request?
    private var currentScheduleTimeRequest:Request?
    private var listSelectResult:[TestMedicalSchedule]? = [TestMedicalSchedule]()
    private var medicalSchedule:[TestMedicalSchedule]? = [TestMedicalSchedule]()
    
    // HSSK
    var medicalFor = [String]()
    var arrayHealthRecords = [HealthRecords]()
    var healthRecords:HealthRecords?
    var medicalProfileObj:MedicalProfile?
    var medicalHealFactoryObj:MedicalHealthFactory?
    var medicalHealFactory = MedicalHealthFactory()
    
    var medicalVaccin1:[MedicalVaccin]?
    var medicalVaccin2:[MedicalVaccin]?
    var medicalVaccin3:[MedicalVaccin]?
    var medicalProfiles:[MedicalCilinnical]?
    
    var listMedicalFor:[String]?
    var currentMedicalForIndex:Int? = 0
    
    private var listTestCode:String = ""
    var currentIdx:Int? = 0
    var bookForOtherPerson : Bool?
    
    private var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    private var currentUserNotificationToken:NotificationToken?
    private var specialistNotificationToken:NotificationToken?
    private let specialistResult:Results<Specialist> = {
        let realm = try! Realm()
        return realm.objects(Specialist.self).filter("active == true")
    }()
    
    
    private var organizes:[Organize]? = [Organize]() {
        didSet {
            self.setupOrganizeComboBox()
        }
    }
    
    private var currentSpecialist:Specialist?
    private var currentTimeRange:ScheduleTime?
    private var currentOrganize:Organize? {
        didSet {
            //self.setupScheduleTimeComboBox()
            if let curOrganize = currentOrganize {
                currentScheduleTimeRequest?.cancel()
                let progress = self.showProgress()
                self.currentScheduleTimeRequest = curOrganize.getAllTimeRange(success: { (timeRanges) in
                    self.hideProgress(progress)
                    //self.setupScheduleTimeComboBox()
                }, fail: { (error, response) in
                    self.hideProgress(progress)
                    self.handleNetworkError(error: error, responseData: response.data)
                })
            } else {
                switch typeSegment.selectedSegmentIndex {
                case 0:
                    firstSegmentCurrentTimeRange = nil
                case 1:
                    secondSegmentCurrentTimeRange = nil
                case 2:
                    thirdSegmentCurrentTimeRange = nil
                default:
                    break
                }
            }
        }
    }
    private var firstSegmentCurrentTimeRange:ScheduleTime?
    private var secondSegmentCurrentTimeRange:ScheduleTime?
    private var thirdSegmentCurrentTimeRange:ScheduleTime?
    
    @IBOutlet weak var organizeLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var organizeLabelComboBoxSpaceConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var organizeComboBoxHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var organizeComboBoxSpecialistLabelSpaceConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var specialistLabelHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var specialistNoteLabelSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var specialistComboBoxHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var specialistLabelComboBoxSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var myBorderViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var typeSegment: UISegmentedControl!
    //@IBOutlet weak var timeRangeSpaceCollectionView: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        // Setup the Search Controller
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(SAViewController.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }
        birthdayDatePicker.delegate = self
        appointmentDatePicker.delegate = self
        appointmentDatePicker.minimumDate = Date()
        appointmentDatePicker.titleFont = UIFont.boldSystemFont(ofSize: 15.0)
        appointmentDatePicker.textColor = UIColor(hex: "1d6edc")
        appointmentDatePicker.contentHorizontalAlignment = .right
        if let nvc = self.navigationController {
            if nvc.viewControllers.count == 1 {
                self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Đóng", style: UIBarButtonItemStyle.plain, target: self, action: #selector(touchCloseButton(_:)));
            }
        }
        if currentUser == nil {
            self.navigationController?.dismiss(animated: true, completion: nil)
            return
        }
        
        let btnButton =  UIButton(type: .custom)
        btnButton.titleLabel?.font = UIFont.fontAwesome(ofSize: 15.0)
        let buttonName = String.fontAwesomeIcon(name: .send)
        btnButton.setTitle(buttonName, for: .normal)
        
        btnButton.addTarget(self, action: #selector(SAViewController.touchSendButton), for: .touchUpInside)
        btnButton.frame = CGRect(x: screenSizeWidth-20, y: 0, width: 25, height:25)
        let filterBtnItem = UIBarButtonItem(customView: btnButton)
        self.navigationItem.setRightBarButtonItems([filterBtnItem], animated: true)

        //self.btnSelectPointMedical.titleLabel?.font = UIFont.fontAwesome(ofSize: 15.0)
        let attrs = [
            NSFontAttributeName: UIFont.fontAwesome(ofSize: 16.0),
            NSForegroundColorAttributeName : UIColor(hex:"1976D2"),
            NSUnderlineStyleAttributeName : 1] as [String : Any] as [String : Any]
        let title = String.fontAwesomeIcon(name: .medkit) + "  Chọn chỉ định xét nghiệm"
        let attributedString = NSMutableAttributedString(string:"")
        let buttonTitleStr = NSMutableAttributedString(string: title, attributes:attrs)
        attributedString.append(buttonTitleStr)
        self.btnSelectPointMedical.setAttributedTitle(attributedString, for:.normal)
        
        personComboBox.layer.cornerRadius = 4.0
        personComboBox.layer.masksToBounds = true
        personComboBox.layer.borderColor = UIColor.lightGray.cgColor
        personComboBox.layer.borderWidth = 0.5
        
        relationComboBox.layer.cornerRadius = 4.0
        relationComboBox.layer.masksToBounds = true
        relationComboBox.layer.borderColor = UIColor.lightGray.cgColor
        relationComboBox.layer.borderWidth = 0.5
        
        organizeComboBox.layer.cornerRadius = 4.0
        organizeComboBox.layer.masksToBounds = true
        organizeComboBox.layer.borderColor = UIColor.lightGray.cgColor
        organizeComboBox.layer.borderWidth = 0.5
        
        specialistComboBox.layer.cornerRadius = 4.0
        specialistComboBox.layer.masksToBounds = true
        specialistComboBox.layer.borderColor = UIColor.lightGray.cgColor
        specialistComboBox.layer.borderWidth = 0.5
        
        listScheduleTimeComboBox.layer.cornerRadius = 4.0
        listScheduleTimeComboBox.layer.masksToBounds = true
        listScheduleTimeComboBox.layer.borderColor = UIColor.lightGray.cgColor
        listScheduleTimeComboBox.layer.borderWidth = 0.5
        listScheduleTimeComboBox.comboTextAlignment = .center
        
        descriptionTextView.layer.cornerRadius = 4.0
        descriptionTextView.layer.masksToBounds = true
        descriptionTextView.layer.borderColor = UIColor.lightGray.cgColor
        descriptionTextView.layer.borderWidth = 0.5
    
        pointSymptomsTextView.layer.cornerRadius = 4.0
        pointSymptomsTextView.layer.masksToBounds = true
        pointSymptomsTextView.layer.borderColor = UIColor.lightGray.cgColor
        pointSymptomsTextView.layer.borderWidth = 0.5
        pointSymptomsTextView.isUserInteractionEnabled = false
        self.pointSymptomsTextView.isHidden = true
        
        // show number of hssk in list
        if let id = currentUser?.userDoctor?.userInfo?.id {
            HealthRecords.getMedicalProfileByUserID(userID: id, success: { (data) in
                if data.count > 0 {
                    self.personComboBox.dataSource.append("Tôi")
                    self.arrayHealthRecords = data
                    for medicalProfile in data {
                        self.personComboBox.dataSource.append((medicalProfile.medicalProfile?.patientName)!)
                        if let medicalPerson = medicalProfile.userMedicalProfile?.medicalFor {
                            self.medicalFor.append(medicalPerson)
                        }
                    }
                    self.personComboBox.dataSource.append("Người khác")
                    // check duplicate user
                    self.personComboBox.dataSource.removeAll { $0 == self.currentUser?.userDoctor?.userInfo?.name }
                    self.personComboBox.selectRow(at: 0)
                    self.personComboBox.delegate = self
                    self.personComboBox.comboTextAlignment = .center
                } else {
                    self.personComboBox.dataSource = ["Tôi", "Người khác"]
                    self.personComboBox.selectRow(at: 0)
                    self.personComboBox.delegate = self
                    self.personComboBox.comboTextAlignment = .center
                }
            }) { (error, response) in
                print(error)
            }
        }

        // HSSK
        listMedicalFor = ["Tôi", "Chồng", "Vợ", "Bố", "Mẹ", "Con", "Ông", "Bà", "Anh", "Chị", "Em"]
        self.medicalVaccin1 = [MedicalVaccin]()
        self.medicalVaccin2 = [MedicalVaccin]()
        self.medicalVaccin3 = [MedicalVaccin]()
        self.medicalProfiles = [MedicalCilinnical]()
        self.medicalProfileObj = MedicalProfile()
        self.medicalHealFactoryObj = medicalHealFactory
        
        relationComboBox.dataSource = listMedicalFor!
        relationComboBox.defaultTitle = (listMedicalFor?[currentMedicalForIndex!])!
        relationComboBox.selectRow(at: 0)
        relationComboBox.delegate = self
        relationComboBox.comboTextAlignment = .center
        
        bookForOtherPerson = false
        
        // hide combobox
        relationComboBox.isHidden = true
        lblRelation.isHidden = true
        topConstraint.constant = 120
        
        if let avatarURL = currentUser?.userDoctor?.userInfo?.getAvatarURL() {
            avatarImageView.imageURL(URL: avatarURL)
        }
        if let doctorIDCode = currentUser?.userDoctor?.userInfo?.doctorIDCode{
            if !doctorIDCode.isEmpty{
                doctorIDTextField.text = "\(doctorIDCode)"
                doctorIDTextField.isUserInteractionEnabled = false
            }
        }else{
            doctorIDTextField.isUserInteractionEnabled = true
        }
        
        //timeRangeLabel.font = UIFont.fontAwesome(ofSize: 14.0)
        //let timeRange = String.fontAwesomeIcon(name: .clockO)
        //timeRangeLabel.text = timeRange + "   Chọn nơi khám để hiển thị khung giờ"
        phoneNumberTextField.text = currentUser?.userDoctor?.userInfo?.phone
        fullNameTextField.text = currentUser?.userDoctor?.userInfo?.name
        phoneNumberTextField.addRegx(strRegx: "^[0-9]{10,11}$", errorMsg: "Số điện thoại không đúng định dạng")
        doctorIDTextField.addRegx(strRegx: "^[0-9]{10,11}$", errorMsg: "Mã bác sĩ không đúng định dạng")
        phoneNumberTextField.messageForValidatingLength = "Số điện thoại không được bỏ trống"
        fullNameTextField.messageForValidatingLength = "Tên không được bỏ trống"
        addressTextField.messageForValidatingLength = "Địa chỉ không được bỏ trống"
        genderTextField.messageForValidatingLength = "Bạn chưa chọn giới tính"
//        birthdayTextField.messageForValidatingLength = "Năm sinh không được bỏ trống"
        
        addressTextField.text = currentUser?.userDoctor?.userInfo?.address
        if let birthday = currentUser?.userDoctor?.userInfo?.birthday {
            birthdayDatePicker.currentDate = birthday
        }
//        birthdayTextField.text = year
        //maleRadioButton.isUserInteractionEnabled = false
        //femaleRadioButton.isUserInteractionEnabled = false
        if let gender = currentUser?.userDoctor?.userInfo?.gender {
            if gender == "M" {
                genderTextField.text = "Nam"
                maleRadioButton.isSelected = true
            } else {
                genderTextField.text = "Nữ"
                femaleRadioButton.isSelected = true
            }
        } else {
            genderTextField.text = ""
            maleRadioButton.isSelected = false
            femaleRadioButton.isSelected = false
        }
        
//        timeRangeCollectionView.isHidden = true
//        timeRangeCollectionView.dataSource = self
//        timeRangeCollectionView.delegate = self
//        timeRangeCollectionView.collectionViewLayout = SACollectionViewFlowLayout()
        currentUserNotificationToken = currentUser?.observe({[weak self] (change) in
            switch change {
            case .change:
                print("Thay doi")
            case .error(let error):
                print("Co loi \(error)")
            case .deleted:
                print("bi xoa")
                self?.navigationController?.dismiss(animated: true, completion: nil)
            }
        })
        specialistNotificationToken = specialistResult.observe({[weak self] (changes) in
            switch changes {
            case .initial:
                // Results are now populated and can be accessed without blocking the UI
                self?.setupSpecialistComboBox()
                break
            case .update:
                // Query results have changed, so apply them to the UITableView
                self?.setupSpecialistComboBox()
                break
            case .error(let error):
                // An error occurred while opening the Realm file on the background worker thread
                fatalError("\(error)")
                break
            }
        })
        birthdayDatePicker.delegate = self
        doctorIDTextField.delegate = self
        phoneNumberTextField.delegate = self
        fullNameTextField.delegate = self
        addressTextField.delegate = self
        genderTextField.delegate = self
//        birthdayTextField.delegate = self
        descriptionTextView.delegate = self
        specialistComboBox.delegate = self
        organizeComboBox.delegate = self
        listScheduleTimeComboBox.delegate = self
        //timeRangeSpaceCollectionView.constant = 0
        myBorderViewHeightConstraint.constant = 1000
        registerForNotifications()
        requestData()
        // Do any additional setup after loading the view.
//        self.getListDictMedical()
        self.checkTrackingFeature()
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: "Đặt lịch")
        guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
        tracker.send(builder.build() as [NSObject : AnyObject])
    }
    
    func checkTrackingFeature(){
        if currentUser != nil{
            let userDefaults = UserDefaults.standard
            let token = userDefaults.object(forKey: "token") as! String
            TrackingFeature().checkTrackingFeature(fcm:token, featureName:Constant.SCHEDULE, categoryID: 0,  success: {(result) in
                if result != nil{
                    print("flow track done:", Constant.SCHEDULE)
                }else{
                    print("flow track fail:", Constant.SCHEDULE)
                }
            }, fail: { (error, response) in
                self.handleNetworkError(error: error, responseData: response.data)
            })
        }else{
            return
        }
    }
    
    @IBAction func touchGenderRadioButton(_ sender: ISRadioButton) {
        if sender == maleRadioButton {
            genderTextField.text = "Nam"
        } else {
            genderTextField.text = "Nữ"
        }
    }
    
    override func requestData() {
        currentSpecialistRequest?.cancel()
        currentOrganizeRequest?.cancel()
        currentScheduleTimeRequest?.cancel()
        let progress = self.showProgress()
        currentSpecialistRequest = Specialist.getAllSpecialist(success: { (data) in
            self.currentOrganizeRequest = Organize.GetAllOrganizedSchedule(success: { (organizes) in
                self.organizes = organizes
                if let curOrganize = self.currentOrganize {
                    self.currentScheduleTimeRequest = curOrganize.getAllTimeRange(success: { (timeRanges) in
                        self.hideProgress(progress)
                        self.setupScheduleTimeComboBox()
                        //self.setupTimeRangeCollectionView()
                    }, fail: { (error, response) in
                        self.hideProgress(progress)
                        self.handleNetworkError(error: error, responseData: response.data)
                    })
                } else {
                    self.hideProgress(progress)
                }
                
            }) { (error, response) in
                self.hideProgress(progress)
                self.handleNetworkError(error: error, responseData: response.data)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func setupOrganizeComboBox() {
        if let organizes = self.organizes {
            let activeOrganizes = organizes.filter({ (organizes) -> Bool in
                return organizes.active
            })
            organizeComboBox.dataSource = activeOrganizes.map({ (organizes) -> String in
                return organizes.organizeName!
            })
            
            organizeComboBox.selectRow(at: 0)
            currentOrganize = organizes[0]
            self.currentIdx = 1060
            myBorderViewHeightConstraint.constant = CGFloat(self.currentIdx!) + 16*CGFloat((self.listSelectResult?.count)!)
        } else {
            organizeComboBox.selectRow(at: nil)
            organizeComboBox.dataSource = [String]()
        }
    }
    
//    private func setupOrganizeComboBox() {
//        if typeSegment.selectedSegmentIndex == 0 {
//            organizeComboBox.dataSource = organizes.map({ (organize) -> String in
//                return organize.organizeName ?? ""
//            })
//        }
//        if let curOrganize = currentOrganize {
//            let index = organizes.index(where: { (organize) -> Bool in
//                organize.id == curOrganize.id
//            })
//            if index == nil {
//                currentOrganize = nil
//            }
//            if typeSegment.selectedSegmentIndex == 0 {
//                organizeComboBox.selectRow(at: index)
//            }
//        }
//    }
//
    private func setupSpecialistComboBox() {
        specialistComboBox.dataSource = specialistResult.map({ (specialist) -> String in
            return specialist.name ?? ""
        })
        specialistComboBox.selectRow(at: 0)
        currentSpecialist = specialistResult[0]
        if let curSpecialist = currentSpecialist {
            let index = specialistResult.index(where: { (specialist) -> Bool in
                specialist.id == curSpecialist.id
            })
            specialistComboBox.selectRow(at: index)
        }
    }
    
    private func setupScheduleTimeComboBox() {
        listScheduleTimeComboBox.dataSource = (currentOrganize?.timeRanges.map({ (scheduleTime) -> String in
            return scheduleTime.timeString ?? ""
        }))!
        
        listScheduleTimeComboBox.selectRow(at: 0)
        currentTimeRange = currentOrganize?.timeRanges[0]
    }
    
//    private func setupTimeRangeCollectionView() {
//        let currentTimeRange = typeSegment.selectedSegmentIndex == 0 ? firstSegmentCurrentTimeRange : typeSegment.selectedSegmentIndex == 1 ? secondSegmentCurrentTimeRange : thirdSegmentCurrentTimeRange
//        if let curTimeRange = currentTimeRange {
//            let index = currentOrganize?.timeRanges.index(where: { (timeRange) -> Bool in
//                timeRange.id == curTimeRange.id
//            })
//            if index == nil {
//                switch typeSegment.selectedSegmentIndex {
//                case 0:
//                    firstSegmentCurrentTimeRange = nil
//                case 1:
//                    secondSegmentCurrentTimeRange = nil
//                case 2:
//                    thirdSegmentCurrentTimeRange = nil
//                default:
//                    break
//                }
//            }
//        }
//        timeRangeCollectionView.reloadData()
//    }
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fwComboBoxWillShow(comboBox:FWComboBox) {
        currentTextField?.resignFirstResponder()
        currentTextView?.resignFirstResponder()
    }
    
    func fwComboBox(comboBox: FWComboBox, didSelectAtIndex index: Int) {
        if comboBox == personComboBox {
            if index == 0 {
                // hide combobox
                relationComboBox.isHidden = true
                lblRelation.isHidden = true
                topConstraint.constant = 120
                relationComboBox.isUserInteractionEnabled = true
                bookForOtherPerson = false
                
                if let avatarURL = currentUser?.userDoctor?.userInfo?.getAvatarURL(){
                    avatarImageView.imageURL(URL: avatarURL)
                }
                if let doctorIDCode = currentUser?.userDoctor?.userInfo?.doctorIDCode{
                    if !doctorIDCode.isEmpty{
                        doctorIDTextField.text = "\(doctorIDCode)"
                        doctorIDTextField.isUserInteractionEnabled = false
                    }
                }else{
                    doctorIDTextField.isUserInteractionEnabled = true
                }
                
                phoneNumberTextField.text = currentUser?.userDoctor?.userInfo?.phone
                addressTextField.text = currentUser?.userDoctor?.userInfo?.address
                fullNameTextField.text = currentUser?.userDoctor?.userInfo?.name
                if let birthday = currentUser?.userDoctor?.userInfo?.birthday {
                    birthdayDatePicker.currentDate = birthday
                }
                //maleRadioButton.isUserInteractionEnabled = false
                //femaleRadioButton.isUserInteractionEnabled = false
                if let gender = currentUser?.userDoctor?.userInfo?.gender {
                    if gender == "M" {
                        genderTextField.text = "Nam"
                        maleRadioButton.isSelected = true
                        femaleRadioButton.isSelected = false
                    } else {
                        genderTextField.text = "Nữ"
                        femaleRadioButton.isSelected = true
                        maleRadioButton.isSelected = false
                    }
                }
            // for last choose
            } else if index == personComboBox.dataSource.count - 1 {
                // show custom combobox
                relationComboBox.dataSource = listMedicalFor!
                relationComboBox.dataSource.remove(at: 0)
                relationComboBox.selectRow(at: 0)
                relationComboBox.alpha = 1
                relationComboBox.isHidden = false
                lblRelation.isHidden = false
                topConstraint.constant = 142
                relationComboBox.isUserInteractionEnabled = true
                // change to other booking
                bookForOtherPerson = true
                
                avatarImageView.reset()
                if let doctorIDCode = currentUser?.userDoctor?.userInfo?.doctorIDCode{
                    if !doctorIDCode.isEmpty{
                        doctorIDTextField.text = "\(doctorIDCode)"
                        doctorIDTextField.isUserInteractionEnabled = false
                    }
                }else{
                    doctorIDTextField.isUserInteractionEnabled = true
                }
                
                phoneNumberTextField.text = ""
                genderTextField.text = ""
                femaleRadioButton.isSelected = false
                maleRadioButton.isSelected = false
                addressTextField.text = ""
                fullNameTextField.text = ""
                birthdayDatePicker.currentDate = Date()
//                birthdayTextField.text = ""
                //maleRadioButton.isUserInteractionEnabled = true
                //femaleRadioButton.isUserInteractionEnabled = true
            // for other choose
            } else {
                // show combobox
                relationComboBox.isHidden = false
                lblRelation.isHidden = false
                topConstraint.constant = 142
                
                // custom combo box
                relationComboBox.dataSource.removeAll()
                relationComboBox.dataSource = [medicalFor[index]]
                relationComboBox.selectRow(at: 0)
                relationComboBox.reloadInputViews()
                relationComboBox.alpha = 0.5
                relationComboBox.isUserInteractionEnabled = false
                bookForOtherPerson = false
                
                if let imgString = arrayHealthRecords[index].medicalProfile?.imgUrl {
                    if let id = arrayHealthRecords[index].userMedicalProfile?.userID {
                        let urlString = API.baseURLImage + API.iCNMImage + "\(id)/\(imgString)".trim()
                        if let url = URL(string: urlString) {
                            avatarImageView.imageURL(URL: url)
                        }
                    } else {
                        avatarImageView.reset()
                    }
                }
                
                phoneNumberTextField.text = arrayHealthRecords[index].medicalProfile?.mobilePhone
                addressTextField.text = arrayHealthRecords[index].medicalProfile?.address
                fullNameTextField.text = arrayHealthRecords[index].medicalProfile?.patientName
                if let dateString = arrayHealthRecords[index].medicalProfile?.birthDay {
                    let dateFormater = DateFormatter()
                    dateFormater.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    birthdayDatePicker.currentDate = dateFormater.date(from: dateString)!
                }
                //maleadioButton.isUserInteractionEnabled = false
                //femaleRadioButton.isUserInteractionEnabled = false
                if let gender = arrayHealthRecords[index].medicalProfile?.gender {
                    if gender == "M" {
                        genderTextField.text = "Nam"
                        femaleRadioButton.isSelected = false
                        maleRadioButton.isSelected = true
                    } else {
                        genderTextField.text = "Nữ"
                        femaleRadioButton.isSelected = true
                        maleRadioButton.isSelected = false
                    }
                }
            }
        } else if comboBox == organizeComboBox {
            currentOrganize = organizes?[index]
            if typeSegment.selectedSegmentIndex == 0{
                self.currentIdx = 1060
                myBorderViewHeightConstraint.constant = CGFloat(self.currentIdx!) + 16*CGFloat((self.listSelectResult?.count)!)
            }else if typeSegment.selectedSegmentIndex == 1{
                self.currentIdx = 1000
                myBorderViewHeightConstraint.constant = CGFloat(self.currentIdx!) + 16*CGFloat((self.listSelectResult?.count)!)
            }else{
                self.currentIdx = 1000
                myBorderViewHeightConstraint.constant = CGFloat(self.currentIdx!) + 16*CGFloat((self.listSelectResult?.count)!)
            }
            
        } else if comboBox == specialistComboBox {
            currentSpecialist = specialistResult[index]
        } else if comboBox == listScheduleTimeComboBox{
            self.currentTimeRange = currentOrganize!.timeRanges[index]
        } else if comboBox == relationComboBox {
            currentMedicalForIndex = index
        }
    }
    
    @IBAction func pointSymptonBtn(_ sender: Any) {
        self.showListPointMedical()
    }
   
    func gobackAppointmentListVC(listSelectMedicalSchedule: [TestMedicalSchedule]?, medicalSchedule: [TestMedicalSchedule]?) {
        if listSelectMedicalSchedule?.count != 0{
            self.pointSymptomsTextView.text = ""
            self.pointSymptomsTextView.isHidden = false
            self.listSelectResult = listSelectMedicalSchedule
            self.medicalSchedule = medicalSchedule
            var resultStr = ""
            self.listTestCode = ""
            var total:Float = 0
            for item in listSelectMedicalSchedule! {
                self.listTestCode = self.listTestCode + "\(item.testName); "
                resultStr = resultStr + "\(item.testName)\n"
                total = total + item.price
                let format = formatString(value: total)
                if let count = listSelectMedicalSchedule?.count{
                    let title = String.fontAwesomeIcon(name: .medkit) + "  Chỉ định đã chọn:" + " \(count) (\(format))"
                    self.btnSelectPointMedical.setTitle(title, for: .normal)
                }
            }
            self.pointSymptomsTextView.text = String(resultStr.dropLast(1))
            self.pointSymptomsTextView.textColor = UIColor(hex:"1D6EDC")
            self.pointSymptomsTextView.layoutSubviews()
            myBorderViewHeightConstraint.constant = CGFloat(self.currentIdx!) + CGFloat(16*(listSelectMedicalSchedule?.count)!)
        }else{
            self.listSelectResult = [TestMedicalSchedule]()
            self.listTestCode = ""
            self.pointSymptomsTextView.text = ""
            self.pointSymptomsTextView.isHidden = true
            myBorderViewHeightConstraint.constant = CGFloat(self.currentIdx!) + CGFloat(16*(listSelectMedicalSchedule?.count)!)
            let title = String.fontAwesomeIcon(name: .medkit) + "  Chọn chỉ định xét nghiệm"
            self.btnSelectPointMedical.setTitle(title, for: .normal)
        }
    }
    
    func showListPointMedical(){
        let pointSymptonVC = PointSymptonVC(nibName: "PointSymptonVC", bundle: nil)
        pointSymptonVC.delegate = self
        pointSymptonVC.listSelectResult = self.listSelectResult!
        pointSymptonVC.medicalSchedule = self.medicalSchedule
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(pointSymptonVC, animated: true)
    }
   
    @IBAction func touchCloseButton(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func touchSendButton(_ sender: Any) {
        
        if !addressTextField.validate() {
            addressTextField.becomeFirstResponder()
            if IS_IPHONE_X{
                self.scrollView.setContentOffset(CGPoint.init(x: 0, y: -86), animated: true)
            }else{
                self.scrollView.setContentOffset(CGPoint.init(x: 0, y: -62), animated: true)
            }
        }
        
        if !phoneNumberTextField.validate() {
            phoneNumberTextField.messageForValidatingLength = "Số điện thoại không được bỏ trống"
            phoneNumberTextField.becomeFirstResponder()
            if IS_IPHONE_X{
                self.scrollView.setContentOffset(CGPoint.init(x: 0, y: -86), animated: true)
            }else{
                self.scrollView.setContentOffset(CGPoint.init(x: 0, y: -62), animated: true)
            }
        }
        
        if !fullNameTextField.validate() {
            fullNameTextField.becomeFirstResponder()
            if IS_IPHONE_X{
                self.scrollView.setContentOffset(CGPoint.init(x: 0, y: -86), animated: true)
            }else{
                self.scrollView.setContentOffset(CGPoint.init(x: 0, y: -62), animated: true)
            }
        }
        
        currentTextField?.resignFirstResponder()
        currentTextView?.resignFirstResponder()
        if fullNameTextField.validate() && phoneNumberTextField.validate() && addressTextField.validate() && genderTextField.validate() {
//            if birthdayTextField.text?.count != 4{
//                showAlertView(title: "Năm sinh không đúng định dạng", view: self)
//                return
//            }else{
//                let date = Date()
//                let calendar = Calendar.current
//                let currentYear = Int(calendar.component(.year, from: date))
//
//                if let birthDay = Int(birthdayTextField.text!) {
//                    if birthDay > currentYear{
//                        showAlertView(title: "Năm sinh không lớn hơn năm hiện tại", view: self)
//                        return
//                    }
//                }
//            }
            
            if currentOrganize == nil && typeSegment.selectedSegmentIndex == 0 {
                let alertViewController = UIAlertController(title: "Thông báo", message: "Bạn chưa chọn nơi khám", preferredStyle: .alert)
                let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                alertViewController.addAction(noAction)
                self.present(alertViewController, animated: true, completion: nil)
                return
            }
            if currentSpecialist == nil && typeSegment.selectedSegmentIndex != 2 {
                let alertViewController = UIAlertController(title: "Thông báo", message: "Bạn chưa chọn chuyên khoa", preferredStyle: .alert)
                let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                alertViewController.addAction(noAction)
                self.present(alertViewController, animated: true, completion: nil)
                return
            }
            
            if self.currentTimeRange == nil {
                let alertViewController = UIAlertController(title: "Thông báo", message: "Bạn chưa chọn khung giờ", preferredStyle: .alert)
                let noAction = UIAlertAction(title: "Đóng", style: UIAlertActionStyle.default, handler: nil)
                alertViewController.addAction(noAction)
                self.present(alertViewController, animated: true, completion: nil)
                return
            }
            
            if !self.listTestCode.isEmpty{
                let alert = UIAlertController(title: "Thông báo", message: "Bạn có chắc chắn muốn chọn các chỉ định này không?", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Đồng ý", style: UIAlertActionStyle.default, handler: { action in
                    self.sendScheduleAppoint(currentTimeRange: self.currentTimeRange!)
                }))
                
                alert.addAction(UIAlertAction(title: "Huỷ bỏ", style: UIAlertActionStyle.cancel, handler: {(action:UIAlertAction!) in
                }))
                self.present(alert, animated: true, completion: nil)
            }else{
                
                // check who book
                if self.bookForOtherPerson == true {
                    let alert = UIAlertController(title: "Hồ sơ sức khoẻ", message: "Bạn có muốn tạo hồ sơ sức khoẻ cho \(self.fullNameTextField.text!) không", preferredStyle: .alert)
                    let action1 = UIAlertAction(title: "Đồng ý", style: .default, handler: { (action1) in
                        self.autoGenHSSK()
                        self.sendScheduleAppoint(currentTimeRange: self.currentTimeRange!)
                    })
                    let action2 = UIAlertAction(title: "Không", style: .cancel) { (action2) in
                        self.sendScheduleAppoint(currentTimeRange: self.currentTimeRange!)
                    }
                    alert.addAction(action1)
                    alert.addAction(action2)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func sendScheduleAppoint(currentTimeRange:ScheduleTime){
        let schedule = Schedule()
        schedule.timeID = currentTimeRange.id
        schedule.typeID = typeSegment.selectedSegmentIndex == 0 ? 2 : typeSegment.selectedSegmentIndex == 1 ? 1:3
        schedule.userID = currentUser!.id
        schedule.userName = fullNameTextField.text!
        schedule.organizeID = currentOrganize!.id
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy"
        schedule.birthYear = Int(dateFormater.string(from: birthdayDatePicker.currentDate))!
        schedule.medicalProfileID = medicalProfileObj?.medicalProfileID
    
        if let doctorIDCode = doctorIDTextField.text{
            if !doctorIDCode.isEmpty{
                schedule.doctorIDCode = doctorIDCode
                schedule.note = descriptionTextView.text + " - [Bacsi_MED]"
            }else{
                let doctorID = currentUser?.userDoctor?.userInfo?.doctorID
                schedule.doctorID = doctorID!
                schedule.note = descriptionTextView.text + " - [Bacsi_iCNM]"
            }
        }else{
            let doctorID = currentUser?.userDoctor?.userInfo?.doctorID
            schedule.doctorID = doctorID!
            schedule.note = descriptionTextView.text + " - [Bacsi_iCNM]"
        }
        
//        if let birthYear = birthdayTextField.text?.trim(){
//            schedule.birthYear = Int(birthYear)!
//        }else{
//            schedule.birthYear = 0
//        }
        
        if let specialistID = self.currentSpecialist?.id{
            schedule.specialistID = specialistID
        }
        
        schedule.phone = phoneNumberTextField.text!
        schedule.address = addressTextField.text!
        schedule.gender = maleRadioButton.isSelected ? "M":"F"
        schedule.dateSchedule = appointmentDatePicker.currentDate
        schedule.listTestCode = String(self.listTestCode.dropLast(2))
        if typeSegment.selectedSegmentIndex != 2{
            schedule.specialistID = currentSpecialist!.id
        }
        
        currentBookingRequest?.cancel()
        let progress = self.showProgress()
        print(schedule.toJSON())

        currentBookingRequest = Schedule.bookAnAppointment(parameter: schedule.toJSON(), success: { (result) in
            self.hideProgress(progress)
            if result == "true" {
                let alert = UIAlertController(title: "Thông báo", message: "Lịch hẹn đã được đăng ký thành công. Vui lòng kiểm tra ở phần 'Lịch sử lịch hẹn' trong menu ứng dụng", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Lịch sử lịch hẹn", style: UIAlertActionStyle.default, handler: { alertAction in
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let scheduleAnAppointmentVC = storyboard.instantiateViewController(withIdentifier: "ViewAppointmentListVC") as! ViewAppointmentListViewController
                    self.tabBarController?.tabBar.isHidden = true
                    self.navigationController?.pushViewController(scheduleAnAppointmentVC, animated: true)
                }))
                alert.addAction(UIAlertAction(title: "Đóng", style: UIAlertActionStyle.cancel, handler: { alertAction in
                    self.pointSymptomsTextView.text = ""
                    self.pointSymptomsTextView.isHidden = true
                    self.descriptionTextView.text = ""
                    self.listTestCode = ""
                    let title = String.fontAwesomeIcon(name: .medkit) + "  Chọn chỉ định xét nghiệm"
                    self.btnSelectPointMedical.setTitle(title, for: .normal)
                    self.listSelectResult = [TestMedicalSchedule]()
                    self.reloadDataInSegment(segmentIndex: self.typeSegment.selectedSegmentIndex)
                    self.requestData()
                    self.view.setNeedsLayout()
                    self.view.layoutIfNeeded()
                }))
                self.present(alert, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: "Thông báo", message: "Lịch hẹn đã được đăng ký thành công. Vui lòng kiểm tra ở phần 'Lịch sử lịch hẹn' trong menu ứng dụng", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Lịch sử lịch hẹn", style: UIAlertActionStyle.default, handler: { alertAction in
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let scheduleAnAppointmentVC = storyboard.instantiateViewController(withIdentifier: "ViewAppointmentListVC") as! ViewAppointmentListViewController
                    self.tabBarController?.tabBar.isHidden = true
                    self.navigationController?.pushViewController(scheduleAnAppointmentVC, animated: true)
                }))
                alert.addAction(UIAlertAction(title: "Đóng", style: UIAlertActionStyle.cancel, handler: { alertAction in
                    self.pointSymptomsTextView.text = ""
                    self.pointSymptomsTextView.isHidden = true
                    self.descriptionTextView.text = ""
                    self.listTestCode = ""
                    let title = String.fontAwesomeIcon(name: .medkit) + "  Chọn chỉ định xét nghiệm"
                    self.btnSelectPointMedical.setTitle(title, for: .normal)
                    self.listSelectResult = [TestMedicalSchedule]()
                    self.reloadDataInSegment(segmentIndex: self.typeSegment.selectedSegmentIndex)
                    self.requestData()
                    self.view.setNeedsLayout()
                    self.view.layoutIfNeeded()
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    func autoGenHSSK() {
        medicalProfileObj?.patientName = fullNameTextField.text!
        medicalProfileObj?.gender = maleRadioButton.isSelected ? "M":"F"
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd-MM-yyyy"
        medicalProfileObj?.birthDay = dateFormater.string(from: birthdayDatePicker.currentDate)
        medicalProfileObj?.mobilePhone = phoneNumberTextField.text!
        medicalProfileObj?.address = addressTextField.text!
        
        let medicalHealthFactoryUser = MedicalHealthFactoryUser()
        medicalHealthFactoryUser.medicalProfile = MedicalProfile()
        medicalHealthFactoryUser.medicalHealFactory = self.medicalHealFactoryObj
        medicalHealthFactoryUser.medicalProfile? = self.medicalProfileObj!
        medicalHealthFactoryUser.medicalFor = (listMedicalFor?[currentMedicalForIndex! + 1])!
        medicalHealthFactoryUser.medicalVaccinsTreEm = self.medicalVaccin1!
        medicalHealthFactoryUser.medicalVaccinsTCMR = self.medicalVaccin2!
        medicalHealthFactoryUser.medicalVaccinsUonVan = self.medicalVaccin3!
        medicalHealthFactoryUser.medicalCilinicals = self.medicalProfiles!
        if currentUser != nil {
            if let user = currentUser {
                medicalHealthFactoryUser.userID = user.id
            }
        }
        
        let progress = self.showProgress()
        medicalHealthFactoryUser.addEditMedicalHealthFactory(success: {(result) in
            if result != nil {
                self.hideProgress(progress)
                return
            } else {
//                showAlertView(title: "Không thể tạo mới hồ sơ sức khoẻ!", view: self)
            }
        }, fail: { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        })
    }
    
    @objc private func keyboardWillShow(aNotification:Notification) {
        if let curTextField = currentTextField {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            scrollView.contentInset = contentInset
            scrollView.scrollIndicatorInsets = contentInset
            if let rect = curTextField.superview?.convert(curTextField.frame, to: curTextField.superview?.superview) {
                let test = view.bounds.height - (rect.size.height + rect.origin.y + scrollView.contentInset.top) - 60.0
                let different = keyboardFrame.size.height - test
                if different > 0 {
                    scrollView.contentOffset.y = different - self.scrollView.contentInset.top
                }
            }
        } else if let curTextView = currentTextView {
            var userInfo = aNotification.userInfo!
            var keyboardFrame:CGRect = (userInfo[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
            keyboardFrame = view.convert(keyboardFrame, from: nil)
            var contentInset:UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height
            scrollView.contentInset = contentInset
            scrollView.scrollIndicatorInsets = contentInset
            if let rect = curTextView.superview?.convert(curTextView.frame, to: curTextView.superview?.superview) {
                let test = view.bounds.height - (rect.size.height + rect.origin.y + scrollView.contentInset.top) - 60.0
                let different = keyboardFrame.size.height - test
                if different > 0 {
                    scrollView.contentOffset.y = different - self.scrollView.contentInset.top
                }
            }
        }
    }
    
    @objc private func keyboardWillHide(aNotification:Notification) {
        var contentInset:UIEdgeInsets = scrollView.contentInset
        contentInset.bottom = 0
        scrollView.contentInset = contentInset
        scrollView.scrollIndicatorInsets = contentInset
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        currentTextField = textField
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        currentTextField = nil
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        currentTextField = nil
        return true
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if textField == birthdayTextField{
//            guard let text = textField.text else { return true }
//            let newLength = text.count + string.count - range.length
//            return newLength <= 4
//        }else
        if textField == doctorIDTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 8
        }else if textField == fullNameTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 50
        }else if textField == addressTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 100
        }else if textField == phoneNumberTextField{
            guard let text = textField.text else { return true }
            let newLength = text.count + string.count - range.length
            return newLength <= 12
        }else{
            return true
        }
    }

    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = descriptionTextView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        
        return changedText.count <= 100
    }
        
    @IBAction func touchScreen(_ sender: Any) {
        currentTextField?.resignFirstResponder()
        currentTextView?.resignFirstResponder()
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        currentTextView = textView
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        currentTextView = nil
        return true
    }
    
    deinit {
        specialistNotificationToken?.invalidate()
        currentUserNotificationToken?.invalidate()
        currentSpecialistRequest?.cancel()
        currentOrganizeRequest?.cancel()
        currentScheduleTimeRequest?.cancel()
        NotificationCenter.default.removeObserver(self)
    }
    
//    //UICollectionView Datasource
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
//
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        let count = currentOrganize?.timeRanges.count ?? 0
//        if count == 0 {
//            timeRangeSpaceCollectionView.constant = 0
//        } else {
//            timeRangeSpaceCollectionView.constant = 14
//        }
//        return count
//    }
//
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TimeRangeCell", for: indexPath) as! SACollectionViewCell
//        let index = indexPath.row
//        let timeRange = currentOrganize!.timeRanges[index]
//        cell.timeButton.tag = index
//        cell.timeButton.layer.cornerRadius = 14.0
//        cell.timeButton.layer.borderWidth = 1.0
//        let currentTimeRange = typeSegment.selectedSegmentIndex == 0 ? firstSegmentCurrentTimeRange : typeSegment.selectedSegmentIndex == 1 ? secondSegmentCurrentTimeRange : thirdSegmentCurrentTimeRange
//        if let curTimeRange = currentTimeRange, timeRange.id == curTimeRange.id {
//            cell.timeButton.layer.borderColor = UIColor(hex: "1d6edc").cgColor
//            if let timeStr = timeRange.timeString {
//                let attributedString = NSMutableAttributedString(string: " \(timeStr)")
//                if IS_IPHONE_5{
//                    attributedString.addAttribute(NSFontAttributeName, value:
//                        UIFont.boldSystemFont(ofSize: 11.0), range: NSMakeRange(2, timeStr.count))
//                }else{
//                    attributedString.addAttribute(NSFontAttributeName, value:
//                        UIFont.boldSystemFont(ofSize: 12.0), range: NSMakeRange(2, timeStr.count))
//                }
//                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor(hex:"1d6edc"), range: NSMakeRange(2, timeStr.count))
//                if IS_IPHONE_5{
//                    attributedString.addAttribute(NSFontAttributeName, value: FontFamily.FontAwesome.regular.font(size: 11.0), range: NSMakeRange(0, 1))
//                }else{
//                    attributedString.addAttribute(NSFontAttributeName, value: FontFamily.FontAwesome.regular.font(size: 12.0), range: NSMakeRange(0, 1))
//                }
//                attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor(hex:"39b54a"), range: NSMakeRange(0, 1))
//                cell.timeButton.setAttributedTitle(attributedString, for: .normal)
//            }
//        } else {
//            cell.timeButton.layer.borderColor = UIColor(hex: "cccccc").cgColor
//            cell.timeButton.setAttributedTitle(nil, for: .normal)
//            if IS_IPHONE_5{
//                cell.timeButton.titleLabel?.font = UIFont.systemFont(ofSize: 11.0)
//            }else{
//                cell.timeButton.titleLabel?.font = UIFont.systemFont(ofSize: 12.0)
//            }
//            cell.timeButton.setTitle(timeRange.timeString, for: .normal)
//        }
//        cell.timeButton.addTarget(self, action: #selector(touchTimeRangeButton(sender:)), for: .touchUpInside)
//        return cell
//    }
//
//    func touchTimeRangeButton(sender:UIButton) {
//        switch typeSegment.selectedSegmentIndex {
//        case 0:
//            firstSegmentCurrentTimeRange = currentOrganize?.timeRanges[sender.tag]
//        case 1:
//            secondSegmentCurrentTimeRange = currentOrganize?.timeRanges[sender.tag]
//        case 2:
//            thirdSegmentCurrentTimeRange = currentOrganize?.timeRanges[sender.tag]
//        default:
//            break
//        }
//        timeRangeCollectionView.reloadData()
//    }
    
    @IBAction func segmentValueChanged(_ sender: Any) {
        if let segment = sender as? UISegmentedControl {
            self.reloadDataInSegment(segmentIndex: segment.selectedSegmentIndex)
        }
    }
    
    func reloadDataInSegment(segmentIndex:Int) {
        switch(segmentIndex) {
        case 0:
            myBorderViewHeightConstraint.constant = 1000 + 16*CGFloat((self.listSelectResult?.count)!)
            organizeLabel.isHidden = false
            if let index = organizeComboBox.currentRow {
                currentOrganize = organizes?[index]
                self.currentIdx = 1060
                myBorderViewHeightConstraint.constant = CGFloat(self.currentIdx!) + 16*CGFloat((self.listSelectResult?.count)!)
            } else {
                currentOrganize = nil
            }
            organizeLabelHeightConstraint.constant = 17
            organizeComboBoxHeightConstraint.constant = 30
            organizeLabelComboBoxSpaceConstraint.constant = 8
            organizeComboBoxSpecialistLabelSpaceConstraint.constant = 8
            specialistLabelHeightConstraint.constant = 17
            specialistComboBoxHeightConstraint.constant = 30
            specialistLabelComboBoxSpaceConstraint.constant = 8
            specialistNoteLabelSpaceConstraint.constant = 8
        case 1:
            organizeLabel.isHidden = false
            myBorderViewHeightConstraint.constant = 925 + 16*CGFloat((self.listSelectResult?.count)!)
            if let index = organizeComboBox.currentRow {
                currentOrganize = organizes?[index]
                self.currentIdx = 1000
                myBorderViewHeightConstraint.constant = CGFloat(self.currentIdx!) + 16*CGFloat((self.listSelectResult?.count)!)
            } else {
                currentOrganize = nil
            }
            organizeLabelHeightConstraint.constant = 17
            organizeComboBoxHeightConstraint.constant = 30
            organizeLabelComboBoxSpaceConstraint.constant = 8
            organizeComboBoxSpecialistLabelSpaceConstraint.constant = 8
            specialistLabelHeightConstraint.constant = 0
            specialistComboBoxHeightConstraint.constant = 0
            specialistLabelComboBoxSpaceConstraint.constant = 0
            specialistNoteLabelSpaceConstraint.constant = 0
        case 2:
            organizeLabel.isHidden = false
            myBorderViewHeightConstraint.constant = 925 + 16*CGFloat((self.listSelectResult?.count)!)
            if let index = organizeComboBox.currentRow {
                currentOrganize = organizes?[index]
                self.currentIdx = 1000
                myBorderViewHeightConstraint.constant = CGFloat(self.currentIdx!) + 16*CGFloat((self.listSelectResult?.count)!)
            } else {
                currentOrganize = nil
            }
            organizeComboBoxHeightConstraint.constant = 17
            organizeLabelComboBoxSpaceConstraint.constant = 8
            organizeComboBoxSpecialistLabelSpaceConstraint.constant = 8
            specialistLabelHeightConstraint.constant = 0
            specialistComboBoxHeightConstraint.constant = 0
            specialistLabelComboBoxSpaceConstraint.constant = 0
            specialistNoteLabelSpaceConstraint.constant = 0
            //organizeLabelHeightConstraint.constant = 900
        default:
            break;
        }
    }
    
//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        super.viewWillTransition(to: size, with: coordinator)
//        if let flowLayout = timeRangeCollectionView.collectionViewLayout as? SACollectionViewFlowLayout {
//            flowLayout.invalidateLayout()
//        }
//    }
//
    func willShow(_ picker:FWDatePicker) {
        self.view.endEditing(true)
        currentTextField?.resignFirstResponder()
        currentTextView?.resignFirstResponder()
    }
}
