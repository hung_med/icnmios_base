//
//  SupportFeedbackVC.swift
//  iCNM
//
//  Created by Hung on 10/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import MessageUI
import StoreKit

class SupportFeedbackVC: UITableViewController, UIViewControllerTransitioningDelegate, MFMailComposeViewControllerDelegate {
        var listItems: [String]?
        @IBOutlet weak var version: UILabel!
        var currentUser:User? = nil
        override func viewDidLoad() {
            super.viewDidLoad()
            tableView.estimatedRowHeight = 60
            tableView.rowHeight = UITableViewAutomaticDimension
            tableView.backgroundColor = UIColor.groupTableViewBackground
            tableView.separatorStyle = UITableViewCellSeparatorStyle.none
            navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
            
            listItems = [String]()
            if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String, let text = Bundle.main.infoDictionary?["CFBundleVersion"]  as? String {
                self.version.text = "Version: \(text) (\(version)) \n ©Copyright 2017 by iCNM"
                self.version.lineBreakMode = .byWordWrapping
                self.version.numberOfLines = 0
            }
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }
    
        override func viewDidDisappear(_ animated:Bool){
            super.viewDidDisappear(false)
        }
    
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(false)
            let currentUser:User? = {
                let realm = try! Realm()
                return realm.currentUser()
            }()
            self.currentUser = currentUser
            
            if currentUser == nil{
                listItems = ["Gửi góp ý/hỗ trợ", "Gửi Email", "Đánh giá ứng dụng", "Fanpage iCNM", "Kích hoạt thẻ khuyến mại", ""]
            }else{
                listItems = ["Gửi góp ý/hỗ trợ", "Gửi Email", "Đánh giá ứng dụng", "Fanpage iCNM", "Kích hoạt thẻ khuyến mại", "Người giới thiệu cài đặt", "Từ khoá quan tâm", "Mã QR Code của tôi", ""]
            }
            tableView.reloadData()
        }
    
        override func viewWillDisappear(_ animated:Bool){
            super.viewWillDisappear(false)
            //let noti = Notification.init(name: Notification.Name("categoryClosed"))
            //NotificationCenter.default.post(noti)
        }
    
        // MARK: - Table view data source
        override func numberOfSections(in tableView: UITableView) -> Int {
            return 1
        }
        
        override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.listItems!.count
        }
        
        override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            let f = cell.contentView.frame
            let fr = UIEdgeInsetsInsetRect(f, UIEdgeInsetsMake(5, 5, 5, 5))
            cell.contentView.frame = fr
            cell.layer.cornerRadius = 2
            cell.layer.masksToBounds = true
            
            cell.layer.masksToBounds = false
            cell.layer.shadowOffset = CGSize(width: 0, height:0)
            cell.layer.shadowColor = UIColor.black.cgColor
            cell.layer.shadowOpacity = 0.4
            cell.layer.shadowRadius = 2
            
            cell.textLabel?.text = self.listItems?[indexPath.row]
            cell.textLabel?.textAlignment = NSTextAlignment.left
            cell.textLabel?.font = UIFont.systemFont(ofSize: 15.0)
            return cell
        }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let item = self.listItems?[indexPath.row]
        if indexPath.row == 0{
            let supportVC = SupportViewController(nibName: "SupportViewController", bundle: nil)
            supportVC.modalPresentationStyle = .custom
            supportVC.transitioningDelegate = self
            self.present(supportVC, animated: true, completion: nil)
        }else if indexPath.row == 1{
            self.configuredMailCompose()
        }else if indexPath.row == 2{
            if #available(iOS 10.3, *) {
                SKStoreReviewController.requestReview()
            } else {
                // Fallback on earlier versions
                UIApplication.tryURL(urls: [Constant.linkAppStore])
            }
        }else if indexPath.row == 3{
            UIApplication.tryURL(urls: [Constant.linkFanPage])
        }
        else if indexPath.row == 4{
            let promotionalCreditCard = PromotionalCreditCard(nibName: "PromotionalCreditCard", bundle: nil)
            promotionalCreditCard.modalPresentationStyle = .custom
            promotionalCreditCard.transitioningDelegate = self
            self.present(promotionalCreditCard, animated: true, completion: nil)
        }else if indexPath.row == 5{
            let confirmAccountDoctorVC = ConfirmAccountDoctorVC(nibName: "ConfirmAccountDoctorVC", bundle: nil)
            confirmAccountDoctorVC.modalPresentationStyle = .custom
            confirmAccountDoctorVC.transitioningDelegate = self
            self.present(confirmAccountDoctorVC, animated: true, completion: nil)
        }else if indexPath.row == 6{
            let tagSpecialistVC = TagSpecialistVC(nibName: "TagSpecialistVC", bundle: nil)
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(tagSpecialistVC, animated: true)
        }else if indexPath.row == 7{
            let get_qrcode = UIStoryboard.init(name: "Chat", bundle: nil).instantiateViewController(withIdentifier: "qrcode_vc")
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(get_qrcode, animated: true)
        }else{
            
        }
    }
    
    func configuredMailCompose(){
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        if currentUser != nil{
            if let email = currentUser?.userDoctor?.userInfo?.email{
                mailComposerVC.setToRecipients([email])
                mailComposerVC.setCcRecipients(["support@medlatec.com"])
                
                mailComposerVC.setSubject("Góp ý/Hỗ trợ")
                mailComposerVC.setMessageBody("Xin chào iCNM", isHTML: false)
            }
        }
       
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
//        showAlertView(title: "Thiết bị của bạn không thể gửi e-mail. Vui lòng kiểm tra cấu hình e-mail và thử lại.", view: self)
        return
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
