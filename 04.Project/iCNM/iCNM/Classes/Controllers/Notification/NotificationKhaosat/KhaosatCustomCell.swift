//
//  KhaosatCustomCell.swift
//  iCNM
//
//  Created by Mac osx on 12/4/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

protocol KhaosatCustomCellDelegate {
    func didToggleRadioButton(_ indexPath: IndexPath)
}
class KhaosatCustomCell: UITableViewCell {
    @IBOutlet weak var radio_btn: ISRadioButton!
    @IBOutlet weak var lblName: UILabel!
   // @IBOutlet weak var txtOther: UITextField!
    var delegate:KhaosatCustomCellDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      //  txtOther.isHidden = true
        self.selectionStyle = .none
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func addShadow(cell:UITableViewCell) {
        cell.layer.cornerRadius = 2
        cell.layer.masksToBounds = true
        
        cell.layer.masksToBounds = false
        cell.layer.shadowOffset = CGSize(width: 0, height:0)
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOpacity = 0.23
        cell.layer.shadowRadius = 2
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
