//
//  NotificationRemindVC.swift
//  iCNM
//
//  Created by Mac osx on 11/13/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire

class NotificationRemindVC: BaseViewControllerNoSearchBar, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var myTableView: UITableView!
    @IBOutlet weak var lblInfor: UILabel!
    @IBOutlet weak var btnRemind: UIButton!
    @IBOutlet weak var btnSchedule: UIButton!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    var featuredNews: [FeaturedNews]?
    var storedOffsets = [Int: CGFloat]()
    var dataNotification:String? = nil
    var titleNotification:String? = nil
    var descNotification:String? = nil
    var listNewsID:String? = nil
    var currentRequest:Request?
    var currentUser:User? = {
        let realm = try! Realm()
        return realm.currentUser()
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let data = splitDataRemindToString(str: dataNotification!)[0]
        self.tabBarController?.tabBar.isHidden = true
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), for:UIBarMetrics.default)
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesBackButton = true
            let button: UIButton = UIButton(type: .system)
            button.titleLabel?.font = UIFont.fontAwesome(ofSize: 40.0)
            let backTitle = String.fontAwesomeIcon(name: .angleLeft)
            button.setTitle(backTitle, for: .normal)
            button.addTarget(self, action: #selector(NotificationRemindVC.back(sender:)), for: UIControlEvents.touchUpInside)
            let newBackButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = newBackButton
        }else{
            //if IS_IPHONE_5{
                heightConstraint.constant = -60
           // }
        }
            
        navigationController?.navigationBar.barTintColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
        btnRemind.setTitle(data, for: UIControlState.normal)
        btnRemind.layer.borderColor = UIColor.lightGray.cgColor
        btnRemind.layer.borderWidth = 0.5
        btnRemind.layer.cornerRadius = 4.0
        btnRemind.layer.masksToBounds = true
        
        btnSchedule.layer.cornerRadius = 10.0
        btnSchedule.layer.masksToBounds = true
        btnSchedule.titleLabel?.font = UIFont.fontAwesome(ofSize: 16.0)
        let titleBtn = String.fontAwesomeIcon(name: .calendarPlusO) + " Đặt lịch khám ngay"
        btnSchedule.setTitle(titleBtn, for: .normal)
        
        title = titleNotification
        lblInfor.text = descNotification
        lblInfor.lineBreakMode = .byWordWrapping
        lblInfor.numberOfLines = 0
        
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.estimatedRowHeight = 200
        myTableView.rowHeight = UITableViewAutomaticDimension
        myTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        let nib = UINib(nibName: "CustomTableViewCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "Cell")
        self.getRalatedNews()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func back(sender: UIBarButtonItem) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func getRalatedNews(){
        currentRequest?.cancel()
        let progress = self.showProgress()
        self.featuredNews = [FeaturedNews]()
        currentRequest = FeaturedNews.getRelatedNewsTK(stringRequest: listNewsID!, success: { (data) in
            self.hideProgress(progress)
            self.featuredNews = data
            self.myTableView.reloadData()
            self.hideProgress(progress)
        }) { (error, response) in
            self.hideProgress(progress)
            self.handleNetworkError(error: error, responseData: response.data)
        }
    }
    
    @IBAction func btnRemindAction(_ sender: Any) {
        let result = splitDataRemindToString(str: dataNotification!)
        if let viewController = UIStoryboard(name: "DetailLookUp", bundle: nil).instantiateViewController(withIdentifier: "DetailLookUpResultVC") as? DetailLookUpResultVC{
            let sid = result[0]
            let organizeID = result[1]
            viewController.getListLookUpResultBySID(sid: sid, pID: "", organizeID:organizeID)
            self.navigationController!.pushViewController(viewController, animated: true)
        }
    }

    @IBAction func btnScheduleAction(_ sender: Any) {
        if currentUser != nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let scheduleAnAppointmentVC = storyboard.instantiateViewController(withIdentifier: "ScheduleAppointmentVC") as! SAViewController
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.pushViewController(scheduleAnAppointmentVC, animated: true)
        }else{
            confirmLogin(myView: self)
            return
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if featuredNews!.count > 0{
            let value = Int(screenSizeHeight/4.05714)
            if IS_IPHONE_5 || IS_IPHONE_6 || IS_IPHONE_8{
                if #available(iOS 11.0, *) {
                    return CGFloat(240 + value*(featuredNews!.count/2-1)-40)
                }else{
                    if IS_IPHONE_5{
                        return CGFloat(240 + value*(featuredNews!.count/2-1)-70)
                    }else{
                        return CGFloat(240 + value*(featuredNews!.count/2-1)-40)
                    }
                }
            }else{
                return CGFloat(240 + value*(featuredNews!.count/2-1)-15)
            }
            
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CustomTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        cell.collectionView.register(UINib(nibName: "CustomNewsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NewsCell")
        cell.collectionView.viewWithTag(5)
        if #available(iOS 10, *) {
            // use an api that requires the minimum version to be 10
        } else {
            cell.collectionView.collectionViewLayout.invalidateLayout()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? CustomTableViewCell else { return }
        tableViewCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.section)
        tableViewCell.collectionViewOffset = storedOffsets[indexPath.row] ?? 0
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let tableViewCell = cell as? CustomTableViewCell else { return }
        storedOffsets[indexPath.row] = tableViewCell.collectionViewOffset
    }
}

extension NotificationRemindVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        collectionView.collectionViewLayout.invalidateLayout()
        return featuredNews!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        collectionView.collectionViewLayout.invalidateLayout()
        let itemWidth = getItemWidth(boundWidth: collectionView.bounds.size.width, column:2)
        let value = Int(screenSizeHeight/4.05714)
        return CGSize(width: itemWidth, height: value)
    }
    
    func getItemWidth(boundWidth: CGFloat, column:CGFloat) -> Int {
        let totalWidth = boundWidth - (Constant.offset) - (column - 1) * Constant.minItemSpacing
        return Int(totalWidth / column)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        collectionView.collectionViewLayout.invalidateLayout()
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsCell", for: indexPath) as! CustomNewsCollectionViewCell
        cell.layer.cornerRadius = 4.0
        cell.layer.masksToBounds = true
        cell.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        cell.layer.borderWidth = 1.0
        
        let featuredNew = featuredNews?[indexPath.row]
        DispatchQueue.main.async(execute: {
            if let tmpImageName = featuredNew?.img {
                cell.imageThumb.contentMode = .scaleToFill
                let urlBase = tmpImageName.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)
                cell.imageThumb.loadImageUsingUrlString(urlString: urlBase!)
            }
            if let tmpTitle = featuredNew?.title{
                cell.lblTitleNews.text = tmpTitle
                cell.lblTitleNews.lineBreakMode = .byWordWrapping
                cell.lblTitleNews.numberOfLines = 0
                cell.lblTitleNews.adjustsFontSizeToFitWidth = true
            }
            if let categoryName = featuredNew?.categoryName, let tmpDateTime = featuredNew?.newsModel.datePhan{
                //get datetime server
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                var date = dateFormatter.date(from: tmpDateTime)
                if date == nil {
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
                    date = dateFormatter.date(from: tmpDateTime)
                }
                
                let dateTime = date?.timeAgoSinceNow()
                if let currentDateTime = dateTime{
                    cell.lblDepartment.text = "\(categoryName) - \(currentDateTime)"
                    cell.lblDepartment.textColor = UIColor(hexColor: 0x03A9F4, alpha: 1.0)
                }
            }
        })
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let newsDetailVC = StoryboardScene.Main.newsDetailViewController.instantiate()
        if let featuredNewsObj = featuredNews?[indexPath.row] {
            let newsCategory = NewsCategory()
            newsCategory.id = featuredNewsObj.categoryID
            newsCategory.name = featuredNewsObj.categoryName
            newsDetailVC.newsCategoryInput = newsCategory
            let model =  featuredNewsObj.newsModel
            newsDetailVC.newsModelInput = model
            self.navigationController?.pushViewController(newsDetailVC, animated: true)
        }
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
