//
//  NotificationNewsCell.swift
//  iCNM
//
//  Created by Mac osx on 9/22/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class NotificationNewsCell: UITableViewCell {

    @IBOutlet weak var imgThumb: CustomImageView!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDateCreate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
