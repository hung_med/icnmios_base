//
//  CustomScheduleDoctorCell.swift
//  iCNM
//
//  Created by Mac osx on 7/14/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class CustomScheduleDoctorCell: UITableViewCell {

    @IBOutlet weak var lineView2: UIView!
    @IBOutlet weak var groupView: UIView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPhone: UILabel!
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblScheduleTime: UILabel!
    @IBOutlet weak var lblReason: UILabel!
    @IBOutlet weak var lblHistory: UILabel!
    @IBOutlet weak var btnShow: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        let f = contentView.frame
        let fr = UIEdgeInsetsInsetRect(f, UIEdgeInsetsMake(15, 15, 15, 15))
        contentView.frame = fr
        //addShadow(cell: self)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    private func addShadow(cell:UITableViewCell) {
        groupView.layer.cornerRadius = 4
        groupView.layer.masksToBounds = false
        groupView.layer.shadowOffset = CGSize(width: 0, height:0)
        groupView.layer.shadowColor = UIColor.darkGray.cgColor
        groupView.layer.shadowOpacity = 0.5
        groupView.layer.shadowRadius = 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
