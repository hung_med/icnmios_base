//
//  Constant.swift
//  iCNM
//
//  Created by Medlatec on 5/11/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

let screenSizeWidth = UIScreen.main.bounds.width
let screenSizeHeight = UIScreen.main.bounds.height
struct ScreenSize
{
    static let SCREEN_WIDTH = screenSizeWidth
    static let SCREEN_HEIGHT = screenSizeHeight
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
let IS_IPHONE_8 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad

let IS_IPHONE_XS = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 896.0

public class Constant {
    public static let HOTLINE = "1900 565656"
    public static let baseURLWeb = "http://icnm.vn/cau-hoi"
    public static let baseURLResult = "http://img.icnm.vn/"
    public static let apiBaseUrl = "http://icnmservice.icnm.vn/"
    public static let imageBaseURL = "http://icnmservice.icnm.vn/iCNMImage/"
    public static let usageTermsURL = "http://medlatec.vn/DK_Icnm.htm"
    public static let locationImageBaseURL:String = "http://icnmservice.icnm.vn/iCNMImage/LocationImage/"
    public static let FCMBaseURL:String = "https://fcm.googleapis.com/fcm/send"
    public static let keyBase:String = "key=AAAAxxr7Fck:APA91bHgMV8fRLHjRzrRqXpOuG-WqUBU1lBqQb2Gd-aUiOBl4nFO1Q1P2HF2c37uMWG6rBw1tM9vb26oeq8xGGELBCCWgMC6kuLgkd-Bd_E39bTWjzyTFhItu4v9dem-xO9Rax5e5Dn1"
    public static let linkAppStore = "https://itunes.apple.com/za/app/icnm/id1132002156?mt=8"
    public static let linkFanPage = "https://www.facebook.com/icnm.vn/"
    public static let totalItem: CGFloat = 6
    public static let column: CGFloat = 3
    public static let minLineSpacing: CGFloat = 5.0
    public static let minItemSpacing: CGFloat = 5.0
    public static let offset: CGFloat = 5.0 // TODO: for each side, define its offset
    public static let MAX_ROW: Int = 100
    public static let questionBeta:String = "1.Định nghĩa về beta(phần này thường cố định)\nKhi phụ nữ có thai, các tế bào hình thành nên nhau thai sẽ sản xuất ra Beta hCG. Chúng có chức năng nuôi dưỡng trứng sau khi được thụ tinh và dính vào thành tử cung. Đồng thời sự có mặt của Beta hCG trong máu và nước tiểu giúp nhận biết sự có thai cũng như một số tình trạng bất thường của thai nghén. Nồng độ Beta hCG có thể được phát hiện lần đầu tiên bằng xét nghiệm máu trong vòng 11 ngày sau khi thụ thai và khoảng 12-14 ngày sau thụ thai nếu xét nghiệm bằng nước tiểu. Thông thường, nồng độ hCG sẽ tăng lên gấp đôi sau mỗi 48-72 giờ.\n\n2.Trà lời cho kh về beta của họ.Ví dụ 1 trường hợp  (phần này tùy vào BN)\nNồng độ Beta hCG của bạn 48729 U/L, tương đương với thai 10 tuần. Theo bảng giới bình thường nồng độ Beta HCG  làm trên máy và hóa chất đồng bộ của hãng Roche (đơn vị U/L) với thai 10 tuần thì:\n- Nồng độ Beta HCG trung bình: 85172 U/L.\n- Nồng độ Beta HCG mức thấp nhất: 46509 U/L.\n- Nồng độ Beta HCG mức cao nhất: 186977 U/L.\nProgesterone: là một hormone steroid có trọng lượng phân tử là 515.3 dalton. Progesterone huyết thanh thể hiện khả năng tồn tại của hoàng thể. Nồng độ Progesterone tăng dần và ít thay đổi trong 8-10 tuần đầu của thai kỳ.\nNồng độ Progesterone huyết thanh ở người có thai từ tuần thứ 9 đến 12 tuần bình thường từ: 118-137 nmol/L (37-43 ng/mL). Kết quả của chị 96.43 nmol/L, giảm nhẹ.\nKhoảng giới hạn bình thường của Beta HCG rất rộng, người ta ít chú ý đến giới hạn này mà quan tâm đến nồng độ Beta HCG tăng lên sau 48 giờ làm xét nghiệm. Nếu nồng độ Beta HCG sau 48 giờ tăng lên gấp đôi, chứng tỏ thai nhi đang phát triển bình thường.\n\n3.Khuyên và cám ơn(Phần này thường là cố định)\nTrường hợp của chị cần làm lại xét nghiệm Beta HCG sau 48 giờ và được tư vấn trực tiếp của bác sỹ chuyên khoa.\nChúc chị mạnh khỏe và mẹ tròn con vuông."
    public static let questionTripleTest:String = "1.Định nghĩa về beta(phần này thường cố định)\nTriple test được thực hiện bằng cách đo lượng AFP (alpha fetoprotein), β-hCG (beta-human chorionic gonadotropin) và estriol không liên hợp uE3 (unconjugated estriol) còn gọi là estriol tự do (free estriol) trong máu thai phụ, sau đó được tính toán cùng với cân nặng, chiều cao của mẹ, tuổi thai, … nhờ một phần mềm chuyên dụng để đánh giá nguy cơ các hội chứng Down, Edward hoặc dị tật ống thần kinh của thai ở quý 2 của thai kỳ.\nXét nghiệm này được chỉ định ở tất cả các thai phụ có thai ở tuần thứ 15 đến tuần thứ 22, kết quả chính xác nhất ở tuần thứ 16 đến tuần thứ 18.\n\n2.Trà lời cho kh về beta của họ.Ví dụ 1 trường hợp  (phần này tùy vào BN)\nTrường hợp của chị với kết quả:\nHội chứng Down (Tr.21)            1: 6294                  (<1:380)\nDị tật ống thần kinh        0.79        MoM        ( <2.5)\nHội chứng Edward (Tr.18)          <1:50000  (<1:350)\nNhư vậy với kết quả trên thì thai nhi của chị có nguy cơ rất thấp với cả 3 mặt bệnh là Down, Dị tật ống thần kình và HC Edward.\n\n3.Khuyên và cám ơn(Phần này thường là cố định)\nHiện tại Bệnh viện Đa khoa MEDLATEC đang thực hiện xét nghiệm này hàng ngày và được chạy trên hệ thống máy miễn dịch tự động hoàn toàn cho kết quả nhanh nhất có thể. Nếu chị đến xét nghiệm tại bệnh viện cho kết quả sau 2-3h sau khi lấy mẫu. Ngoài ra, chị cũng có thể đăng ký xét nghiệm và trả kết quả tại nhà, nếu chị lấy máu xét nghiệm sáng có thể cho kết quả ngay trong ngày và được các bác sĩ chuyên khoa tư vấn sau khi có đủ kết quả.\n\nKính chúc chị luôn mạnh khỏe và hạnh phúc."
    public static let questionDoubleTest:String = "1.Định nghĩa về beta(phần này thường cố định)\nXét nghiệm Double test: là xét nghiệm máu thai phụ 2 chỉ số sinh hóa gồm PAPP-A và Free Beta- hCG. Sau khi có kết quả xét nghiệm được khai báo vào phần mềm, kết hợp với các chỉ số khác liên quan đến thai phụ như ngày tháng năm sinh, tuổi thai, chiều cao, cân nặng mẹ, đo chiều dài đầu mông, đo độ mờ da gáy,... phần mềm đưa ra kết quả các nguy cơ.\n\n2.Trà lời cho kh về beta của họ.Ví dụ 1 trường hợp  (phần này tùy vào BN)\nKết quả của chị khi đã được hiệu chỉnh như sau:\n- Hội chứng Down (Tr.21): 1: 3896, nghĩa là với 3896 thai phụ, có 1 thai phụ sinh con có nguy cơ mắc hội chứng Down (Tr.21), tức có nguy cơ thấp với hội chứng Down.\n- Hội chứng Edward (Tr.18) 1: 257002, nghĩa là với 257002 thai phụ, có 1 thai phụ sinh con có nguy cơ mắc hội chứng Edward (Tr.18), tức là nguy cơ thấp.\n- Hội chứng Patau (Tr.13): 1: 604999, có nghĩa là với 604999 thai phụ, có 1 thai phụ sinh con có nguy cơ mắc hội chứng Patau (Tr.13), tức là nguy cơ thấp.\nNhư vậy, chị có kết quả sàng lọc trước sinh đều nằm ở nguy cơ thấp. Chị cần siêu âm định kỳ bằng máy siêu âm có độ phân dải cao để theo dõi.\n\n3.Khuyên và cám ơn(Phần này thường là cố định)\n\nLưu ý: thủ thuật chọc ối nên được thực hiện tốt nhất trong khoảng thai 16 - 18 tuần tuổi.\n\nChúc chị và gia đình sức khỏe, gặp nhiều may mắn."
    public static let questionOther:String = "- FSH: 8.28 mU/l là hoàn toàn bình thường trong giới hạn (3.5 - 12.5) tương ứng với giai đoạn thể nang. Hoormon này được tiết ra từ tuyến yên, có tác dụng kích thích nang noãn phát triển.Việc định lượng FSH được tiến hành khi có nghi ngờ các rối loạn về nội tiết, suy buồng trứng nguyên phát, suy buồng trứng sớm (mãn kinh sớm), suy tuyến yên.\n\n- LH: 2.52 mU/l là bình thường trong giới hạn (2.4 - 12.6). Định lượng nồng độ LH là một việc làm thiết yếu xác định thời điểm rụng trứng, thời điểm thụ tinh và có thể chẩn đoán được sự rối loạn về trục dưới đồi - tuyến yên, xác định thời điểm giao hợp tốt nhất và thụ tinh nhân tạo (do LH tăng trước khi rụng trứng). Mặt khác, việc định lượng LH ở nữ còn mang lại lợi ích trong việc chẩn đoán các hiện tượng vô kinh, mãn  kinh, vòng kinh không rụng trứng, hội trứng PCOS (hội chứng buồng trứng đa nang), suy vùng dưới đồi.\n\n- Prolactin: 214.1 microU/ml là bình thường trong giới hạn (127 - 637). Đây là hormon được tiết ra từ thùy trước tuyến yên có vai trò kích thích tiết sữa khi phụ nữ có thai. Trường hợp không có thai mà tăng cao là bất thường do một số nguyên nhân hay gặp như do dùng một số thuốc nội tiêt hoặc bệnh lý tại tuyến yên...\n\n- Estradiol (E2): 88.16 pmol/l là bình thường trong giới hạn từ (46 - 607). Estradiol được tạo ra từ buồng trứng và nhau thai. Nồng độ E2 thấp nhất lúc có kinh và trong pha nang noãn sớm, sau đó tăng lên trong pha nang noãn muộn trước khi xuất hiện LH.\n\n- Progesterone: 1.16 nmol/l là bình thường trong giới hạn (0.6-4.7) là một hoormone do hoàng thể tiết ra sau khi có sự phóng noãn xảy ra. Mục đích đo nồng độ Progesterone là xác định có phóng noãn hay không dù là phóng noãn tự nhiên hay dùng thuốc.\n\n- Testosterone: 0.365 nmol/l là bình thường trong giới hạn (0.22-2.9). Đây là nội tiết tố nam trong cơ thể phụ nữ để cân bằng với nồng độ hoormone nữ.\n\n- AMH: 2.15 ng/ml là bình thường trong giới hạn (0.672 - 7.55). Đây là xét nghiệm đánh giá khả năng dự trữ buồng trứng, thường tăng trong những trường hợp có hội chứng buồng trứng đa nang..."
    
    public static let questionNoiKhoaOther:String = "Nội tiết tố sẽ được kiểm tra vào ngày thứ 2 -3 của chu kỳ kinh (đối với phụ nữ hàng tháng vẫn có kinh nguyệt đều). Còn với những trường hợp rối loạn kinh hoặc vô kinh thì có thể làm vào thời điểm bất kỳ. Kết quả nội tiết tố cơ bản của chị được phân tích như sau:\n\n- FSH: 8.28 mU/l là hoàn toàn bình thường trong giới hạn (3.5 - 12.5) tương ứng với giai đoạn thể nang. Hoormon này được tiết ra từ tuyến yên, có tác dụng kích thích nang noãn phát triển.Việc định lượng FSH được tiến hành khi có nghi ngờ các rối loạn về nội tiết, suy buồng trứng nguyên phát, suy buồng trứng sớm (mãn kinh sớm), suy tuyến yên.\n\n- LH: 2.52 mU/l là bình thường trong giới hạn (2.4 - 12.6). Định lượng nồng độ LH là một việc làm thiết yếu xác định thời điểm rụng trứng, thời điểm thụ tinh và có thể chẩn đoán được sự rối loạn về trục dưới đồi - tuyến yên, xác định thời điểm giao hợp tốt nhất và thụ tinh nhân tạo (do LH tăng trước khi rụng trứng). Mặt khác, việc định lượng LH ở nữ còn mang lại lợi ích trong việc chẩn đoán các hiện tượng vô kinh, mãn  kinh, vòng kinh không rụng trứng, hội trứng PCOS (hội chứng buồng trứng đa nang), suy vùng dưới đồi.\n\n- Prolactin: 214.1 microU/ml là bình thường trong giới hạn (127 - 637). Đây là hormon được tiết ra từ thùy trước tuyến yên có vai trò kích thích tiết sữa khi phụ nữ có thai. Trường hợp không có thai mà tăng cao là bất thường do một số nguyên nhân hay gặp như do dùng một số thuốc nội tiêt hoặc bệnh lý tại tuyến yên...\n\n- Estradiol (E2): 88.16 pmol/l là bình thường trong giới hạn từ (46 - 607). Estradiol được tạo ra từ buồng trứng và nhau thai. Nồng độ E2 thấp nhất lúc có kinh và trong pha nang noãn sớm, sau đó tăng lên trong pha nang noãn muộn trước khi xuất hiện LH.\n\n- Progesterone: 1.16 nmol/l là bình thường trong giới hạn (0.6-4.7) là một hoormone do hoàng thể tiết ra sau khi có sự phóng noãn xảy ra. Mục đích đo nồng độ Progesterone là xác định có phóng noãn hay không dù là phóng noãn tự nhiên hay dùng thuốc.\n\n- Testosterone: 0.365 nmol/l là bình thường trong giới hạn (0.22-2.9). Đây là nội tiết tố nam trong cơ thể phụ nữ để cân bằng với nồng độ hoormone nữ.\n\n- AMH: 2.15 ng/ml là bình thường trong giới hạn (0.672 - 7.55). Đây là xét nghiệm đánh giá khả năng dự trữ buồng trứng, thường tăng trong những trường hợp có hội chứng buồng trứng đa nang..."
    
    public static let questionXN_1:String = "Triple test được thực hiện bằng cách đo lượng AFP (alpha fetoprotein), β-hCG (beta-human chorionic gonadotropin) và estriol không liên hợp uE3 (unconjugated estriol) còn gọi là estriol tự do (free estriol) trong máu thai phụ, sau đó được tính toán cùng với cân nặng, chiều cao của mẹ, tuổi thai, … nhờ một phần mềm chuyên dụng để đánh giá nguy cơ các hội chứng Down, Edward hoặc dị tật ống thần kinh của thai ở quý 2 của thai kỳ.\n\nXét nghiệm này được chỉ định ở tất cả các thai phụ có thai ở tuần thứ 15 đến tuần thứ 22, kết quả chính xác nhất ở tuần thứ 16 đến tuần thứ 18.\n\nTrường hợp của chị với kết quả:\n\nHội chứng Down (Tr.21)            1: 6294                  (<1:380)\n\nDị tật ống thần kinh        0.79        MoM        ( <2.5)\n\nHội chứng Edward (Tr.18)          <1:50000  (<1:350)\n\nNhư vậy với kết quả trên thì thai nhi của chị có nguy cơ rất thấp với cả 3 mặt bệnh là Down, Dị tật ống thần kình và HC Edward.\n\nChị nên khám thai và siêu âm thai định kỳ theo hướng dẫn của bác sỹ chuyên khoa"
    
    public static let questionXN_2:String = "Xét nghiệm Double test là xét nghiệm 2 chất sinh hóa trong huyết thanh của thai phụ gồm PAPP-A và Free Beta HCG. Sau khi có kết quả xét nghiệm được khai báo vào phần mềm, kết hợp với các chỉ số khác liên quan đến thai phụ như ngày tháng năm sinh, tuổi thai, chiều cao, cân nặng mẹ, đo chiều dài đầu mông, đo độ mờ da gáy,... phần mềm đưa ra kết quả các nguy cơ đã được hiệu chỉnh. Cụ thể kết quả của chị khi đã được hiệu chỉnh như sau:\n\n- Hội chứng Down (Tr.21): 1:81, có nghĩa là với 81 thai phụ có 1 thai phụ sinh con bị hội chứng Down (Tr.21), còn 80 thai phụ sinh con không bị hội chứng Down (Tr.21). Vậy với kết quả của chị có nguy cơ cao (bình thường  <1:250);\n\n- Hội chứng Edward (Tr.18) 1:33122, có nghĩa là với 33122 thai phụ có 1 thai phụ sinh con bị hội chứng Edward (Tr.18), còn 33121 thai phụ sinh con không bị hội chứng Edward (Tr.18). Vậy với kết quả của chị có nguy cơ rất thấp (bình thường  <1:250);\n\n- Hội chứng Patau (Tr.13): 1:103792, có nghĩa là với 103792 thai phụ có 1 thai phụ sinh con bị hội chứng Patau (Tr.13), còn 103791 thai phụ sinh con không bị hội chứng Edward (Tr.18). Vậy với kết quả của chị có nguy cơ rất thấp (bình thường  <1:250)."
    
    public static let container_QuestionXN1:String = "1. Nêu khái niệm chỉ số xét nghiệm/xét nghiệm đó giúp khách hàng hiểu rõ;\n\n2. So với giá trị bình thường để biết chỉ số đó tăng, giảm nhiều hay ít, từ đó định hướng tới bệnh lý của người bệnh;\n\n3. Trường hợp KH chỉ đưa đơn độc chỉ số có thể sẽ khó đưa ra kết luận chính xác, BS có thể gửi lại thư để yêu cầu khách hàng bổ sung thêm thông tin như các biểu hiện bất thường, bệnh lý nào không, ...\n\n4. Hoặc có thể khuyên khách hàng nên đi đến bệnh viện để được khám thêm chuyên khoa, từ đó có tư vấn chính xác về tình trạng sức khỏe."
    
    public static let container_QuestionXN2:String = "1. Nêu khái niệm chỉ số xét nghiệm đó;\n\n2. So với giá trị bình thường, phân tích chỉ số đó tăng hoặc giảm;\n\n3. Phân tích chỉ số đó có ý nghĩa gì, yêu cầu KH cung cấp thêm biểu hiện, triệu chứng (nếu cần) để kết hợp tư vấn chính xác."
    
    public static let container_QuestionXN3:String = "1. So với giá trị bình thường, kết quả của khách hàng có ý nghĩa gì?\n\n2. Từ kết quả đó gợi ý chế độ dinh dưỡng phù hợp.\n\n3. Trường hợp khách hàng không cung cấp đầy đủ các triệu chứng/biểu hiện thì có thể tư vấn chế độ dinh dưỡng cho những trường hợp xảy ra như:\n\n+ Chỉ số này tăng/giảm + có biểu hiện thì nên làm gì?\n\n+ Hoặc chỉ này tăng/giảm + không có biểu hiện thì nên làm gì?\n\n4. Hoặc tư vấn nên khám chuyên khoa gì?"
    
    public static let container_Specalist:String = "Khách hàng nêu rõ triệu chứng, biểu hiện bệnh và kết quả xét nghiệm, khám --> muốn tư vấn xem đó là bệnh gì?\n\nKhách hàng đưa ra biểu hiện và hỏi đó là bệnh gì?\n\nKhách hàng đã được chẩn đoán nhưng băn khoăn với kết quả chẩn đoán đó, xin bác sĩ tư vấn\n\nKhách hàng hỏi với đơn thuốc đang dùng, nhưng triệu chứng bệnh vẫn không giảm. KH xin tư vấn cần làm gì?\n\nKH hỏi bệnh lý đã được xác định và cần tư vấn phương pháp điều trị\n\nBệnh nhân đang được điều trị những có các tác dụng phụ nên hỏi hướng xử tri như thế nào?\n\nKH hỏi về triệu chứng bệnh, hướng xử trí các bệnh cấp cứu như tiêu chảy, bỏng, ngộ độc, dị ứng,.."
    
    public static let container_ServiceMedlatec:String = "1. Cảm ơn khách hàng đã quan tâm, tin tưởng sử dụng dịch vụ của BV ĐK MEDLATEC.\n\n2. Tùy theo nội dung mà có thể linh hoạt trả lời theo yêu cầu KH hỏi.\n\n3. Có thể hướng tới KH sử dụng dịch vụ tại BV ĐK MEDLATEC hoặc PKĐK MEDLATEC, hoặc có thể hướng tới khách hàng sử dụng dịch vụ bác sĩ gia đình,..."
    
    public static let DEFIND_PID:String = "PID là mã thẻ khách hàng do MEDLATEC cung cấp, được gửi dưới dạng tin nhắn, nhằm lưu trữ kết quả khám của bệnh nhân qua các lần khám."
    public static let DEFIND_SID:String = "SID là mã khám chữa bệnh theo ngày do MEDLATEC cung cấp, được gửi dưới dạng tin nhắn hoặc in trong hóa đơn khám chữa bệnh tại đơn vị."
    public static let DEFIND_Donvi:String = "Đơn vị là mã đơn vị gửi mẫu liên kết với MEDLATEC hoặc mã đơn vị khám sức khỏe tại MEDLATEC."
    public static let DEFIND_ORGANIZE:String = "Đơn vị khám: Bệnh viện đa khoa MEDLATEC"
    public static let ADVISORY:String = "Quý khách hàng vui lòng gặp bác sĩ chỉ định khám hoặc liên hệ 1900 565656 để được tư vấn khi có đầy đủ kết quả."
    public static let ADVISORY_NO:String = "Bạn sẽ được tư vấn ngay sau khi có kết quả! Xin cảm ơn!"
    public static let RE_EXAMINATION:String = "Với kết quả khám như hiện tại, quý KH nên tái khám lại vào ngày"
    public static let RE_EXAMINATION_SUB:String = "để đảm bảo tình trạng sức khỏe tốt nhất. Xin cảm ơn!"
    public static let RE_NO_EXAMINATION:String = "Bạn sẽ nhận được thông báo tái khám ngay sau khi có kết quả. Xin cảm ơn!"
    
    public static let NEWS:String = "Tin tức"
    public static let NEWS_DETAIL:String = "Tin tức chi tiết"
    public static let SCHEDULE:String = "Đặt lịch"
    public static let RESULT_TEST:String = "Tra cứu kết quả"
    public static let MAPS:String = "Bản đồ"
    public static let HEALTH_RECORDS:String = "Hồ sơ sức khỏe"
    public static let CREATE_QUESTION:String = "Đặt câu hỏi"
    public static let QUESTION_ANSWER:String = "Hỏi đáp"
    public static let PACKAGE_SERVICE:String = "Gói khám/Dịch vụ"
    public static let PACKAGE_SERVICE_DETAIL:String = "Gói khám/Dịch vụ Chi tiết"
    public static let NEWS_NOTIFICATION:String = "Thông báo tin tức"
    
    public class NotificationMessage {
        public static let noInternetConnection = Notification.Name(rawValue:"NoInternetConnection")
        public static let internetComeBack = Notification.Name(rawValue:"InternetComeBack")
        public static let loginSuccess = Notification.Name(rawValue:"LoginSuccess")
        public static let loginSuccess2 = Notification.Name(rawValue:"LoginSuccess2")
        public static let handleNotification = Notification.Name(rawValue:"handleNotification")
        public static let registerTokenApp = Notification.Name(rawValue:"registerTokenApp")
        public static let prepareLogout = Notification.Name(rawValue:"PrepareLogout")
        public static let logout = Notification.Name(rawValue:"Logout")
        public static let profileEdited = Notification.Name(rawValue:"ProfileEdited")
        public static let specialistSelectedChanged = Notification.Name(rawValue:"SpecialistSelectedChanged")
    }
}
