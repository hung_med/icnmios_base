//
//  DoctorInformation.swift
//  iCNM
//
//  Created by Medlatec on 5/16/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

class DoctorInformation:Object,Mappable {
    dynamic var id = 0
    var doctorSpecialists = List<DoctorSpecialist>()
    var educateds = List<Education>()
    var specialists = List<Specialist>()
    var workExperiences = List<WorkExperience>()
    var prizes = List<Prize>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        doctorSpecialists <- (map["DoctorSpecialists"], ListTransform<DoctorSpecialist>())
        educateds <- (map["Educateds"], ListTransform<Education>())
        if map.mappingType == .fromJSON {
            specialists <- (map["Specialists"], ListTransform<Specialist>())
        }
        prizes <- (map["Prizes"], ListTransform<Prize>())
        workExperiences <- (map["WorkExperiences"], ListTransform<WorkExperience>())
    }
    
}
