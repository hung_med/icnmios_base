//
//  DoctorNearBy.swift
//  iCNM
//
//  Created by Nguyen Van Dung on 8/1/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import ObjectMapper

class DoctorNearBy: Mappable {

    var doctorID:Int = 0
    var startTime:String = ""
    var summary:String = ""
    var showPhone:String = ""
    var answer:Bool = false
    var verify:Bool = false
    var block:Bool = false
    var rating:Int = 0
    var specialistStr:String = ""
    var timeWorkStr:String = ""
    var doctorName:String = ""
    var img:String = ""
    var showHome:Bool = false
    var position:String = ""
    var specialistRegister:String = ""
    var timeRegister:String = ""
    var isCheckRegister:Bool = false
    var rejectAnswer:Bool = false
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        doctorID <- map["DoctorID"]
        startTime <- map["StartTime"]
        summary <- map["Summary"]
        showPhone <- map["ShowPhone"]
        answer <- map["Answer"]
        verify <- map["Verify"]
        block <- map["Block"]
        rating <- map["Rating"]
        specialistStr <- map["SpecialistStr"]
        timeWorkStr <- map["TimeWorkStr"]
        doctorName <- map["DoctorName"]
        img <- map["Img"]
        showPhone <- map["ShowHome"]
        position <- map["Position"]
        specialistRegister <- map["SpecialistRegister"]
        timeRegister <- map["TimeRegister"]
        isCheckRegister <- map["IsCheckRegister"]
        rejectAnswer <- map["RejectAnswer"]
    }
}
