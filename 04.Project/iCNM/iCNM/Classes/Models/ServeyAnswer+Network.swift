//
//  ServeyAnswer+Network.swift
//  iCNM
//
//  Created by Quang Hung on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension ServeyAnswer {
    
    @discardableResult
    func SendServeyAnswer(listServey:[ServeyAnswer], success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        var parameterDic = [String: Any]()
        parameterDic = ["ServeyListPost":listServey.toJSON()]
        
        return API.requestJSON(endpoint: .SendServeyAnswer(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as? String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
