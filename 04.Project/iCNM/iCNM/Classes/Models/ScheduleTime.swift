//
//  ScheduleTime.swift
//  iCNM
//
//  Created by Medlatec on 7/27/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class ScheduleTime:Object, Mappable {
    dynamic var id = 0
    dynamic var timeString:String?
    dynamic var startTime:Date?
    dynamic var endTime:Date?
    dynamic var dateCreate:Date?
    dynamic var active = false
    dynamic var organizeID = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "HH:mm:ss"
        id <- map["ScheduleTimeID"]
        timeString <- map["ScheduleTimeStr"]
        startTime <- (map["StartTime"],DateFormatterTransform(dateFormatter:dateFormatter2))
        endTime <- (map["EndTime"],DateFormatterTransform(dateFormatter:dateFormatter2))
        dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
        active <- map["Active"]
        organizeID <- map["OrganizeID"]
    }
    
}
