//
//  UserNewsCommentModel+Network.swift
//  iCNM
//
//  Created by Len Pham on 8/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import Alamofire

extension UserNewsCommentModel {
    @discardableResult
    static func getListCommentOfNews(newsId:Int, pageNumber:Int, countNumber:Int , success:@escaping([UserNewsCommentModel]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        var stringParam: String = String()
        stringParam = stringParam.appendingFormat("%d,%d,%d",newsId,pageNumber,countNumber)
        
        return API.requestJSON(endpoint: .getListCommentOfNews(stringParam), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [UserNewsCommentModel]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let newsModel = UserNewsCommentModel(JSON: item) {
                            data.append(newsModel)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    static func addNewComment(newsId:Int, userId:Int, contentComment:String , success:@escaping(Bool) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        var paramDict = [String:Any]()
        paramDict["NewsID"] = newsId
        paramDict["UserID"] = userId
        paramDict["Comment"] = contentComment

        return API.requestJSON(endpoint: .addNewsComment(paramDict), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let data = value as? Bool {
                    success(data)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
   }
