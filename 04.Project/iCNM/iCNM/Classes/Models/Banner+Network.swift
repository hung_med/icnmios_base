//
//  Banner+Network.swift
//  iCNM
//
//  Created by Quang Hung on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension Banner {
    @discardableResult
    func getAllBanner(success:@escaping([Banner]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getAllBanner, completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [Banner]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let banner = Banner(JSON: item) {
                            data.append(banner)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
