//
//  DictDiseaseGroups.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class DictDiseaseGroups:Mappable {
    var active:Bool = false
    var dGName = ""
    var dateCreate = ""
    var diseaseGroupID:Int = 0
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            active <- map["Active"]
            dGName <- map["DGName"]
            dateCreate <- map["DateCreate"]
            diseaseGroupID <- map["DiseaseGroupID"]
        }        
    }
}
