//
//  MedicalCilinnical.swift
//  iCNM
//
//  Created by Ta Quang Hung on 20/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper

class MedicalCilinnical:Mappable {
    var active:Bool? = false
    var bMI:Int = 0
    var bacSiName = ""
    var chanDoan = ""
    var cilinnicID:Int = 0
    var coQuanKhac = ""
    var coXuongKhop = ""
    var daLieu = ""
    var daNiemMac = ""
    var danhGia = ""
    var dateCilin = ""
    var dateSchedule = ""
    var dinhDuong = ""
    var ha:Int = 0
    var height:Int = 0
    var historyCilin = ""
    var hoHap = ""
    var huyetHoc = ""
    var leftEye:Int = 0
    var leftEyeGlass:Int = 0
    var mach:Int = 0
    var mat = ""
    var ngoaiKhoa = ""
    var nhietDo:Int = 0
    var nhipTho:Int = 0
    var noiTiet = ""
    var rangHamMat = ""
    var rightEye:Int = 0
    var rightEyeGlass:Int = 0
    var sanPhuKhoa = ""
    var sieuAmOBung = ""
    var sinhHoaMau = ""
    var sinhHoaNuocTieu = ""
    var taiMuiHong = ""
    var tamThan = ""
    var thanKinh = ""
    var tietNieu = ""
    var tieuHoa = ""
    var timMach = ""
    var toanThanNote = ""
    var tuVan = ""
    var vanDong = ""
    var vongBung:Int = 0
    var weight:Int = 0
    // PRAGMA - ObjectMapper
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        active <- map["Active"]
        bMI <- map["BMI"]
        bacSiName <- map["BacSiName"]
        chanDoan <- map["ChanDoan"]
        cilinnicID <- map["CilinnicID"]
        coQuanKhac <- map["CoQuanKhac"]
        coXuongKhop <- map["CoXuongKhop"]
        daLieu <- map["DaLieu"]
        daNiemMac <- map["DaNiemMac"]
        danhGia <- map["DanhGia"]
        dateCilin <- map["DateCilin"]
        dateSchedule <- map["DateSchedule"]
        dinhDuong <- map["DinhDuong"]
        ha <- map["HA"]
        height <- map["Height"]
        historyCilin <- map["HistoryCilin"]
        hoHap <- map["HoHap"]
        huyetHoc <- map["HuyetHoc"]
        leftEye <- map["LeftEye"]
        leftEyeGlass <- map["LeftEyeGlass"]
        mach <- map["Mach"]
        mat <- map["Mat"]
        ngoaiKhoa <- map["NgoaiKhoa"]
        nhietDo <- map["NhietDo"]
        nhipTho <- map["NhipTho"]
        noiTiet <- map["NoiTiet"]
        rangHamMat <- map["RangHamMat"]
        rightEye <- map["RightEye"]
        rightEyeGlass <- map["RightEyeGlass"]
        sanPhuKhoa <- map["SanPhuKhoa"]
        sieuAmOBung <- map["SieuAmOBung"]
        sinhHoaMau <- map["SinhHoaMau"]
        sinhHoaNuocTieu <- map["SinhHoaNuocTieu"]
        taiMuiHong <- map["TaiMuiHong"]
        tamThan <- map["TamThan"]
        thanKinh <- map["ThanKinh"]
        tietNieu <- map["TietNieu"]
        tieuHoa <- map["TieuHoa"]
        timMach <- map["TimMach"]
        toanThanNote <- map["ToanThanNote"]
        tuVan <- map["TuVan"]
        vanDong <- map["VanDong"]
        vongBung <- map["VongBung"]
        weight <- map["Weight"]
    }
    
}
