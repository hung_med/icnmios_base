//
//  Conversations.swift
//  iCNM
//
//  Created by Thanh Huyen on 6/7/18.
//  Copyright © 2018 Medlatec. All rights reserved.
//

import Foundation

class Conversations {
    
    var avatar : String
    var from : Int
    var message : String
    var name : String
    var orderTime : NSNumber
    var searchKey : String
    var seen : Bool?
    var status : String
    var timeStamp : NSNumber
    var chatId : Int?
    
    init(avatarString : String, fromString : Int, messageString : String, nameString : String, orderTimeNumber : NSNumber, serachKeyString : String, statusString : String, timeStampNumber : NSNumber) {
        
        avatar = avatarString
        from = fromString
        message = messageString
        name = nameString
        orderTime = orderTimeNumber
        searchKey = serachKeyString
        status = statusString
        timeStamp = timeStampNumber
    }
}
