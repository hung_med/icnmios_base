//
//  ServiceGroups.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class ServiceGroups:Mappable {
    var serviceGroupID:Int = 0
    var name = ""
    var description = ""
    var active:Bool = false
    var dateCreate = ""
    var orderNum:Int = 0
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            serviceGroupID <- map["ServiceGroupID"]
            name <- map["Name"]
            description <- map["Description"]
            active <- map["Active"]
            dateCreate <- map["DateCreate"]
            orderNum <- map["OrderNum"]
        }
    }
}
