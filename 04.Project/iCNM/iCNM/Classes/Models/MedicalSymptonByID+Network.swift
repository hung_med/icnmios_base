//
//  UserRegister+Network.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension MedicalSymptonByID {
    
    @discardableResult
    func getMedicalSymptonByID(symptonID:Int, success:@escaping(MedicalSymptonByID?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getMedicalSymptonByID(symptonID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let medicalSymptonObj = MedicalSymptonByID(JSON: item) {
                        success(medicalSymptonObj)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
//    @discardableResult
//    func getMedicalSymptonByID(symptonID:Int, success:@escaping([MedicalSymptonByID]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
//        return API.requestJSON(endpoint: .getMedicalSymptonByID(symptonID), completionHandler: { (response) in
//            switch response.result {
//            case .success(let value):
//                var data = [MedicalSymptonByID]()
//                if let array = value as? [[String:Any]]  {
//                    for item in array {
//                        if let medicalSymptonObj = MedicalSymptonByID(JSON: item) {
//                            data.append(medicalSymptonObj)
//                        }
//                    }
//                }
//                success(data)
//            case .failure(let error):
//                fail(error,response)
//            }
//        })
//    }
}
