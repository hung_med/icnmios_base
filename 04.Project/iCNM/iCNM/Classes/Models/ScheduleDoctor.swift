//
//  ScheduleDoctor.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class ScheduleDoctor:Mappable {
    var doctorID:Int = 0
    var doctorIDCode:String? = nil
    var userID:Int = 0
    var name:String? = nil
    var specialName:String? = nil
    var rating:Double? = 0.0
    var price:Float? = 0.0
    var personRate:Int = 0
    var listLichBS:[ScheduleTimeDoctor]? = nil
    var avatar:String? = nil
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            doctorID <- map["DoctorID"]
            doctorIDCode <- map["DoctorIDCode"]
            userID <- map["UserID"]
            name <- map["Name"]
            specialName <- map["SpecialName"]
            rating <- map["Rating"]
            price <- map["Price"]
            personRate <- map["PersonRate"]
            listLichBS <- map["timeBS"]
            avatar <- map["Avartar"]
        }
    }
}
