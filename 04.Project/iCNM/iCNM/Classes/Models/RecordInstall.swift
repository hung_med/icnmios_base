//
//  RecordInstall.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class RecordInstall:Mappable {
    var userID:String = ""
    var insDevice:Int = 2
    var dateCreate:Date?
    var active:Bool? = false
    var phone:Int = 0
    var isInstall:Bool? = false
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            userID <- map["UserID"]
            insDevice <- map["InsDevice"]
            active <- map["Active"]
            phone <- map["Phone"]
            isInstall <- map["IsInstall"]
            dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
            if dateCreate == nil {
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
            }
        }
    }
}
