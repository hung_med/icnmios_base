//
//  LocationNearBy.swift
//  iCNM
//
//  Created by Nguyen Van Dung on 7/29/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import ObjectMapper

class LocationNearBy: Mappable {
    
    var address:String = ""
    var averageRating: Double = 0
    var countLike:Int = 0
    var distance:Double = 0
    var imageStr = ""
    var imageThumnailStr = ""
    var intro = ""
    var lat:Double = 0.0
    var long:Double = 0.0
    var locationID = 0
    var locationTypeID = 0
    var locationTypeName:String = ""
    var number = 0
    var name = ""
    var specialist = ""
    var locations:LocationNearBy?
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            address <- map["Address"]
            averageRating <- map["AverageRating"]
            countLike <- map["CountLike"]
            distance <- map["Distance"]
            imageStr <- map["ImagesStr"]
            imageThumnailStr <- map["ImagesThumbnailStr"]
            intro <- map["Intro"]
            lat <- map["Lat"]
            long <- map["Long"]
            locationID <- map["LocationID"]
            locationTypeID <- map["LocationTypeID"]
            locationTypeName <- map["LocationTypeName"]
            number <- map["NUMBER"]
            name <- map["Name"]
            specialist <- map["Specialist"]
        }
        
    }
}
