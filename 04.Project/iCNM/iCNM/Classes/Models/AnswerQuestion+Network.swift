//
//  UserRegister+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire

extension AnswerQuestion {
    
    @discardableResult
    static func getAllAnswerQuestion(success:@escaping([QuestionAnswer]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getAnswerQuestion, completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [QuestionAnswer]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let answerQues = QuestionAnswer(JSON: item) {
                            data.append(answerQues)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
