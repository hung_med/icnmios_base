//
//  Schedule.swift
//  iCNM
//
//  Created by Medlatec on 8/3/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper

class Schedule:Mappable {
    var id = 0
    var timeID = 0
    var typeID = 0
    var userID = 0
    var userName:String?
    var organizeID = 0
    var organizeCode:String?
    var doctorID = 0
    var doctorIDCode:String?
    var dateSchedule:Date?
    var dateCreate:Date?
    var note:String?
    var active = false
    var isCancel = false
    var phone:String?
    var gender:String?
    var address:String?
    var birthYear:Int = 0
    var birthDay:Date?
    var specialistID = 0
    var maGG:String?
    var scheduleType:ScheduleType?
    var scheduleTime:ScheduleTime?
    var organize:Organize?
    var listTestCode:String? = ""
    var noiKham:String?
    var ngayKham:String?
    var gioHen:String?
    var lydoKham:String?
    var maGen:String?
    var ngaySD:Date?
    var ngayHuyLich:Date?
    var medicalProfileID:Int?
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
//        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
//        dateFormatter2.timeZone = TimeZone(secondsFromGMT: 0)
        let dateFormatter3 = DateFormatter()
        dateFormatter3.dateFormat = "yyyy-MM-dd"
        if map.mappingType == .fromJSON {
            id <- map["scheduleI.ScheduleID"]
            dateSchedule <- (map["scheduleI.DateSchedule"],DateFormatterTransform(dateFormatter:dateFormatter))
            dateCreate <- (map["scheduleI.DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter2))
            active <- map["scheduleI.Active"]
            isCancel <- map["scheduleI.IsCancel"]
            scheduleType <- map["scheduleTypeI"]
            scheduleTime <- map["scheduleTimeI"]
            organize <- map["organizeI"]
            organizeCode <- map["scheduleI.OrganizeCode"]
            doctorID <- map["scheduleI.DoctorID"]
            doctorIDCode <- map["scheduleI.DoctorIDCode"]
            timeID <- map["scheduleI.ScheduleTimeID"]
            typeID <- map["scheduleI.ScheduleTypeID"]
            userID <- map["scheduleI.UserID"]
            userName <- map["scheduleI.Name"]
            organizeID <- map["scheduleI.OrganizeID"]
            note <- map["scheduleI.Note"]
            phone <- map["scheduleI.Phone"]
            address <- map["scheduleI.Address"]
            birthYear <- map["scheduleI.BirthYear"]
            birthDay <- map["scheduleI.Birthday"]
            gender <- map["scheduleI.Gender"]
            maGG <- map["scheduleI.MaGG"]
            noiKham <- map["scheduleI.Noikham"]
            ngayKham <- map["scheduleI.NgayKham"]
            gioHen <- map["scheduleI.GioHen"]
            lydoKham <- map["scheduleI.LydoKham"]
            maGen <- map["scheduleI.MaGen"]
            ngaySD <- map["scheduleI.NgaySD"]
            ngayHuyLich<-(map["scheduleI.NgayHuyLich"],DateFormatterTransform(dateFormatter:dateFormatter2))
            medicalProfileID <- map["MedicalProfileID"]
        } else {
            dateSchedule <- (map["DateSchedule"],DateFormatterTransform(dateFormatter:dateFormatter3))
            timeID <- map["ScheduleTimeID"]
            typeID <- map["ScheduleTypeID"]
            userID <- map["UserID"]
            userName <- map["Name"]
            organizeID <- map["OrganizeID"]
            organizeCode <- map["OrganizeCode"]
            note <- map["Note"]
            phone <- map["Phone"]
            doctorID <- map["DoctorID"]
            doctorIDCode <- map["DoctorIDCode"]
            address <- map["Address"]
            birthYear <- map["BirthYear"]
            birthDay <- map["Birthday"]
            gender <- map["Gender"]
            maGG <- map["MaGG"]
            listTestCode <- map["TestCode"]
            noiKham <- map["Noikham"]
            ngayKham <- map["NgayKham"]
            gioHen <- map["GioHen"]
            lydoKham <- map["LydoKham"]
            maGen <- map["MaGen"]
            ngaySD <- map["NgaySD"]
            ngayHuyLich<-(map["NgayHuyLich"],DateFormatterTransform(dateFormatter:dateFormatter2))
            medicalProfileID <- map["MedicalProfileID"]
        }
        specialistID <- map["SpecialistID"]
    }
//    
//    func toStringRequest()->String {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "dd-MM-yyyy"
//        return "\(timeID),\(typeID),\(userID),\(organizeID),\(phone ?? ""),\(address ?? ""),\(dateFormatter.string(from:dateSchedule!)),\(note ?? ""),\(specialistID)"
//    }
    
}
