//
//  TrackingNewsID+Network.swift
//  iCNM
//
//  Created by Quang Hung on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension TrackingNewsID {
    
    @discardableResult
    func sendTrackingNewsID(userID:Int, newsID:Int, deviceType:String, success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        let parameterDic:[String:Any] = [
            "UserID": userID,
            "NewsID": newsID,
            "TypeDevice": deviceType
        ]
        print(parameterDic)
        return API.requestJSON(endpoint: .sendTrackingNewsID(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as? String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
