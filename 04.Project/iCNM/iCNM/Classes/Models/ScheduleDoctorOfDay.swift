//
//  ScheduleDoctorOfDay.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper

class ScheduleDoctorOfDay:Mappable {
    var userID = 0
    var name = ""
    var birthYear = ""
    var phone = ""
    var address = ""
    var email = ""
    var lydoKham:String? = nil
    var ngaySD = ""
    var dateCreate = ""
    var gender = ""
    var giohen = ""
    var ngaykham = ""
    var active:Bool = false
    var KHCu = 0
    var medicalProfileID = 0
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            userID <- map["UserID"]
            name <- map["Name"]
            birthYear <- map["BirthYear"]
            phone <- map["Phone"]
            address <- map["Address"]
            email <- map["Email"]
            lydoKham <- map["LydoKham"]
            ngaySD <- map["NgaySD"]
            dateCreate <- map["DateCreate"]
            gender <- map["Gender"]
            giohen <- map["GioHen"]
            ngaykham <- map["NgayKham"]
            active <- map["Active"]
            KHCu <- map["KHCu"]
            medicalProfileID <- map["MedicalProfileID"]
        }
    }
}
