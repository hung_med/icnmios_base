//
//  MedicalVaccin+Network.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension MedicalVaccin {
    //post data vaccin
    
    @discardableResult
    func addEditMedicalVaccin(vaccinTypeID:Int, medicalVaccin:MedicalVaccin?, medicalProfileID:Int, success:@escaping(MedicalVaccin?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        var parameterDic = [String: Any]()
        let dicData = [
                   "VaccinTypeID": vaccinTypeID,
                   "VaccinID":medicalVaccin?.vaccinID as Any,
                   "VaccinName":medicalVaccin?.vaccinName as Any,
                   "VaccinDate":medicalVaccin?.vaccinDate as Any,
                   "VaccinSchedule":medicalVaccin?.vaccinSchedule as Any,
                   "VaccinReact":medicalVaccin?.vaccinReact as Any,
                   "MonthPreg": medicalVaccin?.monthPreg as Any,
                   "Active":medicalVaccin?.active as Any]
        
        parameterDic = ["MedicalVaccin": dicData,
                            "MedicalProfileID": medicalProfileID]
        
        return API.requestJSON(endpoint: .addEditMedicalVaccin(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let medicalVaccin = MedicalVaccin(JSON: item) {
                        success(medicalVaccin)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func getAllVaccin(typeID:Int, factoryID:Int, success:@escaping([MedicalVaccin]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        if typeID == 1{
            return API.requestJSON(endpoint: .getVaccinTreEm(factoryID), completionHandler: { (response) in
                switch response.result {
                case .success(let value):
                    var data = [MedicalVaccin]()
                    if let array = value as? [[String:Any]]  {
                        for item in array {
                            if let medicalVaccin = MedicalVaccin(JSON: item) {
                                data.append(medicalVaccin)
                            }
                        }
                    }
                    success(data)
                case .failure(let error):
                    fail(error,response)
                }
            })
        }else if typeID == 2{
            return API.requestJSON(endpoint: .getVaccinTCMR(factoryID), completionHandler: { (response) in
                switch response.result {
                case .success(let value):
                    var data = [MedicalVaccin]()
                    if let array = value as? [[String:Any]]  {
                        for item in array {
                            if let medicalVaccin = MedicalVaccin(JSON: item) {
                                data.append(medicalVaccin)
                            }
                        }
                    }
                    success(data)
                case .failure(let error):
                    fail(error,response)
                }
            })
        }else{
            return API.requestJSON(endpoint: .getVaccinUonVan(factoryID), completionHandler: { (response) in
                switch response.result {
                case .success(let value):
                    var data = [MedicalVaccin]()
                    if let array = value as? [[String:Any]]  {
                        for item in array {
                            if let medicalVaccin = MedicalVaccin(JSON: item) {
                                data.append(medicalVaccin)
                            }
                        }
                    }
                    success(data)
                case .failure(let error):
                    fail(error,response)
                }
            })
        }
    }
}
