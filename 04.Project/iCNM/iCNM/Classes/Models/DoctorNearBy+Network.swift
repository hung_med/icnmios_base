//
//  DoctorNearBy+Network.swift
//  iCNM
//
//  Created by Nguyen Van Dung on 8/1/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Alamofire

extension DoctorNearBy {

    static func getDoctorNearBy(locationID: Int, completionHandler: @escaping([Doctor]?, Error?) -> ()) {
        
        let urlStr = Constant.apiBaseUrl + "api/locations/\(148)/Doctor"
        
        let escapedUrl = urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        Alamofire.request(escapedUrl!, method: .get, parameters: nil,
                          encoding: URLEncoding.default).validate().responseJSON { response in
                            switch response.result {
                            case .success(let value):
                                
                                var data = [Doctor]()
                                if let array = value as? [[String:Any]] {
                                    for item in array {
                                        if let doctorNearBy = Doctor(JSON: item) {
                                            data.append(doctorNearBy)
                                        }
                                    }
                                }
                                completionHandler(data, nil)
                                
                            case .failure(let error):
                                print("GetLocationNearBy Error : \(error.localizedDescription)")
                            }
        }
        
    }

}
