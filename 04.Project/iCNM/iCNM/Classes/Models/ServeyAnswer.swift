//
//  ServeyAnswer.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class ServeyAnswer:Mappable {
    var userID:Int? = 0
    var serveyID:Int = 0
    var nServeyQuesID:Int? = 0
    var nServeyAnsID:Int? = 0
    var dateCreate:Date? = nil
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .toJSON {
            userID <- map["UserID"]
            serveyID <- map["ServeyID"]
            nServeyQuesID <- map["NServeyQuesID"]
            nServeyAnsID <- map["NServeyAnsID"]
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
            if dateCreate == nil {
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
            }
        }
    }
}
