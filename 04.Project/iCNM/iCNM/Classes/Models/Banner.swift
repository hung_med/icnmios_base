//
//  Banner.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class Banner:Mappable {
    var bannerID:Int = 0
    var bannerTypeID:Int = 0
    var bannerTypeName = ""
    var des = ""
    var imgStr = ""
    var content = ""
    var dateCreate = ""
    var active:Bool = false
    var dataID:Int = 0
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            bannerID <- map["UserID"]
            bannerTypeID <- map["BannerTypeID"]
            bannerTypeName <- map["BannerTypeName"]
            des <- map["Des"]
            imgStr <- map["ImgStr"]
            content <- map["Content"]
            dateCreate <- map["DateCreate"]
            active <- map["Active"]
            dataID <- map["DataID"]
        }
    }
}
