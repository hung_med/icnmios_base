//
//  Specialist.swift
//  iCNM
//
//  Created by Medlatec on 5/11/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import ObjectMapper
import RealmSwift

class Specialist:Object,Mappable {
    dynamic var id = 0
    dynamic var name:String?
    dynamic var createdDate:Date?
    dynamic var orderNum = 0
    dynamic var active = false
    //property for selected 
    dynamic var selected = true
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        id <- map["SpecialistID"]
        name <- map["SpecialName"]
        createdDate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
        orderNum <- map["OrderNum"]
        active <- map["Active"]
        let realm = try! Realm()
        if let currentObject = realm.object(ofType: Specialist.self, forPrimaryKey:id) {
            selected = currentObject.selected
        }
    }
    
}
