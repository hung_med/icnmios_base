//
//  GeneralID.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class GeneralID:Mappable {
    var mmagen = ""
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        mmagen <- map["magen"]
    }
}
