//
//  ScheduleType.swift
//  iCNM
//
//  Created by Balua on 8/14/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper

class ScheduleType:Mappable {
    var id = 0
    var name:String?
    var description:String?
    var active = false
    var imageURL:String?
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["ScheduleTypeID"]
        name <- map["ScheduleTypeName"]
        description <- map["ScheduleTypeDescription"]
        active <- map["Active"]
        imageURL <- map["ImgScheduleType"]
    }
    
}
