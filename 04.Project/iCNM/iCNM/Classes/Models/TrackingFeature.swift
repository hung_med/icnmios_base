//
//  TrackingFeature.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class TrackingFeature:Mappable {
    var fCM:String = ""
    var featureName:String = ""
    var dateCreate:Date?
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            fCM <- map["FCM"]
            featureName <- map["FeatureName"]
            dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
            if dateCreate == nil {
                    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
                    dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
            }
        }
    }
}
