//
//  Servey.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class Servey:Mappable {
    var serveyID:Int = 0
    var serveyTitle:String? = nil
    var nServeyQuesID:Int = 0
    var nServeyQuesContent:String? = nil
    var nServeyAnsID:Int = 0
    var nServeyAnsContent:String? = nil
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            serveyID <- map["ServeyID"]
            serveyTitle <- map["ServeyTitle"]
            nServeyQuesID <- map["NServeyQuesID"]
            nServeyQuesContent <- map["NServeyQuesContent"]
            nServeyAnsID <- map["NServeyAnsID"]
            nServeyAnsContent <- map["NServeyAnsContent"]
        }
    }
}
