//
//  NotificationUser+Network.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension NotificationUser {

    @discardableResult
    func getAllNotificationUser(userID:Int, doctorID:Int, success:@escaping([NotificationUser]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getAllNotificationUser(userID, doctorID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [NotificationUser]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let notificationUser = NotificationUser(JSON: item) {
                            data.append(notificationUser)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func updateStatusReadNotification(notificationID:Int, userID:Int, success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .updateStatusReadNotification(notificationID, userID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as? String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
