//
//  NewsCategory+Network.swift
//  iCNM
//
//  Created by Len Pham on 8/21/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import Alamofire

extension NewsCategory {
@discardableResult
    static func getListNewsOfAllCategory(categoryIds:[Int], success:@escaping([NewsCategory]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
    var stringParam: String = String()
    stringParam = stringParam.appendingFormat("%@",(categoryIds.map{String($0)}).joined(separator: "-"))
    return API.requestJSON(endpoint: .getNewsOfAllCategory(stringParam), completionHandler: { (response) in
        switch response.result {
        case .success(let value):
            var data = [NewsCategory]()
            if let array = value as? [[String:Any]] {
                for item in array {
                    if let newsCategoryModel = NewsCategory(JSON: item) {
                        data.append(newsCategoryModel)
                    }
                }
            }
            success(data)
        case .failure(let error):
            fail(error,response)
        }
    })
    }
}
