//
//  LookUpResult+Network.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension LookUpResult {
    
    @discardableResult
    static func getListLookUpResultByPID(pID:String, organizeID:String, success:@escaping([Patient]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getListLookUpResultByPID(pID, organizeID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [Patient]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let lookUpResult = Patient(JSON: item) {
                            data.append(lookUpResult)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
