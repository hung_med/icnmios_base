//
//  LookUpChart.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class LookUpChart:Mappable {
    var normalRange = ""
    var imgUrl = ""
    var conclusion = ""
    var lowLimit = 0.0
    var resultTypeID = ""
    var hightLimit = 0.0
    var testHead = ""
    var printOrder = ""
    var sID = ""
    var price = 0
    var testCode = ""
    var testName = ""
    var resultTime = ""
    var resultText = ""
    var resultID = ""
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            normalRange <- map["NormalRange"]
            imgUrl <- map["ImgUrl"]
            conclusion <- map["Conclusion"]
            lowLimit <- map["LowLimit"]
            resultTypeID <- map["ResultTypeID"]
            hightLimit <- map["HightLimit"]
            testHead <- map["TestHead"]
            printOrder <- map["PrintOrder"]
            sID <- map["SID"]
            price <- map["Price"]
            testCode <- map["TestCode"]
            testName <- map["TestName"]
            resultTime <- map["ResultTime"]
            if resultTime != "" && !resultTime.isEmpty{
                let partText = resultTime.components(separatedBy: "T")[0].components(separatedBy: "-")
                let yyyy = partText[0]
                let mm = partText[1]
                let dd = partText[2]
                resultTime = "\(dd)-\(mm)-\(yyyy)"
            }
            
            resultText <- map["ResultText"]
            resultID <- map["ResultID"]
        }
    }
    
}
