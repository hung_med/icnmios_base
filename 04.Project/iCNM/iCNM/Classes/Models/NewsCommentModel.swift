//
//  NewsCommentModel.swift
//  iCNM
//
//  Created by Len Pham on 8/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper

class NewsCommentModel: Mappable {
    
    var newsCommentId = ""
    var categoryId = 0
    var newsId = 0
    var userId = 0
    var comment = ""
    var dateCreate = ""
    var dateEdit = ""
    var active = 0
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            newsCommentId <- map["NewsCommentID"]
            newsId <- map["NewsID"]
            userId <- map["UserID"]
            comment <- map["Comment"]
            dateCreate <- map["DateCreate"]
            dateEdit <- map["DateEdit"]
            active <- map["Active"]
            
        }
    }
    

    
}
