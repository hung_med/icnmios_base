//
//  Version.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class Version:Mappable {
    var versionNum:Int = 0
    var versionType:Int = 0
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            versionNum <- map["VerNum"]
            versionType <- map["VerType"]
        }
    }
}
