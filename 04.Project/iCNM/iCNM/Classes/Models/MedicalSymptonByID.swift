//
//  UserRegister.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class MedicalSymptonByID:Mappable {
    var medicalDictionary:[MedicalDetailsByGroups]?
    var medicalSympton:MedicalSymptonObject?
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
      if map.mappingType == .fromJSON {
            medicalDictionary <- map["MedicalDictionaries"]
            medicalSympton <- map["MedicalSympton"]
        }
    }
}
