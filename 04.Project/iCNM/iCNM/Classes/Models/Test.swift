//
//  Test.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class Test:Mappable {
    var testCode = ""
    var quickCode = ""
    var testName = ""
    var category = ""
    var price:Int = 0
    var normalRange = ""
    var normalRangeF = ""
    var unit = ""
    var printOrder:Int = 0
    var testHead:Bool = false
    var normalResult = ""
    var lowerLimit:Float = 0.0
    var higherLimit:Float = 0.0
    var profile:Bool = false
    var bold:Bool = false
    var userI = ""
    var userU = ""
    var intime = ""
    var updateTime = ""
    var stat:Bool = false
    var criteria = ""
    var lowHigh:Bool = false
    var lHVal = ""
    var type = ""
    var optional:Bool = false
    var bLLower = ""
    var bLHigher = ""
    var testNameE = ""
    var bLLowerE = ""
    var bLHigherE = ""
    var showTestResult:Bool = false
    var resultType:Int = 0
    var diagnosticResult = ""
    var conclusion = ""
    var rowguid = ""
    var i_name = ""
    var i_pay:Bool = false
    var i_cost:Int = 0
    var hang:Int = 0
    var hightech:Bool = false
    var valid_Check = ""
    var costMale:Int = 0
    var costFemale:Int = 0
    var discountMale:Float = 0.0
    var discountFemale:Float = 0.0
    var isMale:Bool = false
    var IsFemale:Bool = false
   
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            testCode <- map["TestCode"]
            quickCode <- map["QuickCode"]
            testName <- map["TestName"]
            category <- map["Category"]
            price <- map["Price"]
            normalRange <- map["NormalRange"]
            normalRangeF <- map["NormalRangeF"]
            unit <- map["Unit"]
            printOrder <- map["PrintOrder"]
            testHead <- map["TestHead"]
            normalResult <- map["NormalResult"]
            lowerLimit <- map["LowerLimit"]
            higherLimit <- map["HigherLimit"]
            profile <- map["Profile"]
            bold <- map["Bold"]
            userI <- map["UserI"]
            userU <- map["UserU"]
            intime <- map["Intime"]
            updateTime <- map["UpdateTime"]
            stat <- map["Stat"]
            criteria <- map["Criteria"]
            lowHigh <- map["LowHigh"]
            lHVal <- map["LHVal"]
            type <- map["Type"]
            optional <- map["Optional"]
            bLLower <- map["BLLower"]
            bLHigher <- map["BLHigher"]
            testNameE <- map["TestNameE"]
            bLLowerE <- map["BLLowerE"]
            bLHigherE <- map["BLHigherE"]
            showTestResult <- map["ShowTestResult"]
            resultType <- map["ResultType"]
            diagnosticResult <- map["DiagnosticResult"]
            conclusion <- map["Conclusion"]
            rowguid <- map["Rowguid"]
            i_name <- map["I_name"]
            i_pay <- map["I_pay"]
            i_cost <- map["I_cost"]
            hang <- map["Hang"]
            hightech <- map["Hightech"]
            valid_Check <- map["Valid_Check"]
            costMale <- map["CostMale"]
            costFemale <- map["CostFemale"]
            discountMale <- map["DiscountMale"]
            discountFemale <- map["DiscountFemale"]
            isMale <- map["isMale"]
            IsFemale <- map["IsFemale"]
        }
    }
}
