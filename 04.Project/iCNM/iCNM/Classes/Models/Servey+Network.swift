//
//  Servey+Network.swift
//  iCNM
//
//  Created by Quang Hung on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension Servey {
    @discardableResult
    func getServey(serveyID:Int, success:@escaping([Servey]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getServey(serveyID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [Servey]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let servey = Servey(JSON: item) {
                            data.append(servey)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
