//
//  Question+Network.swift
//  iCNM
//
//  Created by Medlatec on 7/27/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import Alamofire

extension Question {
    @discardableResult
    func create(success:@escaping(String) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<String>)->Void) -> Request {
        let paramDic = self.toJSON()
        return API.requestString(endpoint: .CreateQuestion(paramDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    static func uploadPhoto(paramDic:[String:Any],success:@escaping(String) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<String>)->Void) -> Request {
        return API.requestString(endpoint: .UploadQuestionImage(paramDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    static func removeComment(paramDic:[String:Any],success:@escaping(String) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<String>)->Void) -> Request {
        return API.requestString(endpoint: .RemoveCommentQuestion(paramDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
