//
//  ServiceRatingFollowUser+Network.swift
//  iCNM
//
//  Created by Quang Hung on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension ServiceRatingFollowUser {
    @discardableResult
    static func getAllServiceRating(pageID:Int, serviceID:Int, success:@escaping([ServiceRatingFollowUser]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getAllServiceRating(pageID, serviceID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [ServiceRatingFollowUser]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let serviceRatingFollowUser = ServiceRatingFollowUser(JSON: item) {
                            data.append(serviceRatingFollowUser)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func postServiceRating(serRateContent:String, serRateNum:Float, serviceID:Int, userID:Int, success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        var parameterDic = [String: Any]()
       
        parameterDic = ["SerRateContent": serRateContent,
                        "SerRateNum": serRateNum,
                        "ServiceID": serviceID,
                        "UserID": userID]
        
        return API.requestJSON(endpoint: .postServiceRating(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as? String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    func putServiceRating(serRateID:Int, serRateContent:String, serRateNum:Float, serviceID:Int, userID:Int, success:@escaping(String?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        let parameterDic:[String:Any] = [
            "serRateID": serRateID,
            "SerRateContent": serRateContent,
            "SerRateNum": serRateNum,
            "ServiceID": serviceID,
            "UserID": userID
        ]

        return API.requestJSON(endpoint: .putServiceRating(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as? String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
