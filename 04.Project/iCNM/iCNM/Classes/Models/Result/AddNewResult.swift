//
//  AddNewResult.swift
//  iCNM
//
//  Created by Thanh Huyen on 4/14/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper

class AddNewResult: Mappable {
    var resultTypeID = 0
    var resulsTypeCode = ""
    var resultTypeName = ""
    var active = Bool()
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            resultTypeID <- map["ResultTypeID"]
            resulsTypeCode <- map["ResultTypeCode"]
            resultTypeName <- map["ResultTypeName"]
            active <- map["Active"]
        }
    }
}
