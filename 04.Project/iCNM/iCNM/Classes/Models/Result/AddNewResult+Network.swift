//
//  AddNewResult+Network.swift
//  iCNM
//
//  Created by Thanh Huyen on 4/14/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import Foundation
import Alamofire

extension AddNewResult {
    @discardableResult
    func getAllResultType(success:@escaping([AddNewResult]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getAllResultType(), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [AddNewResult]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let getAllResult = AddNewResult(JSON: item) {
                            data.append(getAllResult)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    static func addPatientResult(parameterDic:[String:Any], success:@escaping(String) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request{
        return API.requestJSON(endpoint: .addPatientResult(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as! String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}

