//
//  DetailLookUpResultBySID.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper

class ResultPatient:Mappable {
    var conclusion = ""
    var hightLimit:CGFloat = 0.0
    var imgUrl = ""
    var lowLimit:CGFloat = 0.0
    var normalRange = ""
    var price = 0
    var printOrder = 0
    var resultID = 0
    var resultText = ""
    var resultTime = ""
    var resultTypeID = ""
    var sID = ""
    var testCode = ""
    var testHead = 0
    var testName = ""
    var doctorName = ""
    var unit = ""
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .toJSON {
            conclusion <- map["Conclusion"]
            hightLimit <- map["HightLimit"]
            imgUrl <- map["ImgUrl"]
            lowLimit <- map["LowLimit"]
            normalRange <- map["NormalRange"]
            price <- map["Price"]
            printOrder <- map["PrintOrder"]
            resultID <- map["ResultID"]
            resultText <- map["ResultText"]
            resultTime <- map["ResultTime"]
            resultTypeID <- map["ResultTypeID"]
            sID <- map["SID"]
            testCode <- map["TestCode"]
            testHead <- map["TestHead"]
            testName <- map["TestName"]
            doctorName <- map["DoctorName"]
            unit <- map["Unit"]
        }
    }
}
