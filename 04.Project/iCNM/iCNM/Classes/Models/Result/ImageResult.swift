//
//  AddNewResult.swift
//  iCNM
//
//  Created by Thanh Huyen on 4/14/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper

class ImageResult: Mappable {
    var base64String = ""
    var name = ""
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .toJSON {
            base64String <- map["Base64String"]
            name <- map["Name"]
        }
    }
}
