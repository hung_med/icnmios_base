//
//  Patient.swift
//  iCNM
//
//  Created by Thanh Huyen on 4/18/19.
//  Copyright © 2019 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper

class Patient: Mappable {
    var PatientID = 0
    var PID = ""
    var SID = ""
    var Code = ""
    var SidCode = ""
    var Phone = ""
    var Email = ""
    var PatientName = ""
    var OrganizeID = 0
    var MedicalProfileID = 0
    var DateCreate = Date()
    var InTime = ""
    var ReExaminationDate = Date()
    var Gender = ""
    var Active = Bool()
    var SumMoney:Float? = 0.0
    var Age = 0
    var Reason = ""
    var Place = ""
    var Conclude = ""
    var Note = ""
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
        
        if map.mappingType == .toJSON {
            PatientID <- map["PatientID"]
            PID <- map["PID"]
            SID <- map["SID"]
            Code <- map["Code"]
            SidCode <- map["SidCode"]
            Phone <- map["Phone"]
            Email <- map["Email"]
            PatientName <- map["PatientName"]
            OrganizeID <- map["OrganizeID"]
            MedicalProfileID <- map["MedicalProfileID"]
            DateCreate <- map["DateCreate"]
            InTime <- map["InTime"]
            if InTime != "" && !InTime.isEmpty{
                let partText = InTime.components(separatedBy: "T")[0].components(separatedBy: "-")
                let yyyy = partText[0]
                let mm = partText[1]
                let dd = partText[2]
                InTime = "\(dd)-\(mm)-\(yyyy)"
            }
            ReExaminationDate <- map["ReExaminationDate"]
            Gender <- map["Gender"]
            Active <- map["Active"]
            SumMoney <- map["SumMoney"]
            Age <- map["Age"]
            Reason <- map["Reason"]
            Place <- map["Place"]
            Conclude <- map["Conclude"]
            Note <- map["Note"]
        } else {
            PatientID <- map["PatientID"]
            PID <- map["PID"]
            SID <- map["SID"]
            Code <- map["Code"]
            SidCode <- map["SidCode"]
            Phone <- map["Phone"]
            Email <- map["Email"]
            PatientName <- map["PatientName"]
            OrganizeID <- map["OrganizeID"]
            MedicalProfileID <- map["MedicalProfileID"]
            DateCreate <- map["DateCreate"]
            InTime <- map["InTime"]
            if InTime != "" && !InTime.isEmpty{
                let partText = InTime.components(separatedBy: "T")[0].components(separatedBy: "-")
                let yyyy = partText[0]
                let mm = partText[1]
                let dd = partText[2]
                InTime = "\(dd)-\(mm)-\(yyyy)"
            }
            ReExaminationDate <- map["ReExaminationDate"]
            Gender <- map["Gender"]
            Active <- map["Active"]
            SumMoney <- map["SumMoney"]
            Age <- map["Age"]
            Reason <- map["Reason"]
            Place <- map["Place"]
            Conclude <- map["Conclude"]
            Note <- map["Note"]
        }
    }
}
