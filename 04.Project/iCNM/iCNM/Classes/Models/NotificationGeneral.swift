//
//  NotificationGeneral.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class NotificationGeneral:Mappable {
    
    var notificationID:Int = 0
    var userID:Int = 0
    var doctorID:Int = 0
    var doctorIDCode:Int = 0
    var title = ""
    var description = ""
    var img = ""
    var data = ""
    var organizeID:Int = 0
    var dateCreate = ""
    var send:Bool? = false
    var isRead:Bool? = false
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            notificationID <- map["NotificationID"]
            userID <- map["UserID"]
            doctorID <- map["DoctorID"]
            doctorIDCode <- map["DoctorIDCode"]
            title <- map["Title"]
            description <- map["Description"]
            img <- map["Img"]
            data <- map["Data"]
            organizeID <- map["OrganizeID"]
            dateCreate <- map["DateCreate"]
            send <- map["Send"]
            isRead <- map["IsRead"]
        }
    }
}
