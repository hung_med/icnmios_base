//
//  MedicalHealthFactory.swift
//  iCNM
//
//  Created by Ta Quang Hung on 20/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper

class MedicalHealthFactory:Mappable {
    
    var activate:Bool? = false
    var pPTranhThaiDD = ""
    var benhBuouCo:Bool? = false
    var benhDaDay:Bool? = false
    var benhNote = ""
    var benhPhoiManTinh:Bool? = false
    var benhPhuKhoa = ""
    var benhTimMach:Bool? = false
    var biNgat:Bool? = false
    var boHutThuoc:Bool? = false
    var boMaTuy:Bool? = false
    var boRuouBia:Bool? = false
    var cdLucDe:Int = 0
    var cnLucDe:Int = 0
    var coThai:Int = 0
    var congVeoCS = ""
    var duHoaChatMP = ""
    var duNote = ""
    var duThucPham = ""
    var daiThaoDuong:Bool? = false
    var deDuThang:Int = 0
    var deKho:Int = 0
    var deMo:Bool?=false
    var deMoNum:Int = 0
    var deNon:Int = 0
    var deThieuThang:Bool? = false
    var deThuong:Bool? = false
    var deThuongNum:Int = 0
    var diTatBS = ""
    var diUngThuoc = ""
    var dongKinh:Bool? = false
    var henSuyen:Bool? = false
    var hutThuoc:Bool? = false
    var ktChan = ""
    var ktTay = ""
    var ktThiLuc = ""
    var kheHoMoiVM = ""
    var khuyetTatNote = ""
    var khuyetTatThinhLuc = ""
    var kyThaiCC = ""
    var lao = ""
    var loaiHoXi = ""
    var maTuy:Bool? = false
    var maTuyNgay:Bool? = false
    var medicalHealthFactoryID:Int = 0
    var nghiTheDuc:Bool? = false
    var nguyCoNote = ""
    var note = ""
    var phaThai:Int = 0
    var ruouBia:Bool? = false
    var ruouBiaNgay:Int = 0
    var sayThai:Int = 0
    var sinhDe:Int = 0
    var soConHienSong:Int = 0
    var tsGDBenhTimMach:Bool? = false
    var tsGDBenhTimMachName = ""
    var tsGDDaiThaoDuong:Bool? = false
    var tsGDDaiThaoDuongName = ""
    var tsGDDongKinh:Bool? = false
    var tsGDDongKinhName = ""
    var tsGDHenSuyen:Bool? = false
    var tsGDHenSuyenName = ""
    var tsGDHoaChatMyPham = ""
    var tsGDHoaChatMyPhamName = ""
    var tsGDLao = ""
    var tsGDLaoName = ""
    var tsGDNote = ""
    var tsGDNoteAnother = ""
    var tsGDNoteAnotherName = ""
    var tsGDNoteName = ""
    var tsGDTamThan:Bool? = false
    var tsGDTamThanName = ""
    var tsGDTangHuyetAp:Bool? = false
    var tsGDTangHuyetApName = ""
    var tsGDThucPham = ""
    var tsGDThucPhamName = ""
    var tsGDThuoc = ""
    var tsGDThuocName = ""
    var tsGDUngThu = ""
    var tsGDUngThuName = ""
    var ttHutThuoc:Bool? = false
    var ttLSNote = ""
    var tamThan:Bool? = false
    var tangHuyetAp:Bool? = false
    var theDuc:Bool? = false
    var theDucNgay:Bool? = false
    var tienSuPhauThuat = ""
    var timBamSinh:Bool? = false
    var tuKy:Bool? = false
    var ungThu = ""
    var vacXinUVNum:Int = 0
    var viemGan:Bool? = false
    var yeuToTXNN = ""
    var yeuToTXNNTime = ""
    // PRAGMA - ObjectMapper
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        activate <- map["Activate"]
        pPTranhThaiDD <- map["BPTranhThaiDD"]
        benhBuouCo <- map["BenhBuouCo"]
        benhDaDay <- map["BenhDaDay"]
        benhNote <- map["BenhNote"]
        benhPhoiManTinh <- map["BenhPhoiManTinh"]
        benhPhuKhoa <- map["BenhPhuKhoa"]
        benhTimMach <- map["BenhTimMach"]
        biNgat <- map["BiNgat"]
        boHutThuoc <- map["BoHutThuoc"]
        boMaTuy <- map["BoMaTuy"]
        boRuouBia <- map["BoRuouBia"]
        cdLucDe <- map["CDLucDe"]
        cnLucDe <- map["CNLucDe"]
        coThai <- map["CoThai"]
        congVeoCS <- map["CongVeoCS"]
        duHoaChatMP <- map["DUHoaChatMP"]
        duNote <- map["DUNote"]
        duThucPham <- map["DUThucPham"]
        daiThaoDuong <- map["DaiThaoDuong"]
        deDuThang <- map["DeDuThang"]
        deKho <- map["DeKho"]
        deMo <- map["DeMo"]
        deMoNum <- map["DeMoNum"]
        deNon <- map["DeNon"]
        deThieuThang <- map["DeThieuThang"]
        deThuong <- map["DeThuong"]
        deThuongNum <- map["DeThuongNum"]
        diTatBS <- map["DiTatBS"]
        diUngThuoc <- map["DiUngThuoc"]
        dongKinh <- map["DongKinh"]
        henSuyen <- map["HenSuyen"]
        hutThuoc <- map["HutThuoc"]
        ktChan <- map["KTChan"]
        ktTay <- map["KTTay"]
        ktThiLuc <- map["KTThiLuc"]
        kheHoMoiVM <- map["KheHoMoiVM"]
        khuyetTatNote <- map["KhuyetTatNote"]
        khuyetTatThinhLuc <- map["KhuyetTatThinhLuc"]
        kyThaiCC <- map["KyThaiCC"]
        lao <- map["Lao"]
        loaiHoXi <- map["LoaiHoXi"]
        maTuy <- map["MaTuy"]
        maTuyNgay <- map["MaTuyNgay"]
        medicalHealthFactoryID <- map["MedicalHealthFactoryID"]
        nghiTheDuc <- map["NghiTheDuc"]
        nguyCoNote <- map["NguyCoNote"]
        note <- map["Note"]
        phaThai <- map["PhaThai"]
        ruouBia <- map["RuouBia"]
        ruouBiaNgay <- map["RuouBiaNgay"]
        sayThai <- map["SayThai"]
        sinhDe <- map["SinhDe"]
        soConHienSong <- map["SoConHienSong"]
        tsGDBenhTimMach <- map["TSGDBenhTimMach"]
        tsGDBenhTimMachName <- map["TSGDBenhTimMachName"]
        tsGDDaiThaoDuong <- map["TSGDDaiThaoDuong"]
        tsGDDaiThaoDuongName <- map["TSGDDaiThaoDuongName"]
        tsGDDongKinh <- map["TSGDDongKinh"]
        tsGDDongKinhName <- map["TSGDDongKinhName"]
        tsGDHenSuyen <- map["TSGDHenSuyen"]
        tsGDHenSuyenName <- map["TSGDHenSuyenName"]
        tsGDHoaChatMyPham <- map["TSGDHoaChatMyPham"]
        tsGDHoaChatMyPhamName <- map["TSGDHoaChatMyPhamName"]
        tsGDLao <- map["TSGDLao"]
        tsGDLaoName <- map["TSGDLaoName"]
        tsGDNote <- map["TSGDNote"]
        tsGDNoteAnother <- map["TSGDNoteAnother"]
        tsGDNoteAnotherName <- map["TSGDNoteAnotherName"]
        tsGDNoteName <- map["TSGDNoteName"]
        tsGDTamThan <- map["TSGDTamThan"]
        tsGDTamThanName <- map["TSGDTamThanName"]
        tsGDTangHuyetAp <- map["TSGDTangHuyetAp"]
        tsGDTangHuyetApName <- map["TSGDTangHuyetApName"]
        tsGDThucPham <- map["TSGDThucPham"]
        tsGDThucPhamName <- map["TSGDThucPhamName"]
        tsGDThuoc <- map["TSGDThuoc"]
        tsGDThuocName <- map["TSGDThuocName"]
        tsGDUngThu <- map["TSGDUngThu"]
        tsGDUngThuName <- map["TSGDUngThuName"]
        ttHutThuoc <- map["TTHutThuoc"]
        ttLSNote <- map["TTLSNote"]
        tamThan <- map["TamThan"]
        tangHuyetAp <- map["TangHuyetAp"]
        theDuc <- map["TheDuc"]
        theDucNgay <- map["TheDucNgay"]
        tienSuPhauThuat <- map["TienSuPhauThuat"]
        timBamSinh <- map["TimBamSinh"]
        tuKy <- map["TuKy"]
        ungThu <- map["UngThu"]
        vacXinUVNum <- map["VacXinUVNum"]
        viemGan <- map["ViemGan"]
        yeuToTXNN <- map["YeuToTXNN"]
        yeuToTXNNTime <- map["YeuToTXNNTime"]
    }
    
}
