//
//  District.swift
//  iCNM
//
//  Created by Medlatec on 5/11/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import ObjectMapper
import RealmSwift

class District:Object,Mappable {
    dynamic var id:String?
    dynamic var provinceID:String?
    dynamic var name:String?
    dynamic var active = false
    dynamic var rowGUID:String?
    dynamic var createdDate:Date?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        id <- map["QuanHuyen_ID"]
        provinceID <- map["TinhThanh_ID"]
        name <- map["QuanHuyen"]
        active <- map["KichHoat"]
        rowGUID <- map["rowguid"]
        createdDate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
    }
    
}
