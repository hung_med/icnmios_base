//
//  TestMedicalSchedule.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class TestMedicalSchedule:Mappable {
    var testCode:String = ""
    var testName:String = ""
    var price:Float = 0.0
    var categoryName:String = ""
    var profile:Bool? = false
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            testCode <- map["TestCode"]
            testName <- map["TestName"]
            price <- map["Price"]
            categoryName <- map["CategoryName"]
            profile <- map["Profile"]
        }        
    }
}
