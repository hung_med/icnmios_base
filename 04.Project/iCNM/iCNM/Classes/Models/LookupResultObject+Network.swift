//
//  LookupResultObject+Network.swift
//  iCNM
//
//  Created by Quang Hung on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension LookupResultObject {
    @discardableResult
    static func getListLookUpResultUnit(maBS:String, currentMonth:String, currentYear:String, organizeID:String, success:@escaping(LookupResultObject?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getListLookUpResultUnit(maBS, currentMonth, currentYear, organizeID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let lookUpResult = LookupResultObject(JSON: item) {
                        success(lookUpResult)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
}
