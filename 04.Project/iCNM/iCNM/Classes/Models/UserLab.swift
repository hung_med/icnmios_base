//
//  Process.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class UserLab:Mappable {
    var userID:String? = nil
    var userName:String? = nil
    var phone:String? = nil
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        if map.mappingType == .fromJSON {
            userID <- map["UserID"]
            userName <- map["UserName"]
            phone <- map["Phone"]
        }
    }
}
