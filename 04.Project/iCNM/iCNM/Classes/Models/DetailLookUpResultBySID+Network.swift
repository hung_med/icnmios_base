//
//  DetailLookUpResultBySID.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire

extension DetailLookUpResultBySID {
    
    @discardableResult
    static func getListDetailLookUpResultBySID(sID:String, organizeID:String, success:@escaping([DetailLookUpResultBySID]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getListDetailLookUpResultBySID(sID, organizeID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [DetailLookUpResultBySID]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let detailLookUpObj = DetailLookUpResultBySID(JSON: item) {
                            data.append(detailLookUpObj)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    static func getListDetailLookUpResultByPhone(phone:String, success:@escaping([DetailLookUpResultBySID]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getListDetailLookUpResultByPhone(phone), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [DetailLookUpResultBySID]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let detailLookUpObj = DetailLookUpResultBySID(JSON: item) {
                            data.append(detailLookUpObj)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
