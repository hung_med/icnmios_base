//
//  UserQuestionComment.swift
//  iCNM
//
//  Created by Medlatec on 6/28/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class UserQuestionComment:Mappable {
    var user:UserInfo?
    var questionComment:QuestionComment?
    
    // PRAGMA - ObjectMapper
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        user <- map["User"]
        questionComment <- map["QuestionComment"]
    }
    
}
