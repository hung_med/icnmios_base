//
//  ServiceGroupsTest.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class ServiceGroupsTest:Mappable {
    var serviceDetailID:Int = 0
    var serviceID:Int = 0
    var testName:String = ""
    var testCode:Int = 0
    var price:Float = 0.0
    var groupTestName:String = ""
    var orderNumber:String = ""
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            serviceDetailID <- map["ServiceDetailID"]
            serviceID <- map["ServiceID"]
            testName <- map["TestName"]
            testCode <- map["TestCode"]
            price <- map["Price"]
            groupTestName <- map["GroupTestName"]
            orderNumber <- map["OrderNumber"]
        }
    }
}
