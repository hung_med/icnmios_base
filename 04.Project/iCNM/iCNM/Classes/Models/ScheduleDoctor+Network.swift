//
//  ScheduleDoctor+Network.swift
//  iCNM
//
//  Created by Quang Hung on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension ScheduleDoctor {
    @discardableResult
    func getDoctorFollowSpecialist(organizeID:Int, currentDate:String, specialistID:Int, success:@escaping([ScheduleDoctor]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getDoctorFollowSpecialist(organizeID, specialistID, currentDate), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [ScheduleDoctor]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let scheduleDoctor = ScheduleDoctor(JSON: item) {
                            data.append(scheduleDoctor)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
