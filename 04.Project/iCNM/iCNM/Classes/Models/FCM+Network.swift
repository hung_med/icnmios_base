//
//  FCM+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension FCM {
    @discardableResult
    func addFCM(key:String, userID:Int, doctorID:Int, doctorIDCode:String, success:@escaping(FCM?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        let parameterDic:[String:Any] = [
            "FCMKey": key,
            "UserID": userID,
            "DoctorID": doctorID,
            "DoctorIDCode": doctorIDCode,
            "DeviceType":2
        ]
        return API.requestJSON(endpoint: .addFCM(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let fcm = FCM(JSON: item) {
                        success(fcm)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
