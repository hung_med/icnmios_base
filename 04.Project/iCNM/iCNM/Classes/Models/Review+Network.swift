//
//  Review+Network.swift
//  iCNM
//
//  Created by Nguyen Van Dung on 8/12/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Alamofire

extension Review {

    @discardableResult
    static func userAddReview(rateContent:String, general:Double, medicalEthics:Double, technique:Double, userID:Int, locationID:Int, sumRate:Double, success:@escaping(String) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        let parameterDic:[String:Any] = [
            "RateContent": rateContent,
            "General": general,
            "MedicalEthics": medicalEthics,
            "Technique": technique,
            "UserID": userID,
            "LocationID": locationID,
            "SumRate": sumRate
        ]
        
        return API.requestJSON(endpoint: .userAddReview(parameterDic), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                success(value as! String)
            case .failure(let error):
                fail(error,response)
            }
        })
    }

    
    static func userUpdateReview(rateID:Int, rateContent:String, general:Double, medicalEthics:Double, technique:Double, userID:Int, locationID:Int, sumRate:Double, completionHandler: @escaping(String?, Error?) -> ()) {
        
        let urlStr = Constant.apiBaseUrl + "api/ratings/updateRating/\(rateID)"
        let escapedUrl = urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        let paramDic:[String:Any] = [
            "RateContent": rateContent,
            "General": general,
            "MedicalEthics": medicalEthics,
            "Technique": technique,
            "UserID": userID,
            "LocationID": locationID,
            "SumRate": sumRate
        ]
        
        Alamofire.request(escapedUrl!, method: .post, parameters: paramDic,
                          encoding: JSONEncoding.default).validate().responseJSON { response in
                            switch response.result {
                            case .success(let value):
                                
                               print("\(value)")
                                
                            case .failure(let error):
                                print("UpdateLikeStatus Error : \(error.localizedDescription)")
                            }
        }
        
    }
}
