//
//  ServiceDetailGroups.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class ServiceDetailGroups:Mappable {
    var service:Service? = nil
    var provinceName:String = ""
    var serviceDetail = [ServiceDetail]()
    var location:LocationNearBy? = nil
    var listCustomServiceDetail = [ListCustomServiceDetail]()
    //var serviceGroupsTest = [ServiceGroupsTestObject]()
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            serviceDetail <- map["ServiceDetail"]
            provinceName <- map["ProvinceName"]
            service <- map["Service"]
            location <- map["Location"]
            listCustomServiceDetail <- map["ListCustomServiceDetail"]
        }
    }
}
