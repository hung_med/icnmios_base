//
//  DatetimeServer.swift
//  iCNM
//
//  Created by Medlatec on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class DatetimeServer:Mappable {
    var currentDateTime:Date? = nil
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ+"
        dateFormatter.amSymbol = "PM"
        if map.mappingType == .fromJSON {
            currentDateTime <- (map["CurrentDateTime"], DateFormatterTransform(dateFormatter:dateFormatter))
            
            if currentDateTime == nil {
                dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
                dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"
                dateFormatter.amSymbol = "PM"
                currentDateTime <- (map["CurrentDateTime"], DateFormatterTransform(dateFormatter:dateFormatter))
            }
        }
    }
}
