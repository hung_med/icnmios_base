//
//  Rating.swift
//  iCNM
//
//  Created by Nguyen Van Dung on 8/5/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import ObjectMapper

class Rating: Mappable {
    
    var rateID:Int = 0
    var title:String = ""
    var rateContent:String = ""
    var general:Int = 0
    var medicalEthics:Int = 0
    var technique:Int = 0
    var dateCreate2 = ""
    var dateCreate:Date?
    var dateEdit:Date?
    var active:Bool = false
    var doctorID:Int = 0
    var userID:Int = 0
    var locationID:Int = 0
    var sumRate:Double = 0
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        rateID <- map["RateID"]
        title <- map["title"]
        rateContent <- map["RateContent"]
        general <- map["General"]
        medicalEthics <- map["MedicalEthics"]
        technique <- map["Technique"]
        dateCreate2 <- map["DateCreate"]
        dateCreate <- (map["DateCreate"],DateFormatterTransform(dateFormatter:dateFormatter))
        dateEdit <- (map["DateEdit"], DateFormatterTransform(dateFormatter:dateFormatter))
        active <- map["Active"]
        doctorID <- map["DoctorID"]
        userID <- map["UserID"]
        locationID <- map["LocationID"]
        sumRate <- map["SumRate"]
    }
    
}
