//
//  UserNewsCommentModel.swift
//  iCNM
//
//  Created by Len Pham on 8/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper
class UserNewsCommentModel:Mappable {
    var user:UserInfo?
    var newsCommentModel:NewsCommentModel?
    
    // PRAGMA - ObjectMapper
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        user <- map["User"]
        newsCommentModel <- map["NewsComment"]
    }
    
    

}
