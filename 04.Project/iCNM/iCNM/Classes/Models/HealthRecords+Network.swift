//
//  HealthRecords+Network.swift
//  iCNM
//
//  Created by Ta Quang Hung on 20/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import Alamofire

extension HealthRecords {
    
    @discardableResult
    func addEditUserMedicalProfiles(userMedicalProfileID:Int, userID:Int, active:Bool, success:@escaping(HealthRecords?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        var parameterDic:[String:Any]?
        parameterDic = [
                "UserMedicalProfileID": userMedicalProfileID,
                "UserID": userID,
                "Active": active
        ]

        return API.requestJSON(endpoint: .addEditUserMedicalProfiles(parameterDic!), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let healthRecords = HealthRecords(JSON: item) {
                        success(healthRecords)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    static func getMedicalProfileByUserID(userID:Int, success:@escaping([HealthRecords]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getMedicalProfileByUserID(userID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [HealthRecords]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let healthRecords = HealthRecords(JSON: item) {
                            data.append(healthRecords)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
    
    @discardableResult
    static func getMedicalProfileOneUser(userID:Int, medicalProfileID:Int, success:@escaping([HealthRecords]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getMedicalProfileOneUser(userID, medicalProfileID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [HealthRecords]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let healthRecords = HealthRecords(JSON: item) {
                            data.append(healthRecords)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
