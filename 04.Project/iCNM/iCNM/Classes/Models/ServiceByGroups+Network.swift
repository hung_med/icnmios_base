//
//  ServiceByGroups+Network.swift
//  iCNM
//
//  Created by Quang Hung on 11/01/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension ServiceByGroups {
    
    @discardableResult
    func getServiceByGroups(groupID:Int, success:@escaping([ServiceByGroups]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        
        return API.requestJSON(endpoint: .getServiceByGroups(groupID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [ServiceByGroups]()
                if let array = value as? [[String:Any]]  {
                    for item in array {
                        if let serviceByGroups = ServiceByGroups(JSON: item) {
                            data.append(serviceByGroups)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
