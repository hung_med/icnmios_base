//
//  UserRegister.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class MedicalSymptonObject:Mappable {
    
    var active:Bool = false
    var dateCreate = ""
    var description = ""
    var descriptionPictures = ""
    var diagnosis = ""
    var diseaseGroupID = ""
    var medicalSymptonID = 0
    var keySearchName = ""
    var mean = ""
    var medicalDicID = 0
    var medicalSymptonIDStr = ""
    var name = ""
    var pictures = ""
    var prevention = ""
    var reason = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
      if map.mappingType == .fromJSON {
            active <- map["Active"]
            dateCreate <- map["DateCreate"]
            description <- map["Description"]
            descriptionPictures <- map["DescriptionPictures"]
            diagnosis <- map["Diagnosis"]
            diseaseGroupID <- map["DiseaseGroupID"]
            medicalSymptonID <- map["MedicalSymptonID"]
            keySearchName <- map["KeySearchName"]
            mean <- map["Mean"]
            medicalDicID <- map["MedicalDicID"]
            medicalSymptonIDStr <- map["MedicalSymptonIDStr"]
            name <- map["Name"]
            pictures <- map["Pictures"]
            prevention <- map["Prevention"]
            reason <- map["Reason"]
        }
    }
}
