//
//  Location.swift
//  iCNM
//
//  Created by Medlatec on 7/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import ObjectMapper

class Location: Mappable {
    var email:String = ""
    var address:String = ""
    var averageRating: Double = 0.0
    var cName:String = ""
    var city:String = ""
    //var cType:Bool = false
    var dayStr:String = ""
    var district:String = ""
    var imageStr:String = ""
    var imageThumnailStr:String = ""
    var intro = ""
    var lat:Double = 0.0
    var locationID = 0
    //var locationTimeID:Bool = false
    var locationTypeID = 0
    var long: Double = 0.0
    var mobile: String = ""
    var name: String = ""
    var quanHuyenID:Int = 0
    var service:String = ""
    var specialist:String = ""
    var tinhThanhID:Int = 0
    var workingTime:String = ""
    var urlWebsite:String = ""
    //var workingTimeStr:Bool = false

    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        email <- map["Email"]
        address <- map["Address"]
        averageRating <- map["AverageRating"]
        cName <- map["CName"]
        city <- map["City"]
        //cType <- map["Ctype"]
        dayStr <- map["DayStr"]
        district <- map["District"]
        imageStr <- map["ImagesStr"]
        imageThumnailStr <- map["ImagesThumbnailStr"]
        intro <- map["Intro"]
        lat <- map["Lat"]
        locationID <- map["LocationID"]
        //locationTimeID <- map["LocationTimeID"]
        locationTypeID <- map["LocationTypeID"]
        long <- map["Long"]
        mobile <- map["Mobile"]
        name <- map["Name"]
        quanHuyenID <- map["QuanHuyenID"]
        service <- map["Service"]
        specialist <- map["Specialist"]
        tinhThanhID <- map["TinhThanhID"]
        workingTime <- map["WorkingTime"]
        urlWebsite <- map["UrlWebsite"]
    }
    
}
