//
//  UpdatePID+Network.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire
import RealmSwift

extension UpdatePID {
    
    @discardableResult
    func checkUpdatePID(medicalProfileID:Int, organizeID:String, pid:String, sid:String, password:String, success:@escaping(UpdatePID?) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        var stringParam: String = String()
        stringParam = stringParam.appendingFormat("%@,%@,%@,%d,%d",organizeID,pid,password,0, medicalProfileID)
        return API.requestJSON(endpoint: .checkUpdatePID(stringParam), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                if let item = value as? [String:Any] {
                    if let updatePID = UpdatePID(JSON: item) {
                        success(updatePID)
                    }
                } else {
                    success(nil)
                }
            case .failure(let error):
                fail(error,response)
            }
        })
    }

}
