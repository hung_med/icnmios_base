//
//  SpecialistServicesGroup+Network.swift
//  iCNM
//
//  Created by Quang Hung on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Alamofire

extension SpecialistServicesGroup {
    
    @discardableResult
    static func getSpecialistServicesGroup(specialistID:Int, success:@escaping([SpecialistServicesGroup]) -> Void, fail:@escaping (_ error:Error, _ response:DataResponse<Any>)->Void) -> Request {
        return API.requestJSON(endpoint: .getSpecialistServicesGroup(specialistID), completionHandler: { (response) in
            switch response.result {
            case .success(let value):
                var data = [SpecialistServicesGroup]()
                if let array = value as? [[String:Any]] {
                    for item in array {
                        if let specialistServicesGroup = SpecialistServicesGroup(JSON: item) {
                            data.append(specialistServicesGroup)
                        }
                    }
                }
                success(data)
            case .failure(let error):
                fail(error,response)
            }
        })
    }
}
