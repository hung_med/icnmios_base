//
//  UserNewsCategories.swift
//  iCNM
//
//  Created by Len Pham on 9/2/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import ObjectMapper

class UserNewsCategories:Mappable {
  
    var userCategories: [NewsCategoryModel]?
    var categories: [NewsCategoryModel]?

    // PRAGMA - ObjectMapper
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        userCategories <- map["UserCategory"]
        categories <- map["Category"]
    }
}
