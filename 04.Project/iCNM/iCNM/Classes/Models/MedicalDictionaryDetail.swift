//
//  UserRegister.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class MedicalDictionaryDetail:Mappable {
    var active:Bool = false
    var dateCreate = ""
    var descriptionPictures = ""
    var diseaseGroupID:Int = 0
    var keySearchName = ""
    var mean = ""
    var medicalDicID = 0
    var medicalSymptonIDStr = ""
    var name = ""
    var overview = ""
    var pictures = ""
    var prevention = ""
    var reason = ""
    var symptom = ""
    var classify = ""
    var medicalSymptons = [MedicalSymptonObject]()
    // PRAGMA - ObjectMapper
    
    var description = ""
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            medicalSymptons <- map["MedicalSymptons"]
            
            active <- map["MedicalDictionary.Active"]
            dateCreate <- map["MedicalDictionary.DateCreate"]
            descriptionPictures <- map["MedicalDictionary.DescriptionPictures"]
            diseaseGroupID <- map["MedicalDictionary.DiseaseGroupID"]
            keySearchName <- map["MedicalDictionary.KeySearchName"]
            mean <- map["MedicalDictionary.Mean"]
            medicalDicID <- map["MedicalDictionary.MedicalDicID"]
            medicalSymptonIDStr <- map["MedicalDictionary.MedicalSymptonIDStr"]
            name <- map["MedicalDictionary.Name"]
            overview <- map["MedicalDictionary.Overview"]
            pictures <- map["MedicalDictionary.Pictures"]
            prevention <- map["MedicalDictionary.Prevention"]
            reason <- map["MedicalDictionary.Reason"]
            symptom <- map["MedicalDictionary.Symptom"]
            classify <- map["MedicalDictionary.Classify"]
            
        }
    }
    
}
