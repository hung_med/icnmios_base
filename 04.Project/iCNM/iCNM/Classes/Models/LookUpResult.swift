//
//  LookUpResult.swift
//  iCNM
//
//  Created by Medlatec on 5/31/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation

import ObjectMapper

class LookUpResult:Mappable {
    var patientID = 0
    var pID = ""
    var sID = ""
    var code = ""
    var phone = ""
    var email = ""
    var patientName = ""
    var organizeID:Int = 0
    var dateCreate = ""
    var medicalProfileID = ""
    var gender = false
    var age = 0
    var inTime = ""
    var active:Bool = false
    
    var sumPerTage:Float? = 0.0
    var sumTransport:Float? = 0.0
    var discount:Float? = 0.0
    var sumMoney:Float? = 0.0
    
    // PRAGMA - ObjectMapper
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        if map.mappingType == .fromJSON {
            patientID <- map["PatientID"]
            pID <- map["PID"]
            sID <- map["SID"]
            code <- map["Code"]
            phone <- map["Phone"]
            email <- map["Email"]
            patientName <- map["PatientName"]
            organizeID <- map["OrganizeID"]
            dateCreate <- map["DateCreate"]
            medicalProfileID <- map["MedicalProfileID"]
            gender <- map["Gender"]
            age <- map["Age"]
            inTime <- map["InTime"]
            if inTime != "" && !inTime.isEmpty{
                let partText = inTime.components(separatedBy: "T")[0].components(separatedBy: "-")
                let yyyy = partText[0]
                let mm = partText[1]
                let dd = partText[2]
                inTime = "\(dd)-\(mm)-\(yyyy)"
            }
            sumPerTage <- map["SumPerTage"]
            sumTransport <- map["SumTransport"]
            discount <- map["Discount"]
            sumMoney <- map["SumMoney"]
            active <- map["Active"]
        }
    }
    
}
