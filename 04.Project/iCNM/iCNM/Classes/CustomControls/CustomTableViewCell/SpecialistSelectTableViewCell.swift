//
//  SpecialistSelectTableViewCell.swift
//  iCNM
//
//  Created by Medlatec on 6/27/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit

class SpecialistSelectTableViewCell: UITableViewCell {
    @IBOutlet weak var selectButton: UIButton!
    @IBOutlet weak var specialistTitle: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
