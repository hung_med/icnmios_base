//
//  FWTagLabel.swift
//  iCNM
//
//  Created by Medlatec on 6/19/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit


class FWTextFieldErrorLabel: UILabel {
    @IBInspectable var horizontalPadding:CGFloat = 0 {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }

    @IBInspectable var topPadding:CGFloat = 0 {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    @IBInspectable var bottomPadding:CGFloat = 0 {
        didSet {
            self.invalidateIntrinsicContentSize()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.clipsToBounds = true
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var intrinsicSuperViewContentSize = super.intrinsicContentSize
            intrinsicSuperViewContentSize.width += horizontalPadding * 2
            intrinsicSuperViewContentSize.height += (self.text != nil) ? (self.text!.characters.count > 0 ? topPadding + bottomPadding : 0) : 0
            return intrinsicSuperViewContentSize
        }
    }
    
    override func drawText(in rect: CGRect) {
        let insets: UIEdgeInsets = UIEdgeInsets(top: topPadding, left: horizontalPadding, bottom: bottomPadding, right: horizontalPadding)
        super.drawText(in: UIEdgeInsetsInsetRect(rect, insets))
    }

}
