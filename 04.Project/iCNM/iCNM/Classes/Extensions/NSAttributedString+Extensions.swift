//
//  NSAttributedString+Extensions.swift
//  ezCloudHotelManager
//
//  Created by Medlatec on 1/11/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import Foundation
import UIKit

extension NSAttributedString {
    func sizeWithConstrainedWidth(width: CGFloat) -> CGSize {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        let size = CGSize(width:ceil(boundingBox.size.width), height:ceil(boundingBox.size.height))
        return size
    }
}
