//
//  AppDelegate.swift
//  iCNM
//
//  Created by Medlatec on 5/8/17.
//  Copyright © 2017 Medlatec. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireNetworkActivityIndicator
import CocoaBar
import RealmSwift
import Firebase
import FirebaseDatabase
import GoogleSignIn
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import FirebaseMessaging
import UserNotifications
import FontAwesome_swift

//import Fabric
//import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var json_data = [String : Any]()
    var remoteId : String?
    var window: UIWindow?
    var timer:Timer? = nil
    var myCounter:Int = 0
    var btnNotification:UIButton? = nil
    let networkManager = NetworkReachabilityManager(host:"www.apple.com")
    var internetSnackbar:CocoaBar?
    let requireNetwork = "Ứng dụng cần có mạng để sử dụng!"
    let userDefaults = UserDefaults.standard
    let gcmMessageIDKey = "gcm.message_id"
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
//        Fabric.sharedSDK().debug = true
//        Fabric.with([Crashlytics.self])
        
        guard let gai = GAI.sharedInstance() else {
            assert(false, "Google Analytics not configured correctly")
            return true//Base on your function return type, it may be returning something else
        }
        
        gai.tracker(withTrackingId: "UA-105087885-1")
        // Optional: automatically report uncaught exceptions.
        gai.trackUncaughtExceptions = true
        
        // Optional: set Logger to VERBOSE for debug information.
        // Remove before app release.
        gai.logger.logLevel = .verbose
        
        if #available(iOS 9.0, *) {
            UILabel.appearance(whenContainedInInstancesOf: [UIAlertController.self]).numberOfLines = 2
        } else {
            // Fallback on earlier versions
        }
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        //FirebaseApp.configure()
        
        // Provide the Places API with your API key.
        GMSPlacesClient.provideAPIKey("AIzaSyAKPZikI2zWLDxU81mX7rG8z-OMJ6MvW-8")
        //Google Map
        GMSServices.provideAPIKey("AIzaSyAKPZikI2zWLDxU81mX7rG8z-OMJ6MvW-8")
        
        //Keyboard manager
        IQKeyboardManager.sharedManager().enable = true
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        FirebaseApp.configure()
        Database.database().isPersistenceEnabled = true
        Database.database().reference(withPath: "Messages").keepSynced(true)
        Database.database().reference(withPath: "Conversations").keepSynced(true)

        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        
        let config = Realm.Configuration(
            schemaVersion: 8,
            migrationBlock: { migration, oldSchemaVersion in
                // We haven’t migrated anything yet, so oldSchemaVersion == 0
                if (oldSchemaVersion < 5) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
        })
        
        Realm.Configuration.defaultConfiguration = config
        //        print("\(config.fileURL?.absoluteString)")
        
        NetworkActivityIndicatorManager.shared.isEnabled = true
        NetworkActivityIndicatorManager.shared.startDelay = 0.2
        
        // Override point for customization after application launch.
        UINavigationBar.appearance().barTintColor = UIColor(hex:"1d6edc")
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        
        internetSnackbar = CocoaBar(window: window)
        startListenToNetworkStatus()
        let pageControl = UIPageControl.appearance()
        pageControl.pageIndicatorTintColor = UIColor(hex: "e7e7e7")
        pageControl.currentPageIndicatorTintColor = UIColor(hex: "4a8be3")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification(_:)),
                                               name: Notification.Name.InstanceIDTokenRefresh, object: nil)
        if let token = InstanceID.instanceID().token() {
            print("TOKEN....", token)
            connectToFcm()
        }
        
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print("Registration succeeded! Token: ", token)
        
        Messaging.messaging().setAPNSToken(deviceToken, type: MessagingAPNSTokenType.sandbox)
        Messaging.messaging().setAPNSToken(deviceToken, type: MessagingAPNSTokenType.prod)
        FBSDKAppEvents.setPushNotificationsDeviceToken(deviceToken)
        userDefaults.set(token, forKey: "deviceToken")
        userDefaults.synchronize()
   
    }
    
    func tokenRefreshNotification(_ notification: Notification) {
        if let token = InstanceID.instanceID().token() {
            print("InstanceID token: \(token)")
            userDefaults.set(token, forKey: "token")
            userDefaults.synchronize()
        }
        // Connect to FCM since connection may have failed when attempted before having a token.
        self.connectToFcm()
    }
    
    func connectToFcm() {
        // Won't connect since there is no token
        guard InstanceID.instanceID().token() != nil else {
            print("FCM: Token does not exist.")
            return
        }
        
        // Disconnect previous FCM connection if it exists.
        Messaging.messaging().disconnect()
        Messaging.messaging().connect { (error) in
            if error != nil {
                print("FCM: Unable to connect with FCM. \(error.debugDescription)")
            } else {
                print("Connected to FCM.")
                if let token = InstanceID.instanceID().token(){
                    self.registerTokenApp(token: token)
                }
            }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Registration failed!")
    }
    
    // registerTokenApp
    func registerTokenApp(token:String) {
        let currentUser:User? = {
            let realm = try! Realm()
            return realm.currentUser()
        }()
        
        if currentUser != nil {
            if let user = currentUser {
                var doctorIDCode = user.userDoctor?.userInfo?.doctorIDCode
                if doctorIDCode == nil{
                    doctorIDCode = ""
                }
                self.addFCM(key: token, userID: user.id, doctorID: user.doctorID, doctorIDCode: doctorIDCode!)
            }
        }
    }
    
    func addFCM(key:String, userID:Int, doctorID:Int, doctorIDCode:String){
        FCM().addFCM(key: key, userID:userID, doctorID: doctorID, doctorIDCode:doctorIDCode, success: { (data) in
            if data != nil {
                print("register token success")
                
            } else {
                print("register token failed")
            }
        }, fail: { (error, response) in
            print("register token failed")
        })
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        let currentUser:User? = {
            let realm = try! Realm()
            return realm.currentUser()
        }()
        
        // remove online status firebase
        guard let id = currentUser?.userDoctor?.userInfo?.id else {return}
        let date = Date()
        let time = Int((date.timeIntervalSince1970) * 1000)
        
        let databaseReference = Database.database().reference()
        databaseReference.child("Helpers").child("Users").child("\(id)").updateChildValues(["isOnline" : false, "time": time])
        
        databaseReference.child("Helpers").child("Conversations").child("\(id)").observe(.childAdded, with: { (snapshot) in
            databaseReference.child("Helpers").child("Conversations").child("\(id)").child(snapshot.key).child("isOnlineOnChat").setValue(false)
        })
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        if (application.applicationState == UIApplicationState.active ) {
            let localNotification:UILocalNotification = UILocalNotification()
            localNotification.soundName = UILocalNotificationDefaultSoundName
            UIApplication.shared.scheduleLocalNotification(localNotification)
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        Messaging.messaging().shouldEstablishDirectChannel = true
        application.applicationIconBadgeNumber = 0
        self.connectToFcm()
        
        let currentUser:User? = {
            let realm = try! Realm()
            return realm.currentUser()
        }()
        let databaseReference = Database.database().reference()
        guard let id = currentUser?.userDoctor?.userInfo?.id else {return}
        let date = Date()
        if #available(iOS 10, *) {
            let time = Int((date.timeIntervalSince1970) * 1000)
            let value = ["userID": id as Any, "type": "Doctor", "time": time, "isOnline": true] as [String : Any]
            databaseReference.child("Helpers").child("Users").child("\(id)").setValue(value)
        }else{
            let time = Double((date.timeIntervalSince1970) * 1000)
            let value = ["userID": id as Any, "type": "Doctor", "time": time, "isOnline": true] as [String : Any]
            databaseReference.child("Helpers").child("Users").child("\(id)").setValue(value)
        }
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // Firebase notification received
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
        completionHandler(.alert)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
        print("Handle push from background or closed\(response.notification.request.content.userInfo)")
        if let dictionary = response.notification.request.content.userInfo["gcm.notification.message"]{
            NotificationCenter.default.post(name: Constant.NotificationMessage.handleNotification, object: dictionary)
            let aps = response.notification.request.content.userInfo["aps"]  as? NSDictionary
            let data : [String : Any] = aps!["alert"] as! [String : Any]
            let body : String = data["body"] as! String
            let title : String = data["title"] as! String
            userDefaults.set(body, forKey: "body")
            userDefaults.set(title, forKey: "title")
            userDefaults.set(dictionary, forKey: "receiveNotification")
            userDefaults.synchronize()
        }
        else if let dictionary = response.notification.request.content.userInfo["aps"]  as? NSDictionary{
            let data : [String : Any] = dictionary["alert"] as! [String : Any]
            let body : String = data["body"] as! String
            NotificationCenter.default.post(name: Constant.NotificationMessage.handleNotification, object: body)
            userDefaults.set(body, forKey: "receiveNotification")
            userDefaults.synchronize()
        }
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        NSLog("[RemoteNotification] didRefreshRegistrationToken: \(fcmToken)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        if UIApplication.shared.applicationState == .active {
            //TODO: Handle foreground notification
        } else {
            //TODO: Handle background notification
        }
    }

    private func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // Let FCM know about the message for analytics etc.
        Messaging.messaging().appDidReceiveMessage(userInfo)
    }
    
    private func startListenToNetworkStatus() {
        networkManager?.listener = { [unowned self] status in
            switch status {
            case .reachable:
                self.internetSnackbar?.hideAnimated(true, completion: nil)
                NotificationCenter.default.post(Notification(name:Constant.NotificationMessage.internetComeBack))
            case .notReachable:
                self.showInternetSnackbar(self.requireNetwork, duration: .indeterminate)
            default:
                break
            }
        }
        manualCheckNetworkStatus()
        networkManager?.startListening()
    }
    
    private func manualCheckNetworkStatus() {
        if networkManager != nil, networkManager!.isReachable == false {
            showInternetSnackbar(requireNetwork, duration: .indeterminate)
        } else {
            if let snackbar = internetSnackbar {
                if snackbar.isShowing {
                    snackbar.hideAnimated(true, completion: nil)
                }
            }
        }
    }
    
    func showInternetSnackbar(_ errorString:String, duration: CocoaBar.DisplayDuration) {
        self.internetSnackbar?.showAnimated(true, duration: duration, style: .default, populate: { (layout) in
            layout.backgroundStyle = .blurDark
            layout.displayStyle = .roundRectangle
            if let defaultLayout = layout as? CocoaBarDefaultLayout {
                if IS_IPHONE_5{
                    defaultLayout.titleLabel?.font = UIFont.fontAwesome(ofSize: 12.0)
                }else{
                    defaultLayout.titleLabel?.font = UIFont.fontAwesome(ofSize: 16.0)
                }
                let title = errorString + "       Thử lại " + String.fontAwesomeIcon(name: .refresh)
                defaultLayout.titleLabel?.text = title
                defaultLayout.titleLabel?.numberOfLines = 0
                defaultLayout.titleLabel?.lineBreakMode = .byWordWrapping
            }
        }, completion: nil)
        NotificationCenter.default.post(Notification(name:Constant.NotificationMessage.noInternetConnection))
    }
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any])
        -> Bool {
            let googleHandler = GIDSignIn.sharedInstance().handle(url,
                                                                  sourceApplication:options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                                  annotation: [:])
            let facebookHandler = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as! String?, annotation: options[UIApplicationOpenURLOptionsKey.annotation])
            return googleHandler || facebookHandler
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        let googleHandler = GIDSignIn.sharedInstance().handle(url,
                                                              sourceApplication: sourceApplication,
                                                              annotation: annotation)
        let facebookHandler = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
        return googleHandler || facebookHandler
    }
}
