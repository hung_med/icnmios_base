// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

// swiftlint:disable sorted_imports
import Foundation
import UIKit
import SideMenu

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

protocol StoryboardType {
  static var storyboardName: String { get }
}

extension StoryboardType {
  static var storyboard: UIStoryboard {
    return UIStoryboard(name: self.storyboardName, bundle: Bundle(for: BundleToken.self))
  }
}

struct SceneType<T: Any> {
  let storyboard: StoryboardType.Type
  let identifier: String

  func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateViewController(withIdentifier: identifier) as? T else {
      fatalError("ViewController '\(identifier)' is not of the expected class \(T.self).")
    }
    return controller
  }
}

struct InitialSceneType<T: Any> {
  let storyboard: StoryboardType.Type

  func instantiate() -> T {
    guard let controller = storyboard.storyboard.instantiateInitialViewController() as? T else {
      fatalError("ViewController is not of the expected class \(T.self).")
    }
    return controller
  }
}

protocol SegueType: RawRepresentable { }

extension UIViewController {
  func perform<S: SegueType>(segue: S, sender: Any? = nil) where S.RawValue == String {
    performSegue(withIdentifier: segue.rawValue, sender: sender)
  }
}

// swiftlint:disable explicit_type_interface identifier_name line_length type_body_length type_name
enum StoryboardScene {
  enum Chat: StoryboardType {
    static let storyboardName = "Chat"

    static let chatVc = SceneType<iCNM.ChatViewController>(storyboard: Chat.self, identifier: "chat_vc")

    static let chathistoryVc = SceneType<iCNM.ChatHistoryViewController>(storyboard: Chat.self, identifier: "chathistory_vc")

    static let mediaVc = SceneType<iCNM.MediaSentViewController>(storyboard: Chat.self, identifier: "media_vc")

    static let naviChat = SceneType<UINavigationController>(storyboard: Chat.self, identifier: "navi_chat")

    static let qrcodeVc = SceneType<iCNM.GenQRCodeViewController>(storyboard: Chat.self, identifier: "qrcode_vc")
  }
  enum DetailLookUp: StoryboardType {
    static let storyboardName = "DetailLookUp"

    static let detailLookUpResultVC = SceneType<iCNM.DetailLookUpResultVC>(storyboard: DetailLookUp.self, identifier: "DetailLookUpResultVC")
  }
  enum LaunchScreen: StoryboardType {
    static let storyboardName = "LaunchScreen"

    static let initialScene = InitialSceneType<UIViewController>(storyboard: LaunchScreen.self)
  }
  enum LoginRegister: StoryboardType {
    static let storyboardName = "LoginRegister"

    static let confirmCodePasswordVC = SceneType<iCNM.ConfirmCodePasswordVC>(storyboard: LoginRegister.self, identifier: "ConfirmCodePasswordVC")

    static let confirmPasswordVC = SceneType<iCNM.ConfirmPasswordVC>(storyboard: LoginRegister.self, identifier: "ConfirmPasswordVC")

    static let forgetPasswordVC = SceneType<iCNM.ForgetPasswordVC>(storyboard: LoginRegister.self, identifier: "ForgetPasswordVC")

    static let loginNavigationController = SceneType<UINavigationController>(storyboard: LoginRegister.self, identifier: "LoginNavigationController")

    static let loginViewController = SceneType<iCNM.LoginViewController>(storyboard: LoginRegister.self, identifier: "LoginViewController")

    static let registerViewController = SceneType<iCNM.RegisterViewController>(storyboard: LoginRegister.self, identifier: "RegisterViewController")
  }
  enum Main: StoryboardType {
    static let storyboardName = "Main"

    static let initialScene = InitialSceneType<iCNM.SplashViewController>(storyboard: Main.self)

    static let appointmentListNavigationController = SceneType<UINavigationController>(storyboard: Main.self, identifier: "AppointmentListNavigationController")

    static let createQuestionNavigationController = SceneType<UINavigationController>(storyboard: Main.self, identifier: "CreateQuestionNavigationController")

    static let createQuestionVC = SceneType<iCNM.CreateQuestionViewController>(storyboard: Main.self, identifier: "CreateQuestionVC")

    static let leftMenuNavigationController = SceneType<SideMenu.UISideMenuNavigationController>(storyboard: Main.self, identifier: "LeftMenuNavigationController")

    static let mainTabBarController = SceneType<iCNM.BaseTabBarController>(storyboard: Main.self, identifier: "MainTabBarController")

    static let menuTableViewController = SceneType<iCNM.MenuTableViewController>(storyboard: Main.self, identifier: "MenuTableViewController")

    static let newsCategoriesSelectTableViewController = SceneType<iCNM.NewsCategoriesSelectTableViewController>(storyboard: Main.self, identifier: "NewsCategoriesSelectTableViewController")

    static let newsCategoryListItemsViewController = SceneType<iCNM.NewsCategoryListItemsViewController>(storyboard: Main.self, identifier: "NewsCategoryListItemsViewController")

    static let newsCommentListViewController = SceneType<iCNM.NewsCommentListViewController>(storyboard: Main.self, identifier: "NewsCommentListViewController")

    static let newsDetailViewController = SceneType<iCNM.NewsDetailViewController>(storyboard: Main.self, identifier: "NewsDetailViewController")

    static let newsHomeViewController = SceneType<iCNM.NewsHomeViewController>(storyboard: Main.self, identifier: "NewsHomeViewController")

    static let processManagerVC = SceneType<iCNM.ProcessManagerViewController>(storyboard: Main.self, identifier: "ProcessManagerVC")

    static let qaComment = SceneType<iCNM.QACommentViewController>(storyboard: Main.self, identifier: "QAComment")

    static let qaDetail = SceneType<iCNM.QADetailViewController>(storyboard: Main.self, identifier: "QADetail")

    static let qaNavigationController = SceneType<UINavigationController>(storyboard: Main.self, identifier: "QANavigationController")

    static let scheduleAppointmentNavigationController = SceneType<UINavigationController>(storyboard: Main.self, identifier: "ScheduleAppointmentNavigationController")

    static let scheduleAppointmentVC = SceneType<iCNM.SAViewController>(storyboard: Main.self, identifier: "ScheduleAppointmentVC")

    static let specialistMenuNavigationController = SceneType<SideMenu.UISideMenuNavigationController>(storyboard: Main.self, identifier: "SpecialistMenuNavigationController")

    static let specialistSelectTableVC = SceneType<iCNM.SpecialistSelectTableViewController>(storyboard: Main.self, identifier: "SpecialistSelectTableVC")

    static let splashViewController = SceneType<iCNM.SplashViewController>(storyboard: Main.self, identifier: "SplashViewController")

    static let supportCategoriesMenuNavigationController = SceneType<SideMenu.UISideMenuNavigationController>(storyboard: Main.self, identifier: "SupportCategoriesMenuNavigationController")

    static let supportFeedbackVC = SceneType<iCNM.SupportFeedbackVC>(storyboard: Main.self, identifier: "SupportFeedbackVC")

    static let topQAVC = SceneType<iCNM.TopQATableViewController>(storyboard: Main.self, identifier: "TopQAVC")

    static let viewAppointmentListVC = SceneType<iCNM.ViewAppointmentListViewController>(storyboard: Main.self, identifier: "ViewAppointmentListVC")

    static let newsCategoriesMenuNavigationController = SceneType<SideMenu.UISideMenuNavigationController>(storyboard: Main.self, identifier: "newsCategoriesMenuNavigationController")
  }
  enum OnBoard: StoryboardType {
    static let storyboardName = "OnBoard"

    static let onBoardPageViewController = SceneType<iCNM.OnboardPageViewController>(storyboard: OnBoard.self, identifier: "OnBoardPageViewController")

    static let stepFour = SceneType<iCNM.StepFourViewController>(storyboard: OnBoard.self, identifier: "StepFour")

    static let stepOne = SceneType<iCNM.StepOneViewController>(storyboard: OnBoard.self, identifier: "StepOne")

    static let stepThree = SceneType<iCNM.StepThreeViewController>(storyboard: OnBoard.self, identifier: "StepThree")

    static let stepTwo = SceneType<iCNM.StepTwoViewController>(storyboard: OnBoard.self, identifier: "StepTwo")
  }
  enum Profile: StoryboardType {
    static let storyboardName = "Profile"

    static let profileNavigationController = SceneType<UINavigationController>(storyboard: Profile.self, identifier: "ProfileNavigationController")

    static let profileTableViewController = SceneType<iCNM.ProfileTableViewController>(storyboard: Profile.self, identifier: "ProfileTableViewController")

    static let specialistPopup = SceneType<iCNM.SpecialistPopupViewController>(storyboard: Profile.self, identifier: "SpecialistPopup")
  }
  enum SearchStoryboard: StoryboardType {
    static let storyboardName = "SearchStoryboard"

    static let searchVC = SceneType<iCNM.SearchViewController>(storyboard: SearchStoryboard.self, identifier: "SearchVC")
  }
}

enum StoryboardSegue {
  enum LoginRegister: String, SegueType {
    case confirmCodePasswordVC = "ConfirmCodePasswordVC"
    case confirmPasswordVC = "ConfirmPasswordVC"
    case showCreatePasswordViewController = "ShowCreatePasswordViewController"
    case showInputBirthdayViewController = "ShowInputBirthdayViewController"
    case showInputGenderViewController = "ShowInputGenderViewController"
    case showNameInputViewController = "ShowNameInputViewController"
    case showVerifyPhoneScene = "ShowVerifyPhoneScene"
  }
  enum Main: String, SegueType {
    case showQuestionAnswerDetail = "ShowQuestionAnswerDetail"
  }
  enum Profile: String, SegueType {
    case showAddEducation = "ShowAddEducation"
    case showAddPrize = "ShowAddPrize"
    case showAddSpecialistPopup = "ShowAddSpecialistPopup"
    case showAddWorkExperiencePopup = "ShowAddWorkExperiencePopup"
    case showEditEducation = "ShowEditEducation"
    case showEditPrize = "ShowEditPrize"
    case showEditProfieSegue = "ShowEditProfieSegue"
    case showEditWorkExperiencePopup = "ShowEditWorkExperiencePopup"
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length type_body_length type_name

private final class BundleToken {}
