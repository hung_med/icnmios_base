// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

#if os(iOS) || os(tvOS) || os(watchOS)
  import UIKit.UIImage
  typealias Image = UIImage
#elseif os(OSX)
  import AppKit.NSImage
  typealias Image = NSImage
#endif

// swiftlint:disable file_length
// swiftlint:disable line_length

// swiftlint:disable type_body_length
enum Asset: String {
  case addPhoto = "AddPhoto"
  case angleDown = "angle-down"
  case angleUp = "angle-up"
  case answerBubble = "AnswerBubble"
  case attention = "Attention"
  case avatarHolder = "AvatarHolder"
  case backgroundMaterial = "BackgroundMaterial"
  case barChart = "bar-chart"
  case birthday = "birthday"
  case btnComment = "btn_comment"
  case btnLiked = "btn_liked"
  case btnShare = "btn_share"
  case buttonBackgroundFoursquareHighlight = "button_background_foursquare_highlight"
  case calendarPlusO = "calendar-plus-o"
  case checkBox = "CheckBox"
  case checkBoxChecked = "CheckBoxChecked"
  case checkBoxOutline = "CheckBoxOutline"
  case checkedBox = "CheckedBox"
  case commentO = "comment-o"
  case comment = "comment"
  case dropdown = "dropdown"
  case editProfileIcon = "EditProfileIcon"
  case email = "email"
  case facebookLogo = "FacebookLogo"
  case facebookLogoBig = "FacebookLogoBig"
  case female = "female"
  case firstOnBoarding = "FirstOnBoarding"
  case googleLogo = "GoogleLogo"
  case googleLogoBig = "GoogleLogoBig"
  case graduation = "graduation"
  case home = "home"
  case homeIcon = "HomeIcon"
  case hospital = "hospital"
  case icArrowDropDown = "ic_arrow_drop_down"
  case icHistory = "ic_history"
  case icKeyboardArrowLeftWhite48pt = "ic_keyboard_arrow_left_white_48pt"
  case icnDropdown = "icn-dropdown"
  case iconArrowDown = "icon_arrow_down"
  case iconBirthdayGrey = "icon_birthday_grey"
  case iconBirthday = "icon_birthday"
  case iconBlood = "icon_blood"
  case iconBluebook = "icon_bluebook"
  case iconBluedot = "icon_bluedot"
  case iconBluepencil = "icon_bluepencil"
  case iconBlueplus = "icon_blueplus"
  case iconBluestar = "icon_bluestar"
  case iconBlueuser = "icon_blueuser"
  case iconBookingWhite = "icon_booking_white"
  case iconBooking = "icon_booking"
  case iconCalendar = "icon_calendar"
  case iconCamera1 = "icon_camera-1"
  case iconCamera = "icon_camera"
  case iconChangePwd = "icon_change_pwd"
  case iconChart = "icon_chart"
  case iconCheck = "icon_check"
  case iconChecked = "icon_checked"
  case iconClock = "icon_clock"
  case iconClose = "icon_close"
  case iconComment = "icon_comment"
  case iconContact = "icon_contact"
  case iconDocument = "icon_document"
  case iconEditProfile = "icon_edit_profile"
  case iconEmailGrey = "icon_email_grey"
  case iconEmail = "icon_email"
  case iconFacebook = "icon_facebook"
  case iconFolder = "icon_folder"
  case iconGenderGrey = "icon_gender_grey"
  case iconGender = "icon_gender"
  case iconGoogle = "icon_google"
  case iconGreenDot = "icon_green_dot"
  case iconGreydot = "icon_greydot"
  case iconHeart = "icon_heart"
  case iconHeight = "icon_height"
  case iconHome = "icon_home"
  case iconHospital = "icon_hospital"
  case iconIdCard = "icon_id_card"
  case iconLatlong = "icon_latlong"
  case iconLike1 = "icon_like-1"
  case iconLike = "icon_like"
  case iconLiked = "icon_liked"
  case iconLocationGrey = "icon_location_grey"
  case iconLocation = "icon_location"
  case iconLogout = "icon_logout"
  case iconMapOn = "icon_map_on"
  case iconMap = "icon_map"
  case iconMenu = "icon_menu"
  case iconMessageOn = "icon_message_on"
  case iconMessage = "icon_message"
  case iconMessage2 = "icon_message2"
  case iconMore = "icon_more"
  case iconNewsLike = "icon_news_like"
  case iconNewsOn = "icon_news_on"
  case iconNews = "icon_news"
  case iconNotificationOn = "icon_notification_on"
  case iconNotification = "icon_notification"
  case iconOffice = "icon_office"
  case iconOpenTime = "icon_open_time"
  case iconPassword = "icon_password"
  case iconPencil = "icon_pencil"
  case iconPride = "icon_pride"
  case iconQa = "icon_qa"
  case iconQualify = "icon_qualify"
  case iconRedHeart = "icon_red_heart"
  case iconReply = "icon_reply"
  case iconRightarrow = "icon_rightarrow"
  case iconRoute = "icon_route"
  case iconSetting = "icon_setting"
  case iconShare1 = "icon_share-1"
  case iconShare = "icon_share"
  case iconStar = "icon_star"
  case iconSupport = "icon_support"
  case iconTelGrey = "icon_tel_grey"
  case iconTel = "icon_tel"
  case iconTudienBenh = "icon_tudien_benh"
  case iconTudienDichvu = "icon_tudien_dichvu"
  case iconTudienThuoc = "icon_tudien_thuoc"
  case iconTudienXetnghiem = "icon_tudien_xetnghiem"
  case iconUnchecked = "icon_unchecked"
  case iconUnlike = "icon_unlike"
  case iconUploadBlue = "icon_upload_blue"
  case iconUploadGreen = "icon_upload_green"
  case iconUser = "icon_user"
  case iconUsername = "icon_username"
  case iconViewProfilePage = "icon_view_profile_page"
  case iconView = "icon_view"
  case iconWeb = "icon_web"
  case iconWeight = "icon_weight"
  case iconWorkGrey = "icon_work_grey"
  case iconWork = "icon_work"
  case leftArrow = "LeftArrow"
  case listAlt = "list-alt"
  case lock = "lock"
  case male = "male"
  case mapDoctorOff = "map_doctor_off"
  case mapDoctorOn = "map_doctor_on"
  case mapHospitalOff = "map_hospital_off"
  case mapHospitalOn = "map_hospital_on"
  case mapIconDoctor = "map_icon_doctor"
  case mapIconHospital = "map_icon_hospital"
  case mapIconMedicin = "map_icon_medicin"
  case mapMedicinOff = "map_medicin_off"
  case mapMedicinOn = "map_medicin_on"
  case mapIcon = "MapIcon"
  case medkit = "medkit"
  case menuIcon = "menu_icon"
  case menu = "menu"
  case more90 = "more_90"
  case newsIcon = "NewsIcon"
  case notificationIcon = "NotificationIcon"
  case phoneSquare = "phone-square"
  case qaIcon = "QAIcon"
  case questionCircle = "question-circle"
  case questionBubble = "QuestionBubble"
  case removeIcon = "RemoveIcon"
  case rightArrow = "right_arrow"
  case rssSquare = "rss-square"
  case secondOnBoarding = "SecondOnBoarding"
  case splashIcon = "SplashIcon"
  case streetView = "street-view"
  case tempEquipment = "TempEquipment"
  case thirdOnBoarding = "ThirdOnBoarding"
  case trophy = "trophy"
  case userCircle1 = "user-circle-1"
  case userCircle = "user-circle"
  case userMd = "user-md"
  case userPlus = "user-plus"

  var image: Image {
    let bundle = Bundle(for: BundleToken.self)
    #if os(iOS) || os(tvOS)
    let image = Image(named: rawValue, in: bundle, compatibleWith: nil)
    #elseif os(OSX)
    let image = bundle.image(forResource: rawValue)
    #elseif os(watchOS)
    let image = Image(named: rawValue)
    #endif
    guard let result = image else { fatalError("Unable to load image \(rawValue).") }
    return result
  }
}
// swiftlint:enable type_body_length

extension Image {
  convenience init!(asset: Asset) {
    #if os(iOS) || os(tvOS)
    let bundle = Bundle(for: BundleToken.self)
    self.init(named: asset.rawValue, in: bundle, compatibleWith: nil)
    #elseif os(OSX) || os(watchOS)
    self.init(named: asset.rawValue)
    #endif
  }
}

private final class BundleToken {}
